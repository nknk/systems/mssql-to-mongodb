import { Model, STRING, BOOLEAN } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { IDENTITY_DB } from "../config";

class AdminUserPermission extends Model {}

AdminUserPermission.init({
    Code: {
        type: STRING,
        allowNull: false
    },
    Description: {
        type: STRING,
        allowNull: false
    },
    Grant: {
        type: BOOLEAN,
        allowNull: false
    },
    Deny: {
        type: BOOLEAN,
        allowNull: false
    },
    Modify: {
        type: BOOLEAN,
        allowNull: false
    },
    Delete: {
        type: BOOLEAN,
        allowNull: false
    },
    ModifyApplicable: {
        type: BOOLEAN,
        allowNull: false
    },
    DeleteApplicable: {
        type: BOOLEAN,
        allowNull: false
    }
}, {
    sequelize,
    modelName: "AdminUserPermission",
    freezeTableName: true,
    tableName: "AdminUserPermission",
    timestamps: false
});

const AdminUserPermissionSchema = new mongoose.Schema({
    Code: {
        type: String
    },
    Description: {
        type: String
    },
    Grant: {
        type: Boolean
    },
    Deny: {
        type: Boolean
    },
    Modify: {
        type: Boolean
    },
    Delete: {
        type: Boolean
    },
    ModifyApplicable: {
        type: Boolean
    },
    DeleteApplicable: {
        type: Boolean
    }
}, {
    versionKey: false,
    collection: "AdminUserPermissions",
    _id: false
});

const db = mongoose.connection.useDb(IDENTITY_DB);
const AdminUserPermissionCollection = db.model("AdminUserPermissions", AdminUserPermissionSchema);

export { AdminUserPermission, AdminUserPermissionCollection, AdminUserPermissionSchema };
