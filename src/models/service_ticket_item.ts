import { STRING, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class ServiceTicketItem extends ModelWithTenantId {
    ServiceTicketId: number;
}

ServiceTicketItem.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    ServiceTicketId: {
        type: BIGINT
    },
    Description: {
        type: STRING
    },
    Comment: {
        type: STRING
    }
}, {
    sequelize,
    modelName: "ServiceTicketItem",
    freezeTableName: true,
    tableName: "ServiceTicketItem",
    timestamps: false
});


const ServiceTicketItemSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    ServiceTicketId: {
        type: mongoose.Types.ObjectId
    },
    Description: {
        type: String,
    },
    Comment: {
        type: String,
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "ServiceTicketItems"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const ServiceTicketItemCollection = db.model("ServiceTicketItems", ServiceTicketItemSchema);

export { ServiceTicketItem, ServiceTicketItemCollection, ServiceTicketItemSchema };
