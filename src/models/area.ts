import { STRING, BOOLEAN, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { RATING_DB } from "../config";

class Area extends ModelWithTenantId {
    CountryId: number;
    RegionId: number;
    SubRegionId: number;
}

Area.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    PostalCode: {
        type: STRING,
        allowNull: false
    },
    CountryId: {
        type: BIGINT,
        allowNull: false
    },
    UseSubRegion: {
        type: BOOLEAN,
        allowNull: false
    },
    RegionId: {
        type: BIGINT,
        allowNull: false
    },
    SubRegionId: {
        type: BIGINT,
        allowNull: false
    },
}, {
    sequelize,
    modelName: "Area",
    freezeTableName: true,
    tableName: "Area",
    timestamps: false
});


const AreaSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    },
    PostalCode: {
        type: String
    },
    CountryId: {
        type: mongoose.Types.ObjectId
    },
    UseSubRegion: {
        type: Boolean
    },
    RegionId: {
        type: mongoose.Types.ObjectId
    },
    SubRegionId: {
        type: mongoose.Types.ObjectId
    },
}, {
    versionKey: false,
    collection: "Areas"
});

const db = mongoose.connection.useDb(RATING_DB);
const AreaCollection = db.model("Areas", AreaSchema);

export { Area, AreaSchema, AreaCollection };
