import { STRING, INTEGER } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { BaseModel } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class RefCommissionPaymentType extends BaseModel {
    CommissionPaymentTypeIndex: number;
}

RefCommissionPaymentType.init({

    CommissionPaymentTypeIndex: {
        type: INTEGER,
        primaryKey: true
    },
    CommissionPaymentTypeText: {
        type: STRING
    },

}, {
    sequelize,
    modelName: "RefCommissionPaymentType",
    freezeTableName: true,
    tableName: "RefCommissionPaymentType",
    timestamps: false
});


const RefCommissionPaymentTypeSchema = new mongoose.Schema({
    CommissionPaymentTypeIndex: {
        type: Number
    },
    CommissionPaymentTypeText: {
        type: String
    }, 
}, {
    versionKey: false,
    collection: "RefCommissionPaymentTypes"
});


const db = mongoose.connection.useDb(OPERATIONS_DB);
const RefCommissionPaymentTypeCollection = db.model(
    "RefCommissionPaymentTypes",
    RefCommissionPaymentTypeSchema
);

export {
    RefCommissionPaymentType,
    RefCommissionPaymentTypeCollection,
    RefCommissionPaymentTypeSchema
};
