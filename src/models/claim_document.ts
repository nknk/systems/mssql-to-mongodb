import { STRING, DATE, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class ClaimDocument extends ModelWithTenantId {
    ClaimId: number;
    DocumentTagId: number;
}

ClaimDocument.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT
    },
    ClaimId: {
        type: BIGINT
    },
    LocationPath: {
        type: STRING
    },
    DocumentTagId: {
        type: BIGINT
    },
    Name: {
        type: STRING
    },
    Description: {
        type: STRING
    },
    DateCreated: {
        type: DATE
    }
}, {
    sequelize,
    modelName: "ClaimDocument",
    freezeTableName: true,
    tableName: "ClaimDocument",
    timestamps: false
});


const ClaimDocumentSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    ClaimId: {
        type: mongoose.Types.ObjectId
    },
    LocationPath: {
        type: String
    },
    DocumentTagId: {
        type: mongoose.Types.ObjectId
    },
    Name: {
        type: String
    },
    Description: {
        type: String
    },
    DateCreated: {
        type: Date
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "ClaimDocuments"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const ClaimDocumentCollection = db.model("ClaimDocuments", ClaimDocumentSchema);

export { ClaimDocument, ClaimDocumentCollection, ClaimDocumentSchema };
