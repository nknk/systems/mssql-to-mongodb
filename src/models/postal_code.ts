import { STRING, BOOLEAN, FLOAT, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { BaseModel } from "./contracts/base_models";
import { IDENTITY_DB } from "../config";

class PostalCode extends BaseModel {
    CountryId: number;
}

PostalCode.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    Code: {
        type: STRING,
        allowNull: false
    },
    City: {
        type: STRING,
        allowNull: false
    },
    CityAlias: {
        type: STRING,
        allowNull: false
    },
    State: {
        type: STRING,
        allowNull: false
    },
    CountryId: {
        type: BIGINT,
        allowNull: false
    },
    Primary: {
        type: BOOLEAN,
        allowNull: false
    },
    Latitude: {
        type: FLOAT,
        allowNull: false
    },
    Longitude: {
        type: FLOAT,
        allowNull: false
    },
}, {
    sequelize,
    modelName: "PostalCode",
    freezeTableName: true,
    tableName: "PostalCode",
    timestamps: false
});


const PostalCodeSchema = new mongoose.Schema({
    Code: {
        type: String,
    },
    City: {
        type: String,
    },
    CityAlias: {
        type: String,
    },
    State: {
        type: String,
    },
    CountryId: {
        type: mongoose.Types.ObjectId,
    },
    Primary: {
        type: Boolean,
    },
    Latitude: {
        type: Number,
    },
    Longitude: {
        type: Number,
    },
    OldId: {
        type: Number
    },
}, {
    versionKey: false,
    collection: "PostalCodes"
});

const db = mongoose.connection.useDb(IDENTITY_DB);
const PostalCodeCollection = db.model("PostalCodes", PostalCodeSchema);

export { PostalCode, PostalCodeCollection, PostalCodeSchema };
