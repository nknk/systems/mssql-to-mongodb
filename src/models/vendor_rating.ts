import { STRING, BOOLEAN, INTEGER, DATE, FLOAT, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { RATING_DB } from "../config";

class VendorRating extends ModelWithTenantId {
    FuelChargeCodeId: number;
    FuelTableId: number;
    VendorId: number;
    LtlPackageSpecificRatePackageTypeId: number;
    LTLPackageSpecificRatePackageTypeId?: number;
    LTLPackageSpecificFreightChargeCodeId: number;
    EnableLtlPackageSpecificRates: boolean;
    EnableLTLPackageSpecificRates?: boolean;
}

VendorRating.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    Name: {
        type: STRING
    },
    DisplayName: {
        type: STRING
    },
    FuelIndexRegion: {
        type: INTEGER
    },
    CurrentLTLFuelMarkup: {
        type: DECIMAL
    },
    FuelMarkupCeiling: {
        type: DECIMAL
    },
    FuelMarkupFloor: {
        type: DECIMAL
    },
    FuelUpdatesOn: {
        type: INTEGER
    },
    FuelChargeCodeId: {
        type: BIGINT
    },
    HasAdditionalCharges: {
        type: BOOLEAN
    },
    LTLTariff: {
        type: STRING
    },
    HasLTLCubicFootCapacityRules: {
        type: BOOLEAN
    },
    LTLTruckLength: {
        type: DECIMAL
    },
    LTLTruckWidth: {
        type: DECIMAL
    },
    LTLTruckHeight: {
        type: DECIMAL
    },
    LTLTruckWeight: {
        type: DECIMAL
    },
    LTLMinPickupWeight: {
        type: DECIMAL
    },
    LTLTruckUnitWeight: {
        type: DECIMAL
    },
    HasLTLOverLengthRules: {
        type: BOOLEAN
    },
    FuelTableId: {
        type: BIGINT
    },
    VendorId: {
        type: BIGINT
    },
    OverrideAddress: {
        type: STRING
    },
    Active: {
        type: BOOLEAN
    },
    EnableLTLPcfToFcConversion: {
        type: BOOLEAN
    },
    LTLMaxCfForPcfConv: {
        type: DECIMAL
    },
    EnableLtlPackageSpecificRates: {
        type: BOOLEAN
    },
    LtlPackageSpecificRatePackageTypeId: {
        type: BIGINT
    },
    LTLPackageSpecificFreightChargeCodeId: {
        type: BIGINT
    },
    CubicCapacityPenaltyHeight: {
        type: DECIMAL
    },
    CubicCapacityPenaltyWidth: {
        type: DECIMAL
    },
    ExpirationDate: {
        type: DATE
    },
    MaxPackageQuantityApplies: {
        type: BOOLEAN
    },
    IndividualItemLimitsApply: {
        type: BOOLEAN
    },
    MaxPackageQuantity: {
        type: INTEGER
    },
    MaxItemLength: {
        type: INTEGER
    },
    MaxItemWidth: {
        type: INTEGER
    },
    MaxItemHeight: {
        type: INTEGER
    },
    RatingDetailNotes: {
        type: STRING
    },
    CriticalBOLNotes: {
        type: STRING
    },
    Smc3ServiceLevel: {
        type: STRING
    },
    Project44Profile: {
        type: BOOLEAN
    },
    Project44TradingPartnerCode: {
        type: STRING
    },
    MaximumLiabilityAmount: {
        type: DECIMAL
    },
    UsedGoodsLiabilityPerPound: {
        type: DECIMAL
    },
    SpotQuoteLiabilityPerPound: {
        type: DECIMAL
    },
    CanadaOriginLiabilityPerPound: {
        type: DECIMAL
    },
    NewGoodsLiabilityPerPound: {
        type: DECIMAL
    },
    FreightClassLiabilityOverrideEnabled: {
        type: BOOLEAN
    },
    MexicoOriginLiabilityPerPound: {
        type: FLOAT
    },
    MaxCubicFeet: {
        type: INTEGER
    },
    MaxCubicFeetApplies: {
        type: BOOLEAN
    }
}, {
    sequelize,
    modelName: "VendorRating",
    freezeTableName: true,
    tableName: "VendorRating",
    timestamps: false
});


const VendorRatingSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    Name: {
        type: String
    },
    DisplayName: {
        type: String
    },
    FuelIndexRegion: {
        type: Number
    },
    CurrentLTLFuelMarkup: {
        type: Number
    },
    FuelMarkupCeiling: {
        type: Number
    },
    FuelMarkupFloor: {
        type: Number
    },
    FuelUpdatesOn: {
        type: Number
    },
    FuelChargeCodeId: {
        type: mongoose.Types.ObjectId
    },
    HasAdditionalCharges: {
        type: Boolean
    },
    LTLTariff: {
        type: String
    },
    HasLTLCubicFootCapacityRules: {
        type: Boolean
    },
    LTLTruckLength: {
        type: Number
    },
    LTLTruckWidth: {
        type: Number
    },
    LTLTruckHeight: {
        type: Number
    },
    LTLTruckWeight: {
        type: Number
    },
    LTLMinPickupWeight: {
        type: Number
    },
    LTLTruckUnitWeight: {
        type: Number
    },
    HasLTLOverLengthRules: {
        type: Boolean
    },
    FuelTableId: {
        type: mongoose.Types.ObjectId
    },
    VendorId: {
        type: mongoose.Types.ObjectId
    },
    OverrideAddress: {
        type: String
    },
    Active: {
        type: Boolean
    },
    EnableLTLPcfToFcConversion: {
        type: Boolean
    },
    LTLMaxCfForPcfConv: {
        type: Number
    },
    EnableLTLPackageSpecificRates: {
        type: Boolean
    },
    LTLPackageSpecificRatePackageTypeId: {
        type: mongoose.Types.ObjectId
    },
    LTLPackageSpecificFreightChargeCodeId: {
        type: mongoose.Types.ObjectId
    },
    CubicCapacityPenaltyHeight: {
        type: Number
    },
    CubicCapacityPenaltyWidth: {
        type: Number
    },
    ExpirationDate: {
        type: Date
    },
    MaxPackageQuantityApplies: {
        type: Boolean
    },
    IndividualItemLimitsApply: {
        type: Boolean
    },
    MaxPackageQuantity: {
        type: Number
    },
    MaxItemLength: {
        type: Number
    },
    MaxItemWidth: {
        type: Number
    },
    MaxItemHeight: {
        type: Number
    },
    RatingDetailNotes: {
        type: String
    },
    CriticalBOLNotes: {
        type: String
    },
    Smc3ServiceLevel: {
        type: String
    },
    Project44Profile: {
        type: Boolean
    },
    Project44TradingPartnerCode: {
        type: String
    },
    MaximumLiabilityAmount: {
        type: Number
    },
    UsedGoodsLiabilityPerPound: {
        type: Number
    },
    SpotQuoteLiabilityPerPound: {
        type: Number
    },
    CanadaOriginLiabilityPerPound: {
        type: Number
    },
    NewGoodsLiabilityPerPound: {
        type: Number
    },
    FreightClassLiabilityOverrideEnabled: {
        type: Boolean
    },
    MexicoOriginLiabilityPerPound: {
        type: Number
    },
    MaxCubicFeet: {
        type: Number
    },
    MaxCubicFeetApplies: {
        type: Boolean
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "VendorRatings"
});

declare interface IVendorRatingDoc extends mongoose.Document {
    LTLPackageSpecificRatePackageTypeId: string;
    EnableLTLPackageSpecificRates: boolean;
}


const db = mongoose.connection.useDb(RATING_DB);
const VendorRatingCollection = db.model<IVendorRatingDoc>(
    "VendorRatings",
    VendorRatingSchema
);

export { VendorRating, VendorRatingCollection, VendorRatingSchema };
