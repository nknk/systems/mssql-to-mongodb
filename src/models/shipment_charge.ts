import { STRING, INTEGER, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class ShipmentCharge extends ModelWithTenantId {
    ShipmentId: number;
    ChargeCodeId: number;
    VendorId: number;
    VendorBillId: number;

}

ShipmentCharge.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    ShipmentId: {
        type: BIGINT
    },
    Quantity: {
        type: INTEGER
    },
    UnitBuy: {
        type: DECIMAL
    },
    UnitSell: {
        type: DECIMAL
    },
    UnitDiscount: {
        type: DECIMAL
    },
    Comment: {
        type: STRING
    },
    ChargeCodeId: {
        type: BIGINT
    },
    VendorId: {
        type: BIGINT
    },
    VendorBillId: {
        type: BIGINT
    }
}, {
    sequelize,
    modelName: "ShipmentCharge",
    freezeTableName: true,
    tableName: "ShipmentCharge",
    timestamps: false
});


const ShipmentChargeSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    ShipmentId: {
        type: mongoose.Types.ObjectId
    },
    Quantity: {
        type: Number
    },
    UnitBuy: {
        type: Number
    },
    UnitSell: {
        type: Number
    },
    UnitDiscount: {
        type: Number
    },
    Comment: {
        type: String
    },
    ChargeCodeId: {
        type: mongoose.Types.ObjectId
    },
    VendorId: {
        type: mongoose.Types.ObjectId
    },
    VendorBillId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "ShipmentCharges"
});

export interface IShipmentChargeDocument extends mongoose.Document {
    ApplyToDocumentId: string;
}

const db = mongoose.connection.useDb(OPERATIONS_DB);
const ShipmentChargeCollection = db.model<IShipmentChargeDocument>(
    "ShipmentCharges",
    ShipmentChargeSchema
);

export { ShipmentCharge, ShipmentChargeCollection, ShipmentChargeSchema };
