import { STRING, INTEGER, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { REPORTING_DB } from "../config";

class QlikSheet extends ModelWithTenantId {
    ReportConfigurationId: number;
 }

QlikSheet.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    Name: {
        type: STRING
    },
    Link: {
        type: STRING
    },
    ReportConfigurationId: {
        type: BIGINT
    },
    DisplayOrder: {
        type: INTEGER
    }
}, {
    sequelize,
    modelName: "QlikSheet",
    freezeTableName: true,
    tableName: "QlikSheet",
    timestamps: false
});

const QlikSheetSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    Name: {
        type: String
    },
    Link: {
        type: String
    },
    ReportConfigurationId: {
        type: mongoose.Types.ObjectId
    },
    DisplayOrder: {
        type: Number
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "QlikSheets"
});

const db = mongoose.connection.useDb(REPORTING_DB);
const QlikSheetCollection = db.model("QlikSheets", QlikSheetSchema);

export { QlikSheet, QlikSheetCollection, QlikSheetSchema };
