import { STRING, DATE, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { BaseModel } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class Project44RatingResponse extends BaseModel {
    UserId: number;
}

Project44RatingResponse.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    UserId: {
        type: BIGINT,
    },
    RequestData: {
        type: STRING,
    },
    ResponseData: {
        type: STRING,
    },
    DateCreated: {
        type: DATE,
    },
    Scac: {
        type: STRING,
    },
}, {
    sequelize,
    modelName: "Project44RatingResponse",
    freezeTableName: true,
    tableName: "Project44RatingResponse",
    timestamps: false
});


const Project44RatingResponseSchema = new mongoose.Schema({
    OldId: {
        type: Number
    },
    UserId: {
        type: mongoose.Types.ObjectId
    },
    RequestData: {
        type: String
    },
    ResponseData: {
        type: String
    },
    DateCreated: {
        type: Date
    },
    Scac: {
        type: String
    },
}, {
    versionKey: false,
    collection: "Project44RatingResponses"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const Project44RatingResponseCollection = db.model("Project44RatingResponses", Project44RatingResponseSchema);

export { Project44RatingResponse, Project44RatingResponseSchema, Project44RatingResponseCollection };
