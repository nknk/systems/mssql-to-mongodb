import { STRING, BOOLEAN, INTEGER, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { REGISTRY_DB } from "../config";
class EquipmentType extends ModelWithTenantId { }

EquipmentType.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    Code: {
        type: STRING,
        allowNull: false
    },
    TypeName: {
        type: STRING,
        allowNull: false
    },
    Active: {
        type: BOOLEAN,
        allowNull: false
    },
    Group: {
        type: INTEGER,
        allowNull: false
    },
    DatEquipmentType: {
        type: INTEGER,
        allowNull: false
    },
}, {
    sequelize,
    modelName: "EquipmentType",
    freezeTableName: true,
    tableName: "EquipmentType",
    timestamps: false
});


const EquipmentTypeSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    Code: {
        type: String,
    },
    TypeName: {
        type: String,
    },
    Active: {
        type: Boolean,
    },
    Group: {
        type: Number,
    },
    DatEquipmentType: {
        type: Number,
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "EquipmentTypes"
});

const db = mongoose.connection.useDb(REGISTRY_DB);
const EquipmentTypeCollection = db.model("EquipmentTypes", EquipmentTypeSchema);

export { EquipmentType, EquipmentTypeCollection, EquipmentTypeSchema };
