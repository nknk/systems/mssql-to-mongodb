import { STRING, BOOLEAN, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class AddressBookContact extends ModelWithTenantId {
    AddressBookId: number;
    ContactTypeId: number;
}

AddressBookContact.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    AddressBookId: {
        type: BIGINT,
        allowNull: false
    },
    Name: {
        type: STRING,
        allowNull: false
    },
    Phone: {
        type: STRING,
        allowNull: false
    },
    Mobile: {
        type: STRING,
        allowNull: false
    },
    Fax: {
        type: STRING,
        allowNull: false
    },
    Email: {
        type: STRING,
        allowNull: false
    },
    Comment: {
        type: STRING,
        allowNull: false
    },
    Primary: {
        type: BOOLEAN,
        allowNull: false
    },
    ContactTypeId: {
        type: BIGINT,
        allowNull: false
    },
}, {
    sequelize,
    modelName: "AddressBookContact",
    freezeTableName: true,
    tableName: "AddressBookContact",
    timestamps: false
});

const AddressBookContactSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    },
    AddressBookId: {
        type: mongoose.Types.ObjectId
    },
    Name: {
        type: String
    },
    Phone: {
        type: String
    },
    Mobile: {
        type: String
    },
    Fax: {
        type: String
    },
    Email: {
        type: String
    },
    Comment: {
        type: String
    },
    Primary: {
        type: Boolean
    },
    ContactTypeId: {
        type: mongoose.Types.ObjectId
    },
}, {
    versionKey: false,
    collection: "AddressBookContacts"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const AddressBookContactCollection = db.model("AddressBookContacts", AddressBookContactSchema);

export { AddressBookContact, AddressBookContactCollection, AddressBookContactSchema };
