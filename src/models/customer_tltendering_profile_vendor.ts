import { INTEGER, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";

class CustomerTLTenderingProfileVendor extends ModelWithTenantId {
    TLTenderingProfileId: number;
    VendorId: number;
}

CustomerTLTenderingProfileVendor.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    VendorId: {
        type: BIGINT
    },
    CommunicationType: {
        type: INTEGER
    },
    TLTenderingProfileId: {
        type: BIGINT
    },
}, {
    sequelize,
    modelName: "CustomerTLTenderingProfileVendor",
    freezeTableName: true,
    tableName: "CustomerTLTenderingProfileVendor",
    timestamps: false
});


const CustomerTLTenderingProfileVendorSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    VendorId: {
        type: mongoose.Types.ObjectId
    },
    CommunicationType: {
        type: Number
    },
    TLTenderingProfileId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "CustomerTLTenderingProfileVendors"
});


const db = mongoose.connection.useDb(FINANCE_DB);
const CustomerTLTenderingProfileVendorCollection = db.model(
    "CustomerTLTenderingProfileVendors",
    CustomerTLTenderingProfileVendorSchema,
);

export {
    CustomerTLTenderingProfileVendor,
    CustomerTLTenderingProfileVendorCollection,
    CustomerTLTenderingProfileVendorSchema
};
