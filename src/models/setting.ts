import { STRING, BIGINT, INTEGER } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { REGISTRY_DB } from "../config";

class Setting extends ModelWithTenantId { }

Setting.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    Code: {
        type: INTEGER,
        allowNull: false
    },
    Value: {
        type: STRING,
        allowNull: false
    }
}, {
    sequelize,
    modelName: "Setting",
    freezeTableName: true,
    tableName: "Setting",
    timestamps: false
});


const SettingSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    Code: {
        type: Number
    },
    OldId: {
        type: Number
    },
    Value: {
        type: String
    }
}, {
    versionKey: false,
    collection: "Settings"
});

const db = mongoose.connection.useDb(REGISTRY_DB);
const SettingCollection = db.model("Settings", SettingSchema);

export { Setting, SettingSchema, SettingCollection };
