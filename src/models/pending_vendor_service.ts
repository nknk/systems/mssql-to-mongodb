import { BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";
class PendingVendorService extends ModelWithTenantId {
    PendingVendorId: number;
    ServiceId: number;
}

PendingVendorService.init({
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    ServiceId: {
        type: BIGINT,
        allowNull: false
    },
    PendingVendorId: {
        type: BIGINT,
        allowNull: false
    }
}, {
    sequelize,
    modelName: "PendingVendorService",
    freezeTableName: true,
    tableName: "PendingVendorService",
    timestamps: false
});


const PendingVendorServiceSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    ServiceId: {
        type: mongoose.Types.ObjectId
    },
    PendingVendorId: {
        type: mongoose.Types.ObjectId
    }
}, {
    versionKey: false,
    collection: "PendingVendorServices"
});

const db = mongoose.connection.useDb(FINANCE_DB);
const PendingVendorServiceCollection = db
    .model("PendingVendorServices", PendingVendorServiceSchema);

export {
    PendingVendorService,
    PendingVendorServiceCollection,
    PendingVendorServiceSchema
};
