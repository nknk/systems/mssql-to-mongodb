import { STRING, BOOLEAN, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class LoadOrderVendor extends ModelWithTenantId {
    LoadOrderId: number;
    VendorId: number;
}

LoadOrderVendor.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    Primary: {
        type: BOOLEAN
    },
    ProNumber: {
        type: STRING
    },
    VendorId: {
        type: BIGINT
    },
    LoadOrderId: {
        type: BIGINT
    }
}, {
    sequelize,
    modelName: "LoadOrderVendor",
    freezeTableName: true,
    tableName: "LoadOrderVendor",
    timestamps: false
});

const LoadOrderVendorSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    Primary: {
        type: Boolean
    },
    ProNumber: {
        type: String
    },
    VendorId: {
        type: mongoose.Types.ObjectId
    },
    LoadOrderId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "LoadOrderVendors"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const LoadOrderVendorCollection = db.model("LoadOrderVendors", LoadOrderVendorSchema);

export { LoadOrderVendor, LoadOrderVendorCollection, LoadOrderVendorSchema };
