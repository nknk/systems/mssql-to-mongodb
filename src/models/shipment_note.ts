import { INTEGER, STRING, BOOLEAN, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class ShipmentNote extends ModelWithTenantId {
    UserId: number;
    ShipmentId: number;
}

ShipmentNote.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    ShipmentId: {
        type: BIGINT
    },
    Message: {
        type: STRING
    },
    Type: {
        type: INTEGER
    },
    Archived: {
        type: BOOLEAN
    },
    Classified: {
        type: BOOLEAN
    },
    UserId: {
        type: BIGINT
    }  
}, {
    sequelize,
    modelName: "ShipmentNote",
    freezeTableName: true,
    tableName: "ShipmentNote",
    timestamps: false
});


const ShipmentNoteSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    ShipmentId: {
        type: mongoose.Types.ObjectId
    },
    Message: {
        type: String
    },
    Type: {
        type: Number
    },
    Archived: {
        type: Boolean
    },
    Classified: {
        type: Boolean
    },
    UserId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "ShipmentNotes"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const ShipmentNoteCollection = db.model("ShipmentNotes", ShipmentNoteSchema);

export { ShipmentNote, ShipmentNoteCollection, ShipmentNoteSchema };
