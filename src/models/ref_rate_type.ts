import { STRING, INTEGER } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import {BaseModel } from "./contracts/base_models";
import {  OPERATIONS_DB } from "../config";

class RefRateType extends BaseModel {
   RateTypeIndex: number; 
}

RefRateType.init({
 
    RateTypeIndex: {
        type: INTEGER,
        primaryKey: true
    },
    RateTypeText: {
        type: STRING
    },
  
}, {
    sequelize,
    modelName: "RefRateType",
    freezeTableName: true,
    tableName: "RefRateType",
    timestamps: false
});


const RefRateTypeSchema = new mongoose.Schema({
    RateTypeIndex: {
        type: Number,
    },
    RateTypeText: {
        type:String
    },
}, {
    versionKey: false,
    collection: "RefRateTypes"
});


const db = mongoose.connection.useDb(OPERATIONS_DB);
const RefRateTypeCollection = db.model(
    "RefRateTypes",
    RefRateTypeSchema
);

export { RefRateType, RefRateTypeCollection, RefRateTypeSchema };
