import { STRING, BOOLEAN, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { IDENTITY_DB } from "../config";

class GroupPermission extends ModelWithTenantId {
    GroupId: number;
}

GroupPermission.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    GroupId: {
        type: BIGINT
    },
    Code: {
        type: STRING
    },
    Description: {
        type: STRING
    },
    Grant: {
        type: BOOLEAN
    },
    Deny: {
        type: BOOLEAN
    },
    Modify: {
        type: BOOLEAN
    },
    Delete: {
        type: BOOLEAN
    },
    ModifyApplicable: {
        type: BOOLEAN
    },
    DeleteApplicable: {
        type: BOOLEAN
    }

}, {
    sequelize,
    modelName: "GroupPermission",
    freezeTableName: true,
    tableName: "GroupPermission",
    timestamps: false
});


const GroupPermissionSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    GroupId: {
        type: mongoose.Types.ObjectId
    },
    Code: {
        type: String
    },
    Description: {
        type: String
    },
    Grant: {
        type: Boolean
    },
    Deny: {
        type: Boolean
    },
    Modify: {
        type: Boolean
    },
    Delete: {
        type: Boolean
    },
    ModifyApplicable: {
        type: Boolean
    },
    DeleteApplicable: {
        type: Boolean
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "GroupPermissions"
});

const db = mongoose.connection.useDb(IDENTITY_DB);
const GroupPermissionCollection = db.model("GroupPermissions", GroupPermissionSchema);

export {
    GroupPermission,
    GroupPermissionCollection,
    GroupPermissionSchema
};
