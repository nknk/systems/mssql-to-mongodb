import { STRING, INTEGER, BOOLEAN, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { REGISTRY_DB } from "../config";

class MileageSource extends ModelWithTenantId { }

MileageSource.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    Code: {
        type: STRING,
        allowNull: false
    },
    Description: {
        type: STRING,
        allowNull: false
    },
    Primary: {
        type: BOOLEAN,
        allowNull: false
    },
    MileageEngine: {
        type: INTEGER,
        allowNull: false
    },
}, {
    sequelize,
    modelName: "MileageSource",
    freezeTableName: true,
    tableName: "MileageSource",
    timestamps: false
});


const MileageSourceSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    Code: {
        type: String,
    },
    Description: {
        type: String,
    },
    Primary: {
        type: Boolean,
    },
    MileageEngine: {
        type: Number,
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "MileageSources"
});

const db = mongoose.connection.useDb(REGISTRY_DB);
const MileageSourceCollection = db.model("MileageSources", MileageSourceSchema);

export { MileageSource, MileageSourceCollection, MileageSourceSchema };
