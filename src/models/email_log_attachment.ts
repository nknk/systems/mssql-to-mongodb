import { STRING, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { UTILITIES_DB } from "../config";

class EmailLogAttachment extends ModelWithTenantId {
    EmailLogId: number;
    AttachmentBytes: string;
}

EmailLogAttachment.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    EmailLogId: {
        type: BIGINT
    },
    Name: {
        type: STRING
    }
}, {
    sequelize,
    modelName: "EmailLogAttachment",
    freezeTableName: true,
    tableName: "EmailLogAttachment",
    timestamps: false
});


const EmailLogAttachmentSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.SchemaTypes.ObjectId
    },
    OldId: {
        type: Number
    },
    EmailLogId: {
        type: mongoose.SchemaTypes.ObjectId
    },
    Name: {
        type: String
    }
}, {
    versionKey: false,
    collection: "EmailLogAttachments"
});


const db = mongoose.connection.useDb(UTILITIES_DB);
const EmailLogAttachmentCollection = db.model("EmailLogAttachments", EmailLogAttachmentSchema);

export {
    EmailLogAttachment,
    EmailLogAttachmentCollection,
    EmailLogAttachmentSchema
};
