import { STRING, INTEGER } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { BaseModel } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class RefChargeLineType extends BaseModel {
    ChargeLineTypeIndex: number;
}

RefChargeLineType.init({

    ChargeLineTypeIndex: {
        type: INTEGER,
        primaryKey: true
    },
    ChargeLineTypeText: {
        type: STRING
    },

}, {
    sequelize,
    modelName: "RefChargeLineType",
    freezeTableName: true,
    tableName: "RefChargeLineType",
    timestamps: false
});


const RefChargeLineTypeSchema = new mongoose.Schema({
    ChargeLineTypeIndex: {
        type: Number,
        primaryKey: true
    },
    ChargeLineTypeText: {
        type: String
    },
}, {
    versionKey: false,
    collection: "RefChargeLineTypes"
});


const db = mongoose.connection.useDb(OPERATIONS_DB);
const RefChargeLineTypeCollection = db.model(
    "RefChargeLineTypes",
    RefChargeLineTypeSchema
);

export {
    RefChargeLineType,
    RefChargeLineTypeCollection,
    RefChargeLineTypeSchema
};
