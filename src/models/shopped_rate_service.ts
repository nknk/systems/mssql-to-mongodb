import { BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { OPERATIONS_DB } from "../config";
import { ModelWithTenantId } from "./contracts/base_models";

class ShoppedRateService extends ModelWithTenantId {
    ShoppedRateId: number;
    ServiceId: number;
}

ShoppedRateService.init({    
    TenantId: {
        type: BIGINT
    },
    ShoppedRateId: {
        type: BIGINT
    },
    ServiceId: {
        type: BIGINT
    }
}, {
    sequelize,
    modelName: "ShoppedRateService",
    freezeTableName: true,
    tableName: "ShoppedRateService",
    timestamps: false
});


const ShoppedRateServiceSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    ShoppedRateId: {
        type: mongoose.Types.ObjectId
    },
    ServiceId: {
        type: mongoose.Types.ObjectId
    }
}, {
    versionKey: false,
    collection: "ShoppedRateServices"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const ShoppedRateServiceCollection = db.model("ShoppedRateServices", ShoppedRateServiceSchema);

export { ShoppedRateService, ShoppedRateServiceCollection, ShoppedRateServiceSchema };
