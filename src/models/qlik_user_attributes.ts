import { STRING } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { BaseModel } from "./contracts/base_models";
import { REPORTING_DB } from "../config";

class QlikUserAttribute extends BaseModel {}

QlikUserAttribute.init({
    userid:{
        type: STRING
    },
    type:{
        type: STRING
    },
    value:{
        type: STRING
    }    
}, {
    sequelize,
    modelName: "QlikUserAttributes",
    freezeTableName: true,
    tableName: "QlikUserAttributes",
    timestamps: false
});

const QlikUserAttributeSchema = new mongoose.Schema({
    userid:{
        type: String
    },
    type:{
        type: String
    },
    value:{
        type: String
    },
}, {
    versionKey: false,
    collection: "QlikUserAttributes"
});

const db = mongoose.connection.useDb(REPORTING_DB);
const QlikUserAttributeCollection = db.model("QlikUserAttributes", QlikUserAttributeSchema);

export { QlikUserAttribute, QlikUserAttributeCollection, QlikUserAttributeSchema };
