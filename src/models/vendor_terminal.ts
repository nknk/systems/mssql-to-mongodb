import { DATE, STRING, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class VendorTerminal extends ModelWithTenantId {
    VendorId: number;
    CountryId: number;
}

VendorTerminal.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    Name: {
        type: STRING
    },
    DateCreated: {
        type: DATE
    },
    VendorId: {
        type: BIGINT
    },
    Street1: {
        type: STRING
    },
    Street2: {
        type: STRING
    },
    City: {
        type: STRING
    },
    State: {
        type: STRING
    },
    CountryId: {
        type: BIGINT
    },
    PostalCode: {
        type: STRING
    },
    ContactName: {
        type: STRING
    },
    Comment: {
        type: STRING
    },
    Phone: {
        type: STRING
    },
    Mobile: {
        type: STRING
    },
    Fax: {
        type: STRING
    },
    Email: {
        type: STRING
    }
}, {
    sequelize,
    modelName: "VendorTerminal",
    freezeTableName: true,
    tableName: "VendorTerminal",
    timestamps: false
});


const VendorTerminalSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    Name: {
        type: String
    },
    DateCreated: {
        type: Date
    },
    VendorId: {
        type: mongoose.Types.ObjectId
    },
    Street1: {
        type: String
    },
    Street2: {
        type: String
    },
    City: {
        type: String
    },
    State: {
        type: String
    },
    CountryId: {
        type: mongoose.Types.ObjectId
    },
    PostalCode: {
        type: String
    },
    ContactName: {
        type: String
    },
    Comment: {
        type: String
    },
    Phone: {
        type: String
    },
    Mobile: {
        type: String
    },
    Fax: {
        type: String
    },
    Email: {
        type: String
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "VendorTerminals"
});


const db = mongoose.connection.useDb(OPERATIONS_DB);
const VendorTerminalCollection = db.model(
    "VendorTerminals",
    VendorTerminalSchema
);

export { VendorTerminal, VendorTerminalCollection, VendorTerminalSchema };
