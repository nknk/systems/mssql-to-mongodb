import { BIGINT, INTEGER } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { REGISTRY_DB } from "../config";

class P44ChargeCodeMapping extends ModelWithTenantId {
    ChargeCodeId: number;
}

P44ChargeCodeMapping.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    Project44Code: {
        type: INTEGER,
        allowNull: false
    },
    ChargeCodeId: {
        type: BIGINT,
        allowNull: false
    }
}, {
    sequelize,
    modelName: "P44ChargeCodeMapping",
    freezeTableName: true,
    tableName: "P44ChargeCodeMapping",
    timestamps: false
});


const P44ChargeCodeMappingSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    Project44Code: {
        type: Number
    },
    ChargeCodeId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "P44ChargeCodeMappings"
});

const db = mongoose.connection.useDb(REGISTRY_DB);
const P44ChargeCodeMappingCollection = db.model("P44ChargeCodeMappings", P44ChargeCodeMappingSchema);

export { P44ChargeCodeMapping, P44ChargeCodeMappingSchema, P44ChargeCodeMappingCollection };
