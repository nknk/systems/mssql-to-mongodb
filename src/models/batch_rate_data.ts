import { STRING, BOOLEAN, INTEGER, DATE, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { RATING_DB } from "../config";

class BatchRateData extends ModelWithTenantId {
    ChargeCodeId: number;
    VendorRatingId: number;
    SubmittedByUserId: number;
    PackageTypeId: number;
 }

BatchRateData.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    Name: {
        type: STRING,
    },
    LaneData: {
        type: STRING,
        allowNull: false,
    },
    ResultData: {
        type: STRING,
        allowNull: false,
    },
    AnalysisEffectiveDate: {
        type: DATE,
        allowNull: false,
    },
    DateCreated: {
        type: DATE,
        allowNull: false,
    },
    DateCompleted: {
        type: DATE,
        allowNull: false,
    },
    ServiceMode: {
        type: INTEGER,
        allowNull: false,
    },
    LTLIndirectPointEnabled: {
        type: BOOLEAN,
        allowNull: false,
    },
    SmallPackageEngine: {
        type: INTEGER,
        allowNull: false,
    },
    ChargeCodeId: {
        type: BIGINT,
        allowNull: false,
    },
    VendorRatingId: {
        type: BIGINT,
        allowNull: false,
    },
    SubmittedByUserId: {
        type: BIGINT,
        allowNull: false,
    },
    SmallPackageEngineType: {
        type: STRING,
        allowNull: false,
    },
    PackageTypeId: {
        type: BIGINT,
        allowNull: false,
    },
    GroupCode: {
        type: STRING,
        allowNull: false,
    }
}, {
    sequelize,
    modelName: "BatchRateData",
    freezeTableName: true,
    tableName: "BatchRateData",
    timestamps: false
});


const BatchRateDataSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    },
    Name: {
        type: String,
    },
    LaneData: {
        type: String,
    },
    ResultData: {
        type: String,
    },
    AnalysisEffectiveDate: {
        type: Date,
    },
    DateCreated: {
        type: Date,
    },
    DateCompleted: {
        type: Date,
    },
    ServiceMode: {
        type: Number,
    },
    LTLIndirectPointEnabled: {
        type: Boolean,
    },
    SmallPackageEngine: {
        type: Number,
    },
    ChargeCodeId: {
        type: mongoose.Types.ObjectId,
    },
    VendorRatingId: {
        type: mongoose.Types.ObjectId,
    },
    SubmittedByUserId: {
        type: mongoose.Types.ObjectId,
    },
    SmallPackageEngineType: {
        type: String,
    },
    PackageTypeId: {
        type: mongoose.Types.ObjectId,
    },
    GroupCode: {
        type: String,
    }
}, {
    versionKey: false,
    collection: "BatchRateData"
});

const db = mongoose.connection.useDb(RATING_DB);
const BatchRateDataCollection = db.model("BatchRateData", BatchRateDataSchema);

export { BatchRateData, BatchRateDataSchema, BatchRateDataCollection };
