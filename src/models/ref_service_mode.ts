import { STRING, INTEGER } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import {BaseModel } from "./contracts/base_models";
import {  OPERATIONS_DB } from "../config";

class RefServiceMode extends BaseModel {
   ServiceModeIndex: number; 
}

RefServiceMode.init({
 
    ServiceModeIndex: {
        type: INTEGER,
        primaryKey: true
    },
    ServiceModeText: {
        type: STRING
    },
  
}, {
    sequelize,
    modelName: "RefServiceMode",
    freezeTableName: true,
    tableName: "RefServiceMode",
    timestamps: false
});


const RefServiceModeSchema = new mongoose.Schema({
    ServiceModeIndex: {
        type: Number,
    },
    ServiceModeText: {
        type:String
    },
}, {
    versionKey: false,
    collection: "RefServiceModes"
});


const db = mongoose.connection.useDb(OPERATIONS_DB);
const RefServiceModeCollection = db.model(
    "RefServiceModes",
    RefServiceModeSchema
);

export { RefServiceMode, RefServiceModeCollection, RefServiceModeSchema };
