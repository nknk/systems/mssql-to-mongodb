import { STRING, INTEGER } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import {BaseModel } from "./contracts/base_models";
import {  OPERATIONS_DB } from "../config";

class RefSmallPackageEngine extends BaseModel {
   SmallPackageEngineIndex: number; 
}

RefSmallPackageEngine.init({
 
    SmallPackageEngineIndex: {
        type: INTEGER,
        primaryKey: true
    },
    SmallPackageEngineText: {
        type: STRING
    },
  
}, {
    sequelize,
    modelName: "RefSmallPackageEngine",
    freezeTableName: true,
    tableName: "RefSmallPackageEngine",
    timestamps: false
});


const RefSmallPackageEngineSchema = new mongoose.Schema({
    SmallPackageEngineIndex: {
        type: Number,
    },
    SmallPackageEngineText: {
        type:String
    },
}, {
    versionKey: false,
    collection: "RefSmallPackageEngines"
});


const db = mongoose.connection.useDb(OPERATIONS_DB);
const RefSmallPackageEngineCollection = db.model(
    "RefSmallPackageEngines",
    RefSmallPackageEngineSchema
);

export { RefSmallPackageEngine, RefSmallPackageEngineCollection, RefSmallPackageEngineSchema };
