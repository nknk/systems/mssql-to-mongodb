import { STRING, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { RATING_DB } from "../config";

class LTLTerminalInfo extends ModelWithTenantId {
    ShipmentId: string;
    CountryId: number;

}

LTLTerminalInfo.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    ShipmentId: {
        type: BIGINT,
    },
    Code: {
        type: STRING,
    },
    Name: {
        type: STRING,
    },
    Phone: {
        type: STRING,
    },
    TollFree: {
        type: STRING,
    },
    Fax: {
        type: STRING,
    },
    Email: {
        type: STRING,
    },
    ContactName: {
        type: STRING,
    },
    ContactTitle: {
        type: STRING,
    },
    Street1: {
        type: STRING,
    },
    Street2: {
        type: STRING,
    },
    City: {
        type: STRING,
    },
    State: {
        type: STRING,
    },
    CountryId: {
        type: BIGINT,
    },
    PostalCode: {
        type: STRING,
    },
}, {
    sequelize,
    modelName: "LTLTerminalInfo",
    freezeTableName: true,
    tableName: "LTLTerminalInfo",
    timestamps: false
});


const LTLTerminalInfoSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    },
    ShipmentId: {
        type: mongoose.Types.ObjectId
    },
    Code: {
        type: String
    },
    Name: {
        type: String
    },
    Phone: {
        type: String
    },
    TollFree: {
        type: String
    },
    Fax: {
        type: String
    },
    Email: {
        type: String
    },
    ContactName: {
        type: String
    },
    ContactTitle: {
        type: String
    },
    Street1: {
        type: String
    },
    Street2: {
        type: String
    },
    City: {
        type: String
    },
    State: {
        type: String
    },
    CountryId: {
        type: mongoose.Types.ObjectId
    },
    PostalCode: {
        type: String
    },
}, {
    versionKey: false,
    collection: "LTLTerminalInfos"
});

declare interface ITerminalInfoCollection extends mongoose.Document {
    ShipmentId: string;
}


const db = mongoose.connection.useDb(RATING_DB);
const LTLTerminalInfoCollection = db
    .model<ITerminalInfoCollection>("LTLTerminalInfos", LTLTerminalInfoSchema);

export { LTLTerminalInfo, LTLTerminalInfoCollection, LTLTerminalInfoSchema };
