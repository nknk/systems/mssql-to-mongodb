import { STRING, INTEGER, DATE, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class AssetLog extends ModelWithTenantId {

    AssetId: number;
    ShipmentId: number;
    CountryId: number;
}

AssetLog.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    AssetId: {
        type: BIGINT,
        allowNull: false
    },
    Comment: {
        type: STRING,
        allowNull: false
    },
    DateCreated: {
        type: DATE,
        allowNull: false
    },
    LogDate: {
        type: DATE,
        allowNull: false
    },
    MileageEngine: {
        type: INTEGER,
        allowNull: false
    },
    MilesRun: {
        type: DECIMAL,
        allowNull: false
    },
    ShipmentId: {
        type: BIGINT,
        allowNull: false
    },
    Street1: {
        type: STRING,
        allowNull: false
    },
    Street2: {
        type: STRING,
        allowNull: false
    },
    City: {
        type: STRING,
        allowNull: false
    },
    State: {
        type: STRING,
        allowNull: false
    },
    PostalCode: {
        type: STRING,
        allowNull: false
    },
    CountryId: {
        type: BIGINT,
        allowNull: false
    },
}, {
    sequelize,
    modelName: "AssetLog",
    freezeTableName: true,
    tableName: "AssetLog",
    timestamps: false
});

const AssetLogSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    },
    AssetId: {
        type: mongoose.Types.ObjectId
    },
    Comment: {
        type: String
    },
    DateCreated: {
        type: Date
    },
    LogDate: {
        type: Date
    },
    MileageEngine: {
        type: Number
    },
    MilesRun: {
        type: Number
    },
    ShipmentId: {
        type: mongoose.Types.ObjectId
    },
    Street1: {
        type: String
    },
    Street2: {
        type: String
    },
    City: {
        type: String
    },
    State: {
        type: String
    },
    PostalCode: {
        type: String
    },
    CountryId: {
        type: mongoose.Types.ObjectId
    },
}, {
    versionKey: false,
    collection: "AssetLogs"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const AssetLogCollection = db.model("AssetLogs", AssetLogSchema);

export { AssetLog, AssetLogCollection, AssetLogSchema };