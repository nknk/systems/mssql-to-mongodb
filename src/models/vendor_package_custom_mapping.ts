import { STRING, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";

class VendorPackageCustomMapping extends ModelWithTenantId {
    PackageTypeId: number;
    VendorId: number;
}

VendorPackageCustomMapping.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    PackageTypeId: {
        type: BIGINT
    },
    VendorId: {
        type: BIGINT
    },
    VendorCode: {
        type: STRING
    }
}, {
    sequelize,
    modelName: "VendorPackageCustomMapping",
    freezeTableName: true,
    tableName: "VendorPackageCustomMapping",
    timestamps: false
});


const VendorPackageCustomMappingSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    PackageTypeId: {
        type: mongoose.Types.ObjectId
    },
    VendorId: {
        type: mongoose.Types.ObjectId
    },
    VendorCode: {
        type: String
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "VendorPackageCustomMappings"
});


const db = mongoose.connection.useDb(FINANCE_DB);
const VendorPackageCustomMappingCollection = db.model(
    "VendorPackageCustomMappings",
    VendorPackageCustomMappingSchema
);

export { VendorPackageCustomMapping, VendorPackageCustomMappingCollection, VendorPackageCustomMappingSchema };
