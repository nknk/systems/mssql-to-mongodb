import { STRING, INTEGER, BOOLEAN, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";
class LoadOrderNote extends ModelWithTenantId {
    UserId: number;
    LoadOrderId: number;
}

LoadOrderNote.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    LoadOrderId: {
        type: BIGINT
    },
    Message: {
        type: STRING
    },
    Type: {
        type: INTEGER
    },
    Archived: {
        type: BOOLEAN
    },
    Classified: {
        type: BOOLEAN
    },
    UserId: {
        type: BIGINT
    }
}, {
    sequelize,
    modelName: "LoadOrderNote",
    freezeTableName: true,
    tableName: "LoadOrderNote",
    timestamps: false
});

const LoadOrderNoteSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    LoadOrderId: {
        type: mongoose.Types.ObjectId
    },
    Message: {
        type: String
    },
    Type: {
        type: Number
    },
    Archived: {
        type: Boolean
    },
    Classified: {
        type: Boolean
    },
    UserId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "LoadOrderNotes"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const LoadOrderNoteCollection = db.model("LoadOrderNotes", LoadOrderNoteSchema);

export { LoadOrderNote, LoadOrderNoteCollection, LoadOrderNoteSchema };
