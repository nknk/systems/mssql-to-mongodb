import { INTEGER, DATE, STRING, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { RATING_DB } from "../config";

class LTLGuaranteedCharge extends ModelWithTenantId {
    ChargeCodeId: number;
    VendorRatingId: number;
}

LTLGuaranteedCharge.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    Description: {
        type: STRING,
    },
    Time: {
        type: STRING,
    },
    RateType: {
        type: INTEGER,
    },
    Rate: {
        type: DECIMAL,
    },
    FloorValue: {
        type: DECIMAL,
    },
    CeilingValue: {
        type: DECIMAL,
    },
    EffectiveDate: {
        type: DATE,
    },
    ChargeCodeId: {
        type: BIGINT,
    },
    VendorRatingId: {
        type: BIGINT,
    },
    CriticalNotes: {
        type: STRING,
    },
}, {
    sequelize,
    modelName: "LTLGuaranteedCharge",
    freezeTableName: true,
    tableName: "LTLGuaranteedCharge",
    timestamps: false
});


const LTLGuaranteedChargeSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    },
    Description: {
        type: String
    },
    Time: {
        type: String
    },
    RateType: {
        type: Number
    },
    Rate: {
        type: Number
    },
    FloorValue: {
        type: Number
    },
    CeilingValue: {
        type: Number
    },
    EffectiveDate: {
        type: Date
    },
    ChargeCodeId: {
        type: mongoose.Types.ObjectId
    },
    VendorRatingId: {
        type: mongoose.Types.ObjectId
    },
    CriticalNotes: {
        type: String
    },
}, {
    versionKey: false,
    collection: "LTLGuaranteedCharges"
});

const db = mongoose.connection.useDb(RATING_DB);
const LTLGuaranteedChargeCollection = db
    .model("LTLGuaranteedCharges", LTLGuaranteedChargeSchema);

export { LTLGuaranteedCharge, LTLGuaranteedChargeCollection, LTLGuaranteedChargeSchema };
