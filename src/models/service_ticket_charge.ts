import { INTEGER, STRING, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class ServiceTicketCharge extends ModelWithTenantId {
    ServiceTicketId: number;
    ChargeCodeId: number;
    VendorId: number;
    VendorBillId: number;
}

ServiceTicketCharge.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    ServiceTicketId: {
        type: BIGINT
    },
    Quantity: {
        type: INTEGER
    },
    UnitBuy: {
        type: DECIMAL
    },
    UnitSell: {
        type: DECIMAL
    },
    UnitDiscount: {
        type: DECIMAL
    },
    Comment: {
        type: STRING
    },
    ChargeCodeId: {
        type: BIGINT
    },
    VendorId: {
        type: BIGINT
    },
    VendorBillId: {
        type: BIGINT
    }
}, {
    sequelize,
    modelName: "ServiceTicketCharge",
    freezeTableName: true,
    tableName: "ServiceTicketCharge",
    timestamps: false
});


const ServiceTicketChargeSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    ServiceTicketId: {
        type: mongoose.Types.ObjectId
    },
    Quantity: {
        type: Number
    },
    UnitBuy: {
        type: Number
    },
    UnitSell: {
        type: Number
    },
    UnitDiscount: {
        type: Number
    },
    Comment: {
        type: String
    },
    ChargeCodeId: {
        type: mongoose.Types.ObjectId
    },
    VendorId: {
        type: mongoose.Types.ObjectId
    },
    VendorBillId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "ServiceTicketCharges"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const ServiceTicketChargeCollection = db.model("ServiceTicketCharges", ServiceTicketChargeSchema);

export { ServiceTicketCharge, ServiceTicketChargeCollection, ServiceTicketChargeSchema };
