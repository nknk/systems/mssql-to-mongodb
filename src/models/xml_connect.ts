import { INTEGER, STRING, BOOLEAN, DATE, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class XMLConnect extends ModelWithTenantId {
}

XMLConnect.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    ShipmentIdNumber: {
        type: STRING
    },
    Xml: {
        type: STRING
    },
    OriginStreet1: {
        type: STRING
    },
    OriginStreet2: {
        type: STRING
    },
    OriginCity: {
        type: STRING
    },
    OriginState: {
        type: STRING
    },
    OriginCountryName: {
        type: STRING
    },
    OriginCountryCode: {
        type: STRING
    },
    OriginPostalCode: {
        type: STRING
    },
    DestinationStreet1: {
        type: STRING
    },
    DestinationStreet2: {
        type: STRING
    },
    DestinationCity: {
        type: STRING
    },
    DestinationState: {
        type: STRING
    },
    DestinationCountryName: {
        type: STRING
    },
    DestinationCountryCode: {
        type: STRING
    },
    DestinationPostalCode: {
        type: STRING
    },
    ExpirationDate: {
        type: DATE
    },
    DateCreated: {
        type: DATE
    },
    ReceiptDate: {
        type: DATE
    },
    Direction: {
        type: INTEGER
    },
    CustomerNumber: {
        type: STRING
    },
    VendorNumber: {
        type: STRING
    },
    VendorScac: {
        type: STRING
    },
    PurchaseOrderNumber: {
        type: STRING
    },
    ShipperReference: {
        type: STRING
    },
    EquipmentDescriptionCode: {
        type: STRING
    },
    TotalStops: {
        type: INTEGER
    },
    TotalWeight: {
        type: DECIMAL
    },
    TotalPackages: {
        type: INTEGER
    },
    TotalPieces: {
        type: INTEGER
    },
    DocumentType: {
        type: INTEGER
    },
    Status: {
        type: INTEGER
    },
    StatusMessage: {
        type: STRING
    },
    TransmissionOkay: {
        type: BOOLEAN
    },
    FtpTransmission: {
        type: BOOLEAN
    },
    ControlNumber: {
        type: STRING
    },
    VendorPro: {
        type: STRING
    }
}, {
    sequelize,
    modelName: "XMLConnect",
    freezeTableName: true,
    tableName: "XMLConnect",
    timestamps: false
});


const XMLConnectSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    ShipmentIdNumber: {
        type: String
    },
    Xml: {
        type: String
    },
    OriginStreet1: {
        type: String
    },
    OriginStreet2: {
        type: String
    },
    OriginCity: {
        type: String
    },
    OriginState: {
        type: String
    },
    OriginCountryName: {
        type: String
    },
    OriginCountryCode: {
        type: String
    },
    OriginPostalCode: {
        type: String
    },
    DestinationStreet1: {
        type: String
    },
    DestinationStreet2: {
        type: String
    },
    DestinationCity: {
        type: String
    },
    DestinationState: {
        type: String
    },
    DestinationCountryName: {
        type: String
    },
    DestinationCountryCode: {
        type: String
    },
    DestinationPostalCode: {
        type: String
    },
    ExpirationDate: {
        type: Date
    },
    DateCreated: {
        type: Date
    },
    ReceiptDate: {
        type: Date
    },
    Direction: {
        type: Number
    },
    CustomerNumber: {
        type: String
    },
    VendorNumber: {
        type: String
    },
    VendorScac: {
        type: String
    },
    PurchaseOrderNumber: {
        type: String
    },
    ShipperReference: {
        type: String
    },
    EquipmentDescriptionCode: {
        type: String
    },
    TotalStops: {
        type: Number
    },
    TotalWeight: {
        type: Number
    },
    TotalPackages: {
        type: Number
    },
    TotalPieces: {
        type: Number
    },
    DocumentType: {
        type: Number
    },
    Status: {
        type: Number
    },
    StatusMessage: {
        type: String
    },
    TransmissionOkay: {
        type: Boolean
    },
    FtpTransmission: {
        type: Boolean
    },
    ControlNumber: {
        type: String
    },
    VendorPro: {
        type: String
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "XMLConnects"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const XMLConnectCollection = db
    .model("XMLConnects", XMLConnectSchema);

export { XMLConnect, XMLConnectCollection, XMLConnectSchema };
