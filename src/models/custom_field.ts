import { STRING, BOOLEAN, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";

class CustomField extends ModelWithTenantId {
    CustomerId: number;
}

CustomField.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    Name:{
        type: STRING
    },
    DisplayOnOrigin:{
        type: BOOLEAN
    },
    DisplayOnDestination:{
        type: BOOLEAN
    },
    ShowOnDocuments:{
        type: BOOLEAN
    },
    Required:{
        type: BOOLEAN
    },
    CustomerId:{
        type: BIGINT
    }
}, {
    sequelize,
    modelName: "CustomField",
    freezeTableName: true,
    tableName: "CustomField",
    timestamps: false
});

const CustomFieldSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    Name:{
        type: String
    },
    DisplayOnOrigin:{
        type: Boolean
    },
    DisplayOnDestination:{
        type: Boolean
    },
    ShowOnDocuments:{
        type: Boolean
    },
    Required:{
        type: Boolean
    },
    CustomerId:{
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "CustomFields"
});

const db = mongoose.connection.useDb(FINANCE_DB);
const CustomFieldCollection = db.model("CustomFields", CustomFieldSchema);

export { CustomField, CustomFieldCollection, CustomFieldSchema };
