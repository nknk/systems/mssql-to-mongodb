import { STRING, INTEGER } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import {BaseModel } from "./contracts/base_models";
import {  OPERATIONS_DB } from "../config";

class RefJobStatus extends BaseModel {
   JobStatusIndex: number; 
}

RefJobStatus.init({
 
    JobStatusIndex: {
        type: INTEGER,
        primaryKey: true
    },
    JobStatusText: {
        type: STRING
    },
  
}, {
    sequelize,
    modelName: "RefJobStatus",
    freezeTableName: true,
    tableName: "RefJobStatus",
    timestamps: false
});


const RefJobStatusSchema = new mongoose.Schema({
    JobStatusIndex: {
        type: Number,
    },
    JobStatusText: {
        type:String
    },
    
}, {
    versionKey: false,
    collection: "RefJobStatuses"
});


const db = mongoose.connection.useDb(OPERATIONS_DB);
const RefJobStatusCollection = db.model(
    "RefJobStatuses",
    RefJobStatusSchema
);

export { RefJobStatus, RefJobStatusCollection, RefJobStatusSchema };
