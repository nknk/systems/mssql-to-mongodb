import { STRING, BOOLEAN, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class LoadOrderReference extends ModelWithTenantId {
    LoadOrderId: number;
}

LoadOrderReference.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    LoadOrderId:{
        type: BIGINT
    },
    Name:{
        type: STRING
    },
    Value:{
        type: STRING
    },
    DisplayOnOrigin:{
        type: BOOLEAN
    },
    DisplayOnDestination:{
        type: BOOLEAN
    },
    Required:{
        type: BOOLEAN
    }
}, {
    sequelize,
    modelName: "LoadOrderReference",
    freezeTableName: true,
    tableName: "LoadOrderReference",
    timestamps: false
});

const LoadOrderReferenceSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    LoadOrderId:{
        type: mongoose.Types.ObjectId
    },
    Name:{
        type: String
    },
    Value:{
        type: String
    },
    DisplayOnOrigin:{
        type: Boolean
    },
    DisplayOnDestination:{
        type: Boolean
    },
    Required:{
        type: Boolean
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "LoadOrderReferences"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const LoadOrderReferenceCollection = db.model("LoadOrderReferences", LoadOrderReferenceSchema);

export { LoadOrderReference, LoadOrderReferenceCollection, LoadOrderReferenceSchema };
