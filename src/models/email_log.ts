import { STRING, DATE, BIGINT, BOOLEAN } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { UTILITIES_DB } from "../config";
class EmailLog extends ModelWithTenantId {}

EmailLog.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    From: {
        type: STRING
    },
    Tos: {
        type: STRING
    },
    Ccs: {
        type: STRING
    },
    Bbcs: {
        type: STRING
    },
    Body: {
        type: STRING
    },
    Subject: {
        type: STRING,
    },
    DateCreated: {
        type: DATE
    },
    Sent: {
        type: BOOLEAN,
    }
}, {
    sequelize,
    modelName: "EmailLog",
    freezeTableName: true,
    tableName: "EmailLog",
    timestamps: false
});


const EmailLogSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    From: {
        type: String
    },
    Tos: {
        type: String
    },
    Ccs: {
        type: String
    },
    Bbcs: {
        type: String
    },
    Body: {
        type: String
    },
    Subject: {
        type: String,
    },
    DateCreated: {
        type: Date
    },
    Sent: {
        type: Boolean,
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "EmailLogs"
});


const db = mongoose.connection.useDb(UTILITIES_DB);
const EmailLogCollection = db.model("EmailLogs", EmailLogSchema);

export {
    EmailLog,
    EmailLogCollection,
    EmailLogSchema
};
