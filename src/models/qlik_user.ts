import { STRING } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { BaseModel } from "./contracts/base_models";
import { REPORTING_DB } from "../config";

class QlikUser extends BaseModel {}

QlikUser.init({
    userid:{
        type: STRING
    },
    name:{
        type: STRING
    }    
}, {
    sequelize,
    modelName: "QlikUsers",
    freezeTableName: true,
    tableName: "QlikUsers",
    timestamps: false
});

const QlikUserSchema = new mongoose.Schema({
    userid:{
        type: String
    },
    name:{
        type: String
    },
}, {
    versionKey: false,
    collection: "QlikUsers"
});

const db = mongoose.connection.useDb(REPORTING_DB);
const QlikUserCollection = db.model("QlikUsers", QlikUserSchema);

export { QlikUser, QlikUserCollection, QlikUserSchema };
