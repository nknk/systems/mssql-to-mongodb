import { BOOLEAN, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class ServiceTicketVendor extends ModelWithTenantId {
    ServiceTicketId: number;
    VendorId: number;
}

ServiceTicketVendor.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    ServiceTicketId: {
        type: BIGINT
    },
    VendorId: {
        type: BIGINT
    },
    Primary: {
        type: BOOLEAN
    }
}, {
    sequelize,
    modelName: "ServiceTicketVendor",
    freezeTableName: true,
    tableName: "ServiceTicketVendor",
    timestamps: false
});


const ServiceTicketVendorSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    ServiceTicketId: {
        type: mongoose.Types.ObjectId
    },
    VendorId: {
        type: mongoose.Types.ObjectId
    },
    Primary: {
        type: Boolean
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "ServiceTicketVendors"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const ServiceTicketVendorCollection = db.model("ServiceTicketVendors", ServiceTicketVendorSchema);

export { ServiceTicketVendor, ServiceTicketVendorCollection, ServiceTicketVendorSchema };
