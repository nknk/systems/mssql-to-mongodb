import { STRING, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class ClaimVendor extends ModelWithTenantId {
    VendorId: number;
    ClaimId: number;
}

ClaimVendor.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    VendorId: {
        type: BIGINT
    },
    ClaimId: {
        type: BIGINT
    },
    ProNumber: {
        type: STRING
    },
    FreightBillNumber: {
        type: STRING
    }    
}, {
    sequelize,
    modelName: "ClaimVendor",
    freezeTableName: true,
    tableName: "ClaimVendor",
    timestamps: false
});


const ClaimVendorSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    VendorId: {
        type: mongoose.Types.ObjectId
    },
    ClaimId: {
        type: mongoose.Types.ObjectId
    },
    ProNumber: {
        type: String
    },
    FreightBillNumber: {
        type: String
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "ClaimVendors"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const ClaimVendorCollection = db.model("ClaimVendors", ClaimVendorSchema);

export { ClaimVendor, ClaimVendorCollection, ClaimVendorSchema };
