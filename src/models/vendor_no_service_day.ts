import { BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { OPERATIONS_DB } from "../config";
import { ModelWithTenantId } from "./contracts/base_models";

class VendorNoServiceDay extends ModelWithTenantId {
    NoServiceDayId: number;
    VendorId: number;
}

VendorNoServiceDay.init({    
    TenantId: {
        type: BIGINT
    },
    NoServiceDayId: {
        type: BIGINT
    },
    VendorId: {
        type: BIGINT
    }
}, {
    sequelize,
    modelName: "VendorNoServiceDay",
    freezeTableName: true,
    tableName: "VendorNoServiceDay",
    timestamps: false
});


const VendorNoServiceDaySchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    NoServiceDayId: {
        type: mongoose.Types.ObjectId
    },
    VendorId: {
        type: mongoose.Types.ObjectId
    }
}, {
    versionKey: false,
    collection: "VendorNoServiceDays"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const VendorNoServiceDayCollection = db.model("VendorNoServiceDays", VendorNoServiceDaySchema);

export { VendorNoServiceDay, VendorNoServiceDayCollection, VendorNoServiceDaySchema };
