import { STRING, BOOLEAN, INTEGER, DATE, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";
class Asset extends ModelWithTenantId { }

Asset.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    AssetNumber: {
        type: STRING,
        allowNull: false
    },
    DateCreated: {
        type: DATE,
        allowNull: false
    },
    AssetType: {
        type: INTEGER,
        allowNull: false
    },
    Active: {
        type: BOOLEAN,
        allowNull: false
    },
    Description: {
        type: STRING,
        allowNull: false
    },
}, {
    sequelize,
    modelName: "Asset",
    freezeTableName: true,
    tableName: "Asset",
    timestamps: false
});

const AssetSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    },
    AssetNumber: {
        type: String
    },
    DateCreated: {
        type: Date
    },
    AssetType: {
        type: Number
    },
    Active: {
        type: Boolean
    },
    Description: {
        type: String
    },
}, {
    versionKey: false,
    collection: "Assets"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const AssetCollection = db.model("Assets", AssetSchema);

export { Asset, AssetCollection, AssetSchema };
