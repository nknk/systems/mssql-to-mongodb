import { STRING, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { REGISTRY_DB } from "../config";

class InsuranceType extends ModelWithTenantId { }

InsuranceType.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    Code: {
        type: STRING,
        allowNull: false
    },
    Description: {
        type: STRING,
        allowNull: false
    }
}, {
    sequelize,
    modelName: "InsuranceType",
    freezeTableName: true,
    tableName: "InsuranceType",
    timestamps: false
});


const InsuranceTypeSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    Code: {
        type: String
    },
    Description: {
        type: String
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "InsuranceTypes"
});

const db = mongoose.connection.useDb(REGISTRY_DB);
const InsuranceTypeCollection = db.model("InsuranceTypes", InsuranceTypeSchema);

export { InsuranceType, InsuranceTypeSchema, InsuranceTypeCollection };
