import { STRING, INTEGER } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import {BaseModel } from "./contracts/base_models";
import {  OPERATIONS_DB } from "../config";

class RefDirection extends BaseModel {
   DirectionIndex: number; 
}

RefDirection.init({
 
    DirectionIndex: {
        type: INTEGER,
        primaryKey: true
    },
    DirectionText: {
        type: STRING
    },
  
}, {
    sequelize,
    modelName: "RefDirection",
    freezeTableName: true,
    tableName: "RefDirection",
    timestamps: false
});


const RefDirectionSchema = new mongoose.Schema({
    DirectionIndex: {
        type:Number
    },
    DirectionText: {
        type:String
    },
}, {
    versionKey: false,
    collection: "RefDirections"
});


const db = mongoose.connection.useDb(OPERATIONS_DB);
const RefDirectionCollection = db.model(
    "RefDirections",
    RefDirectionSchema
);

export { RefDirection, RefDirectionCollection, RefDirectionSchema };
