import { STRING, INTEGER } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import {BaseModel } from "./contracts/base_models";
import {  OPERATIONS_DB } from "../config";

class RefInDisputeReason extends BaseModel {
   InDisputeReasonIndex: number; 
}

RefInDisputeReason.init({
 
    InDisputeReasonIndex: {
        type: INTEGER,
        primaryKey: true
    },
    InDisputeReasonText: {
        type: STRING
    },
  
}, {
    sequelize,
    modelName: "RefInDisputeReason",
    freezeTableName: true,
    tableName: "RefInDisputeReason",
    timestamps: false
});


const RefInDisputeReasonSchema = new mongoose.Schema({
    InDisputeReasonIndex: {
        type: Number,
    },
    InDisputeReasonText: {
        type:String
    },
}, {
    versionKey: false,
    collection: "RefInDisputeReasons"
});


const db = mongoose.connection.useDb(OPERATIONS_DB);
const RefInDisputeReasonCollection = db.model(
    "RefInDisputeReasons",
    RefInDisputeReasonSchema
);

export { RefInDisputeReason, RefInDisputeReasonCollection, RefInDisputeReasonSchema };
