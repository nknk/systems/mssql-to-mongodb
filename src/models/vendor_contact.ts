import { STRING, BOOLEAN, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";

class VendorContact extends ModelWithTenantId {
    ContactTypeId: number;
    VendorLocationId: number;

}

VendorContact.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    VendorLocationId: {
        type: BIGINT
    },
    Name: {
        type: STRING
    },
    Phone: {
        type: STRING
    },
    Mobile: {
        type: STRING
    },
    Fax: {
        type: STRING
    },
    Email: {
        type: STRING
    },
    Comment: {
        type: STRING
    },
    Primary: {
        type: BOOLEAN
    },
    ContactTypeId: {
        type: BIGINT
    }
}, {
    sequelize,
    modelName: "VendorContact",
    freezeTableName: true,
    tableName: "VendorContact",
    timestamps: false
});


const VendorContactSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    VendorLocationId: {
        type: mongoose.Types.ObjectId
    },
    Name: {
        type: String
    },
    Phone: {
        type: String
    },
    Mobile: {
        type: String
    },
    Fax: {
        type: String
    },
    Email: {
        type: String
    },
    Comment: {
        type: String
    },
    Primary: {
        type: Boolean
    },
    ContactTypeId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "VendorContacts"
});


const db = mongoose.connection.useDb(FINANCE_DB);
const VendorContactCollection = db.model(
    "VendorContacts",
    VendorContactSchema
);

export { VendorContact, VendorContactCollection, VendorContactSchema };
