import mongoose, { Document } from "mongoose";

const MigrationSchema = new mongoose.Schema({
    file: {
        type: String
    },
}, {
    versionKey: false,
    collection: "Migrations"
});

export interface IMigrationDoc extends Document {
    file: string;
}

const db = mongoose.connection.useDb("MigrationLogs");
const MigrationCollection = db.model<IMigrationDoc>("Migrations", MigrationSchema);

export { MigrationCollection, MigrationSchema };
