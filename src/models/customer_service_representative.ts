import {
    BIGINT
} from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";

class CustomerServiceRepresentative extends ModelWithTenantId {
    CustomerId: number;
    UserId: number;
}

CustomerServiceRepresentative.init({
    CustomerId: {
        type: BIGINT,
        allowNull: false
    },
    UserId: {
        type: BIGINT,
        allowNull: false
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    }
}, {
    sequelize,
    modelName: "CustomerServiceRepresentative",
    freezeTableName: true,
    tableName: "CustomerServiceRepresentative",
    timestamps: false
});


const CustomerServiceRepresentativeSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    CustomerId: {
        type: mongoose.Types.ObjectId
    },
    UserId: {
        type: mongoose.Types.ObjectId
    },
}, {
    versionKey: false,
    collection: "CustomerServiceRepresentatives"
});

const db = mongoose.connection.useDb(FINANCE_DB);
const CustomerServiceRepresentativeCollection = db
    .model("CustomerServiceRepresentatives",
        CustomerServiceRepresentativeSchema
    );

export {
    CustomerServiceRepresentative,
    CustomerServiceRepresentativeSchema,
    CustomerServiceRepresentativeCollection
};
