import { INTEGER, STRING, DATE, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class ShipmentLocation extends ModelWithTenantId {
    ShipmentId: number;
    CountryId: number;
}

ShipmentLocation.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    Description: {
        type: STRING
    },
    StopOrder: {
        type: INTEGER
    },
    ShipmentId: {
        type: BIGINT
    },
    Street1: {
        type: STRING
    },
    Street2: {
        type: STRING
    },
    City: {
        type: STRING
    },
    State: {
        type: STRING
    },
    CountryId: {
        type: BIGINT
    },
    PostalCode: {
        type: STRING
    },
    SpecialInstructions: {
        type: STRING
    },
    GeneralInfo: {
        type: STRING
    },
    Direction: {
        type: STRING
    },
    AppointmentDateTime: {
        type: DATE
    },
    MilesFromPreceedingStop: {
        type: DECIMAL
    }
}, {
    sequelize,
    modelName: "ShipmentLocation",
    freezeTableName: true,
    tableName: "ShipmentLocation",
    timestamps: false
});


const ShipmentLocationSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    Description: {
        type: String
    },
    StopOrder: {
        type: Number
    },
    ShipmentId: {
        type: mongoose.Types.ObjectId
    },
    Street1: {
        type: String
    },
    Street2: {
        type: String
    },
    City: {
        type: String
    },
    State: {
        type: String
    },
    CountryId: {
        type: mongoose.Types.ObjectId
    },
    PostalCode: {
        type: String
    },
    SpecialInstructions: {
        type: String
    },
    GeneralInfo: {
        type: String
    },
    Direction: {
        type: String
    },
    AppointmentDateTime: {
        type: Date
    },
    MilesFromPreceedingStop: {
        type: Number
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "ShipmentLocations"
});

declare interface IShipmentLocationDoc extends mongoose.Document {
    ShipmentId: string;
}

const db = mongoose.connection.useDb(OPERATIONS_DB);
const ShipmentLocationCollection = db.model<IShipmentLocationDoc>(
    "ShipmentLocations",
    ShipmentLocationSchema
);

export { ShipmentLocation, ShipmentLocationCollection, ShipmentLocationSchema };
