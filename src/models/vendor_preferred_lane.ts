import { STRING, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";

class VendorPreferredLane extends ModelWithTenantId {
    OriginCountryId: number;
    DestinationCountryId: number;
    VendorId: number;
}

VendorPreferredLane.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    OriginPostalCode: {
        type: STRING
    },
    OriginCity: {
        type: STRING
    },
    OriginState: {
        type: STRING
    },
    OriginCountryId: {
        type: BIGINT
    },
    DestinationPostalCode: {
        type: STRING
    },
    DestinationCity: {
        type: STRING
    },
    DestinationState: {
        type: STRING
    },
    DestinationCountryId: {
        type: BIGINT
    },
    VendorId: {
        type: BIGINT
    }    
}, {
    sequelize,
    modelName: "VendorPreferredLane",
    freezeTableName: true,
    tableName: "VendorPreferredLane",
    timestamps: false
});


const VendorPreferredLaneSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    OriginPostalCode: {
        type: String
    },
    OriginCity: {
        type: String
    },
    OriginState: {
        type: String
    },
    OriginCountryId: {
        type: mongoose.Types.ObjectId
    },
    DestinationPostalCode: {
        type: String
    },
    DestinationCity: {
        type: String
    },
    DestinationState: {
        type: String
    },
    DestinationCountryId: {
        type: mongoose.Types.ObjectId
    },
    VendorId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "VendorPreferredLanes"
});


const db = mongoose.connection.useDb(FINANCE_DB);
const VendorPreferredLaneCollection = db.model(
    "VendorPreferredLanes",
    VendorPreferredLaneSchema
);

export { VendorPreferredLane, VendorPreferredLaneCollection, VendorPreferredLaneSchema };
