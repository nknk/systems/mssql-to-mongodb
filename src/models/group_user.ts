import { BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { IDENTITY_DB } from "../config";

class GroupUser extends ModelWithTenantId {
    GroupId: number;
    UserId: number;
}

GroupUser.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    GroupId:{
        type: BIGINT
    },
    UserId:{
        type: BIGINT
    },
}, {
    sequelize,
    modelName: "GroupUser",
    freezeTableName: true,
    tableName: "GroupUser",
    timestamps: false
});


const GroupUserSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    GroupId:{
        type: mongoose.Types.ObjectId
    },
    UserId:{
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "GroupUsers"
});

const db = mongoose.connection.useDb(IDENTITY_DB);
const GroupUserCollection = db.model("GroupUsers", GroupUserSchema);

export { GroupUser, GroupUserCollection, GroupUserSchema };
