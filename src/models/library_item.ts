import { STRING, INTEGER, BOOLEAN, FLOAT, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";
class LibraryItem extends ModelWithTenantId {
    CustomerId: number;
    PackageTypeId: number;
}

LibraryItem.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    Description: {
        type: STRING
    },
    FreightClass: {
        type: FLOAT
    },
    Comment: {
        type: STRING
    },
    Weight: {
        type: DECIMAL
    },
    Length: {
        type: DECIMAL
    },
    Width: {
        type: DECIMAL
    },
    Height: {
        type: DECIMAL
    },
    Quantity: {
        type: INTEGER
    },
    PieceCount: {
        type: INTEGER
    },
    PackageTypeId: {
        type: BIGINT,
    },
    NMFCCode: {
        type: STRING
    },
    HTSCode: {
        type: STRING
    },
    CustomerId: {
        type: BIGINT
    },
    IsStackable: {
        type: BOOLEAN
    },
    HazardousMaterial: {
        type: BOOLEAN
    }
}, {
    sequelize,
    modelName: "LibraryItem",
    freezeTableName: true,
    tableName: "LibraryItem",
    timestamps: false
});


const LibraryItemSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    Description: {
        type: String
    },
    FreightClass: {
        type: Number
    },
    Comment: {
        type: String
    },
    Weight: {
        type: Number
    },
    Length: {
        type: Number
    },
    Width: {
        type: Number
    },
    Height: {
        type: Number
    },
    Quantity: {
        type: Number
    },
    PieceCount: {
        type: Number
    },
    PackageTypeId: {
        type: mongoose.Types.ObjectId
    },
    NMFCCode: {
        type: String
    },
    HTSCode: {
        type: String
    },
    CustomerId: {
        type: mongoose.Types.ObjectId
    },
    IsStackable: {
        type: Boolean
    },
    HazardousMaterial: {
        type: Boolean
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "LibraryItems"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const LibraryItemCollection = db.model("LibraryItems", LibraryItemSchema);

export { LibraryItem, LibraryItemCollection, LibraryItemSchema };
