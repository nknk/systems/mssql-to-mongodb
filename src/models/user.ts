import { STRING, INTEGER, BOOLEAN, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { IDENTITY_DB } from "../config";
import { UserPermission, UserPermissionSchema } from "./user_permission";

class User extends ModelWithTenantId {
    CountryId: number;
    CustomerId: number;
    UserDepartmentId: number;
    VendorPortalVendorId: number;
 }

User.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    Username: {
        type: STRING,
        allowNull: false,
    },
    Password: {
        type: STRING,
        allowNull: false,
    },
    FirstName: {
        type: STRING,
        allowNull: false,
    },
    LastName: {
        type: STRING,
        allowNull: false,
    },
    TenantEmployee: {
        type: BOOLEAN,
        allowNull: false,
    },
    Phone: {
        type: STRING,
        allowNull: false,
    },
    Mobile: {
        type: STRING,
        allowNull: false,
    },
    Fax: {
        type: STRING,
        allowNull: false,
    },
    Email: {
        type: STRING,
        allowNull: false,
    },
    Street1: {
        type: STRING,
        allowNull: false,
    },
    Street2: {
        type: STRING,
        allowNull: false,
    },
    City: {
        type: STRING,
        allowNull: false,
    },
    State: {
        type: STRING,
        allowNull: false,
    },
    CountryId: {
        type: BIGINT,
        allowNull: false,
    },
    PostalCode: {
        type: STRING,
        allowNull: false,
    },
    Enabled: {
        type: BOOLEAN,
        allowNull: false,
    },
    FailedLoginAttempts: {
        type: INTEGER,
        allowNull: false,
    },
    ForcePasswordReset: {
        type: BOOLEAN,
        allowNull: false,
    },
    CustomerId: {
        type: BIGINT,
        allowNull: false,
    },
    AlwaysShowRatingNotice: {
        type: BOOLEAN,
        allowNull: false,
    },
    UserDepartmentId: {
        type: BIGINT,
        allowNull: false,
    },
    IsCarrierCoordinator: {
        type: BOOLEAN,
        allowNull: false,
    },
    IsShipmentCoordinator: {
        type: BOOLEAN,
        allowNull: false,
    },
    AlwaysShowFinderParametersOnSearch: {
        type: BOOLEAN,
        allowNull: false,
    },
    QlikUserId: {
        type: STRING,
        allowNull: false,
    },
    QlikUserDirectory: {
        type: STRING,
        allowNull: false,
    },
    DatLoadboardUsername: {
        type: STRING,
        allowNull: false,
    },
    DatLoadboardPassword: {
        type: STRING,
        allowNull: false,
    },
    AdUserName: {
        type: STRING,
        allowNull: false,
    },
    VendorPortalVendorId: {
        type: BIGINT,
        allowNull: false,
    },
    CanSeeAllVendorsInVendorPortal: {
        type: BOOLEAN,
        allowNull: false,
    },
    LastChangeLogVersionViewed: {
        type: STRING,
        allowNull: false,
    },
}, {
    sequelize,
    modelName: "User",
    freezeTableName: true,
    tableName: "User",
    timestamps: false
});


User.hasMany(UserPermission);
UserPermission.belongsTo(User);


const UserSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    Username: {
        type: String,
    },
    Password: {
        type: String,
    },
    FirstName: {
        type: String,
    },
    LastName: {
        type: String,
    },
    TenantEmployee: {
        type: Boolean,
    },
    Phone: {
        type: String,
    },
    Mobile: {
        type: String,
    },
    Fax: {
        type: String,
    },
    Email: {
        type: String,
    },
    Street1: {
        type: String,
    },
    Street2: {
        type: String,
    },
    City: {
        type: String,
    },
    State: {
        type: String,
    },
    CountryId: {
        type: mongoose.Types.ObjectId,
    },
    PostalCode: {
        type: String,
    },
    Enabled: {
        type: Boolean,
    },
    FailedLoginAttempts: {
        type: Number,
    },
    ForcePasswordReset: {
        type: Boolean,
    },
    CustomerId: {
        type: mongoose.Types.ObjectId,
    },
    AlwaysShowRatingNotice: {
        type: Boolean,
    },
    UserDepartmentId: {
        type: mongoose.Types.ObjectId,
    },
    IsCarrierCoordinator: {
        type: Boolean,
    },
    IsShipmentCoordinator: {
        type: Boolean,
    },
    AlwaysShowFinderParametersOnSearch: {
        type: Boolean,
    },
    QlikUserId: {
        type: String,
    },
    QlikUserDirectory: {
        type: String,
    },
    DatLoadboardUsername: {
        type: String,
    },
    DatLoadboardPassword: {
        type: String,
    },
    AdUserName: {
        type: String,
    },
    VendorPortalVendorId: {
        type: mongoose.SchemaTypes.Mixed,
    },
    CanSeeAllVendorsInVendorPortal: {
        type: Boolean,
    },
    LastChangeLogVersionViewed: {
        type: String,
    },
    OldId: {
        type: Number
    },
    UserPermissions: [UserPermissionSchema]
}, {
    versionKey: false,
    collection: "Users"
});

export interface IUserDocument extends mongoose.Document {
    VendorPortalVendorId: string;
}

const db = mongoose.connection.useDb(IDENTITY_DB);
const UserCollection = db.model<IUserDocument>("Users", UserSchema);

export { User, UserCollection, UserSchema };
