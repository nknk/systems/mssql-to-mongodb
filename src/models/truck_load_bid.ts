import { INTEGER, DATE, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { RATING_DB } from "../config";

class TruckloadBid extends ModelWithTenantId {
    LoadOrderId: number;
    TLTenderingProfileId: number;
    TLLaneId: number;
    FirstPreferredVendorId: number;
    SecondPreferredVendorId: number;
    ThirdPreferredVendorId: number;
    UserId: number;
}

TruckloadBid.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    LoadOrderId: {
        type: BIGINT
    },
    TLTenderingProfileId: {
        type: BIGINT
    },
    TLLaneId: {
        type: BIGINT
    },
    FirstPreferredVendorId: {
        type: BIGINT
    },
    SecondPreferredVendorId: {
        type: BIGINT
    },
    ThirdPreferredVendorId: {
        type: BIGINT
    },
    DateCreated: {
        type: DATE
    },
    TimeLoadTendered1: {
        type: DATE,
    },
    TimeLoadTendered2: {
        type: DATE
    },
    TimeLoadTendered3: {
        type: DATE
    },
    ExpirationTime1: {
        type: DATE
    },
    ExpirationTime2: {
        type: DATE
    },
    ExpirationTime3: {
        type: DATE
    },
    UserId: {
        type: BIGINT
    },
    BidStatus: {
        type: INTEGER
    }
}, {
    sequelize,
    modelName: "TruckloadBid",
    freezeTableName: true,
    tableName: "TruckloadBid",
    timestamps: false
});


const TruckloadBidSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    LoadOrderId: {
        type: mongoose.Types.ObjectId
    },
    TLTenderingProfileId: {
        type: mongoose.Types.ObjectId
    },
    TLLaneId: {
        type: mongoose.Types.ObjectId
    },
    FirstPreferredVendorId: {
        type: mongoose.Types.ObjectId
    },
    SecondPreferredVendorId: {
        type: mongoose.Types.ObjectId
    },
    ThirdPreferredVendorId: {
        type: mongoose.Types.ObjectId
    },
    DateCreated: {
        type: Date
    },
    TimeLoadTendered1: {
        type: Date,
    },
    TimeLoadTendered2: {
        type: Date
    },
    TimeLoadTendered3: {
        type: Date
    },
    ExpirationTime1: {
        type: Date
    },
    ExpirationTime2: {
        type: Date
    },
    ExpirationTime3: {
        type: Date
    },
    UserId: {
        type: mongoose.Types.ObjectId
    },
    BidStatus: {
        type: Number
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "TruckloadBids"
});

const db = mongoose.connection.useDb(RATING_DB);
const TruckloadBidCollection = db.model("TruckloadBids", TruckloadBidSchema);

export { TruckloadBid, TruckloadBidCollection, TruckloadBidSchema };
