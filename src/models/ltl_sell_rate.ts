import { INTEGER, DATE, FLOAT, BOOLEAN, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { RATING_DB } from "../config";

class LTLSellRate extends ModelWithTenantId {
    CustomerRatingId: number;
    VendorRatingId: number;
}

LTLSellRate.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    LTLIndirectPointEnabled: {
        type: BOOLEAN,
    },
    EffectiveDate: {
        type: DATE,
    },
    Active: {
        type: BOOLEAN,
    },
    MarkupPercent: {
        type: FLOAT,
    },
    MarkupValue: {
        type: FLOAT,
    },
    UseMinimum: {
        type: BOOLEAN,
    },
    CustomerRatingId: {
        type: BIGINT,
    },
    VendorRatingId: {
        type: BIGINT,
    },
    StartOverrideWeightBreak: {
        type: INTEGER,
    },
    OverrideMarkupPercentL5C: {
        type: DECIMAL,
    },
    OverrideMarkupValueL5C: {
        type: DECIMAL,
    },
    OverrideMarkupPercentM5C: {
        type: DECIMAL,
    },
    OverrideMarkupValueM5C: {
        type: DECIMAL,
    },
    OverrideMarkupPercentM1M: {
        type: DECIMAL,
    },
    OverrideMarkupValueM1M: {
        type: DECIMAL,
    },
    OverrideMarkupPercentM2M: {
        type: DECIMAL,
    },
    OverrideMarkupValueM2M: {
        type: DECIMAL,
    },
    OverrideMarkupPercentM5M: {
        type: DECIMAL,
    },
    OverrideMarkupValueM5M: {
        type: DECIMAL,
    },
    OverrideMarkupPercentM10M: {
        type: DECIMAL,
    },
    OverrideMarkupValueM10M: {
        type: DECIMAL,
    },
    OverrideMarkupPercentM20M: {
        type: DECIMAL,
    },
    OverrideMarkupValueM20M: {
        type: DECIMAL,
    },
    OverrideMarkupPercentM30M: {
        type: DECIMAL,
    },
    OverrideMarkupValueM30M: {
        type: DECIMAL,
    },
    OverrideMarkupPercentM40M: {
        type: DECIMAL,
    },
    OverrideMarkupValueM40M: {
        type: DECIMAL,
    },
    OverrideMarkupPercentVendorFloor: {
        type: DECIMAL,
    },
    OverrideMarkupValueVendorFloor: {
        type: DECIMAL,
    },
    OverrideMarkupVendorFloorEnabled: {
        type: BOOLEAN,
    },
}, {
    sequelize,
    modelName: "LTLSellRate",
    freezeTableName: true,
    tableName: "LTLSellRate",
    timestamps: false
});


const LTLSellRateSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    },
    LTLIndirectPointEnabled: {
        type: Boolean
    },
    EffectiveDate: {
        type: Date
    },
    Active: {
        type: Boolean
    },
    MarkupPercent: {
        type: Number
    },
    MarkupValue: {
        type: Number
    },
    UseMinimum: {
        type: Boolean
    },
    CustomerRatingId: {
        type: mongoose.Types.ObjectId
    },
    VendorRatingId: {
        type: mongoose.Types.ObjectId
    },
    StartOverrideWeightBreak: {
        type: Number
    },
    OverrideMarkupPercentL5C: {
        type: Number
    },
    OverrideMarkupValueL5C: {
        type: Number
    },
    OverrideMarkupPercentM5C: {
        type: Number
    },
    OverrideMarkupValueM5C: {
        type: Number
    },
    OverrideMarkupPercentM1M: {
        type: Number
    },
    OverrideMarkupValueM1M: {
        type: Number
    },
    OverrideMarkupPercentM2M: {
        type: Number
    },
    OverrideMarkupValueM2M: {
        type: Number
    },
    OverrideMarkupPercentM5M: {
        type: Number
    },
    OverrideMarkupValueM5M: {
        type: Number
    },
    OverrideMarkupPercentM10M: {
        type: Number
    },
    OverrideMarkupValueM10M: {
        type: Number
    },
    OverrideMarkupPercentM20M: {
        type: Number
    },
    OverrideMarkupValueM20M: {
        type: Number
    },
    OverrideMarkupPercentM30M: {
        type: Number
    },
    OverrideMarkupValueM30M: {
        type: Number
    },
    OverrideMarkupPercentM40M: {
        type: Number
    },
    OverrideMarkupValueM40M: {
        type: Number
    },
    OverrideMarkupPercentVendorFloor: {
        type: Number
    },
    OverrideMarkupValueVendorFloor: {
        type: Number
    },
    OverrideMarkupVendorFloorEnabled: {
        type: Boolean
    },
}, {
    versionKey: false,
    collection: "LTLSellRates"
});

const db = mongoose.connection.useDb(RATING_DB);
const LTLSellRateCollection = db.model("LTLSellRates", LTLSellRateSchema);

export {
    LTLSellRate,
    LTLSellRateCollection,
    LTLSellRateSchema
};
