import { STRING, BOOLEAN, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class ShipmentReference extends ModelWithTenantId {
    ShipmentId: number;
}

ShipmentReference.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    ShipmentId: {
        type: BIGINT
    },
    Name: {
        type: STRING
    },
    Value: {
        type: STRING
    },
    DisplayOnOrigin: {
        type: BOOLEAN
    },
    DisplayOnDestination: {
        type: BOOLEAN
    },
    Required: {
        type: BOOLEAN
    }    
}, {
    sequelize,
    modelName: "ShipmentReference",
    freezeTableName: true,
    tableName: "ShipmentReference",
    timestamps: false
});


const ShipmentReferenceSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    ShipmentId: {
        type: mongoose.Types.ObjectId
    },
    Name: {
        type: String
    },
    Value: {
        type: String
    },
    DisplayOnOrigin: {
        type: Boolean
    },
    DisplayOnDestination: {
        type: Boolean
    },
    Required: {
        type: Boolean
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "ShipmentReferences"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const ShipmentReferenceCollection = db.model("ShipmentReferences", ShipmentReferenceSchema);

export { ShipmentReference, ShipmentReferenceCollection, ShipmentReferenceSchema };
