import { STRING, INTEGER } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { BaseModel } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class RefInvoiceDetailReferenceType extends BaseModel {
    InvoiceDetailReferenceTypeIndex: number;
}

RefInvoiceDetailReferenceType.init({

    InvoiceDetailReferenceTypeIndex: {
        type: INTEGER,
        primaryKey: true
    },
    InvoiceDetailReferenceTypeText: {
        type: STRING
    },

}, {
    sequelize,
    modelName: "RefInvoiceDetailReferenceType",
    freezeTableName: true,
    tableName: "RefInvoiceDetailReferenceType",
    timestamps: false
});


const RefInvoiceDetailReferenceTypeSchema = new mongoose.Schema({
    InvoiceDetailReferenceTypeIndex: {
        type: Number,
    },
    InvoiceDetailReferenceTypeText: {
        type: String
    },
}, {
    versionKey: false,
    collection: "RefInvoiceDetailReferenceTypes"
});


const db = mongoose.connection.useDb(OPERATIONS_DB);
const RefInvoiceDetailReferenceTypeCollection = db.model(
    "RefInvoiceDetailReferenceTypes",
    RefInvoiceDetailReferenceTypeSchema
);

export {
    RefInvoiceDetailReferenceType,
    RefInvoiceDetailReferenceTypeCollection,
    RefInvoiceDetailReferenceTypeSchema
};
