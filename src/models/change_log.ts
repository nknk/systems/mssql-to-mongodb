import { STRING, BOOLEAN, INTEGER, DATE, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { REGISTRY_DB } from "../config";

class ChangeLog extends ModelWithTenantId {
    UserId: number;
}

ChangeLog.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    Type: {
        type: INTEGER,
        allowNull: false,
    },
    Title: {
        type: STRING,
        allowNull: false,
    },
    Text: {
        type: STRING,
        allowNull: false,
    },
    DateCreated: {
        type: DATE,
        allowNull: false,
    },
    UserId: {
        type: BIGINT,
        allowNull: false,
    },
    AppVersion: {
        type: STRING,
        allowNull: false,
    },
    Archived: {
        type: BOOLEAN,
        allowNull: false,
    },
    ReleaseDate: {
        type: DATE,
        allowNull: false,
    }
}, {
    sequelize,
    modelName: "ChangeLog",
    freezeTableName: true,
    tableName: "ChangeLog",
    timestamps: false
});


const ChangeLogSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    Type: {
        type: Number
    },
    Title: {
        type: String
    },
    Text: {
        type: String
    },
    DateCreated: {
        type: Date
    },
    UserId: {
        type: mongoose.Types.ObjectId
    },
    AppVersion: {
        type: String
    },
    Archived: {
        type: Boolean
    },
    ReleaseDate: {
        type: Date
    },
    OldId: {
        type: Number
    },
}, {
    versionKey: false,
    collection: "ChangeLogs"
});

const db = mongoose.connection.useDb(REGISTRY_DB);
const ChangeLogCollection = db.model("ChangeLogs", ChangeLogSchema);

export { ChangeLog, ChangeLogCollection, ChangeLogSchema };
