import { STRING, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";

class CustomerControlAccount extends ModelWithTenantId {
    CountryId: number;
    CustomerId: number;
}

CustomerControlAccount.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    CustomerId: {
        type: BIGINT,
        allowNull: false
    },
    Street1: {
        type: STRING,
        allowNull: false
    },
    Street2: {
        type: STRING,
        allowNull: false
    },
    City: {
        type: STRING,
        allowNull: false
    },
    State: {
        type: STRING,
        allowNull: false
    },
    CountryId: {
        type: BIGINT,
        allowNull: false
    },
    PostalCode: {
        type: STRING,
        allowNull: false
    },
    Description: {
        type: STRING,
        allowNull: false
    },
    AccountNumber: {
        type: STRING,
        allowNull: false
    },
    GenericCategory: {
        type: STRING,
        allowNull: false
    },
}, {
    sequelize,
    modelName: "CustomerControlAccount",
    freezeTableName: true,
    tableName: "CustomerControlAccount",
    timestamps: false
});


const CustomerControlAccountSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    },
    CustomerId: {
        type: mongoose.Types.ObjectId
    },
    Street1: {
        type: String
    },
    Street2: {
        type: String
    },
    City: {
        type: String
    },
    State: {
        type: String
    },
    CountryId: {
        type: mongoose.Types.ObjectId
    },
    PostalCode: {
        type: String
    },
    Description: {
        type: String
    },
    AccountNumber: {
        type: String
    },
    GenericCategory: {
        type: String
    },
}, {
    versionKey: false,
    collection: "CustomerControlAccounts"
});

const db = mongoose.connection.useDb(FINANCE_DB);
const CustomerControlAccountCollection = db.model("CustomerControlAccounts", CustomerControlAccountSchema);

export { CustomerControlAccount, CustomerControlAccountCollection, CustomerControlAccountSchema };
