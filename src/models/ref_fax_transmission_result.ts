import { STRING, INTEGER } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import {BaseModel } from "./contracts/base_models";
import {  OPERATIONS_DB } from "../config";

class RefFaxTransmissionResult extends BaseModel {
   FaxTransmissionResultIndex: number; 
}

RefFaxTransmissionResult.init({
 
    FaxTransmissionResultIndex: {
        type: INTEGER,
        primaryKey: true
    },
    FaxTransmissionResultText: {
        type: STRING
    },
  
}, {
    sequelize,
    modelName: "RefFaxTransmissionResult",
    freezeTableName: true,
    tableName: "RefFaxTransmissionResult",
    timestamps: false
});


const RefFaxTransmissionResultSchema = new mongoose.Schema({
    FaxTransmissionResultIndex: {
        type:Number
    },
    FaxTransmissionResultText: {
        type:String
    },
}, {
    versionKey: false,
    collection: "RefFaxTransmissionResults"
});


const db = mongoose.connection.useDb(OPERATIONS_DB);
const RefFaxTransmissionResultCollection = db.model(
    "RefFaxTransmissionResults",
    RefFaxTransmissionResultSchema
);

export { RefFaxTransmissionResult, RefFaxTransmissionResultCollection, RefFaxTransmissionResultSchema };
