import { STRING, BIGINT, BOOLEAN, DATE } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";
class Tier extends ModelWithTenantId { }

Tier.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    Name: {
        type: STRING,
        allowNull: false
    },
    LogoUrl: {
        type: STRING,
        allowNull: false
    },
    TierNumber: {
        type: STRING,
        allowNull: false
    },
    AdditionalBillOfLadingText: {
        type: STRING,
        allowNull: false
    },
    GeneralNotificationEmails: {
        type: STRING,
        allowNull: false
    },
    TollFreeContactNumber: {
        type: STRING,
        allowNull: false
    },
    DateCreated: {
        type: DATE,
        allowNull: false
    },
    Active: {
        type: BOOLEAN,
        allowNull: false
    },
    SupportTollFree: {
        type: STRING,
        allowNull: false
    },
    TierSupportEmails: {
        type: STRING,
        allowNull: false
    },
    AirNotificationEmails: {
        type: STRING,
        allowNull: false
    },
    RailNotificationEmails: {
        type: STRING,
        allowNull: false
    },
    SPNotificationEmails: {
        type: STRING,
        allowNull: false
    },
    FTLNotificationEmails: {
        type: STRING,
        allowNull: false
    },
    LTLNotificationEmails: {
        type: STRING,
        allowNull: false
    },
    PendingVendorNotificationEmails: {
        type: STRING,
        allowNull: false
    },
    AccountPayableDisputeNotificationEmails: {
        type: STRING,
        allowNull: false
    },
}, {
    sequelize,
    modelName: "Tier",
    freezeTableName: true,
    tableName: "Tier",
    timestamps: false
});


const TierSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    Name: {
        type: String,
    },
    LogoUrl: {
        type: String,
    },
    TierNumber: {
        type: String,
    },
    AdditionalBillOfLadingText: {
        type: String,
    },
    GeneralNotificationEmails: {
        type: String,
    },
    TollFreeContactNumber: {
        type: String,
    },
    DateCreated: {
        type: Date,
    },
    SupportTollFree: {
        type: String,
    },
    Active: {
        type: Boolean,
    },
    TierSupportEmails: {
        type: String,
    },
    AirNotificationEmails: {
        type: String,
    },
    RailNotificationEmails: {
        type: String,
    },
    SPNotificationEmails: {
        type: String,
    },
    FTLNotificationEmails: {
        type: String,
    },
    LTLNotificationEmails: {
        type: String,
    },
    PendingVendorNotificationEmails: {
        type: String,
    },
    AccountPayableDisputeNotificationEmails: {
        type: String,
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "Tiers"
});

const db = mongoose.connection.useDb(FINANCE_DB);
const TierCollection = db.model("Tiers", TierSchema);

export { Tier, TierCollection, TierSchema };
