import { BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";
class LoadOrderEquipment extends ModelWithTenantId {
    LoadOrderId: number;
    EquipmentTypeId: number;
}

LoadOrderEquipment.init({
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    EquipmentTypeId:{
        type: BIGINT
    },
    LoadOrderId:{
        type: BIGINT
    },
}, {
    sequelize,
    modelName: "LoadOrderEquipment",
    freezeTableName: true,
    tableName: "LoadOrderEquipment",
    timestamps: false
});

const LoadOrderEquipmentSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    EquipmentTypeId:{
        type: mongoose.Types.ObjectId
    },
    LoadOrderId:{
        type: mongoose.Types.ObjectId
    },
}, {
    versionKey: false,
    collection: "LoadOrderEquipments"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const LoadOrderEquipmentCollection = db.model("LoadOrderEquipments", LoadOrderEquipmentSchema);

export { LoadOrderEquipment, LoadOrderEquipmentCollection, LoadOrderEquipmentSchema };
