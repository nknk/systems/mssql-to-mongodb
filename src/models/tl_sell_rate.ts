import { INTEGER, STRING, FLOAT, DATE, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { RATING_DB } from "../config";

class TLSellRate extends ModelWithTenantId {
    EquipmentTypeId: number;
    OriginCountryId: number;
    DestinationCountryId: number;
    CustomerRatingId: number;
    MileageSourceId: number;
}

TLSellRate.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    EquipmentTypeId: {
        type: BIGINT
    },
    RateType: {
        type: INTEGER
    },
    Rate: {
        type: DECIMAL
    },
    MinimumCharge: {
        type: DECIMAL
    },
    MinimumWeight: {
        type: DECIMAL
    },
    MaximumWeight: {
        type: DECIMAL
    },
    TariffType: {
        type: INTEGER
    },
    EffectiveDate: {
        type: DATE,
    },
    OriginCity: {
        type: STRING
    },
    OriginState: {
        type: STRING
    },
    OriginCountryId: {
        type: BIGINT
    },
    OriginPostalCode: {
        type: STRING
    },
    DestinationCity: {
        type: STRING
    },
    DestinationState: {
        type: STRING
    },
    DestinationCountryId: {
        type: BIGINT
    },
    DestinationPostalCode: {
        type: STRING
    },
    CustomerRatingId: {
        type: BIGINT
    },
    Mileage: {
        type: FLOAT,
    },
    MileageSourceId: {
        type: BIGINT
    }
}, {
    sequelize,
    modelName: "TLSellRate",
    freezeTableName: true,
    tableName: "TLSellRate",
    timestamps: false
});


const TLSellRateSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    EquipmentTypeId: {
        type: mongoose.Types.ObjectId
    },
    RateType: {
        type: Number
    },
    Rate: {
        type: Number
    },
    MinimumCharge: {
        type: Number
    },
    MinimumWeight: {
        type: Number
    },
    MaximumWeight: {
        type: Number
    },
    TariffType: {
        type: Number
    },
    EffectiveDate: {
        type: Date,
    },
    OriginCity: {
        type: String
    },
    OriginState: {
        type: String
    },
    OriginCountryId: {
        type: mongoose.Types.ObjectId
    },
    OriginPostalCode: {
        type: String
    },
    DestinationCity: {
        type: String
    },
    DestinationState: {
        type: String
    },
    DestinationCountryId: {
        type: mongoose.Types.ObjectId
    },
    DestinationPostalCode: {
        type: String
    },
    CustomerRatingId: {
        type: mongoose.Types.ObjectId
    },
    Mileage: {
        type: Number,
    },
    MileageSourceId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "TLSellRates"
});

const db = mongoose.connection.useDb(RATING_DB);
const TLSellRateCollection = db.model("TLSellRates", TLSellRateSchema);

export { TLSellRate, TLSellRateCollection, TLSellRateSchema };
