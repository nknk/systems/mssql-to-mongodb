import { STRING, INTEGER, BOOLEAN, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { RATING_DB } from "../config";

class ResellerAddition extends ModelWithTenantId {
    ResellerCustomerAccountId: number;
}

ResellerAddition.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    Name: {
        type: STRING,
        allowNull: false,
    },
    LineHaulType: {
        type: INTEGER,
        allowNull: false,
    },
    LineHaulValue: {
        type: DECIMAL,
        allowNull: false,
    },
    LineHaulPercentage: {
        type: DECIMAL,
        allowNull: false,
    },
    UseLineHaulMinimum: {
        type: BOOLEAN,
        allowNull: false,
    },
    FuelType: {
        type: INTEGER,
        allowNull: false,
    },
    FuelValue: {
        type: DECIMAL,
        allowNull: false,
    },
    FuelPercentage: {
        type: DECIMAL,
        allowNull: false,
    },
    UseFuelMinimum: {
        type: BOOLEAN,
        allowNull: false,
    },
    AccessorialType: {
        type: INTEGER,
        allowNull: false,
    },
    AccessorialValue: {
        type: DECIMAL,
        allowNull: false,
    },
    AccessorialPercentage: {
        type: DECIMAL,
        allowNull: false,
    },
    UseAccessorialMinimum: {
        type: BOOLEAN,
        allowNull: false,
    },
    ServiceType: {
        type: INTEGER,
        allowNull: false,
    },
    ServiceValue: {
        type: DECIMAL,
        allowNull: false,
    },
    ServicePercentage: {
        type: DECIMAL,
        allowNull: false,
    },
    UseServiceMinimum: {
        type: BOOLEAN,
        allowNull: false,
    },
    ResellerCustomerAccountId: {
        type: BIGINT,
        allowNull: false,
    },
}, {
    sequelize,
    modelName: "ResellerAddition",
    freezeTableName: true,
    tableName: "ResellerAddition",
    timestamps: false
});


const ResellerAdditionSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    Name: {
        type: String,
    },
    LineHaulType: {
        type: Number,
    },
    LineHaulValue: {
        type: Number,
    },
    LineHaulPercentage: {
        type: Number,
    },
    UseLineHaulMinimum: {
        type: Boolean,
    },
    FuelType: {
        type: Number,
    },
    FuelValue: {
        type: Number,
    },
    FuelPercentage: {
        type: Number,
    },
    UseFuelMinimum: {
        type: Boolean,
    },
    AccessorialType: {
        type: Number,
    },
    AccessorialValue: {
        type: Number,
    },
    AccessorialPercentage: {
        type: Number,
    },
    UseAccessorialMinimum: {
        type: Boolean,
    },
    ServiceType: {
        type: Number,
    },
    ServiceValue: {
        type: Number,
    },
    ServicePercentage: {
        type: Number,
    },
    UseServiceMinimum: {
        type: Boolean,
    },
    ResellerCustomerAccountId: {
        type: mongoose.Types.ObjectId,
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "ResellerAdditions"
});

const db = mongoose.connection.useDb(RATING_DB);
const ResellerAdditionCollection = db.model("ResellerAdditions", ResellerAdditionSchema);

export {
    ResellerAddition,
    ResellerAdditionCollection,
    ResellerAdditionSchema
};
