import { STRING, BOOLEAN,  DATE, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";

class VendorInsurance extends ModelWithTenantId {
    VendorId: number;
    InsuranceTypeId: number;

}

VendorInsurance.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    CarrierName: {
        type: STRING
    },
    Required: {
        type: BOOLEAN
    },
    PolicyNumber: {
        type: STRING
    },
    EffectiveDate: {
        type: DATE
    },
    ExpirationDate: {
        type: DATE
    },
    CertificateHolder: {
        type: STRING
    },
    CoverageAmount: {
        type: DECIMAL
    },
    SpecialInstruction: {
        type: STRING
    },
    InsuranceTypeId: {
        type: BIGINT
    },
    VendorId: {
        type: BIGINT
    }
}, {
    sequelize,
    modelName: "VendorInsurance",
    freezeTableName: true,
    tableName: "VendorInsurance",
    timestamps: false
});


const VendorInsuranceSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    CarrierName: {
        type: String
    },
    Required: {
        type: Boolean
    },
    PolicyNumber: {
        type: String
    },
    EffectiveDate: {
        type: Date
    },
    ExpirationDate: {
        type: Date
    },
    CertificateHolder: {
        type: String
    },
    CoverageAmount: {
        type: Number
    },
    SpecialInstruction: {
        type: String
    },
    InsuranceTypeId: {
        type: mongoose.Types.ObjectId
    },
    VendorId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "VendorInsurances"
});


const db = mongoose.connection.useDb(FINANCE_DB);
const VendorInsuranceCollection = db.model(
    "VendorInsurances",
    VendorInsuranceSchema
);

export { VendorInsurance, VendorInsuranceCollection, VendorInsuranceSchema };
