import { STRING, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";
class CustomerGroup extends ModelWithTenantId {
}

CustomerGroup.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: STRING,
        allowNull: false
    },
    Name:{
        type: STRING    
    },
    Description:{
        type: STRING  
    }         
}, {
    sequelize,
    modelName: "CustomerGroup",
    freezeTableName: true,
    tableName: "CustomerGroup",
    timestamps: false
});


const CustomerGroupSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    Name:{
        type: String    
    },
    Description:{
        type: String  
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "CustomerGroups"
});


const db = mongoose.connection.useDb(FINANCE_DB);
const CustomerGroupCollection = db.model("CustomerGroups", CustomerGroupSchema);

export {
    CustomerGroup,
    CustomerGroupCollection,
    CustomerGroupSchema
};
