import { BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { REPORTING_DB } from "../config";

class ReportCustomizationGroup extends ModelWithTenantId {

    ReportConfigurationId: number;
    GroupId: number;
}

ReportCustomizationGroup.init({
    TenantId: {
        type: BIGINT,
    },
    ReportConfigurationId: {
        type: BIGINT,
    },
    GroupId: {
        type: BIGINT,
    },
}, {
    sequelize,
    modelName: "ReportCustomizationGroup",
    freezeTableName: true,
    tableName: "ReportCustomizationGroup",
    timestamps: false
});


const ReportCustomizationGroupSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId,
    },
    ReportConfigurationId: {
        type: mongoose.Types.ObjectId,
    },
    GroupId: {
        type: mongoose.Types.ObjectId,
    },
}, {
    versionKey: false,
    collection: "ReportCustomizationGroups"
});

const db = mongoose.connection.useDb(REPORTING_DB);
const ReportCustomizationGroupCollection = db.model("ReportCustomizationGroups", ReportCustomizationGroupSchema);

export { ReportCustomizationGroup, ReportCustomizationGroupCollection, ReportCustomizationGroupSchema };
