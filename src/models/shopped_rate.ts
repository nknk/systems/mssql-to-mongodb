import { INTEGER, STRING, FLOAT, DATE, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class ShoppedRate extends ModelWithTenantId {
    LoadOrderId: number;
    ShipmentId: number;
    CustomerId: number;
    UserId: number;
    OriginCountryId: number;
    DestinationCountryId: number;
}

ShoppedRate.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    ShipmentId: {
        type: BIGINT
    },
    LoadOrderId: {
        type: BIGINT
    },
    ReferenceNumber: {
        type: STRING
    },
    Type: {
        type: INTEGER
    },
    NumberLineItems: {
        type: INTEGER
    },
    Quantity: {
        type: INTEGER
    },
    Weight: {
        type: DECIMAL,
    },
    HighestFreightClass: {
        type: FLOAT
    },
    MostSignificantFreightClass: {
        type: FLOAT
    },
    LowestTotalCharge: {
        type: DECIMAL
    },
    DateCreated: {
        type: DATE
    },
    CustomerId: {
        type: BIGINT
    },
    UserId: {
        type: BIGINT
    },
    OriginCity: {
        type: STRING
    },
    OriginState: {
        type: STRING
    },
    OriginPostalCode: {
        type: STRING
    },
    DestinationCity: {
        type: STRING
    },
    DestinationState: {
        type: STRING
    },
    DestinationPostalCode: {
        type: STRING
    },
    OriginCountryId: {
        type: BIGINT
    },
    DestinationCountryId: {
        type: BIGINT
    },
    VendorName: {
        type: STRING
    },
    VendorNumber: {
        type: STRING
    }      
}, {
    sequelize,
    modelName: "ShoppedRate",
    freezeTableName: true,
    tableName: "ShoppedRate",
    timestamps: false
});


const ShoppedRateSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    ShipmentId: {
        type: mongoose.Types.ObjectId
    },
    LoadOrderId: {
        type: mongoose.Types.ObjectId
    },
    ReferenceNumber: {
        type: String
    },
    Type: {
        type: Number
    },
    NumberLineItems: {
        type: Number
    },
    Quantity: {
        type: Number
    },
    Weight: {
        type: Number
    },
    HighestFreightClass: {
        type: Number
    },
    MostSignificantFreightClass: {
        type: Number
    },
    LowestTotalCharge: {
        type: Number
    },
    DateCreated: {
        type: Date
    },
    CustomerId: {
        type: mongoose.Types.ObjectId
    },
    UserId: {
        type: mongoose.Types.ObjectId
    },
    OriginCity: {
        type: String
    },
    OriginState: {
        type: String
    },
    OriginPostalCode: {
        type: String
    },
    DestinationCity: {
        type: String
    },
    DestinationState: {
        type: String
    },
    DestinationPostalCode: {
        type: String
    },
    OriginCountryId: {
        type: mongoose.Types.ObjectId
    },
    DestinationCountryId: {
        type: mongoose.Types.ObjectId
    },
    VendorName: {
        type: String
    },
    VendorNumber: {
        type: String
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "ShoppedRates"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const ShoppedRateCollection = db.model("ShoppedRates", ShoppedRateSchema);

export { ShoppedRate, ShoppedRateCollection, ShoppedRateSchema };
