import {  BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";

class VendorService extends ModelWithTenantId {
    VendorId: number;
    ServiceId: number;
}

VendorService.init({
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    ServiceId: {
        type: BIGINT,
        allowNull: false
    },
    VendorId: {
        type: BIGINT,
        allowNull: false
    }
}, {
    sequelize,
    modelName: "VendorService",
    freezeTableName: true,
    tableName: "VendorService",
    timestamps: false
});


const VendorServiceSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    ServiceId: {
        type: mongoose.Types.ObjectId
    },
    VendorId: {
        type: mongoose.Types.ObjectId
    }
}, {
    versionKey: false,
    collection: "VendorServices"
});

const db = mongoose.connection.useDb(FINANCE_DB);
const VendorServiceCollection = db
    .model("VendorServices", VendorServiceSchema);

export {
    VendorService,
    VendorServiceCollection,
    VendorServiceSchema
};
