import { STRING, DATE, BOOLEAN, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";
class CustomerCommunication extends ModelWithTenantId {
    CustomerId: number;
}

CustomerCommunication.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    ConnectGuid: {
        type: STRING
    },
    EnableWebServiceAPIAccess: {
        type: BOOLEAN
    },
    CustomerId: {
        type: BIGINT
    },
    EdiEnabled: {
        type: BOOLEAN
    },
    EdiCode: {
        type: STRING
    },
    EdiVANUrl: {
        type: STRING
    },
    SecureEdiVAN: {
        type: BOOLEAN
    },
    EdiVANUsername: {
        type: STRING
    },
    EdiVANPassword: {
        type: STRING
    },
    EdiVANDefaultFolder: {
        type: STRING
    },
    FtpEnabled: {
        type: BOOLEAN
    },
    FtpUrl: {
        type: STRING
    },
    SecureFtp: {
        type: BOOLEAN
    },
    FtpUsername: {
        type: STRING
    },
    FtpPassword: {
        type: STRING
    },
    FtpDefaultFolder: {
        type: STRING
    },
    StopVendorNotifications: {
        type: BOOLEAN
    },
    EdiVANEnvelopePath: {
        type: STRING
    },
    UseSelectiveDropOff: {
        type: BOOLEAN
    },
    Pickup997: {
        type: BOOLEAN
    },
    Pickup204: {
        type: BOOLEAN
    },
    AcknowledgePickup204: {
        type: BOOLEAN
    },
    DocDeliveryFtpUrl: {
        type: STRING
    },
    DocDeliverySecureFtp: {
        type: BOOLEAN
    },
    DocDeliveryFtpUsername: {
        type: STRING
    },
    DocDeliveryFtpPassword: {
        type: STRING
    },
    DocDeliveryFtpDefaultFolder: {
        type: STRING
    },
    DeliverBolDoc: {
        type: BOOLEAN
    },
    DeliverShipmentStatementDoc: {
        type: BOOLEAN
    },
    DeliverInvoiceDoc: {
        type: BOOLEAN
    },
    StartDocDeliveriesFrom: {
        type: DATE
    },
    DeliverInternalShipmentDocs: {
        type: BOOLEAN
    },
    FtpDocDeliveryEnabled: {
        type: BOOLEAN
    },
    HoldShipmentDocsTillInvoiced: {
        type: BOOLEAN
    },
    FtpDeleteFileAfterPickup: {
        type: BOOLEAN
    },
    EdiDeleteFileAfterPickup: {
        type: BOOLEAN
    },
    EmailDocDeliveryEnabled: {
        type: BOOLEAN
    },
    SendAveryLabel: {
        type: BOOLEAN
    },
    SendStandardLabel: {
        type: BOOLEAN
    },
    SSHFtp: {
        type: BOOLEAN
    },
    DocumentLinkAccessKey: {
        type: STRING
    }
}, {
    sequelize,
    modelName: "CustomerCommunication",
    freezeTableName: true,
    tableName: "CustomerCommunication",
    timestamps: false
});


const CustomerCommunicationSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    ConnectGuid: {
        type: String
    },
    EnableWebServiceAPIAccess: {
        type: Boolean
    },
    CustomerId: {
        type: mongoose.Types.ObjectId
    },
    EdiEnabled: {
        type: Boolean
    },
    EdiCode: {
        type: String
    },
    EdiVANUrl: {
        type: String
    },
    SecureEdiVAN: {
        type: Boolean
    },
    EdiVANUsername: {
        type: String
    },
    EdiVANPassword: {
        type: String
    },
    EdiVANDefaultFolder: {
        type: String
    },
    FtpEnabled: {
        type: Boolean
    },
    FtpUrl: {
        type: String
    },
    SecureFtp: {
        type: Boolean
    },
    FtpUsername: {
        type: String
    },
    FtpPassword: {
        type: String
    },
    FtpDefaultFolder: {
        type: String
    },
    StopVendorNotifications: {
        type: Boolean
    },
    EdiVANEnvelopePath: {
        type: String
    },
    UseSelectiveDropOff: {
        type: Boolean
    },
    Pickup997: {
        type: Boolean
    },
    Pickup204: {
        type: Boolean
    },
    AcknowledgePickup204: {
        type: Boolean
    },
    DocDeliveryFtpUrl: {
        type: String
    },
    DocDeliverySecureFtp: {
        type: Boolean
    },
    DocDeliveryFtpUsername: {
        type: String
    },
    DocDeliveryFtpPassword: {
        type: String
    },
    DocDeliveryFtpDefaultFolder: {
        type: String
    },
    DeliverBolDoc: {
        type: Boolean
    },
    DeliverShipmentStatementDoc: {
        type: Boolean
    },
    DeliverInvoiceDoc: {
        type: Boolean
    },
    StartDocDeliveriesFrom: {
        type: Date
    },
    DeliverInternalShipmentDocs: {
        type: Boolean
    },
    FtpDocDeliveryEnabled: {
        type: Boolean
    },
    HoldShipmentDocsTillInvoiced: {
        type: Boolean
    },
    FtpDeleteFileAfterPickup: {
        type: Boolean
    },
    EdiDeleteFileAfterPickup: {
        type: Boolean
    },
    EmailDocDeliveryEnabled: {
        type: Boolean
    },
    SendAveryLabel: {
        type: Boolean
    },
    SendStandardLabel: {
        type: Boolean
    },
    SSHFtp: {
        type: Boolean
    },
    DocumentLinkAccessKey: {
        type: String
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "CustomerCommunications"
});

declare interface ICustomerCommunicationCollection extends mongoose.Document {
    CustomerId: string;
}

const db = mongoose.connection.useDb(FINANCE_DB);
const CustomerCommunicationCollection = db
    .model<ICustomerCommunicationCollection>(
        "CustomerCommunications",
        CustomerCommunicationSchema
    );

export {
    CustomerCommunication,
    CustomerCommunicationCollection,
    CustomerCommunicationSchema
};
