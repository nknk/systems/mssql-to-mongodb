import { STRING, DATE, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { BaseModel } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class Project44DispatchResponse extends BaseModel {
    ShipmentId: number;
    UserId: number;
}

Project44DispatchResponse.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    ShipmentId: {
        type: BIGINT,
    },
    Project44Id: {
        type: STRING,
    },
    UserId: {
        type: BIGINT,
    },
    RequestData: {
        type: STRING,
    },
    ResponseData: {
        type: STRING,
    },
    DateCreated: {
        type: DATE,
    },
    Scac: {
        type: STRING,
    },
}, {
    sequelize,
    modelName: "Project44DispatchResponse",
    freezeTableName: true,
    tableName: "Project44DispatchResponse",
    timestamps: false
});


const Project44DispatchResponseSchema = new mongoose.Schema({
    OldId: {
        type: Number
    },
    ShipmentId: {
        type: mongoose.Types.ObjectId
    },
    Project44Id: {
        type: String
    },
    UserId: {
        type: mongoose.Types.ObjectId
    },
    RequestData: {
        type: String
    },
    ResponseData: {
        type: String
    },
    DateCreated: {
        type: Date
    },
    Scac: {
        type: String
    },
}, {
    versionKey: false,
    collection: "Project44DispatchResponses"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const Project44DispatchResponseCollection = db
    .model("Project44DispatchResponses", Project44DispatchResponseSchema);

export {
    Project44DispatchResponse,
    Project44DispatchResponseSchema,
    Project44DispatchResponseCollection
};
