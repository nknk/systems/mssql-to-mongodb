import { STRING, INTEGER, BOOLEAN, DATE, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";

class PendingVendor extends ModelWithTenantId {
    CreatedByUserId: number;
}

PendingVendor.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    VendorNumber: {
        type: STRING,
        allowNull: false
    },
    Name: {
        type: STRING,
        allowNull: false
    },
    Notation: {
        type: STRING,
        allowNull: false
    },
    Approved: {
        type: BOOLEAN,
        allowNull: false
    },
    DateCreated: {
        type: DATE,
        allowNull: false
    },
    TSACertified: {
        type: BOOLEAN,
        allowNull: false
    },
    CTPATCertified: {
        type: BOOLEAN,
        allowNull: false
    },
    SmartWayCertified: {
        type: BOOLEAN,
        allowNull: false
    },
    PIPCertified: {
        type: BOOLEAN,
        allowNull: false
    },
    CTPATNumber: {
        type: STRING,
        allowNull: false
    },
    PIPNumber: {
        type: STRING,
        allowNull: false
    },
    Scac: {
        type: STRING,
        allowNull: false
    },
    FederalIDNumber: {
        type: STRING,
        allowNull: false
    },
    BrokerReferenceNumber: {
        type: STRING,
        allowNull: false
    },
    BrokerReferenceNumberType: {
        type: INTEGER,
        allowNull: false
    },
    MC: {
        type: STRING,
        allowNull: false
    },
    DOT: {
        type: STRING,
        allowNull: false
    },
    TrackingUrl: {
        type: STRING,
        allowNull: false
    },
    HandlesLessThanTruckload: {
        type: BOOLEAN,
        allowNull: false
    },
    HandlesTruckload: {
        type: BOOLEAN,
        allowNull: false
    },
    HandlesAir: {
        type: BOOLEAN,
        allowNull: false
    },
    HandlesPartialTruckload: {
        type: BOOLEAN,
        allowNull: false
    },
    HandlesRail: {
        type: BOOLEAN,
        allowNull: false
    },
    HandlesSmallPack: {
        type: BOOLEAN,
        allowNull: false
    },
    IsCarrier: {
        type: BOOLEAN,
        allowNull: false
    },
    IsAgent: {
        type: BOOLEAN,
        allowNull: false
    },
    IsBroker: {
        type: BOOLEAN,
        allowNull: false
    },
    Notes: {
        type: STRING,
        allowNull: false
    },
    Rejected: {
        type: BOOLEAN,
        allowNull: false
    },
    CreatedByUserId: {
        type: BIGINT,
        allowNull: false
    },
}, {
    sequelize,
    modelName: "PendingVendor",
    freezeTableName: true,
    tableName: "PendingVendor",
    timestamps: false
});


const PendingVendorSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    },
    VendorNumber: {
        type: String
    },
    Name: {
        type: String
    },
    Notation: {
        type: String
    },
    Approved: {
        type: Boolean
    },
    DateCreated: {
        type: Date
    },
    TSACertified: {
        type: Boolean
    },
    CTPATCertified: {
        type: Boolean
    },
    SmartWayCertified: {
        type: Boolean
    },
    PIPCertified: {
        type: Boolean
    },
    CTPATNumber: {
        type: String
    },
    PIPNumber: {
        type: String
    },
    Scac: {
        type: String
    },
    FederalIDNumber: {
        type: String
    },
    BrokerReferenceNumber: {
        type: String
    },
    BrokerReferenceNumberType: {
        type: Number
    },
    MC: {
        type: String
    },
    DOT: {
        type: String
    },
    TrackingUrl: {
        type: String
    },
    HandlesLessThanTruckload: {
        type: Boolean
    },
    HandlesTruckload: {
        type: Boolean
    },
    HandlesAir: {
        type: Boolean
    },
    HandlesPartialTruckload: {
        type: Boolean
    },
    HandlesRail: {
        type: Boolean
    },
    HandlesSmallPack: {
        type: Boolean
    },
    IsCarrier: {
        type: Boolean
    },
    IsAgent: {
        type: Boolean
    },
    IsBroker: {
        type: Boolean
    },
    Notes: {
        type: String
    },
    Rejected: {
        type: Boolean
    },
    CreatedByUserId: {
        type: mongoose.Types.ObjectId

    },
}, {
    versionKey: false,
    collection: "PendingVendors"
});

const db = mongoose.connection.useDb(FINANCE_DB);
const PendingVendorCollection = db.model("PendingVendors", PendingVendorSchema);

export { PendingVendor, PendingVendorCollection, PendingVendorSchema };
