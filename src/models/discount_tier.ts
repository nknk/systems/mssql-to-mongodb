import { INTEGER, DATE, DECIMAL, BIGINT, FLOAT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { RATING_DB } from "../config";
class DiscountTier extends ModelWithTenantId {
    VendorRatingId: number;
    FreightChargeCodeId: number;
    OriginRegionId: number;
    DestinationRegionId: number;
}

DiscountTier.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    DiscountPercent: {
        type: DECIMAL
    },
    FloorValue: {
        type: DECIMAL
    },
    CeilingValue: {
        type: DECIMAL
    },
    FreightChargeCodeId: {
        type: BIGINT
    },
    TierPriority: {
        type: INTEGER
    },
    FAK50: {
        type: FLOAT
    },
    FAK55: {
        type: FLOAT
    },
    FAK60: {
        type: FLOAT
    },
    FAK65: {
        type: FLOAT
    },
    FAK70: {
        type: FLOAT
    },
    FAK775: {
        type: FLOAT
    },
    FAK85: {
        type: FLOAT
    },
    FAK925: {
        type: FLOAT
    },
    FAK100: {
        type: FLOAT
    },
    FAK110: {
        type: FLOAT
    },
    FAK125: {
        type: FLOAT
    },
    FAK150: {
        type: FLOAT
    },
    FAK175: {
        type: FLOAT
    },
    FAK200: {
        type: FLOAT
    },
    FAK250: {
        type: FLOAT
    },
    FAK300: {
        type: FLOAT
    },
    FAK400: {
        type: FLOAT
    },
    FAK500: {
        type: FLOAT
    },
    EffectiveDate: {
        type: DATE
    },
    RateBasis: {
        type: INTEGER
    },
    VendorRatingId: {
        type: BIGINT
    },
    OriginRegionId: {
        type: BIGINT
    },
    DestinationRegionId: {
        type: BIGINT
    },
    WeightBreak: {
        type: INTEGER
    }
}, {
    sequelize,
    modelName: "DiscountTier",
    freezeTableName: true,
    tableName: "DiscountTier",
    timestamps: false
});


const DiscountTierSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    DiscountPercent: {
        type: Number
    },
    FloorValue: {
        type: Number
    },
    CeilingValue: {
        type: Number
    },
    FreightChargeCodeId: {
        type: mongoose.Types.ObjectId
    },
    TierPriority: {
        type: Number
    },
    FAK50: {
        type: Number
    },
    FAK55: {
        type: Number
    },
    FAK60: {
        type: Number
    },
    FAK65: {
        type: Number
    },
    FAK70: {
        type: Number
    },
    FAK775: {
        type: Number
    },
    FAK85: {
        type: Number
    },
    FAK925: {
        type: Number
    },
    FAK100: {
        type: Number
    },
    FAK110: {
        type: Number
    },
    FAK125: {
        type: Number
    },
    FAK150: {
        type: Number
    },
    FAK175: {
        type: Number
    },
    FAK200: {
        type: Number
    },
    FAK250: {
        type: Number
    },
    FAK300: {
        type: Number
    },
    FAK400: {
        type: Number
    },
    FAK500: {
        type: Number
    },
    EffectiveDate: {
        type: Date
    },
    RateBasis: {
        type: Number
    },
    VendorRatingId: {
        type: mongoose.Types.ObjectId
    },
    OriginRegionId: {
        type: mongoose.Types.ObjectId
    },
    DestinationRegionId: {
        type: mongoose.Types.ObjectId
    },
    WeightBreak: {
        type: Number
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "DiscountTiers"
});


const db = mongoose.connection.useDb(RATING_DB);
const DiscountTierCollection = db.model("DiscountTiers", DiscountTierSchema);

export {
    DiscountTier,
    DiscountTierCollection,
    DiscountTierSchema
};
