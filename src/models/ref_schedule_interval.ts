import { STRING, INTEGER } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import {BaseModel } from "./contracts/base_models";
import {  OPERATIONS_DB } from "../config";

class RefScheduleInterval extends BaseModel {
   ScheduleIntervalIndex: number; 
}

RefScheduleInterval.init({
 
    ScheduleIntervalIndex: {
        type: INTEGER,
        primaryKey: true
    },
    ScheduleIntervalText: {
        type: STRING
    },
  
}, {
    sequelize,
    modelName: "RefScheduleInterval",
    freezeTableName: true,
    tableName: "RefScheduleInterval",
    timestamps: false
});


const RefScheduleIntervalSchema = new mongoose.Schema({
    ScheduleIntervalIndex: {
        type: Number,
        primaryKey: true
    },
    ScheduleIntervalText: {
        type:String
    }
}, {
    versionKey: false,
    collection: "RefScheduleIntervals"
});


const db = mongoose.connection.useDb(OPERATIONS_DB);
const RefScheduleIntervalCollection = db.model(
    "RefScheduleIntervals",
    RefScheduleIntervalSchema
);

export { RefScheduleInterval, RefScheduleIntervalCollection, RefScheduleIntervalSchema };
