import { STRING, BOOLEAN, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";

class CustomerContact extends ModelWithTenantId {
    ContactTypeId: number;
    CustomerLocationId: number;
}

CustomerContact.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    CustomerLocationId: {
        type: BIGINT,
    },
    Name: {
        type: STRING,
    },
    Phone: {
        type: STRING,
    },
    Mobile: {
        type: STRING,
    },
    Fax: {
        type: STRING,
    },
    Email: {
        type: STRING,
    },
    Comment: {
        type: STRING,
    },
    Primary: {
        type: BOOLEAN,
    },
    ContactTypeId: {
        type: BIGINT,
    },
}, {
    sequelize,
    modelName: "CustomerContact",
    freezeTableName: true,
    tableName: "CustomerContact",
    timestamps: false
});


const CustomerContactSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    },
    CustomerLocationId: {
        type: mongoose.Types.ObjectId,
    },
    Name: {
        type: String,
    },
    Phone: {
        type: String,
    },
    Mobile: {
        type: String,
    },
    Fax: {
        type: String,
    },
    Email: {
        type: String,
    },
    Comment: {
        type: String,
    },
    Primary: {
        type: Boolean,
    },
    ContactTypeId: {
        type: mongoose.Types.ObjectId,
    },
}, {
    versionKey: false,
    collection: "CustomerContacts"
});

const db = mongoose.connection.useDb(FINANCE_DB);
const CustomerContactCollection = db.model("CustomerContacts", CustomerContactSchema);

export { CustomerContact, CustomerContactCollection, CustomerContactSchema };
