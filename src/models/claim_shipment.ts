import { BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { OPERATIONS_DB } from "../config";
import { ModelWithTenantId } from "./contracts/base_models";

class ClaimShipment extends ModelWithTenantId {
    ClaimId: number;
    ShipmentId: number;
}

ClaimShipment.init({    
    TenantId: {
        type: BIGINT
    },
    ClaimId: {
        type: BIGINT
    },
    ShipmentId: {
        type: BIGINT
    }
}, {
    sequelize,
    modelName: "ClaimShipment",
    freezeTableName: true,
    tableName: "ClaimShipment",
    timestamps: false
});


const ClaimShipmentSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    ClaimId: {
        type: mongoose.Types.ObjectId
    },
    ShipmentId: {
        type: mongoose.Types.ObjectId
    }
}, {
    versionKey: false,
    collection: "ClaimShipments"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const ClaimShipmentCollection = db.model("ClaimShipments", ClaimShipmentSchema);

export { ClaimShipment, ClaimShipmentCollection, ClaimShipmentSchema };
