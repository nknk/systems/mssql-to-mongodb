import { STRING, DATE, BOOLEAN, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";
class LoadOrderDocument extends ModelWithTenantId {
    LoadOrderId: number;
    DocumentTagId: number;
}

LoadOrderDocument.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    IsInternal: {
        type: BOOLEAN
    },
    LoadOrderId: {
        type: BIGINT
    },
    LocationPath: {
        type: STRING
    },
    DocumentTagId: {
        type: BIGINT
    },
    Name: {
        type: STRING
    },
    Description: {
        type: STRING
    },
    DateCreated: {
        type: DATE
    }
}, {
    sequelize,
    modelName: "LoadOrderDocument",
    freezeTableName: true,
    tableName: "LoadOrderDocument",
    timestamps: false
});

const LoadOrderDocumentSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    IsInternal: {
        type: Boolean
    },
    LoadOrderId: {
        type: mongoose.Types.ObjectId
    },
    LocationPath: {
        type: String
    },
    DocumentTagId: {
        type: mongoose.Types.ObjectId
    },
    Name: {
        type: String
    },
    Description: {
        type: String
    },
    DateCreated: {
        type: Date
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "LoadOrderDocuments"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const LoadOrderDocumentCollection = db.model("LoadOrderDocuments", LoadOrderDocumentSchema);

export { LoadOrderDocument, LoadOrderDocumentCollection, LoadOrderDocumentSchema };
