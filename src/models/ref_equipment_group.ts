import { STRING, INTEGER } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import {BaseModel } from "./contracts/base_models";
import {  OPERATIONS_DB } from "../config";

class RefEquipmentGroup extends BaseModel {
   EquipmentGroupIndex: number; 
}

RefEquipmentGroup.init({
 
    EquipmentGroupIndex: {
        type: INTEGER,
        primaryKey: true
    },
    EquipmentGroupText: {
        type: STRING
    },
  
}, {
    sequelize,
    modelName: "RefEquipmentGroup",
    freezeTableName: true,
    tableName: "RefEquipmentGroup",
    timestamps: false
});


const RefEquipmentGroupSchema = new mongoose.Schema({
    EquipmentGroupIndex: {
        type:Number
    },
    EquipmentGroupText: {
        type:String
    },
}, {
    versionKey: false,
    collection: "RefEquipmentGroups"
});


const db = mongoose.connection.useDb(OPERATIONS_DB);
const RefEquipmentGroupCollection = db.model(
    "RefEquipmentGroups",
    RefEquipmentGroupSchema
);

export { RefEquipmentGroup, RefEquipmentGroupCollection, RefEquipmentGroupSchema };
