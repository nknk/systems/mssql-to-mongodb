import { STRING, INTEGER } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import {BaseModel } from "./contracts/base_models";
import {  OPERATIONS_DB } from "../config";

class RefCustomerType extends BaseModel {
   CustomerTypeIndex: number; 
}

RefCustomerType.init({
 
    CustomerTypeIndex: {
        type: INTEGER,
        primaryKey: true
    },
    CustomerTypeText: {
        type: STRING
    },
  
}, {
    sequelize,
    modelName: "RefCustomerType",
    freezeTableName: true,
    tableName: "RefCustomerType",
    timestamps: false
});


const RefCustomerTypeSchema = new mongoose.Schema({
    CustomerTypeIndex: {
        type:Number
    },
    CustomerTypeText: {
        type:String
    },
}, {
    versionKey: false,
    collection: "RefCustomerTypes"
});


const db = mongoose.connection.useDb(OPERATIONS_DB);
const RefCustomerTypeCollection = db.model(
    "RefCustomerTypes",
    RefCustomerTypeSchema
);

export { RefCustomerType, RefCustomerTypeCollection, RefCustomerTypeSchema };
