import { STRING, DATE, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";

class SalesRepresentative extends ModelWithTenantId {
    CountryId: number;
}

SalesRepresentative.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    Name: {
        type: STRING,
        allowNull: false
    },
    SalesRepresentativeNumber: {
        type: STRING,
        allowNull: false
    },
    CompanyName: {
        type: STRING,
        allowNull: false
    },
    DateCreated: {
        type: DATE,
        allowNull: false
    },
    Street1: {
        type: STRING,
        allowNull: false
    },
    Street2: {
        type: STRING,
        allowNull: false
    },
    City: {
        type: STRING,
        allowNull: false
    },
    State: {
        type: STRING,
        allowNull: false
    },
    CountryId: {
        type: BIGINT,
        allowNull: false
    },
    PostalCode: {
        type: STRING,
        allowNull: false
    },
    Phone: {
        type: STRING,
        allowNull: false
    },
    Mobile: {
        type: STRING,
        allowNull: false
    },
    Fax: {
        type: STRING,
        allowNull: false
    },
    Email: {
        type: STRING,
        allowNull: false
    },
    AdditionalEntityName: {
        type: STRING,
        allowNull: false
    },
    AdditionalEntityCommPercent: {
        type: DECIMAL,
        allowNull: false
    },
}, {
    sequelize,
    modelName: "SalesRepresentative",
    freezeTableName: true,
    tableName: "SalesRepresentative",
    timestamps: false
});


const SalesRepresentativeSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    Name: {
        type: String,
    },
    SalesRepresentativeNumber: {
        type: String,
    },
    CompanyName: {
        type: String,
    },
    DateCreated: {
        type: Date,
    },
    Street1: {
        type: String,
    },
    Street2: {
        type: String,
    },
    City: {
        type: String,
    },
    State: {
        type: String,
    },
    CountryId: {
        type: mongoose.Types.ObjectId,
    },
    PostalCode: {
        type: String,
    },
    Phone: {
        type: String,
    },
    Mobile: {
        type: String,
    },
    Fax: {
        type: String,
    },
    Email: {
        type: String,
    },
    AdditionalEntityName: {
        type: String,
    },
    AdditionalEntityCommPercent: {
        type: Number,
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "SalesRepresentatives"
});

const db = mongoose.connection.useDb(FINANCE_DB);
const SalesRepresentativeCollection = db.model("SalesRepresentatives", SalesRepresentativeSchema);

export {
    SalesRepresentative,
    SalesRepresentativeCollection,
    SalesRepresentativeSchema
};
