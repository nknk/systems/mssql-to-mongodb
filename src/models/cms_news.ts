import { STRING, INTEGER, DATE, BOOLEAN, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { BaseModel } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class CmsNews extends BaseModel { }

CmsNews.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    NewsKey: {
        type: STRING,
    },
    Title: {
        type: STRING,
    },
    ShortText: {
        type: STRING,
    },
    FullText: {
        type: STRING,
    },
    DateCreated: {
        type: DATE,
    }, 
    ExpirationDate: {
        type: DATE,
    },
    Hidden: {
        type: BOOLEAN,
    },
    Priority: {
        type: INTEGER,
    },
}, {
    sequelize,
    modelName: "CmsNews",
    freezeTableName: true,
    tableName: "CmsNews",
    timestamps: false
});


const CmsNewsSchema = new mongoose.Schema({
    NewsKey: {
        type: String,
    },
    Title: {
        type: String,
    },
    ShortText: {
        type: String,
    },
    FullText: {
        type: String,
    },
    DateCreated: {
        type: Date,
    }, 
    ExpirationDate: {
        type: Date,
    },
    Hidden: {
        type: Boolean,
    },
    Priority: {
        type: Number,
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "CmsNews"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const CmsNewsCollection = db.model("CmsNews", CmsNewsSchema);

export {
    CmsNews,
    CmsNewsCollection,
    CmsNewsSchema
};
