import { STRING, DATE, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { BaseModel } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class Project44TrackingResponse extends BaseModel {
    ShipmentId: number;
    UserId: number;
}

Project44TrackingResponse.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    ShipmentId: {
        type: BIGINT,
    },
    Project44Id: {
        type: STRING,
    },
    RequestData: {
        type: STRING,
    },
    ResponseData: {
        type: STRING,
    },
    DateCreated: {
        type: DATE,
    },
    Scac: {
        type: STRING,
    },
    UserId: {
        type: BIGINT,
    }
}, {
    sequelize,
    modelName: "Project44TrackingResponse",
    freezeTableName: true,
    tableName: "Project44TrackingResponse",
    timestamps: false
});


const Project44TrackingResponseSchema = new mongoose.Schema({
    OldId: {
        type: Number
    },
    ShipmentId: {
        type: mongoose.Types.ObjectId,
    },
    Project44Id: {
        type: String
    },
    RequestData: {
        type: String
    },
    ResponseData: {
        type: String
    },
    DateCreated: {
        type: Date
    },
    Scac: {
        type: String
    },
    UserId: {
        type: mongoose.Types.ObjectId,
    }
}, {
    versionKey: false,
    collection: "Project44TrackingResponses"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const Project44TrackingResponseCollection = db.model("Project44TrackingResponses", Project44TrackingResponseSchema);

export { Project44TrackingResponse, Project44TrackingResponseSchema, Project44TrackingResponseCollection };
