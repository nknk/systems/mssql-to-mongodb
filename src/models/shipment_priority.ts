import { STRING, BOOLEAN, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { REGISTRY_DB } from "../config";

class ShipmentPriority extends ModelWithTenantId { }

ShipmentPriority.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    Code: {
        type: STRING,
        allowNull: false
    },
    Description: {
        type: STRING,
        allowNull: false
    },
    Active: {
        type: BOOLEAN,
        allowNull: false
    },
    Default: {
        type: BOOLEAN,
        allowNull: false
    },
    PriorityColor: {
        type: STRING,
        allowNull: false
    }
}, {
    sequelize,
    modelName: "ShipmentPriority",
    freezeTableName: true,
    tableName: "ShipmentPriority",
    timestamps: false
});


const ShipmentPrioritySchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    Code: {
        type: String
    },
    Description: {
        type: String
    },
    Active: {
        type: Boolean
    },
    Default: {
        type: Boolean
    },
    OldId: {
        type: Number
    },
    PriorityColor: {
        type: String
    }
}, {
    versionKey: false,
    collection: "ShipmentPriorities"
});

const db = mongoose.connection.useDb(REGISTRY_DB);
const ShipmentPriorityCollection = db.model("ShipmentPriorities", ShipmentPrioritySchema);

export { ShipmentPriority, ShipmentPrioritySchema, ShipmentPriorityCollection };
