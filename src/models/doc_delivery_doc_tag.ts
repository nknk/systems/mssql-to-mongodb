import { BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { UTILITIES_DB } from "../config";

class DocDeliveryDocTag extends ModelWithTenantId {
    CustomerCommunicationId: number;
    DocumentTagId: number;

}

DocDeliveryDocTag.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    CustomerCommunicationId: {
        type: BIGINT
    },
    DocumentTagId: {
        type: BIGINT
    }
}, {
    sequelize,
    modelName: "DocDeliveryDocTag",
    freezeTableName: true,
    tableName: "DocDeliveryDocTag",
    timestamps: false
});


const DocDeliveryDocTagSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    CustomerCommunicationId: {
        type: mongoose.Types.ObjectId
    },
    DocumentTagId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "DocDeliveryDocTags"
});


const db = mongoose.connection.useDb(UTILITIES_DB);
const DocDeliveryDocTagCollection = db.model(
    "DocDeliveryDocTags",
    DocDeliveryDocTagSchema
);

export {
    DocDeliveryDocTag,
    DocDeliveryDocTagCollection,
    DocDeliveryDocTagSchema
};
