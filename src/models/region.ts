import { STRING, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { RATING_DB } from "../config";

class Region extends ModelWithTenantId { }

Region.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    Name: {
        type: STRING,
        allowNull: false
    },
}, {
    sequelize,
    modelName: "Region",
    freezeTableName: true,
    tableName: "Region",
    timestamps: false
});


const RegionSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    Name: {
        type: String
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "Regions"
});

const db = mongoose.connection.useDb(RATING_DB);
const RegionCollection = db.model("Regions", RegionSchema);

export { Region, RegionSchema, RegionCollection };
