import { STRING, BOOLEAN, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { IDENTITY_DB } from "../config";

class TenantContact extends ModelWithTenantId {
    ContactTypeId: number;
    TenantLocationId: number;
}

TenantContact.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    TenantLocationId: {
        type: BIGINT
    },
    Name: {
        type: STRING
    },
    Phone: {
        type: STRING
    },
    Mobile: {
        type: STRING
    },
    Fax: {
        type: STRING
    },
    Email: {
        type: STRING
    },
    Comment: {
        type: STRING
    },
    Primary: {
        type: BOOLEAN
    },
    ContactTypeId: {
        type: BIGINT
    }
}, {
    sequelize,
    modelName: "TenantContact",
    freezeTableName: true,
    tableName: "TenantContact",
    timestamps: false
});


const TenantContactSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    TenantLocationId: {
        type: mongoose.Types.ObjectId
    },
    Name: {
        type: String
    },
    Phone: {
        type: String
    },
    Mobile: {
        type: String
    },
    Fax: {
        type: String
    },
    Email: {
        type: String
    },
    Comment: {
        type: String
    },
    Primary: {
        type: Boolean
    },
    ContactTypeId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "TenantContacts"
});

const db = mongoose.connection.useDb(IDENTITY_DB);
const TenantContactCollection = db.model("TenantContacts", TenantContactSchema);

export { TenantContact, TenantContactCollection, TenantContactSchema };
