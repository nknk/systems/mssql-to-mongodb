import { STRING, INTEGER } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import {BaseModel } from "./contracts/base_models";
import {  OPERATIONS_DB } from "../config";

class RefClaimStatus extends BaseModel {
   ClaimStatusIndex: number; 
}

RefClaimStatus.init({
    ClaimStatusIndex: {
        type: INTEGER,
        primaryKey: true
    },
    ClaimStatusText: {
        type: STRING
    },
  
}, {
    sequelize,
    modelName: "RefClaimStatus",
    freezeTableName: true,
    tableName: "RefClaimStatus",
    timestamps: false
});


const RefClaimStatusSchema = new mongoose.Schema({
    ClaimStatusIndex: {
        type: Number,
    },
    ClaimStatusText: {
        type:String
    },
}, {
    versionKey: false,
    collection: "RefClaimStatuses"
});


const db = mongoose.connection.useDb(OPERATIONS_DB);
const RefClaimStatusCollection = db.model(
    "RefClaimStatuses",
    RefClaimStatusSchema
);

export { RefClaimStatus, RefClaimStatusCollection, RefClaimStatusSchema };
