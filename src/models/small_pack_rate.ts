import { INTEGER, STRING, BOOLEAN, DATE, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { RATING_DB } from "../config";

class SmallPackRate extends ModelWithTenantId {
    VendorId: number;
    CustomerRatingId: number;
    ChargeCodeId: number;
}

SmallPackRate.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    VendorId: {
        type: BIGINT
    },
    SmallPackageEngine: {
        type: INTEGER
    },
    SmallPackType: {
        type: STRING
    },
    MarkupPercent: {
        type: DECIMAL,
    },
    MarkupValue: {
        type: DECIMAL,
    },
    UseMinimum: {
        type: BOOLEAN
    },
    EffectiveDate: {
        type: DATE
    },
    Active: {
        type: BOOLEAN
    },
    CustomerRatingId: {
        type: BIGINT
    },
    ChargeCodeId: {
        type: BIGINT
    }
}, {
    sequelize,
    modelName: "SmallPackRate",
    freezeTableName: true,
    tableName: "SmallPackRate",
    timestamps: false
});


const SmallPackRateSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    VendorId: {
        type: mongoose.Types.ObjectId
    },
    SmallPackageEngine: {
        type: Number
    },
    SmallPackType: {
        type: String
    },
    MarkupPercent: {
        type: Number
    },
    MarkupValue: {
        type: Number
    },
    UseMinimum: {
        type: Boolean
    },
    EffectiveDate: {
        type: Date
    },
    Active: {
        type: Boolean
    },
    CustomerRatingId: {
        type: mongoose.Types.ObjectId
    },
    ChargeCodeId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "SmallPackRates"
});

const db = mongoose.connection.useDb(RATING_DB);
const SmallPackRateCollection = db.model("SmallPackRates", SmallPackRateSchema);

export { SmallPackRate, SmallPackRateCollection, SmallPackRateSchema };
