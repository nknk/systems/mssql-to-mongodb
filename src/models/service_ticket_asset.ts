import { BOOLEAN, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class ServiceTicketAsset extends ModelWithTenantId {
    ServiceTicketId: number;
    DriverAssetId: number;
    TractorAssetId: number;
    TrailerAssetId: number;
}

ServiceTicketAsset.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    ServiceTicketId: {
        type: BIGINT,
    },
    DriverAssetId: {
        type: BIGINT
    },
    TractorAssetId: {
        type: BIGINT
    },
    TrailerAssetId: {
        type: BIGINT
    },
    MilesRun: {
        type: DECIMAL
    },
    Primary: {
        type: BOOLEAN
    }
}, {
    sequelize,
    modelName: "ServiceTicketAsset",
    freezeTableName: true,
    tableName: "ServiceTicketAsset",
    timestamps: false
});


const ServiceTicketAssetSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    ServiceTicketId: {
        type: mongoose.Types.ObjectId,
    },
    DriverAssetId: {
        type: mongoose.Types.ObjectId
    },
    TractorAssetId: {
        type: mongoose.Types.ObjectId
    },
    TrailerAssetId: {
        type: mongoose.Types.ObjectId
    },
    MilesRun: {
        type: Number
    },
    Primary: {
        type: Boolean
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "ServiceTicketAssets"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const ServiceTicketAssetCollection = db.model("ServiceTicketAssets", ServiceTicketAssetSchema);

export { ServiceTicketAsset, ServiceTicketAssetCollection, ServiceTicketAssetSchema };
