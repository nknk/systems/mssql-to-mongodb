import { BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";

class RequiredInvoiceDocumentTag extends ModelWithTenantId {
    CustomerId: number;
    DocumentTagId: number;
}

RequiredInvoiceDocumentTag.init({
    CustomerId: {
        type: BIGINT,
    },
    DocumentTagId: {
        type: BIGINT
    },
    TenantId: {
        type: BIGINT,
    }
}, {
    sequelize,
    modelName: "RequiredInvoiceDocumentTag",
    freezeTableName: true,
    tableName: "RequiredInvoiceDocumentTag",
    timestamps: false
});


const RequiredInvoiceDocumentTagSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    CustomerId: {
        type: mongoose.Types.ObjectId
    },
    DocumentTagId: {
        type: mongoose.Types.ObjectId
    },
}, {
    versionKey: false,
    collection: "RequiredInvoiceDocumentTags"
});

const db = mongoose.connection.useDb(FINANCE_DB);
const RequiredInvoiceDocumentTagCollection = db.model(
    "RequiredInvoiceDocumentTags",
    RequiredInvoiceDocumentTagSchema
);

export { RequiredInvoiceDocumentTag, RequiredInvoiceDocumentTagCollection, RequiredInvoiceDocumentTagSchema };
