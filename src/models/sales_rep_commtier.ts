import { INTEGER, DATE, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";

class SalesRepCommTier extends ModelWithTenantId {
    SalesRepresentativeId: number;
    CustomerId: number;
 }

SalesRepCommTier.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT
    },
    SalesRepresentativeId: {
        type: BIGINT
    },
    CustomerId: {
        type: BIGINT
    },
    CommissionPercent: {
        type: DECIMAL
    },
    CeilingValue: {
        type: DECIMAL
    },
    FloorValue: {
        type: DECIMAL
    },
    EffectiveDate: {
        type: DATE
    },
    ExpirationDate: {
        type: DATE
    },
    ServiceMode: {
        type: INTEGER
    },
}, {
    sequelize,
    modelName: "SalesRepCommTier",
    freezeTableName: true,
    tableName: "SalesRepCommTier",
    timestamps: false
});


const SalesRepCommTierSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    },
    SalesRepresentativeId: {
        type: mongoose.Types.ObjectId
    },
    CustomerId: {
        type: mongoose.Types.ObjectId
    },
    CommissionPercent: {
        type: Number
    },
    CeilingValue: {
        type: Number
    },
    FloorValue: {
        type: Number
    },
    EffectiveDate: {
        type: Date
    },
    ExpirationDate: {
        type: Date
    },
    ServiceMode: {
        type: Number
    },
}, {
    versionKey: false,
    collection: "SalesRepCommTiers"
});

const db = mongoose.connection.useDb(FINANCE_DB);
const SalesRepCommTierCollection = db.model("SalesRepCommTiers", SalesRepCommTierSchema);

export { SalesRepCommTier, SalesRepCommTierSchema, SalesRepCommTierCollection };
