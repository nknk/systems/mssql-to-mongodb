import { BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class AddressBookService extends ModelWithTenantId {
    AddressBookId: number;
    ServiceId: number;
}

AddressBookService.init({
    ServiceId: {
        type: BIGINT,
        allowNull: false
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    AddressBookId: {
        type: BIGINT,
        allowNull: false
    }
}, {
    sequelize,
    modelName: "AddressBookService",
    freezeTableName: true,
    tableName: "AddressBookService",
    timestamps: false
});

const AddressBookServiceSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId,
    },
    ServiceId: {
        type: mongoose.Types.ObjectId,
    },
    AddressBookId: {
        type: mongoose.Types.ObjectId,
    }
}, {
    versionKey: false,
    collection: "AddressBookServices"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const AddressBookServiceCollection = db.model("AddressBookServices", AddressBookServiceSchema);

export { AddressBookService, AddressBookServiceCollection, AddressBookServiceSchema };
