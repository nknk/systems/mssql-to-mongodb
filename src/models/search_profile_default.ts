import { STRING, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class SearchProfileDefault extends ModelWithTenantId { 
    UserId: number;
}

SearchProfileDefault.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT
    },
    UserId: {
        type: BIGINT
    },
    Profiles: {
        type: STRING
    },
    RateAndScheduleProfiles: {
        type: STRING
    },
}, {
    sequelize,
    modelName: "SearchProfileDefault",
    freezeTableName: true,
    tableName: "SearchProfileDefault",
    timestamps: false
});

const SearchProfileDefaultSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    },
    UserId: {
        type: mongoose.Types.ObjectId
    },
    Profiles: {
        type: String
    },
    RateAndScheduleProfiles: {
        type: String
    },
}, {
    versionKey: false,
    collection: "SearchProfileDefaults"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const SearchProfileDefaultCollection = db.model("SearchProfileDefaults", SearchProfileDefaultSchema);

export { SearchProfileDefault, SearchProfileDefaultCollection, SearchProfileDefaultSchema };
