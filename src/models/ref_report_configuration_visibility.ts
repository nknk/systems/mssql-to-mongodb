import { STRING, INTEGER } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { BaseModel } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class RefReportConfigurationVisibility extends BaseModel {
    ReportConfigurationVisibilityIndex: number;
}

RefReportConfigurationVisibility.init({

    ReportConfigurationVisibilityIndex: {
        type: INTEGER,
        primaryKey: true
    },
    ReportConfigurationVisibilityText: {
        type: STRING
    },

}, {
    sequelize,
    modelName: "RefReportConfigurationVisibility",
    freezeTableName: true,
    tableName: "RefReportConfigurationVisibility",
    timestamps: false
});


const RefReportConfigurationVisibilitySchema = new mongoose.Schema({
    ReportConfigurationVisibilityIndex: {
        type: Number,
    },
    ReportConfigurationVisibilityText: {
        type: String
    },
}, {
    versionKey: false,
    collection: "RefReportConfigurationVisibilities"
});


const db = mongoose.connection.useDb(OPERATIONS_DB);
const RefReportConfigurationVisibilityCollection = db.model(
    "RefReportConfigurationVisibilities",
    RefReportConfigurationVisibilitySchema
);

export {
    RefReportConfigurationVisibility,
    RefReportConfigurationVisibilityCollection,
    RefReportConfigurationVisibilitySchema
};
