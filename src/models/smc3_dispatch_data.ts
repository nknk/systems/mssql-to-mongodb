import { DATE, STRING, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { BaseModel } from "./contracts/base_models";
import { RATING_DB } from "../config";

class SMC3DispatchData extends BaseModel {
}

SMC3DispatchData.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TransactionId: {
        type: STRING
    },
    Scac: {
        type: STRING
    },
    PickupNumber: {
        type: STRING
    },
    BarcodeNumber: {
        type: STRING
    },
    DateAccepted: {
        type: STRING
    },
    Identifiers: {
        type: STRING
    },
    ResponseStatus: {
        type: STRING
    },
    ResponseCode: {
        type: STRING,
    },
    ResponseMessage: {
        type: STRING
    },
    DateCreated: {
        type: DATE
    }
}, {
    sequelize,
    modelName: "SMC3DispatchData",
    freezeTableName: true,
    tableName: "SMC3DispatchData",
    timestamps: false
});


const SMC3DispatchDataSchema = new mongoose.Schema({
    TransactionId: {
        type: String
    },
    Scac: {
        type: String
    },
    PickupNumber: {
        type: String
    },
    BarcodeNumber: {
        type: String
    },
    DateAccepted: {
        type: String
    },
    Identifiers: {
        type: String
    },
    ResponseStatus: {
        type: String
    },
    ResponseCode: {
        type: String,
    },
    ResponseMessage: {
        type: String
    },
    DateCreated: {
        type: Date
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "SMC3DispatchDatas"
});

const db = mongoose.connection.useDb(RATING_DB);
const SMC3DispatchDataCollection = db.model("SMC3DispatchDatas", SMC3DispatchDataSchema);

export { SMC3DispatchData, SMC3DispatchDataCollection, SMC3DispatchDataSchema };
