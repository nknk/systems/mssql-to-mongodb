import { STRING, BOOLEAN, DATE, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";

class VendorLocation extends ModelWithTenantId {
    CountryId: number;
    VendorId: number;
}

VendorLocation.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT
    },
    LocationNumber: {
        type: STRING
    },
    RemitToLocation: {
        type: BOOLEAN
    },
    MainRemitToLocation: {
        type: BOOLEAN
    },
    Primary: {
        type: BOOLEAN
    },
    DateCreated: {
        type: DATE
    },
    VendorId: {
        type: BIGINT
    },
    Street1: {
        type: STRING
    },
    Street2: {
        type: STRING
    },
    City: {
        type: STRING
    },
    State: {
        type: STRING
    },
    CountryId: {
        type: BIGINT
    },
    PostalCode: {
        type: STRING
    }
}, {
    sequelize,
    modelName: "VendorLocation",
    freezeTableName: true,
    tableName: "VendorLocation",
    timestamps: false
});

const VendorLocationSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    },
    LocationNumber: {
        type: String
    },
    RemitToLocation: {
        type: Boolean
    },
    MainRemitToLocation: {
        type: Boolean
    },
    Primary: {
        type: Boolean
    },
    DateCreated: {
        type: Date
    },
    VendorId: {
        type: mongoose.Types.ObjectId
    },
    Street1: {
        type: String
    },
    Street2: {
        type: String
    },
    City: {
        type: String
    },
    State: {
        type: String
    },
    CountryId: {
        type: mongoose.Types.ObjectId
    },
    PostalCode: {
        type: String
    }    
}, {
    versionKey: false,
    collection: "VendorLocations"
});

const db = mongoose.connection.useDb(FINANCE_DB);
const VendorLocationCollection = db.model("VendorLocations", VendorLocationSchema);

export { VendorLocation, VendorLocationCollection, VendorLocationSchema };
