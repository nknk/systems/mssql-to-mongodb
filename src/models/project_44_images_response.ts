import { STRING, DATE, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { BaseModel } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class Project44ImagesResponse extends BaseModel {
    UserId: number;
}

Project44ImagesResponse.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    PurchaseOrderNumber: {
        type: BIGINT,
    },
    Project44Id: {
        type: STRING,
    },
    ResponseData: {
        type: STRING,
    },
    DateCreated: {
        type: DATE,
    },
    UserId: {
        type: BIGINT,
    },
    Scac: {
        type: STRING,
    },
}, {
    sequelize,
    modelName: "Project44ImagesResponse",
    freezeTableName: true,
    tableName: "Project44ImagesResponse",
    timestamps: false
});


const Project44ImagesResponseSchema = new mongoose.Schema({
    OldId: {
        type: Number
    },
    PurchaseOrderNumber: {
        type: Number
    },
    Project44Id: {
        type: String
    },
    ResponseData: {
        type: String
    },
    DateCreated: {
        type: Date
    },
    UserId: {
        type: mongoose.Types.ObjectId
    },
    Scac: {
        type: String
    },
}, {
    versionKey: false,
    collection: "Project44ImagesResponses"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const Project44ImagesResponseCollection = db.model("Project44ImagesResponses", Project44ImagesResponseSchema);

export { Project44ImagesResponse, Project44ImagesResponseSchema, Project44ImagesResponseCollection };
