import { STRING, BOOLEAN, INTEGER, DATE, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class ClaimNote extends ModelWithTenantId {
    UserId: number;
    ClaimId: number;
}

ClaimNote.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    ClaimId: {
        type: BIGINT
    },
    Message: {
        type: STRING
    },
    Type: {
        type: INTEGER
    },
    Archived: {
        type: BOOLEAN
    },
    Classified: {
        type: BOOLEAN
    },
    UserId: {
        type: BIGINT
    },
    SendReminder: {
        type: BOOLEAN
    },
    DateCreated: {
        type: DATE
    },
    SendReminderOn: {
        type: DATE
    },
    SendReminderEmails: {
        type: STRING
    }
}, {
    sequelize,
    modelName: "ClaimNote",
    freezeTableName: true,
    tableName: "ClaimNote",
    timestamps: false
});


const ClaimNoteSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    ClaimId: {
        type: mongoose.Types.ObjectId
    },
    Message: {
        type: String
    },
    Type: {
        type: Number
    },
    Archived: {
        type: Boolean
    },
    Classified: {
        type: Boolean
    },
    UserId: {
        type: mongoose.Types.ObjectId
    },
    SendReminder: {
        type: Boolean
    },
    DateCreated: {
        type: Date
    },
    SendReminderOn: {
        type: Date
    },
    SendReminderEmails: {
        type: String
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "ClaimNotes"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const ClaimNoteCollection = db.model("ClaimNotes", ClaimNoteSchema);

export { ClaimNote, ClaimNoteCollection, ClaimNoteSchema };
