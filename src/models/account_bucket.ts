import { STRING, BOOLEAN, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { REGISTRY_DB } from "../config";

class AccountBucket extends ModelWithTenantId {}

AccountBucket.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    Code: {
        type: STRING,
        allowNull: false
    },
    Description: {
        type: STRING,
        allowNull: false
    },
    Active: {
        type: BOOLEAN,
        allowNull: false
    }
}, {
    sequelize,
    modelName: "AccountBucket",
    freezeTableName: true,
    tableName: "AccountBucket",
    timestamps: false
});


const AccountBucketSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    Code: {
        type: String
    },
    Description: {
        type: String
    },
    Active: {
        type: Boolean
    },
    OldId: {
        type: Number
    },
}, {
    versionKey: false,
    collection: "AccountBuckets"
});

const db = mongoose.connection.useDb(REGISTRY_DB);
const AccountBucketCollection = db.model("AccountBuckets", AccountBucketSchema);

export { AccountBucket, AccountBucketCollection, AccountBucketSchema };
