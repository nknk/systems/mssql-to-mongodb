import { BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { OPERATIONS_DB } from "../config";
import { ModelWithTenantId } from "./contracts/base_models";

class ServiceTicketService extends ModelWithTenantId {
    ServiceId: number;
    ServiceTicketId: number;
}

ServiceTicketService.init({
    TenantId: {
        type: BIGINT
    },
    ServiceId: {
        type: BIGINT
    },
    ServiceTicketId: {
        type: BIGINT
    }
}, {
    sequelize,
    modelName: "ServiceTicketService",
    freezeTableName: true,
    tableName: "ServiceTicketService",
    timestamps: false
});


const ServiceTicketServiceSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    ServiceId: {
        type: mongoose.Types.ObjectId
    },
    ServiceTicketId: {
        type: mongoose.Types.ObjectId
    }
}, {
    versionKey: false,
    collection: "ServiceTicketServices"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const ServiceTicketServiceCollection = db.model("ServiceTicketServices", ServiceTicketServiceSchema);

export { ServiceTicketService, ServiceTicketServiceCollection, ServiceTicketServiceSchema };
