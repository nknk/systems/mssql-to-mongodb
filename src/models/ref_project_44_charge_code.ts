import { STRING, INTEGER } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { BaseModel } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class RefProject44ChargeCode extends BaseModel {
    Project44ChargeCodeIndex: number;
}

RefProject44ChargeCode.init({
    Project44ChargeCodeIndex: {
        type: INTEGER,
        primaryKey: true
    },
    Project44ChargeCodeText: {
        type: STRING
    },
    Project44ChargeCodeDescription: {
        type: STRING
    }
}, {
    sequelize,
    modelName: "RefProject44ChargeCode",
    freezeTableName: true,
    tableName: "RefProject44ChargeCode",
    timestamps: false
});


const RefProject44ChargeCodeSchema = new mongoose.Schema({
    Project44ChargeCodeIndex: {
        type: Number,
    },
    Project44ChargeCodeText: {
        type: String
    },
    Project44ChargeCodeDescription: {
        type: String
    },
}, {
    versionKey: false,
    collection: "RefProject44ChargeCodes"
});


const db = mongoose.connection.useDb(OPERATIONS_DB);
const RefProject44ChargeCodeCollection = db.model(
    "RefProject44ChargeCodes",
    RefProject44ChargeCodeSchema
);

export { RefProject44ChargeCode, RefProject44ChargeCodeCollection, RefProject44ChargeCodeSchema };
