import { INTEGER, DATE, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { RATING_DB } from "../config";

class LTLAccessorial extends ModelWithTenantId {
    ServiceId: number;
    VendorRatingId: number;
}

LTLAccessorial.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    RateType: {
        type: INTEGER,
    },
    Rate: {
        type: DECIMAL,
    },
    FloorValue: {
        type: DECIMAL,
    },
    CeilingValue: {
        type: DECIMAL,
    },
    EffectiveDate: {
        type: DATE,
    },
    ServiceId: {
        type: BIGINT,
    },
    VendorRatingId: {
        type: BIGINT,
    },
}, {
    sequelize,
    modelName: "LTLAccessorial",
    freezeTableName: true,
    tableName: "LTLAccessorial",
    timestamps: false
});


const LTLAccessorialSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    },
    RateType: {
        type: Number
    },
    Rate: {
        type: Number
    },
    FloorValue: {
        type: Number
    },
    CeilingValue: {
        type: Number
    },
    EffectiveDate: {
        type: Date
    },
    ServiceId: {
        type: mongoose.Types.ObjectId
    },
    VendorRatingId: {
        type: mongoose.Types.ObjectId
    },
}, {
    versionKey: false,
    collection: "LTLAccessorials"
});

const db = mongoose.connection.useDb(RATING_DB);
const LTLAccessorialCollection = db.model("LTLAccessorials", LTLAccessorialSchema);

export { LTLAccessorial, LTLAccessorialCollection, LTLAccessorialSchema };
