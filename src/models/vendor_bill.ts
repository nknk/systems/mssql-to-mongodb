import { STRING, BOOLEAN, INTEGER, DATE, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";

class VendorBill extends ModelWithTenantId {
    ApplyToDocumentId: number;
    VendorLocationId: number;
    UserId: number;

}

VendorBill.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    DocumentNumber: {
        type: STRING
    },
    DateCreated: {
        type: DATE
    },
    DocumentDate: {
        type: DATE
    },
    BillType: {
        type: INTEGER
    },
    Posted: {
        type: BOOLEAN
    },
    Exported: {
        type: BOOLEAN
    },
    PostDate: {
        type: DATE
    },
    ExportDate: {
        type: DATE
    },
    ApplyToDocumentId: {
        type: BIGINT
    },
    VendorLocationId: {
        type: BIGINT
    },
    UserId: {
        type: BIGINT
    }
}, {
    sequelize,
    modelName: "VendorBill",
    freezeTableName: true,
    tableName: "VendorBill",
    timestamps: false
});


const VendorBillSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    DocumentNumber: {
        type: String
    },
    DateCreated: {
        type: Date
    },
    DocumentDate: {
        type: Date
    },
    BillType: {
        type: Number
    },
    Posted: {
        type: Boolean
    },
    Exported: {
        type: Boolean
    },
    PostDate: {
        type: Date
    },
    ExportDate: {
        type: Date
    },
    ApplyToDocumentId: {
        type: mongoose.SchemaTypes.Mixed
    },
    VendorLocationId: {
        type: mongoose.Types.ObjectId
    },
    UserId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "VendorBills"
});

export interface IVendorBillDocument extends mongoose.Document {
    ApplyToDocumentId: string;
}

const db = mongoose.connection.useDb(FINANCE_DB);
const VendorBillCollection = db.model<IVendorBillDocument>(
    "VendorBills",
    VendorBillSchema
);

export { VendorBill, VendorBillCollection, VendorBillSchema };
