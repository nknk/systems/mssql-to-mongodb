import { STRING, INTEGER, DATE, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { IDENTITY_DB } from "../config";

class EntityLock extends ModelWithTenantId {
    LockUserId: number;
    EntityId: number;
    LockKey: string;
 }

EntityLock.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    LockKey:{
        type: STRING
    },
    LockUserId:{
        type: BIGINT
    },
    LockDateTime:{
        type: DATE
    },
    EntityId:{
        type: INTEGER
    }

}, {
    sequelize,
    modelName: "EntityLock",
    freezeTableName: true,
    tableName: "EntityLock",
    timestamps: false
});

const EntityLockSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    LockKey:{
        type: String
    },
    LockUserId:{
        type: mongoose.Types.ObjectId
    },
    LockDateTime:{
        type: Date
    },
    EntityId:{
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "EntityLocks"
});

const db = mongoose.connection.useDb(IDENTITY_DB);
const EntityLockCollection = db.model("EntityLocks", EntityLockSchema);

export { EntityLock, EntityLockCollection, EntityLockSchema };
