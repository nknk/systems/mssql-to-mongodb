import { DATE, STRING, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { RATING_DB } from "../config";

class LTLOverlengthRule extends ModelWithTenantId {
    ChargeCodeId: number;
    VendorRatingId: number;
}

LTLOverlengthRule.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    LowerLengthBound: {
        type: DECIMAL
    },
    UpperLengthBound: {
        type: DECIMAL
    },
    Charge: {
        type: DECIMAL,
    },
    ChargeCodeId: {
        type: BIGINT,
    },
    P44ChargeCode: {
        type: STRING,
    },
    EffectiveDate: {
        type: DATE,
    },
    VendorRatingId: {
        type: BIGINT,
    },
}, {
    sequelize,
    modelName: "LTLOverlengthRule",
    freezeTableName: true,
    tableName: "LTLOverlengthRule",
    timestamps: false
});


const LTLOverlengthRuleSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    },
    LowerLengthBound: {
        type: Number
    },
    UpperLengthBound: {
        type: Number
    },
    Charge: {
        type: Number
    },
    ChargeCodeId: {
        type: mongoose.Types.ObjectId
    },
    P44ChargeCode: {
        type: String
    },
    EffectiveDate: {
        type: Date
    },
    VendorRatingId: {
        type: mongoose.Types.ObjectId
    },
}, {
    versionKey: false,
    collection: "LTLOverlengthRules"
});

const db = mongoose.connection.useDb(RATING_DB);
const LTLOverlengthRuleCollection = db
    .model("LTLOverlengthRules", LTLOverlengthRuleSchema);

export { LTLOverlengthRule, LTLOverlengthRuleCollection, LTLOverlengthRuleSchema };
