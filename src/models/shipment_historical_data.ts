import { INTEGER, BOOLEAN, DATE, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { BaseModel } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class ShipmentHistoricalData extends BaseModel {
    ShipmentId: number;
}

ShipmentHistoricalData.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    ShipmentId: {
        type: BIGINT
    },
    WasInDispute: {
        type: BOOLEAN
    },
    OriginalEstimatedDeliveryDate: {
        type: DATE
    },
    OriginalEstimatedPickupDate: {
        type: DATE
    },
    WasInDisputeReason: {
        type: INTEGER
    }
}, {
    sequelize,
    modelName: "ShipmentHistoricalData",
    freezeTableName: true,
    tableName: "ShipmentHistoricalData",
    timestamps: false
});


const ShipmentHistoricalDataSchema = new mongoose.Schema({
    ShipmentId: {
        type: mongoose.Types.ObjectId
    },
    WasInDispute: {
        type: Boolean
    },
    OriginalEstimatedDeliveryDate: {
        type: Date
    },
    OriginalEstimatedPickupDate: {
        type: Date
    },
    WasInDisputeReason: {
        type: Number
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "ShipmentHistoricalDatas"
});


const db = mongoose.connection.useDb(OPERATIONS_DB);
const ShipmentHistoricalDataCollection = db.model(
    "ShipmentHistoricalDatas",
    ShipmentHistoricalDataSchema
);

export { ShipmentHistoricalData, ShipmentHistoricalDataCollection, ShipmentHistoricalDataSchema };
