import { STRING, INTEGER, BOOLEAN, DATE, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";
class CommissionPayment extends ModelWithTenantId {
    SalesRepresentativeId: number;
}

CommissionPayment.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    SalesRepresentativeId: {
        type: BIGINT,
        allowNull: false
    },
    ReferenceNumber: {
        type: STRING,
        allowNull: false
    },
    Type: {
        type: INTEGER,
        allowNull: false
    },
    DateCreated: {
        type: DATE,
        allowNull: false
    },
    AmountPaid: {
        type: DECIMAL,
        allowNull: false
    },
    PaymentDate: {
        type: DATE,
        allowNull: false
    },
    AdditionalEntityCommission: {
        type: BOOLEAN,
        allowNull: false
    },
}, {
    sequelize,
    modelName: "CommissionPayment",
    freezeTableName: true,
    tableName: "CommissionPayment",
    timestamps: false
});


const CommissionPaymentSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    },
    DateCreated: {
        type: Date
    },
    PaymentDate: {
        type: Date
    },
    SalesRepresentativeId: {
        type: mongoose.Types.ObjectId
    },
    ReferenceNumber: {
        type: String
    },
    Type: {
        type: Number
    },
    AmountPaid: {
        type: Number
    },
    AdditionalEntityCommission: {
        type: Boolean
    },
}, {
    versionKey: false,
    collection: "CommissionPayments"
});

const db = mongoose.connection.useDb(FINANCE_DB);
const CommissionPaymentCollection = db.model("CommissionPayments", CommissionPaymentSchema);

export { CommissionPayment, CommissionPaymentCollection, CommissionPaymentSchema };
