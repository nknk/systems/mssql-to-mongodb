import { STRING, INTEGER, BOOLEAN, DATE, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { REPORTING_DB } from "../config";

class ReportConfiguration extends ModelWithTenantId {

    ReportTemplateId: number;
    CustomerGroupId: number;
    VendorGroupId: number;
    UserId: number;
}

ReportConfiguration.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    ReportTemplateId: {
        type: BIGINT,
    },
    Name: {
        type: STRING,
    },
    Description: {
        type: STRING,
    },
    Visibility: {
        type: INTEGER,
    },
    SerializedCustomization: {
        type: STRING,
    },
    CustomerGroupId: {
        type: BIGINT,
    },
    VendorGroupId: {
        type: BIGINT,
    },
    ReadonlyCustomerGroupFilter: {
        type: BOOLEAN,
    },
    ReadonlyVendorGroupFilter: {
        type: BOOLEAN,
    },
    UserId: {
        type: BIGINT,
    },
    QlikConfiguration: {
        type: BOOLEAN,
    },
    LastRun: {
        type: DATE,
    },
}, {
    sequelize,
    modelName: "ReportConfiguration",
    freezeTableName: true,
    tableName: "ReportConfiguration",
    timestamps: false
});


const ReportConfigurationSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    },
    ReportTemplateId: {
        type: mongoose.Types.ObjectId
    },
    Name: {
        type: String
    },
    Description: {
        type: String
    },
    Visibility: {
        type: Number
    },
    SerializedCustomization: {
        type: String
    },
    CustomerGroupId: {
        type: mongoose.Types.ObjectId
    },
    VendorGroupId: {
        type: mongoose.Types.ObjectId
    },
    ReadonlyCustomerGroupFilter: {
        type: Boolean
    },
    ReadonlyVendorGroupFilter: {
        type: Boolean
    },
    UserId: {
        type: mongoose.Types.ObjectId
    },
    QlikConfiguration: {
        type: Boolean
    },
    LastRun: {
        type: Date
    },
}, {
    versionKey: false,
    collection: "ReportConfigurations"
});

const db = mongoose.connection.useDb(REPORTING_DB);
const ReportConfigurationCollection = db.model("ReportConfigurations", ReportConfigurationSchema);

export { ReportConfiguration, ReportConfigurationCollection, ReportConfigurationSchema };
