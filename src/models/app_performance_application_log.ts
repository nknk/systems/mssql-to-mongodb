import { STRING, DATE, INTEGER } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { BaseModel } from "./contracts/base_models";
import { AUDITLOG_DB } from "../config";
class AppPerformanceApplicationLog extends BaseModel { }

AppPerformanceApplicationLog.init({
    AppLogId: {
        type: INTEGER,
        primaryKey: true
    },
    AppLogDateTime: {
        type: DATE,
        allowNull: false
    },
    AppApplicationType: {
        type: STRING,
        allowNull: false
    },
    AppLogType: {
        type: STRING,
        allowNull: false
    },
    AppErrorType: {
        type: STRING,
        allowNull: false
    },
    AppTenant: {
        type: STRING,
        allowNull: false
    },
    AppUserName: {
        type: STRING,
        allowNull: false
    },
    AppBrowserType: {
        type: STRING,
        allowNull: false
    },
    AppPageAccessed: {
        type: STRING,
        allowNull: false
    },
    AppIPAddress: {
        type: STRING,
        allowNull: false
    },
    AppDescription: {
        type: STRING,
        allowNull: false
    },
    AppLogStatus: {
        type: STRING,
        allowNull: false
    },
}, {
    sequelize,
    modelName: "AppPerformanceApplicationLog",
    freezeTableName: true,
    tableName: "AppPerformanceApplicationLogs",
    timestamps: false
});

const AppPerformanceApplicationLogSchema = new mongoose.Schema({
    OldId: {
        type: Number
    },
    AppLogDateTime: {
        type: Date
    },
    AppApplicationType: {
        type: String
    },
    AppLogType: {
        type: String
    },
    AppErrorType: {
        type: String,
    },
    AppTenant: {
        type: String
    },
    AppUserName: {
        type: String
    },
    AppBrowserType: {
        type: String
    },
    AppPageAccessed: {
        type: String
    },
    AppIPAddress: {
        type: String
    },
    AppDescription: {
        type: String
    },
    AppLogStatus: {
        type: String
    },
}, {
    versionKey: false,
    collection: "AppPerformanceApplicationLogs"
});

const db = mongoose.connection.useDb(AUDITLOG_DB);
const AppPerformanceApplicationLogCollection = db.model(
    "AppPerformanceApplicationLogs",
    AppPerformanceApplicationLogSchema,
);

export {
    AppPerformanceApplicationLog,
    AppPerformanceApplicationLogCollection,
    AppPerformanceApplicationLogSchema,
};
