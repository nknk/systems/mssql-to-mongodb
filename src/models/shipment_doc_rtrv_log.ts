import { STRING, DATE, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class ShipmentDocRtrvLog extends ModelWithTenantId {
    CommunicationId: number;
    UserId: number;
}

ShipmentDocRtrvLog.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    CommunicationId: {
        type: BIGINT
    },
    UserId: {
        type: BIGINT
    },
    ShipmentNumber: {
        type: STRING
    },
    DateCreated: {
        type: DATE
    },
    ProNumber: {
        type: STRING
    },
    PodDate: {
        type: DATE
    },
    BolDate: {
        type: DATE
    },
    WniDate: {
        type: DATE
    },
    ExpirationDate: {
        type: DATE
    }
}, {
    sequelize,
    modelName: "ShipmentDocRtrvLog",
    freezeTableName: true,
    tableName: "ShipmentDocRtrvLog",
    timestamps: false
});


const ShipmentDocRtrvLogSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.SchemaTypes.ObjectId
    },
    CommunicationId: {
        type: mongoose.SchemaTypes.ObjectId
    },
    UserId: {
        type: mongoose.SchemaTypes.ObjectId
    },
    ShipmentNumber: {
        type: String
    },
    DateCreated: {
        type: Date
    },
    ProNumber: {
        type: String
    },
    PodDate: {
        type: Date
    },
    BolDate: {
        type: Date
    },
    WniDate: {
        type: Date
    },
    ExpirationDate: {
        type: Date
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "ShipmentDocRtrvLogs"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const ShipmentDocRtrvLogCollection = db.model(
    "ShipmentDocRtrvLogs",
    ShipmentDocRtrvLogSchema
);

export { ShipmentDocRtrvLog, ShipmentDocRtrvLogCollection, ShipmentDocRtrvLogSchema };
