import { FLOAT, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";

class VendorInsuranceLiabilityTier extends ModelWithTenantId {
    VendorRatingId: number;
}

VendorInsuranceLiabilityTier.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    VendorRatingId: {
        type: BIGINT
    },
    FreightClass: {
        type: FLOAT
    },
    NewGoodsLiabilityPerPound: {
        type: DECIMAL
    }
}, {
    sequelize,
    modelName: "VendorInsuranceLiabilityTier",
    freezeTableName: true,
    tableName: "VendorInsuranceLiabilityTier",
    timestamps: false
});


const VendorInsuranceLiabilityTierSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    VendorRatingId: {
        type: mongoose.Types.ObjectId
    },
    FreightClass: {
        type: Number
    },
    NewGoodsLiabilityPerPound: {
        type: Number
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "VendorInsuranceLiabilityTiers"
});


const db = mongoose.connection.useDb(FINANCE_DB);
const VendorInsuranceLiabilityTierCollection = db.model(
    "VendorInsuranceLiabilityTiers",
    VendorInsuranceLiabilityTierSchema
);

export {
    VendorInsuranceLiabilityTier,
    VendorInsuranceLiabilityTierCollection,
    VendorInsuranceLiabilityTierSchema,
};
