import { STRING, BOOLEAN, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { REGISTRY_DB } from "../config";

class Prefix extends ModelWithTenantId {}

Prefix.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    Code: {
        type: STRING,
        allowNull: false
    },
    Description: {
        type: STRING,
        allowNull: false
    },
    Active: {
        type: BOOLEAN,
        allowNull: false
    }
}, {
    sequelize,
    modelName: "Prefix",
    freezeTableName: true,
    tableName: "Prefix",
    timestamps: false
});


const PrefixSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    Code: {
        type: String
    },
    Description: {
        type: String
    },
    Active: {
        type: Boolean
    },
    OldId: {
        type: Number,
    },
}, {
    versionKey: false,
    collection: "Prefixes"
});

const db = mongoose.connection.useDb(REGISTRY_DB);
const PrefixCollection = db.model("Prefixes", PrefixSchema);

export { Prefix, PrefixCollection, PrefixSchema };
