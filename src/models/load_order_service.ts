import { BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";
class LoadOrderService extends ModelWithTenantId {
    LoadOrderId: number;
    ServiceId: number;
}

LoadOrderService.init({
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    ServiceId:{
        type: BIGINT
    },
    LoadOrderId:{
        type: BIGINT
    },
}, {
    sequelize,
    modelName: "LoadOrderService",
    freezeTableName: true,
    tableName: "LoadOrderService",
    timestamps: false
});

const LoadOrderServiceSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    ServiceId:{
        type: mongoose.Types.ObjectId
    },
    LoadOrderId:{
        type: mongoose.Types.ObjectId
    }
}, {
    versionKey: false,
    collection: "LoadOrderServices"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const LoadOrderServiceCollection = db.model("LoadOrderServices", LoadOrderServiceSchema);

export { LoadOrderService, LoadOrderServiceCollection, LoadOrderServiceSchema };
