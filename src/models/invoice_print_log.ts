import { STRING, DATE, BOOLEAN, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";

class InvoicePrintLog extends ModelWithTenantId {
    InvoiceId: number;
}

InvoicePrintLog.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    InvoiceId: {
        type: BIGINT
    },
    LogDate: {
        type: DATE
    },
    UserLogon: {
        type: STRING
    },
    UserFirstName: {
        type: STRING
    },
    UserLastName: {
        type: STRING
    },
    UserDefaultCustomerNumber: {
        type: STRING
    },
    UserDefaultCustomerName: {
        type: STRING
    },
    TenantEmployee: {
        type: BOOLEAN
    }
}, {
    sequelize,
    modelName: "InvoicePrintLog",
    freezeTableName: true,
    tableName: "InvoicePrintLog",
    timestamps: false
});

const InvoicePrintLogSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    InvoiceId: {
        type: mongoose.Types.ObjectId
    },
    LogDate: {
        type: Date
    },
    UserLogon: {
        type: String
    },
    UserFirstName: {
        type: String
    },
    UserLastName: {
        type: String
    },
    UserDefaultCustomerNumber: {
        type: String
    },
    UserDefaultCustomerName: {
        type: String
    },
    TenantEmployee: {
        type: Boolean
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "InvoicePrintLogs"
});

const db = mongoose.connection.useDb(FINANCE_DB);
const InvoicePrintLogCollection = db.model("InvoicePrintLogs", InvoicePrintLogSchema);

export { InvoicePrintLog, InvoicePrintLogCollection, InvoicePrintLogSchema };
