import { STRING, BOOLEAN, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class ShipmentVendor extends ModelWithTenantId {
    VendorId: number;
    ShipmentId: number;
    FailureCodeId: number;    
}

ShipmentVendor.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    Primary: {
        type: BOOLEAN
    },
    VendorId: {
        type: BIGINT
    },
    ShipmentId: {
        type: BIGINT
    },
    FailureCodeId: {
        type: BIGINT
    },
    FailureComments: {
        type: STRING
    },
    ProNumber: {
        type: STRING
    }     
}, {
    sequelize,
    modelName: "ShipmentVendor",
    freezeTableName: true,
    tableName: "ShipmentVendor",
    timestamps: false
});


const ShipmentVendorSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    Primary: {
        type: Boolean
    },
    VendorId: {
        type: mongoose.Types.ObjectId
    },
    ShipmentId: {
        type: mongoose.Types.ObjectId
    },
    FailureCodeId: {
        type: mongoose.Types.ObjectId
    },
    FailureComments: {
        type: String
    },
    ProNumber: {
        type: String
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "ShipmentVendors"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const ShipmentVendorCollection = db.model("ShipmentVendors", ShipmentVendorSchema);

export { ShipmentVendor, ShipmentVendorCollection, ShipmentVendorSchema };
