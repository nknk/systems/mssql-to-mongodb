import { STRING, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { IDENTITY_DB } from "../config";

class TenantLocation extends ModelWithTenantId {
    CountryId: number;
}

TenantLocation.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    Street1: {
        type: STRING
    },
    Street2: {
        type: STRING
    },
    City: {
        type: STRING
    },
    State: {
        type: STRING
    },
    CountryId: {
        type: BIGINT
    },
    PostalCode: {
        type: STRING
    }     
}, {
    sequelize,
    modelName: "TenantLocation",
    freezeTableName: true,
    tableName: "TenantLocation",
    timestamps: false
});


const TenantLocationSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    Street1: {
        type: String
    },
    Street2: {
        type: String
    },
    City: {
        type: String
    },
    State: {
        type: String
    },
    CountryId: {
        type: mongoose.Types.ObjectId
    },
    PostalCode: {
        type: String
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "TenantLocations"
});

const db = mongoose.connection.useDb(IDENTITY_DB);
const TenantLocationCollection = db.model("TenantLocations", TenantLocationSchema);

export { TenantLocation, TenantLocationCollection, TenantLocationSchema };
