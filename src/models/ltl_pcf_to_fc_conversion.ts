import { DATE, STRING, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { RATING_DB } from "../config";

class LTLPcfToFcConversion extends ModelWithTenantId {
    VendorRatingId: number;
}

LTLPcfToFcConversion.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    EffectiveDate: {
        type: DATE,
    },
    VendorRatingId: {
        type: BIGINT,
    },
    ConversionTable: {
        type: STRING,
    },
}, {
    sequelize,
    modelName: "LTLPcfToFcConversion",
    freezeTableName: true,
    tableName: "LTLPcfToFcConversion",
    timestamps: false
});


const LTLPcfToFcConversionSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    },
    EffectiveDate: {
        type: Date
    },
    VendorRatingId: {
        type: mongoose.Types.ObjectId
    },
    ConversionTable: {
        type: String
    },
}, {
    versionKey: false,
    collection: "LTLPcfToFcConversions"
});

const db = mongoose.connection.useDb(RATING_DB);
const LTLPcfToFcConversionCollection = db
.model("LTLPcfToFcConversions", LTLPcfToFcConversionSchema);

export { LTLPcfToFcConversion, LTLPcfToFcConversionCollection, LTLPcfToFcConversionSchema };
