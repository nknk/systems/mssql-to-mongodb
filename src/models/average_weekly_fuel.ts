import { DATE, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";

class AverageWeeklyFuel extends ModelWithTenantId {
    ChargeCodeId: number;
}

AverageWeeklyFuel.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    DateCreated: {
        type: DATE,
        allowNull: false
    },
    EffectiveDate: {
        type: DATE,
        allowNull: false
    },
    ChargeCodeId: {
        type: BIGINT,
        allowNull: false
    },
    EastCoastCost: {
        type: DECIMAL,
        allowNull: false
    },
    NewEnglandCost: {
        type: DECIMAL,
        allowNull: false
    },
    CentralAtlanticCost: {
        type: DECIMAL,
        allowNull: false
    },
    LowerAtlanticCost: {
        type: DECIMAL,
        allowNull: false
    },
    MidwestCost: {
        type: DECIMAL,
        allowNull: false
    },
    GulfCoastCost: {
        type: DECIMAL,
        allowNull: false
    },
    RockyMountainCost: {
        type: DECIMAL,
        allowNull: false
    },
    WestCoastCost: {
        type: DECIMAL,
        allowNull: false
    },
    WestCoastLessCaliforniaCost: {
        type: DECIMAL,
        allowNull: false
    },
    CaliforniaCost: {
        type: DECIMAL,
        allowNull: false
    },
    NationalCost: {
        type: DECIMAL,
        allowNull: false
    },
}, {
    sequelize,
    modelName: "AverageWeeklyFuel",
    freezeTableName: true,
    tableName: "AverageWeeklyFuel",
    timestamps: false
});


const AverageWeeklyFuelSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    },
    DateCreated: {
        type: Date,
    },
    EffectiveDate: {
        type: Date,
    },
    ChargeCodeId: {
        type: mongoose.Types.ObjectId
    },
    EastCoastCost: {
        type: Number
    },
    NewEnglandCost: {
        type: Number
    },
    CentralAtlanticCost: {
        type: Number
    },
    LowerAtlanticCost: {
        type: Number
    },
    MidwestCost: {
        type: Number
    },
    GulfCoastCost: {
        type: Number
    },
    RockyMountainCost: {
        type: Number
    },
    WestCoastCost: {
        type: Number
    },
    WestCoastLessCaliforniaCost: {
        type: Number
    },
    CaliforniaCost: {
        type: Number
    },
    NationalCost: {
        type: Number
    },
}, {
    versionKey: false,
    collection: "AverageWeeklyFuels"
});

const db = mongoose.connection.useDb(FINANCE_DB);
const AverageWeeklyFuelCollection = db.model("AverageWeeklyFuels", AverageWeeklyFuelSchema);

export { AverageWeeklyFuel, AverageWeeklyFuelCollection, AverageWeeklyFuelSchema };
