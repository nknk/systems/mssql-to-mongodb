import { STRING, INTEGER, DATE, BOOLEAN, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";
class DatLoadBoardAssetPosting extends ModelWithTenantId {
    UserId: number;
}

DatLoadBoardAssetPosting.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    IdNumber:{
        type: STRING
    },
    DateCreated:{
        type: DATE
    },
    ExpirationDate:{
        type: DATE
    },
    AssetId:{
        type: STRING
    },
    HazardousMaterial:{
        type: BOOLEAN
    },
    UserId:{
        type: BIGINT
    },
    BaseRate:{
        type: DECIMAL
    },
    RateType:{
        type: INTEGER
    },
    Mileage:{
        type: INTEGER
    },
    SerializedComments: {
        type: STRING,
    },
}, {
    sequelize,
    modelName: "DatLoadBoardAssetPosting",
    freezeTableName: true,
    tableName: "DatLoadBoardAssetPosting",
    timestamps: false
});


const DatLoadBoardAssetPostingSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    IdNumber:{
        type: String
    },
    DateCreated:{
        type: Date
    },
    ExpirationDate:{
        type: Date
    },
    AssetId:{
        type: String
    },
    HazardousMaterial:{
        type: Boolean
    },
    UserId:{
        type: mongoose.Types.ObjectId
    },
    BaseRate:{
        type: Number
    },
    RateType:{
        type: Number
    },
    Mileage:{
        type: Number
    },
    SerializedComments: {
        type: String,
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "DatLoadBoardAssetPostings"
});


const db = mongoose.connection.useDb(OPERATIONS_DB);
const DatLoadBoardAssetPostingCollection = db.model("DatLoadBoardAssetPostings", DatLoadBoardAssetPostingSchema);

export {
    DatLoadBoardAssetPosting,
    DatLoadBoardAssetPostingCollection,
    DatLoadBoardAssetPostingSchema
};
