import { INTEGER, STRING, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { RATING_DB } from "../config";

class SmallPackagingMap extends ModelWithTenantId {
    PackageTypeId: number;
}

SmallPackagingMap.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    SmallPackageEngine: {
        type: INTEGER
    },
    PackageTypeId: {
        type: BIGINT
    },
    SmallPackEngineType: {
        type: STRING
    },
    RequiredLength: {
        type: DECIMAL
    },
    RequiredWidth: {
        type: DECIMAL
    },
    RequiredHeight: {
        type: DECIMAL
    }    
}, {
    sequelize,
    modelName: "SmallPackagingMap",
    freezeTableName: true,
    tableName: "SmallPackagingMap",
    timestamps: false
});


const SmallPackagingMapSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    SmallPackageEngine: {
        type: Number
    },
    PackageTypeId: {
        type: mongoose.Types.ObjectId
    },
    SmallPackEngineType: {
        type: String
    },
    RequiredLength: {
        type: Number
    },
    RequiredWidth: {
        type: Number
    },
    RequiredHeight: {
        type: Number
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "SmallPackagingMaps"
});

const db = mongoose.connection.useDb(RATING_DB);
const SmallPackagingMapCollection = db.model("SmallPackagingMaps", SmallPackagingMapSchema);

export { SmallPackagingMap, SmallPackagingMapCollection, SmallPackagingMapSchema };
