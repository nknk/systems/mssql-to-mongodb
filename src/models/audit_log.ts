import { STRING, DATE, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { AUDITLOG_DB } from "../config";

class AuditLog extends ModelWithTenantId {
    UserId: number;
    EntityId: number;
    EntityCode: string;
}

AuditLog.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    EntityCode: {
        type: STRING,
    },
    EntityId: {
        type: BIGINT,
    },
    Description: {
        type: STRING,
    },
    LogDateTime: {
        type: DATE,
    },
    UserId: {
        type: BIGINT,
    }
}, {
    sequelize,
    modelName: "AuditLog",
    freezeTableName: true,
    tableName: "AuditLog",
    timestamps: false
});

const AuditLogSchema = new mongoose.Schema({
    OldId: {
        type: Number
    },
    TenantId: {
        type: mongoose.SchemaTypes.ObjectId,
    },
    EntityCode: {
        type: String,
    },
    EntityId: {
        type: mongoose.SchemaTypes.ObjectId,
    },
    Description: {
        type: String,
    },
    LogDateTime: {
        type: Date,
    },
    UserId: {
        type: mongoose.SchemaTypes.ObjectId,
    }
}, {
    versionKey: false,
    collection: "AuditLogs"
});

const db = mongoose.connection.useDb(AUDITLOG_DB);
const AuditLogCollection = db.model("AuditLogs", AuditLogSchema);

export { AuditLog, AuditLogCollection, AuditLogSchema };
