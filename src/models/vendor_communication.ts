import { STRING, INTEGER, BOOLEAN, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";
class VendorCommunication extends ModelWithTenantId {
    VendorId: number;
}

VendorCommunication.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    ConnectGuid: {
        type: STRING,
    },
    EnableWebServiceAPIAccess: {
        type: BOOLEAN,
    },
    VendorId: {
        type: BIGINT,
    },
    EdiEnabled: {
        type: BOOLEAN,
    },
    EdiCode: {
        type: STRING,
    },
    EdiVANUrl: {
        type: STRING,
    },
    SecureEdiVAN: {
        type: BOOLEAN,
    },
    EdiVANUsername: {
        type: STRING,
    },
    EdiVANPassword: {
        type: STRING,
    },
    EdiVANDefaultFolder: {
        type: STRING,
    },
    FtpEnabled: {
        type: BOOLEAN,
    },
    FtpUrl: {
        type: STRING,
    },
    SecureFtp: {
        type: BOOLEAN,
    },
    FtpUsername: {
        type: STRING,
    },
    FtpPassword: {
        type: STRING,
    },
    FtpDefaultFolder: {
        type: STRING,
    },
    EdiVANEnvelopePath: {
        type: STRING,
    },
    UseSelectiveDropOff: {
        type: BOOLEAN,
    },
    Pickup997: {
        type: BOOLEAN,
    },
    Pickup214: {
        type: BOOLEAN,
    },
    Pickup210: {
        type: BOOLEAN,
    },
    AcknowledgePickup214: {
        type: BOOLEAN,
    },
    AcknowledgePickup210: {
        type: BOOLEAN,
    },
    FtpDeleteFileAfterPickup: {
        type: BOOLEAN,
    },
    EdiDeleteFileAfterPickup: {
        type: BOOLEAN,
    },
    LoadTenderExpAllowance: {
        type: INTEGER,
    },
    Pickup990: {
        type: BOOLEAN,
    },
    AcknowledgePickup990: {
        type: BOOLEAN,
    },
    DropOff204: {
        type: BOOLEAN,
    },
    ImgRtrvEnabled: {
        type: BOOLEAN,
    },
    CarrierIntegrationEngine: {
        type: STRING,
    },
    CarrierIntegrationUsername: {
        type: STRING,
    },
    CarrierIntegrationPassword: {
        type: STRING,
    },
    ShipmentDispatchEnabled: {
        type: BOOLEAN,
    },
    DisableGuaranteedServiceDispatch: {
        type: BOOLEAN,
    },
    SMC3EvaEnabled: {
        type: BOOLEAN,
    },
    IsInSMC3Testing: {
        type: BOOLEAN,
    },
    SMC3TestAccountToken: {
        type: STRING,
    },
    SMC3ProductionAccountToken: {
        type: STRING,
    },
    SMC3EvaSupportsDispatch: {
        type: BOOLEAN,
    },
    SMC3EvaSupportsDocumentRetrieval: {
        type: BOOLEAN,
    },
    SMC3EvaSupportsTracking: {
        type: BOOLEAN,
    },
    SSHFtp: {
        type: BOOLEAN,
    },
    Project44Enabled: {
        type: BOOLEAN,
    },
    Project44TrackingEnabled: {
        type: BOOLEAN,
    },
    Project44DispatchEnabled: {
        type: BOOLEAN,
    },
}, {
    sequelize,
    modelName: "VendorCommunication",
    freezeTableName: true,
    tableName: "VendorCommunication",
    timestamps: false
});


const VendorCommunicationSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    },
    ConnectGuid: {
        type: String,
    },
    EnableWebServiceAPIAccess: {
        type: Boolean,
    },
    VendorId: {
        type: mongoose.Types.ObjectId,
    },
    EdiEnabled: {
        type: Boolean,
    },
    EdiCode: {
        type: String,
    },
    EdiVANUrl: {
        type: String,
    },
    SecureEdiVAN: {
        type: Boolean,
    },
    EdiVANUsername: {
        type: String,
    },
    EdiVANPassword: {
        type: String,
    },
    EdiVANDefaultFolder: {
        type: String,
    },
    FtpEnabled: {
        type: Boolean,
    },
    FtpUrl: {
        type: String,
    },
    SecureFtp: {
        type: Boolean,
    },
    FtpUsername: {
        type: String,
    },
    FtpPassword: {
        type: String,
    },
    FtpDefaultFolder: {
        type: String,
    },
    EdiVANEnvelopePath: {
        type: String,
    },
    UseSelectiveDropOff: {
        type: Boolean,
    },
    Pickup997: {
        type: Boolean,
    },
    Pickup214: {
        type: Boolean,
    },
    Pickup210: {
        type: Boolean,
    },
    AcknowledgePickup214: {
        type: Boolean,
    },
    AcknowledgePickup210: {
        type: Boolean,
    },
    FtpDeleteFileAfterPickup: {
        type: Boolean,
    },
    EdiDeleteFileAfterPickup: {
        type: Boolean,
    },
    LoadTenderExpAllowance: {
        type: Number,
    },
    Pickup990: {
        type: Boolean,
    },
    AcknowledgePickup990: {
        type: Boolean,
    },
    DropOff204: {
        type: Boolean,
    },
    ImgRtrvEnabled: {
        type: Boolean,
    },
    CarrierIntegrationEngine: {
        type: String,
    },
    CarrierIntegrationUsername: {
        type: String,
    },
    CarrierIntegrationPassword: {
        type: String,
    },
    ShipmentDispatchEnabled: {
        type: Boolean,
    },
    DisableGuaranteedServiceDispatch: {
        type: Boolean,
    },
    SMC3EvaEnabled: {
        type: Boolean,
    },
    IsInSMC3Testing: {
        type: Boolean,
    },
    SMC3TestAccountToken: {
        type: String,
    },
    SMC3ProductionAccountToken: {
        type: String,
    },
    SMC3EvaSupportsDispatch: {
        type: Boolean,
    },
    SMC3EvaSupportsDocumentRetrieval: {
        type: Boolean,
    },
    SMC3EvaSupportsTracking: {
        type: Boolean,
    },
    SSHFtp: {
        type: Boolean,
    },
    Project44Enabled: {
        type: Boolean,
    },
    Project44TrackingEnabled: {
        type: Boolean,
    },
    Project44DispatchEnabled: {
        type: Boolean,
    },
}, {
    versionKey: false,
    collection: "VendorCommunications"
});

declare interface IVendorCommunicationCollection extends mongoose.Document {
    VendorId: string;
}

const db = mongoose.connection.useDb(FINANCE_DB);
const VendorCommunicationCollection = db
    .model<IVendorCommunicationCollection>(
        "VendorCommunications",
        VendorCommunicationSchema
    );

export {
    VendorCommunication,
    VendorCommunicationCollection,
    VendorCommunicationSchema
};
