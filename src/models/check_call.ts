import { STRING, DATE, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class CheckCall extends ModelWithTenantId {
    UserId: number;
    ShipmentId: number;
}

CheckCall.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    ShipmentId: {
        type: BIGINT,
    },
    UserId: {
        type: BIGINT,
    },
    CallDate: {
        type: DATE,
    },
    CallNotes: {
        type: STRING,
    },
    EdiStatusCode: {
        type: STRING,
    },
    EventDate: {
        type: DATE,
    },
}, {
    sequelize,
    modelName: "CheckCall",
    freezeTableName: true,
    tableName: "CheckCall",
    timestamps: false
});


const CheckCallSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    ShipmentId: {
        type: mongoose.Types.ObjectId,
    },
    UserId: {
        type: mongoose.Types.ObjectId,
    },
    CallDate: {
        type: Date,
    },
    CallNotes: {
        type: String,
    },
    EdiStatusCode: {
        type: String,
    },
    EventDate: {
        type: Date,
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "CheckCalls"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const CheckCallCollection = db.model("CheckCalls", CheckCallSchema);

export {
    CheckCall,
    CheckCallCollection,
    CheckCallSchema
};
