import { STRING, DATE, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { BaseModel } from "./contracts/base_models";
import { AUDITLOG_DB } from "../config";
class AdminAuditLog extends BaseModel {
    AdminUserId: number;
    EntityId: number;
}

AdminAuditLog.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    EntityCode: {
        type: STRING,
        allowNull: false
    },
    EntityId: {
        type: STRING,
        allowNull: false
    },
    Description: {
        type: STRING,
        allowNull: false
    },
    LogDateTime: {
        type: DATE,
        allowNull: false
    },
    AdminUserId: {
        type: BIGINT,
        allowNull: false
    },
}, {
    sequelize,
    modelName: "AdminAuditLog",
    freezeTableName: true,
    tableName: "AdminAuditLog",
    timestamps: false
});

const AdminAuditLogSchema = new mongoose.Schema({
    OldId: {
        type: Number
    },
    EntityCode: {
        type: String
    },
    EntityId: {
        type: mongoose.Types.ObjectId
    },
    Description: {
        type: String
    },
    LogDateTime: {
        type: Date
    },
    AdminUserId: {
        type: mongoose.Types.ObjectId
    },
}, {
    versionKey: false,
    collection: "AdminAuditLogs"
});

const db = mongoose.connection.useDb(AUDITLOG_DB);
const AdminAuditLogCollection = db.model("AdminAuditLogs", AdminAuditLogSchema);

export { AdminAuditLog, AdminAuditLogCollection, AdminAuditLogSchema };
