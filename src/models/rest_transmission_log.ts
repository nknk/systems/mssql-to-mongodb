import { STRING, INTEGER, DATE, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { BaseModel } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";
class RestTransmissionLog extends BaseModel { 
    UserId: number;
}

RestTransmissionLog.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    Source: {
        type: INTEGER
    },
    LogDateTime: {
        type: DATE
    },
    AuthenticationData: {
        type: STRING
    },
    UserId: {
        type: INTEGER
    },
    StreamData: {
        type: STRING
    },
}, {
    sequelize,
    modelName: "RestTransmissionLog",
    freezeTableName: true,
    tableName: "RestTransmissionLog",
    timestamps: false
});

const RestTransmissionLogSchema = new mongoose.Schema({
    OldId: {
        type: Number
    },
    Source: {
        type: Number
    },
    LogDateTime: {
        type: Date
    },
    AuthenticationData: {
        type: String
    },
    UserId: {
        type: mongoose.Types.ObjectId
    },
    StreamData: {
        type: String
    },
}, {
    versionKey: false,
    collection: "RestTransmissionLogs"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const RestTransmissionLogCollection = db.model("RestTransmissionLogs", RestTransmissionLogSchema);

export { RestTransmissionLog, RestTransmissionLogCollection, RestTransmissionLogSchema };
