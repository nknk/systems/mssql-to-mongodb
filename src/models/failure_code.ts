import { STRING, BOOLEAN, INTEGER, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { REGISTRY_DB } from "../config";

class FailureCode extends ModelWithTenantId { }

FailureCode.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    Code: {
        type: STRING,
        allowNull: false
    },
    Description: {
        type: STRING,
        allowNull: false
    },
    Active: {
        type: BOOLEAN,
        allowNull: false
    },
    Category: {
        type: INTEGER,
        allowNull: false
    },
    ExcludeOnScorecard: {
        type: BOOLEAN,
        allowNull: false
    }
}, {
    sequelize,
    modelName: "FailureCode",
    freezeTableName: true,
    tableName: "FailureCode",
    timestamps: false
});


const FailureCodeSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    Code: {
        type: String
    },
    Description: {
        type: String
    },
    Active: {
        type: Boolean
    },
    OldId: {
        type: Number
    },
    Category: {
        type: Number
    },
    ExcludeOnScorecard: {
        type: Boolean
    }
}, {
    versionKey: false,
    collection: "FailureCodes"
});

const db = mongoose.connection.useDb(REGISTRY_DB);
const FailureCodeCollection = db.model("FailureCodes", FailureCodeSchema);

export { FailureCode, FailureCodeSchema, FailureCodeCollection };
