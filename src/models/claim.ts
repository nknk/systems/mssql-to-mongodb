import { STRING, BOOLEAN, INTEGER, DATE, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class Claim extends ModelWithTenantId {
    UserId: number;
    CustomerId: number;
}

Claim.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    ClaimNumber: {
        type: STRING,
        allowNull: false
    },
    DateCreated: {
        type: DATE,
        allowNull: false
    },
    Status: {
        type: INTEGER,
        allowNull: false
    },
    ClaimDetail: {
        type: STRING,
        allowNull: false
    },
    UserId: {
        type: BIGINT,
        allowNull: false
    },
    CustomerId: {
        type: BIGINT,
        allowNull: false
    },
    ClaimDate: {
        type: DATE,
        allowNull: false
    },
    ClaimType: {
        type: INTEGER,
        allowNull: false
    },
    IsRepairable: {
        type: BOOLEAN,
        allowNull: false
    },
    EstimatedRepairCost: {
        type: DECIMAL,
        allowNull: false
    },
    AmountClaimed: {
        type: DECIMAL,
        allowNull: false
    },
    AmountClaimedType: {
        type: INTEGER,
        allowNull: false
    },
    ClaimantReferenceNumber: {
        type: STRING,
        allowNull: false
    },
    CheckNumber: {
        type: STRING,
        allowNull: false
    },
    CheckAmount: {
        type: DECIMAL,
        allowNull: false
    },
    DateLpAcctToPayCarrier: {
        type: DATE,
        allowNull: false
    },
    Acknowledged: {
        type: DATE,
        allowNull: false
    }
}, {
    sequelize,
    modelName: "Claim",
    freezeTableName: true,
    tableName: "Claim",
    timestamps: false
});


const ClaimSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    ClaimNumber: {
        type: String,
    },
    DateCreated: {
        type: Date,
    },
    Status: {
        type: Number,
    },
    ClaimDetail: {
        type: String,
    },
    UserId: {
        type: mongoose.Types.ObjectId,
    },
    CustomerId: {
        type: mongoose.Types.ObjectId,
    },
    ClaimDate: {
        type: Date,
    },
    ClaimType: {
        type: Number,
    },
    IsRepairable: {
        type: Boolean,
    },
    EstimatedRepairCost: {
        type: Number,
    },
    AmountClaimed: {
        type: Number,
    },
    AmountClaimedType: {
        type: Number,
    },
    ClaimantReferenceNumber: {
        type: String,
    },
    CheckNumber: {
        type: String,
    },
    CheckAmount: {
        type: Number,
    },
    DateLpAcctToPayCarrier: {
        type: Date,
    },
    Acknowledged: {
        type: Date,
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "Claims"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const ClaimCollection = db.model("Claims", ClaimSchema);

export { Claim, ClaimCollection, ClaimSchema };
