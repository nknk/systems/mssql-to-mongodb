import { STRING, INTEGER } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import {BaseModel } from "./contracts/base_models";
import {  OPERATIONS_DB } from "../config";

class RefMileageEngine extends BaseModel {
   MileageEngineIndex: number; 
}

RefMileageEngine.init({
 
    MileageEngineIndex: {
        type: INTEGER,
        primaryKey: true
    },
    MileageEngineText: {
        type: STRING
    },
  
}, {
    sequelize,
    modelName: "RefMileageEngine",
    freezeTableName: true,
    tableName: "RefMileageEngine",
    timestamps: false
});


const RefMileageEngineSchema = new mongoose.Schema({
    MileageEngineIndex: {
        type: Number,
    },
    MileageEngineText: {
        type:String
    },
}, {
    versionKey: false,
    collection: "RefMileageEngines"
});


const db = mongoose.connection.useDb(OPERATIONS_DB);
const RefMileageEngineCollection = db.model(
    "RefMileageEngines",
    RefMileageEngineSchema
);

export { RefMileageEngine, RefMileageEngineCollection, RefMileageEngineSchema };
