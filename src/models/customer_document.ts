import { STRING, DATE, BOOLEAN, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";

class CustomerDocument extends ModelWithTenantId {
    CustomerId: number;
    DocumentTagId: number;
}

CustomerDocument.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    CustomerId:{
        type:BIGINT
    },
    
    LocationPath:{
        type:STRING
    },
    
    DocumentTagId:{
        type:BIGINT
    },
    
    Name:{
        type:STRING
    },
    
    Description:{
        type:STRING
    },
    
    IsInternal:{
        type:BOOLEAN
    },
    DateCreated:{
        type:DATE
    }        
}, {
    sequelize,
    modelName: "CustomerDocument",
    freezeTableName: true,
    tableName: "CustomerDocument",
    timestamps: false
});


const CustomerDocumentSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    CustomerId:{
        type:mongoose.Types.ObjectId
    },
    
    LocationPath:{
        type:String
    },
    
    DocumentTagId:{
        type:mongoose.Types.ObjectId
    },
    
    Name:{
        type:String
    },
    
    Description:{
        type:String
    },
    
    IsInternal:{
        type:Boolean
    },
    
    DateCreated:{
        type:Date
    },

    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "CustomerDocuments"
});


const db = mongoose.connection.useDb(FINANCE_DB);
const CustomerDocumentCollection = db.model("CustomerDocuments", CustomerDocumentSchema);

export {
    CustomerDocument,
    CustomerDocumentCollection,
    CustomerDocumentSchema
};
