import { STRING, BOOLEAN, DATE, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";

class PendingVendorInsurance extends ModelWithTenantId {
    PendingVendorId: number;
    InsuranceTypeId: number;
}

PendingVendorInsurance.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    CarrierName: {
        type: STRING,
        allowNull: false
    },
    Required: {
        type: BOOLEAN,
        allowNull: false
    },
    PolicyNumber: {
        type: STRING,
        allowNull: false
    },
    EffectiveDate: {
        type: DATE,
        allowNull: false
    },
    ExpirationDate: {
        type: DATE,
        allowNull: false
    },
    CertificateHolder: {
        type: STRING,
        allowNull: false
    },
    CoverageAmount: {
        type: DECIMAL,
        allowNull: false
    },
    SpecialInstruction: {
        type: STRING,
        allowNull: false
    },
    InsuranceTypeId: {
        type: BIGINT,
        allowNull: false
    },
    PendingVendorId: {
        type: BIGINT,
        allowNull: false
    },
}, {
    sequelize,
    modelName: "PendingVendorInsurance",
    freezeTableName: true,
    tableName: "PendingVendorInsurance",
    timestamps: false
});


const PendingVendorInsuranceSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    },
    CarrierName: {
        type: String

    },
    Required: {
        type: Boolean
    },
    PolicyNumber: {
        type: String
    },
    EffectiveDate: {
        type: Date
    },
    ExpirationDate: {
        type: Date
    },
    CertificateHolder: {
        type: String
    },
    CoverageAmount: {
        type: Number
    },
    SpecialInstruction: {
        type: String
    },
    InsuranceTypeId: {
        type: mongoose.Types.ObjectId
    },
    PendingVendorId: {
        type: mongoose.Types.ObjectId
    },
}, {
    versionKey: false,
    collection: "PendingVendorInsurances"
});

const db = mongoose.connection.useDb(FINANCE_DB);
const PendingVendorInsuranceCollection = db
    .model("PendingVendorInsurances",
        PendingVendorInsuranceSchema);

export {
    PendingVendorInsurance,
    PendingVendorInsuranceCollection,
    PendingVendorInsuranceSchema
};
