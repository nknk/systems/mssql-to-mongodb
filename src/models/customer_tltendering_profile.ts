import { INTEGER, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";
class CustomerTLTenderingProfile extends ModelWithTenantId {
    CustomerId: number;
}

CustomerTLTenderingProfile.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    CustomerId:{
        type: BIGINT
    },
    MaxWaitTime:{
        type: INTEGER
    },
}, {
    sequelize,
    modelName: "CustomerTLTenderingProfile",
    freezeTableName: true,
    tableName: "CustomerTLTenderingProfile",
    timestamps: false
});


const CustomerTLTenderingProfileSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    CustomerId:{
        type: mongoose.Types.ObjectId
    },
    MaxWaitTime:{
        type: Number
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "CustomerTLTenderingProfiles"
});


const db = mongoose.connection.useDb(FINANCE_DB);
const CustomerTLTenderingProfileCollection = db.model("CustomerTLTenderingProfiles", CustomerTLTenderingProfileSchema);

export {
    CustomerTLTenderingProfile,
    CustomerTLTenderingProfileCollection,
    CustomerTLTenderingProfileSchema
};
