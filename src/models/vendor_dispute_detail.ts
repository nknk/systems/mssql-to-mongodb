import { BIGINT, DECIMAL, INTEGER } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";

class VendorDisputeDetail extends ModelWithTenantId {
    DisputedId: number;
    ChargeLineId: number;
    ChargeLineIdType: number;
}

VendorDisputeDetail.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    DisputedId: {
        type: BIGINT
    },
    ChargeLineId: {
        type: BIGINT
    },
    OriginalChargeAmount: {
        type: DECIMAL
    },
    RevisedChargeAmount: {
        type: DECIMAL
    },
    ChargeLineIdType: {
        type: INTEGER
    }
}, {
    sequelize,
    modelName: "VendorDisputeDetail",
    freezeTableName: true,
    tableName: "VendorDisputeDetail",
    timestamps: false
});


const VendorDisputeDetailSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    DisputedId: {
        type: mongoose.Types.ObjectId,
    },
    ChargeLineId: {
        type: mongoose.Types.ObjectId,
    },
    OriginalChargeAmount: {
        type: Number,
    },
    RevisedChargeAmount: {
        type: Number,
    },
    ChargeLineIdType: {
        type: Number,
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "VendorDisputeDetails"
});


const db = mongoose.connection.useDb(FINANCE_DB);
const VendorDisputeDetailCollection = db.model(
    "VendorDisputeDetails",
    VendorDisputeDetailSchema
);

export {
    VendorDisputeDetail,
    VendorDisputeDetailCollection,
    VendorDisputeDetailSchema
};