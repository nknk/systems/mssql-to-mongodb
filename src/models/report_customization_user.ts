import { BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { REPORTING_DB } from "../config";

class ReportCustomizationUser extends ModelWithTenantId {

    ReportConfigurationId: number;
    UserId: number;
}

ReportCustomizationUser.init({
    TenantId: {
        type: BIGINT,
    },
    ReportConfigurationId: {
        type: BIGINT,
    },
    UserId: {
        type: BIGINT,
    },
}, {
    sequelize,
    modelName: "ReportCustomizationUser",
    freezeTableName: true,
    tableName: "ReportCustomizationUser",
    timestamps: false
});


const ReportCustomizationUserSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId,
    },
    ReportConfigurationId: {
        type: mongoose.Types.ObjectId,
    },
    UserId: {
        type: mongoose.Types.ObjectId,
    },
}, {
    versionKey: false,
    collection: "ReportCustomizationUsers"
});

const db = mongoose.connection.useDb(REPORTING_DB);
const ReportCustomizationUserCollection = db.model("ReportCustomizationUsers", ReportCustomizationUserSchema);

export { ReportCustomizationUser, ReportCustomizationUserCollection, ReportCustomizationUserSchema };
