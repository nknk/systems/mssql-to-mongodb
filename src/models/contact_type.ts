import { STRING, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { REGISTRY_DB } from "../config";

class ContactType extends ModelWithTenantId {}

ContactType.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        "type": BIGINT,
        allowNull: false,
    },
    Code: {
        "type": STRING,
        allowNull: false,
    },
    Description: {
        "type": STRING,
        allowNull: false,
    }
}, {
    sequelize,
    modelName: "ContactType",
    freezeTableName: true,
    tableName: "ContactType",
    timestamps: false
});

const ContactTypeSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    Code: {
        type: String
    },
    Description: {
        type: String
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "ContactTypes"
});

const db = mongoose.connection.useDb(REGISTRY_DB);
const ContactTypeCollection = db.model("ContactTypes", ContactTypeSchema);

export { ContactType, ContactTypeCollection, ContactTypeSchema };
