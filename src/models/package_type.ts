import { STRING, BOOLEAN, INTEGER, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { REGISTRY_DB } from "../config";

class PackageType extends ModelWithTenantId { }

PackageType.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    TypeName: {
        type: STRING,
        allowNull: false
    },
    Active: {
        type: BOOLEAN,
        allowNull: false
    },
    DefaultLength: {
        type: DECIMAL,
        allowNull: false
    },
    DefaultHeight: {
        type: DECIMAL,
        allowNull: false
    },
    DefaultWidth: {
        type: DECIMAL,
        allowNull: false
    },
    EditableDimensions: {
        type: BOOLEAN,
        allowNull: false
    },
    SortWeight: {
        type: INTEGER,
        allowNull: false
    },
    EdiOid: {
        type: STRING,
        allowNull: false
    },
    Project44Code: {
        type: INTEGER,
        allowNull: false
    },

}, {
    sequelize,
    modelName: "PackageType",
    freezeTableName: true,
    tableName: "PackageType",
    timestamps: false
});


const PackageTypeSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    },
    TypeName: {
        type: String
    },
    Active: {
        type: Boolean,
    },
    DefaultLength: {
        type: Number
    },
    DefaultHeight: {
        type: Number
    },
    DefaultWidth: {
        type: Number
    },
    EditableDimensions: {
        type: Boolean
    },
    SortWeight: {
        type: Number
    },
    EdiOid: {
        type: String
    },
    Project44Code: {
        type: Number
    },
}, {
    versionKey: false,
    collection: "PackageTypes"
});

const db = mongoose.connection.useDb(REGISTRY_DB);
const PackageTypeCollection = db.model("PackageTypes", PackageTypeSchema);

export { PackageType, PackageTypeSchema, PackageTypeCollection };
