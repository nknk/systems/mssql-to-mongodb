import { BOOLEAN, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";
class LoadOrderAccountBucket extends ModelWithTenantId {
    LoadOrderId: number;
    AccountBucketId: number;
}

LoadOrderAccountBucket.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    Primary:{
        type: BOOLEAN
    },
    LoadOrderId:{
        type: BIGINT
    },
    AccountBucketId:{
        type: BIGINT
    } 
}, {
    sequelize,
    modelName: "LoadOrderAccountBucket",
    freezeTableName: true,
    tableName: "LoadOrderAccountBucket",
    timestamps: false
});

const LoadOrderAccountBucketSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    Primary:{
        type: Boolean
    },
    LoadOrderId:{
        type: mongoose.Types.ObjectId
    },
    AccountBucketId:{
        type: mongoose.Types.ObjectId
    }, 
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "LoadOrderAccountBuckets"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const LoadOrderAccountBucketCollection = db.model("LoadOrderAccountBuckets", LoadOrderAccountBucketSchema);

export { LoadOrderAccountBucket, LoadOrderAccountBucketCollection, LoadOrderAccountBucketSchema };
