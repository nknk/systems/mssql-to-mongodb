import { STRING, INTEGER } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import {BaseModel } from "./contracts/base_models";
import {  OPERATIONS_DB } from "../config";

class RefNoteType extends BaseModel {
   NoteTypeIndex: number; 
}

RefNoteType.init({
 
    NoteTypeIndex: {
        type: INTEGER,
        primaryKey: true
    },
    NoteTypeText: {
        type: STRING
    },
  
}, {
    sequelize,
    modelName: "RefNoteType",
    freezeTableName: true,
    tableName: "RefNoteType",
    timestamps: false
});


const RefNoteTypeSchema = new mongoose.Schema({
    NoteTypeIndex: {
        type: Number,
    },
    NoteTypeText: {
        type:String
    },
}, {
    versionKey: false,
    collection: "RefNoteTypes"
});


const db = mongoose.connection.useDb(OPERATIONS_DB);
const RefNoteTypeCollection = db.model(
    "RefNoteTypes",
    RefNoteTypeSchema
);

export { RefNoteType, RefNoteTypeCollection, RefNoteTypeSchema };
