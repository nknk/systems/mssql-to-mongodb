import { STRING, INTEGER } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { BaseModel } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class RefServiceTicketStatus extends BaseModel {
    ServiceTicketStatusIndex: number;
}

RefServiceTicketStatus.init({

    ServiceTicketStatusIndex: {
        type: INTEGER,
        primaryKey: true
    },
    ServiceTicketStatusText: {
        type: STRING
    },

}, {
    sequelize,
    modelName: "RefServiceTicketStatus",
    freezeTableName: true,
    tableName: "RefServiceTicketStatus",
    timestamps: false
});


const RefServiceTicketStatusSchema = new mongoose.Schema({
    ServiceTicketStatusIndex: {
        type: Number,
    },
    ServiceTicketStatusText: {
        type: String
    },
}, {
    versionKey: false,
    collection: "RefServiceTicketStatuses"
});


const db = mongoose.connection.useDb(OPERATIONS_DB);
const RefServiceTicketStatusCollection = db.model(
    "RefServiceTicketStatuses",
    RefServiceTicketStatusSchema
);

export {
    RefServiceTicketStatus,
    RefServiceTicketStatusCollection,
    RefServiceTicketStatusSchema
};
