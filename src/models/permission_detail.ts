import { STRING, INTEGER, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { REGISTRY_DB } from "../config";

class PermissionDetail extends ModelWithTenantId { }

PermissionDetail.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    Code: {
        type: STRING,
        allowNull: false
    },
    DisplayCode: {
        type: STRING,
        allowNull: false
    },
    NavigationUrl: {
        type: STRING,
        allowNull: false
    },
    Category: {
        type: INTEGER,
        allowNull: false
    },
    PermissionDetailType: {
        type: INTEGER,
        allowNull: false
    }
}, {
    sequelize,
    modelName: "PermissionDetail",
    freezeTableName: true,
    tableName: "PermissionDetail",
    timestamps: false
});


const PermissionDetailSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    Code: {
        type: String
    },
    OldId: {
        type: Number
    },
    Category: {
        type: Number
    },
    DisplayCode: {
        type: String,
    },
    NavigationUrl: {
        type: String,
    },
    PermissionDetailType: {
        type: Number,
    }
}, {
    versionKey: false,
    collection: "PermissionDetails"
});

const db = mongoose.connection.useDb(REGISTRY_DB);
const PermissionDetailCollection = db.model("PermissionDetails", PermissionDetailSchema);

export { PermissionDetail, PermissionDetailSchema, PermissionDetailCollection };
