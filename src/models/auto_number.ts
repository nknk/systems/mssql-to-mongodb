import { INTEGER, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { IDENTITY_DB } from "../config";

class AutoNumber extends ModelWithTenantId {}

AutoNumber.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    Code: {
        type: INTEGER,
        allowNull: false
    },
    NextNumber: {
        type: BIGINT,
        allowNull: false
    }
}, {
    sequelize,
    modelName: "AutoNumber",
    freezeTableName: true,
    tableName: "AutoNumber",
    timestamps: false
});


const AutoNumberSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    Code: {
        type: Number
    },
    NextNumber: {
        type: Number
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "AutoNumbers"
});

const db = mongoose.connection.useDb(IDENTITY_DB);
const AutoNumberCollection = db.model("AutoNumbers", AutoNumberSchema);

export { AutoNumber, AutoNumberCollection, AutoNumberSchema };
