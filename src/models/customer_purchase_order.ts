import { STRING, INTEGER, BOOLEAN, DATE, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";
class CustomerPurchaseOrder extends ModelWithTenantId {
    CustomerId: number;
    CountryId: number;
}

CustomerPurchaseOrder.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    PurchaseOrderNumber: {
        type: STRING,
        allowNull: false
    },
    ExpirationDate: {
        type: DATE,
        allowNull: false
    },
    
    ValidPostalCode: {
        type: STRING,
        allowNull: false
    },
    ApplyOnOrigin: {
        type: BOOLEAN,
        allowNull: false
    },
    ApplyOnDestination: {
        type: BOOLEAN,
        allowNull: false
    },
    MaximumUses: {
        type: INTEGER,
        allowNull: false
    },
    CurrentUses: {
        type: INTEGER,
        allowNull: false
    },
    AdditionalPurchaseOrderNotes: {
        type: STRING,
        allowNull: false
    },
    CustomerId: {
        type: BIGINT,
        allowNull: false
    },
    CountryId: {
        type: BIGINT,
        allowNull: false
    },
}, {
    sequelize,
    modelName: "CustomerPurchaseOrder",
    freezeTableName: true,
    tableName: "CustomerPurchaseOrder",
    timestamps: false
});

const CustomerPurchaseOrderSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    },
    ExpirationDate: {
        type: Date
    },
    PurchaseOrderNumber: {
        type: String
    },
    ValidPostalCode: {
        type: String
    },
    ApplyOnOrigin: {
        type: Boolean
    },
    ApplyOnDestination: {
        type: Number
    },
    MaximumUses: {
        type: Number
    },
    CurrentUses: {
        type: Number
    },
    AdditionalPurchaseOrderNotes: {
        type: String
    },
    CustomerId: {
        type: mongoose.Types.ObjectId
    },
    CountryId: {
        type: mongoose.Types.ObjectId
    },
}, {
    versionKey: false,
    collection: "CustomerPurchaseOrders"
});

const db = mongoose.connection.useDb(FINANCE_DB);
const CustomerPurchaseOrderCollection = db.model(
    "CustomerPurchaseOrders",
    CustomerPurchaseOrderSchema,
);

export {
    CustomerPurchaseOrder,
    CustomerPurchaseOrderCollection,
    CustomerPurchaseOrderSchema,
};
