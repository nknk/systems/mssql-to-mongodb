import { DATE, FLOAT, BOOLEAN, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { RATING_DB } from "../config";

class LTLCubicFootCapacityRule extends ModelWithTenantId {
    VendorRatingId: number;
}

LTLCubicFootCapacityRule.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    LowerBound: {
        type: DECIMAL,
    },
    UpperBound: {
        type: DECIMAL,
    },
    AppliedFreightClass: {
        type: FLOAT,
    },
    ApplyFAK: {
        type: BOOLEAN,
    },
    AveragePoundPerCubicFootLimit: {
        type: DECIMAL,
    },
    AveragePoundPerCubicFootApplies: {
        type: BOOLEAN,
    },
    PenaltyPoundPerCubicFoot: {
        type: DECIMAL,
    },
    EffectiveDate: {
        type: DATE,
    },
    VendorRatingId: {
        type: BIGINT,
    },
    ApplyDiscount: {
        type: BOOLEAN,
    },
}, {
    sequelize,
    modelName: "LTLCubicFootCapacityRule",
    freezeTableName: true,
    tableName: "LTLCubicFootCapacityRule",
    timestamps: false
});


const LTLCubicFootCapacityRuleSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    },
    LowerBound: {
        type: Number
    },
    UpperBound: {
        type: Number
    },
    AppliedFreightClass: {
        type: Number
    },
    ApplyFAK: {
        type: Boolean
    },
    AveragePoundPerCubicFootLimit: {
        type: Number
    },
    AveragePoundPerCubicFootApplies: {
        type: Boolean
    },
    PenaltyPoundPerCubicFoot: {
        type: Number
    },
    EffectiveDate: {
        type: Date
    },
    VendorRatingId: {
        type: mongoose.Types.ObjectId
    },
    ApplyDiscount: {
        type: Boolean
    },
}, {
    versionKey: false,
    collection: "LTLCubicFootCapacityRules"
});

const db = mongoose.connection.useDb(RATING_DB);
const LTLCubicFootCapacityRuleCollection = db
    .model("LTLCubicFootCapacityRules", LTLCubicFootCapacityRuleSchema);

export { LTLCubicFootCapacityRule, LTLCubicFootCapacityRuleCollection, LTLCubicFootCapacityRuleSchema };
