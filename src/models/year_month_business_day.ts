import { STRING, INTEGER, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { REGISTRY_DB } from "../config";

class YearMonthBusinessDay extends ModelWithTenantId { }

YearMonthBusinessDay.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    Year: {
        type: STRING,
        allowNull: false
    },
    January: {
        type: INTEGER,
        primaryKey: true
    },
    February: {
        type: INTEGER,
        primaryKey: true
    },
    March: {
        type: INTEGER,
        primaryKey: true
    },
    April: {
        type: INTEGER,
        primaryKey: true
    },
    May: {
        type: INTEGER,
        primaryKey: true
    },
    June: {
        type: INTEGER,
        primaryKey: true
    },
    July: {
        type: INTEGER,
        primaryKey: true
    },
    August: {
        type: INTEGER,
        primaryKey: true
    },
    September: {
        type: INTEGER,
        primaryKey: true
    },
    October: {
        type: INTEGER,
        primaryKey: true
    },
    November: {
        type: INTEGER,
        primaryKey: true
    },
    December: {
        type: INTEGER,
        primaryKey: true
    },
}, {
    sequelize,
    modelName: "YearMonthBusinessDay",
    freezeTableName: true,
    tableName: "YrMthBusDay",
    timestamps: false
});


const YearMonthBusinessDaySchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    },
    Year: {
        type: String,
    },
    January: {
        type: Number
    },
    February: {
        type: Number
    },
    March: {
        type: Number
    },
    April: {
        type: Number
    },
    May: {
        type: Number
    },
    June: {
        type: Number
    },
    July: {
        type: Number
    },
    August: {
        type: Number
    },
    September: {
        type: Number
    },
    October: {
        type: Number
    },
    November: {
        type: Number
    },
    December: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "YrMthBusDays"
});

const db = mongoose.connection.useDb(REGISTRY_DB);
const YearMonthBusinessDayCollection = db.model("YrMthBusDays", YearMonthBusinessDaySchema);

export { YearMonthBusinessDay, YearMonthBusinessDaySchema, YearMonthBusinessDayCollection };
