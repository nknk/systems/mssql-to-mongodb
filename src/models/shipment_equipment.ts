import { BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { OPERATIONS_DB } from "../config";
import { ModelWithTenantId } from "./contracts/base_models";

class ShipmentEquipment extends ModelWithTenantId {
    EquipmentTypeId: number;
    ShipmentId: number;
}

ShipmentEquipment.init({    
    TenantId: {
        type: BIGINT
    },
    EquipmentTypeId: {
        type: BIGINT
    },
    ShipmentId: {
        type: BIGINT
    }
}, {
    sequelize,
    modelName: "ShipmentEquipment",
    freezeTableName: true,
    tableName: "ShipmentEquipment",
    timestamps: false
});


const ShipmentEquipmentSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    EquipmentTypeId: {
        type: mongoose.Types.ObjectId
    },
    ShipmentId: {
        type: mongoose.Types.ObjectId
    }
}, {
    versionKey: false,
    collection: "ShipmentEquipments"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const ShipmentEquipmentCollection = db.model("ShipmentEquipments", ShipmentEquipmentSchema);

export { ShipmentEquipment, ShipmentEquipmentCollection, ShipmentEquipmentSchema };
