import { STRING, BOOLEAN, INTEGER, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";

class VendorNotification extends ModelWithTenantId {
    VendorCommunicationId: number;
}

VendorNotification.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    VendorCommunicationId: {
        type: BIGINT
    },
    Milestone: {
        type: INTEGER
    },
    NotificationMethod: {
        type: INTEGER
    },
    Email: {
        type: STRING
    },
    Fax: {
        type: STRING
    },
    Enabled: {
        type: BOOLEAN
    }
}, {
    sequelize,
    modelName: "VendorNotification",
    freezeTableName: true,
    tableName: "VendorNotification",
    timestamps: false
});


const VendorNotificationSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    VendorCommunicationId: {
        type: mongoose.Types.ObjectId
    },
    Milestone: {
        type: Number
    },
    NotificationMethod: {
        type: Number
    },
    Email: {
        type: String
    },
    Fax: {
        type: String
    },
    Enabled: {
        type: Boolean
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "VendorNotifications"
});


const db = mongoose.connection.useDb(FINANCE_DB);
const VendorNotificationCollection = db.model(
    "VendorNotifications",
    VendorNotificationSchema
);

export { VendorNotification, VendorNotificationCollection, VendorNotificationSchema };
