import { STRING, BOOLEAN, INTEGER, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { REGISTRY_DB } from "../config";

class ChargeCode extends ModelWithTenantId {
    TenantId: string;
    Code: string;
    Description: string;
    Active: boolean;
    Category: number;
    SurpressOnCarrierRateAgreement: boolean;
    Project44Code: number;
    OldId: number;
}

ChargeCode.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    Code: {
        type: STRING,
        allowNull: false,
    },
    Description: {
        type: STRING,
        allowNull: false,
    },
    Active: {
        type: BOOLEAN,
        allowNull: false,
    },
    Category: {
        type: INTEGER,
        allowNull: false,
    },
    SurpressOnCarrierRateAgreement: {
        type: BOOLEAN,
        allowNull: false,
    },
    Project44Code: {
        type: INTEGER,
        allowNull: false,
    },
}, {
    sequelize,
    modelName: "ChargeCode",
    freezeTableName: true,
    tableName: "ChargeCode",
    timestamps: false
});

const ChargeCodeSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    Code: {
        type: String
    },
    Description: {
        type: String
    },
    Active: {
        type: Boolean
    },
    Category: {
        type: Number,
    },
    SurpressOnCarrierRateAgreement: {
        type: Boolean,
    },
    Project44Code: {
        type: Number,
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "ChargeCodes"
});

declare interface IChargeCodeDoc extends mongoose.Document {
    TenantId: string;
    Code: string;
    Description: string;
    Active: boolean;
    Category: number;
    SurpressOnCarrierRateAgreement: boolean;
    Project44Code: number;
    OldId: number;
}

const db = mongoose.connection.useDb(REGISTRY_DB);
const ChargeCodeCollection = db.model<IChargeCodeDoc>("ChargeCodes", ChargeCodeSchema);

export { ChargeCode, ChargeCodeCollection, ChargeCodeSchema };
