import { STRING, INTEGER, DATE, BOOLEAN, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { UTILITIES_DB } from "../config";

class FaxTransmission extends ModelWithTenantId {}

FaxTransmission.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },

    ShipmentNumber: {
        type: STRING
    },
    SendDateTime: {
        type: DATE
    },
    Status: {
        type: INTEGER
    },
    Message: {
        type: STRING
    },
    ResponseDateTime: {
        type: DATE
    },
    TransmissionKey: {
        type: STRING
    },
    Resolved: {
        type: BOOLEAN
    },
    ResolutionComment: {
        type: STRING
    },
    LastStatusCheckDateTime: {
        type: DATE
    },
    ExternalReference: {
        type: STRING
    },
    FaxWrapper: {
        type: STRING
    },
    ServiceProvider: {
        type: INTEGER
    }
}, {
    sequelize,
    modelName: "FaxTransmission",
    freezeTableName: true,
    tableName: "FaxTransmission",
    timestamps: false
});

const FaxTransmissionSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    ShipmentNumber: {
        type: String
    },
    SendDateTime: {
        type: Date
    },
    Status: {
        type: Number,
    },
    Message: {
        type: String
    },
    ResponseDateTime: {
        type: Date
    },
    TransmissionKey: {
        type: String
    },
    Resolved: {
        type: Boolean
    },
    ResolutionComment: {
        type: String
    },
    LastStatusCheckDateTime: {
        type: Date
    },
    ExternalReference: {
        type: String
    },
    FaxWrapper: {
        type: String
    },
    ServiceProvider: {
        type: Number
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "FaxTransmissions"
});

const db = mongoose.connection.useDb(UTILITIES_DB);
const FaxTransmissionCollection = db.model("FaxTransmissions", FaxTransmissionSchema);

export { FaxTransmission, FaxTransmissionCollection, FaxTransmissionSchema };
