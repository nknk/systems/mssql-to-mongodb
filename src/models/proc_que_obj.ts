import { STRING, INTEGER, DATE, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { BaseModel } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class ProcQueObj extends BaseModel {
}

ProcQueObj.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    ObjType: {
        type: INTEGER
    },
    Xml: {
        type: STRING
    },
    DateCreated: {
        type: DATE
    },
}, {
    sequelize,
    modelName: "ProcQueObj",
    freezeTableName: true,
    tableName: "ProcQueObj",
    timestamps: false
});


const ProcQueObjSchema = new mongoose.Schema({
    OldId: {
        type: Number
    },
    ObjType: {
        type: Number
    },
    Xml: {
        type: String
    },
    DateCreated: {
        type: Date
    },
}, {
    versionKey: false,
    collection: "ProcQueObjs"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const ProcQueObjCollection = db.model(
    "ProcQueObjs",
    ProcQueObjSchema
);

export { ProcQueObj, ProcQueObjCollection, ProcQueObjSchema };
