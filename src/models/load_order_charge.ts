import { STRING, INTEGER, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class LoadOrderCharge extends ModelWithTenantId {
    ChargeCodeId: number;
    VendorId: number;
    LoadOrderId: number;
}

LoadOrderCharge.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    LoadOrderId: {
        type: BIGINT
    },
    Quantity: {
        type: INTEGER
    },
    UnitBuy: {
        type: DECIMAL
    },
    UnitSell: {
        type: DECIMAL
    },
    UnitDiscount: {
        type: DECIMAL
    },
    Comment: {
        type: STRING
    },
    ChargeCodeId: {
        type: BIGINT
    },
    VendorId: {
        type: BIGINT
    }
}, {
    sequelize,
    modelName: "LoadOrderCharge",
    freezeTableName: true,
    tableName: "LoadOrderCharge",
    timestamps: false
});

const LoadOrderChargeSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    LoadOrderId: {
        type: mongoose.Types.ObjectId
    },
    Quantity: {
        type: Number
    },
    UnitBuy: {
        type: Number
    },
    UnitSell: {
        type: Number
    },
    UnitDiscount: {
        type: Number
    },
    Comment: {
        type: String
    },
    ChargeCodeId: {
        type: mongoose.Types.ObjectId
    },
    VendorId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "LoadOrderCharges"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const LoadOrderChargeCollection = db.model("LoadOrderCharges", LoadOrderChargeSchema);

export { LoadOrderCharge, LoadOrderChargeCollection, LoadOrderChargeSchema };
