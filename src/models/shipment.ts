import { STRING, BOOLEAN, INTEGER, DATE, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class Shipment extends ModelWithTenantId {
    PrefixId: number;
    CustomerId: number;
    UserId: number;
    OriginTerminalInfoId: number;
    DestinationTerminalInfoId: number;
    OriginId: number;
    DestinationId: number;
    MileageSourceId: number;
    SalesRepresentativeId: number;
    ResellerAdditionId: number;
    AccountBucketUnitId: number;
    CarrierCoordinatorUserId: number;
    ShipmentCoordinatorUserId: number;
    ShipmentPriorityId: number;
    JobId: number;
}

Shipment.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    ShipmentNumber: {
        type: STRING,
        allowNull: false
    },
    PrefixId: {
        type: BIGINT,
        allowNull: false
    },
    HidePrefix: {
        type: BOOLEAN,
        allowNull: false
    },
    ShipperBol: {
        type: STRING,
        allowNull: false
    },
    EnableQuickPay: {
        type: BOOLEAN,
        allowNull: false
    },
    PurchaseOrderNumber: {
        type: STRING,
        allowNull: false
    },
    ShipperReference: {
        type: STRING,
        allowNull: false
    },
    HazardousMaterial: {
        type: BOOLEAN,
        allowNull: false
    },
    HazardousMaterialContactName: {
        type: STRING,
        allowNull: false
    },
    HazardousMaterialContactPhone: {
        type: STRING,
        allowNull: false
    },
    HazardousMaterialContactMobile: {
        type: STRING,
        allowNull: false
    },
    HazardousMaterialContactEmail: {
        type: STRING,
        allowNull: false
    },
    DesiredPickupDate: {
        type: DATE,
        allowNull: false
    },
    EarlyPickup: {
        type: STRING,
        allowNull: false
    },
    LatePickup: {
        type: STRING,
        allowNull: false
    },
    ActualPickupDate: {
        type: DATE,
        allowNull: false
    },
    EstimatedDeliveryDate: {
        type: DATE,
        allowNull: false
    },
    EarlyDelivery: {
        type: STRING,
        allowNull: false
    },
    LateDelivery: {
        type: STRING,
        allowNull: false
    },
    ActualDeliveryDate: {
        type: DATE,
        allowNull: false
    },
    Status: {
        type: INTEGER,
        allowNull: false
    },
    DateCreated: {
        type: DATE,
        allowNull: false
    },
    ServiceMode: {
        type: INTEGER,
        allowNull: false
    },
    ShipmentAutorated: {
        type: BOOLEAN,
        allowNull: false
    },
    OriginalRateValue: {
        type: DECIMAL,
        allowNull: false
    },
    VendorDiscountPercentage: {
        type: DECIMAL,
        allowNull: false
    },
    VendorFreightFloor: {
        type: DECIMAL,
        allowNull: false
    },
    VendorFreightCeiling: {
        type: DECIMAL,
        allowNull: false
    },
    VendorFuelPercent: {
        type: DECIMAL,
        allowNull: false
    },
    CustomerFreightMarkupValue: {
        type: DECIMAL,
        allowNull: false
    },
    CustomerFreightMarkupPercent: {
        type: DECIMAL,
        allowNull: false
    },
    UseLowerCustomerFreightMarkup: {
        type: BOOLEAN,
        allowNull: false
    },
    BilledWeight: {
        type: DECIMAL,
        allowNull: false
    },
    ApplyDiscount: {
        type: BOOLEAN,
        allowNull: false
    },
    LineHaulProfitAdjustmentRatio: {
        type: DECIMAL,
        allowNull: false
    },
    FuelProfitAdjustmentRatio: {
        type: DECIMAL,
        allowNull: false
    },
    AccessorialProfitAdjustmentRatio: {
        type: DECIMAL,
        allowNull: false
    },
    ServiceProfitAdjustmentRatio: {
        type: DECIMAL,
        allowNull: false
    },
    ResellerAdditionalFreightMarkup: {
        type: DECIMAL,
        allowNull: false
    },
    ResellerAdditionalFreightMarkupIsPercent: {
        type: BOOLEAN,
        allowNull: false
    },
    ResellerAdditionalFuelMarkup: {
        type: DECIMAL,
        allowNull: false
    },
    ResellerAdditionalFuelMarkupIsPercent: {
        type: BOOLEAN,
        allowNull: false
    },
    ResellerAdditionalAccessorialMarkup: {
        type: DECIMAL,
        allowNull: false
    },
    ResellerAdditionalAccessorialMarkupIsPercent: {
        type: BOOLEAN,
        allowNull: false
    },
    ResellerAdditionalServiceMarkup: {
        type: DECIMAL,
        allowNull: false
    },
    ResellerAdditionalServiceMarkupIsPercent: {
        type: BOOLEAN,
        allowNull: false
    },
    DirectPointRate: {
        type: BOOLEAN,
        allowNull: false
    },
    OriginTerminalInfoId: {
        type: BIGINT,
        allowNull: false
    },
    DestinationTerminalInfoId: {
        type: BIGINT,
        allowNull: false
    },
    GeneralBolComments: {
        type: STRING,
        allowNull: false
    },
    CriticalBolComments: {
        type: STRING,
        allowNull: false
    },
    CustomerId: {
        type: BIGINT,
        allowNull: false
    },
    UserId: {
        type: BIGINT,
        allowNull: false
    },
    OriginId: {
        type: BIGINT,
        allowNull: false
    },
    DestinationId: {
        type: BIGINT,
        allowNull: false
    },
    Mileage: {
        type: DECIMAL,
        allowNull: false
    },
    MileageSourceId: {
        type: BIGINT,
        allowNull: false
    },
    SmallPackType: {
        type: STRING,
        allowNull: false
    },
    SmallPackageEngine: {
        type: INTEGER,
        allowNull: false
    },
    VendorRatingOverrideAddress: {
        type: STRING,
        allowNull: false
    },
    DeclineInsurance: {
        type: BOOLEAN,
        allowNull: false
    },
    SalesRepresentativeId: {
        type: BIGINT,
        allowNull: false
    },
    SalesRepresentativeCommissionPercent: {
        type: DECIMAL,
        allowNull: false
    },
    ResellerAdditionId: {
        type: BIGINT,
        allowNull: false
    },
    BillReseller: {
        type: BOOLEAN,
        allowNull: false
    },
    EmptyMileage: {
        type: DECIMAL,
        allowNull: false
    },
    MiscField1: {
        type: STRING,
        allowNull: false
    },
    MiscField2: {
        type: STRING,
    },
    AccountBucketUnitId: {
        type: BIGINT,
    },
    IsPartialTruckload: {
        type: BOOLEAN,
    },
    CarrierCoordinatorUserId: {
        type: BIGINT,
    },
    ShipmentCoordinatorUserId: {
        type: BIGINT,
    },
    AuditedForInvoicing: {
        type: BOOLEAN,
    },
    ShipmentPriorityId: {
        type: BIGINT,
    },
    CreatedInError: {
        type: BOOLEAN,
    },
    CareOfAddressFormatEnabled: {
        type: BOOLEAN,
    },
    LinearFootRuleBypassed: {
        type: BOOLEAN,
    },
    BolFtpDelivered: {
        type: BOOLEAN,
    },
    StatementFtpDelivered: {
        type: BOOLEAN,
    },
    SalesRepAddlEntityName: {
        type: STRING,
    },
    SalesRepAddlEntityCommPercent: {
        type: DECIMAL,
    },
    SalesRepMinCommValue: {
        type: DECIMAL,
    },
    SalesRepMaxCommValue: {
        type: DECIMAL,
    },
    IsLTLPackageSpecificRate: {
        type: BOOLEAN,
    },
    RatedWeight: {
        type: DECIMAL,
    },
    RatedCubicFeet: {
        type: DECIMAL,
    },
    IsGuaranteedDeliveryService: {
        type: BOOLEAN,
    },
    GuaranteedDeliveryServiceTime: {
        type: STRING,
    },
    GuaranteedDeliveryServiceRateType: {
        type: INTEGER,
    },
    GuaranteedDeliveryServiceRate: {
        type: DECIMAL,
    },
    InDispute: {
        type: BOOLEAN,
    },
    InDisputeReason: {
        type: INTEGER,
    },
    DriverName: {
        type: STRING,
    },
    DriverPhoneNumber: {
        type: STRING,
    },
    DriverTrailerNumber: {
        type: STRING,
    },
    JobId: {
        type: BIGINT,
    },
    JobStep: {
        type: INTEGER,
    },
    RatedPcf: {
        type: DECIMAL,
    },
    Project44QuoteNumber: {
        type: STRING,
    },
    IsExpeditedService: {
        type: BOOLEAN,
    },
    ExpeditedServiceRate: {
        type: DECIMAL,
    },
    ExpeditedServiceTime: {
        type: STRING,
    },
    SendShipmentUpdateToOriginPrimaryContact: {
        type: BOOLEAN,
    },
    SendShipmentUpdateToDestinationPrimaryContact: {
        type: BOOLEAN,
    },
    RMANumber: {
        type: STRING,
    }
}, {
    sequelize,
    modelName: "Shipment",
    freezeTableName: true,
    tableName: "Shipment",
    timestamps: false
});

const ShipmentSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    },
    ShipmentNumber: {
        type: String
    },
    PrefixId: {
        type: mongoose.Types.ObjectId
    },
    HidePrefix: {
        type: Boolean
    },
    ShipperBol: {
        type: String
    },
    EnableQuickPay: {
        type: Boolean
    },
    PurchaseOrderNumber: {
        type: String
    },
    ShipperReference: {
        type: String
    },
    HazardousMaterial: {
        type: Boolean
    },
    HazardousMaterialContactName: {
        type: String
    },
    HazardousMaterialContactPhone: {
        type: String
    },
    HazardousMaterialContactMobile: {
        type: String
    },
    HazardousMaterialContactEmail: {
        type: String
    },
    DesiredPickupDate: {
        type: Date
    },
    EarlyPickup: {
        type: String
    },
    LatePickup: {
        type: String
    },
    ActualPickupDate: {
        type: Date
    },
    EstimatedDeliveryDate: {
        type: Date
    },
    EarlyDelivery: {
        type: String
    },
    LateDelivery: {
        type: String
    },
    ActualDeliveryDate: {
        type: Date
    },
    Status: {
        type: Number
    },
    DateCreated: {
        type: Date
    },
    ServiceMode: {
        type: Number
    },
    ShipmentAutorated: {
        type: Boolean
    },
    OriginalRateValue: {
        type: Number
    },
    VendorDiscountPercentage: {
        type: Number
    },
    VendorFreightFloor: {
        type: Number
    },
    VendorFreightCeiling: {
        type: Number
    },
    VendorFuelPercent: {
        type: Number
    },
    CustomerFreightMarkupValue: {
        type: Number
    },
    CustomerFreightMarkupPercent: {
        type: Number
    },
    UseLowerCustomerFreightMarkup: {
        type: Boolean
    },
    BilledWeight: {
        type: Number
    },
    ApplyDiscount: {
        type: Boolean
    },
    LineHaulProfitAdjustmentRatio: {
        type: Number
    },
    FuelProfitAdjustmentRatio: {
        type: Number
    },
    AccessorialProfitAdjustmentRatio: {
        type: Number
    },
    ServiceProfitAdjustmentRatio: {
        type: Number
    },
    ResellerAdditionalFreightMarkup: {
        type: Number
    },
    ResellerAdditionalFreightMarkupIsPercent: {
        type: Boolean
    },
    ResellerAdditionalFuelMarkup: {
        type: Number
    },
    ResellerAdditionalFuelMarkupIsPercent: {
        type: Boolean
    },
    ResellerAdditionalAccessorialMarkup: {
        type: Number
    },
    ResellerAdditionalAccessorialMarkupIsPercent: {
        type: Boolean
    },
    ResellerAdditionalServiceMarkup: {
        type: Number
    },
    ResellerAdditionalServiceMarkupIsPercent: {
        type: Boolean
    },
    DirectPointRate: {
        type: Boolean
    },
    OriginTerminalInfoId: {
        type: mongoose.SchemaTypes.Mixed
    },
    DestinationTerminalInfoId: {
        type: mongoose.SchemaTypes.Mixed
    },
    GeneralBolComments: {
        type: String
    },
    CriticalBolComments: {
        type: String
    },
    CustomerId: {
        type: mongoose.Types.ObjectId
    },
    UserId: {
        type: mongoose.Types.ObjectId
    },
    OriginId: {
        type: mongoose.SchemaTypes.Mixed
    },
    DestinationId: {
        type: mongoose.SchemaTypes.Mixed
    },
    Mileage: {
        type: Number
    },
    MileageSourceId: {
        type: mongoose.Types.ObjectId
    },
    SmallPackType: {
        type: String
    },
    SmallPackageEngine: {
        type: Number
    },
    VendorRatingOverrideAddress: {
        type: String
    },
    DeclineInsurance: {
        type: Boolean
    },
    SalesRepresentativeId: {
        type: mongoose.Types.ObjectId
    },
    SalesRepresentativeCommissionPercent: {
        type: Number
    },
    ResellerAdditionId: {
        type: mongoose.Types.ObjectId
    },
    BillReseller: {
        type: Boolean

    },
    EmptyMileage: {
        type: Number

    },
    MiscField1: {
        type: String
    },
    MiscField2: {
        type: String,
    },
    AccountBucketUnitId: {
        type: mongoose.SchemaTypes.ObjectId,
    },
    IsPartialTruckload: {
        type: Boolean,
    },
    CarrierCoordinatorUserId: {
        type: mongoose.SchemaTypes.ObjectId,
    },
    ShipmentCoordinatorUserId: {
        type: mongoose.SchemaTypes.ObjectId,
    },
    AuditedForInvoicing: {
        type: Boolean,
    },
    ShipmentPriorityId: {
        type: mongoose.SchemaTypes.ObjectId,
    },
    CreatedInError: {
        type: Boolean,
    },
    CareOfAddressFormatEnabled: {
        type: Boolean,
    },
    LinearFootRuleBypassed: {
        type: Boolean,
    },
    BolFtpDelivered: {
        type: Boolean,
    },
    StatementFtpDelivered: {
        type: Boolean,
    },
    SalesRepAddlEntityName: {
        type: String,
    },
    SalesRepAddlEntityCommPercent: {
        type: Number,
    },
    SalesRepMinCommValue: {
        type: Number,
    },
    SalesRepMaxCommValue: {
        type: Number,
    },
    IsLTLPackageSpecificRate: {
        type: Boolean,
    },
    RatedWeight: {
        type: Number,
    },
    RatedCubicFeet: {
        type: Number,
    },
    IsGuaranteedDeliveryService: {
        type: Boolean,
    },
    GuaranteedDeliveryServiceTime: {
        type: String,
    },
    GuaranteedDeliveryServiceRateType: {
        type: Number,
    },
    GuaranteedDeliveryServiceRate: {
        type: Number,
    },
    InDispute: {
        type: Boolean,
    },
    InDisputeReason: {
        type: Number,
    },
    DriverName: {
        type: String,
    },
    DriverPhoneNumber: {
        type: String,
    },
    DriverTrailerNumber: {
        type: String,
    },
    JobId: {
        type: mongoose.SchemaTypes.ObjectId,
    },
    JobStep: {
        type: Number,
    },
    RatedPcf: {
        type: Number,
    },
    Project44QuoteNumber: {
        type: String,
    },
    IsExpeditedService: {
        type: Boolean,
    },
    ExpeditedServiceRate: {
        type: Number,
    },
    ExpeditedServiceTime: {
        type: String,
    },
    SendShipmentUpdateToOriginPrimaryContact: {
        type: Boolean,
    },
    SendShipmentUpdateToDestinationPrimaryContact: {
        type: Boolean,
    },
    RMANumber: {
        type: String,
    },
}, {
    versionKey: false,
    collection: "Shipments"
});

export interface IShipmentDoc extends mongoose.Document {

    OriginId: string;
    DestinationId: string;
    OriginTerminalInfoId: string;
    DestinationTerminalInfoId: string;
    PrefixId: string;
    CustomerId: string;
    UserId: string;
    MileageSourceId: string;
    SalesRepresentativeId: string;
    ResellerAdditionId: string;
    AccountBucketUnitId: string;
    CarrierCoordinatorUserId: string;
    ShipmentCoordinatorUserId: string;
    ShipmentPriorityId: string;
    JobId: string;

}

const db = mongoose.connection.useDb(OPERATIONS_DB);
const ShipmentCollection = db.model<IShipmentDoc>("Shipments", ShipmentSchema);

export { Shipment, ShipmentCollection, ShipmentSchema };
