import { STRING, INTEGER } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { BaseModel } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class RefFaxTransmissionStatus extends BaseModel {
    FaxTransmissionStatusIndex: number;
}

RefFaxTransmissionStatus.init({

    FaxTransmissionStatusIndex: {
        type: INTEGER,
        primaryKey: true
    },
    FaxTransmissionStatusText: {
        type: STRING
    },

}, {
    sequelize,
    modelName: "RefFaxTransmissionStatus",
    freezeTableName: true,
    tableName: "RefFaxTransmissionStatus",
    timestamps: false
});


const RefFaxTransmissionStatusSchema = new mongoose.Schema({
    FaxTransmissionStatusIndex: {
        type: Number
    },
    FaxTransmissionStatusText: {
        type: String
    },
}, {
    versionKey: false,
    collection: "RefFaxTransmissionStatuses"
});


const db = mongoose.connection.useDb(OPERATIONS_DB);
const RefFaxTransmissionStatusCollection = db.model(
    "RefFaxTransmissionStatuses",
    RefFaxTransmissionStatusSchema
);

export {
    RefFaxTransmissionStatus,
    RefFaxTransmissionStatusCollection,
    RefFaxTransmissionStatusSchema
};
