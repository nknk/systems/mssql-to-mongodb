import { STRING, BOOLEAN, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { AdminUserPermission, AdminUserPermissionSchema } from "../models/admin_user_permission";
import { IDENTITY_DB } from "../config";
import { BaseModel } from "./contracts/base_models";

class AdminUser extends BaseModel {}

AdminUser.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    Username: {
        type: STRING,
        allowNull: false
    },
    Password: {
        type: STRING,
        allowNull: false
    },
    FirstName: {
        type: STRING,
        allowNull: false
    },
    LastName: {
        type: STRING,
        allowNull: false
    },
    Email: {
        type: STRING,
        allowNull: false
    },
    Enabled: {
        type: BOOLEAN
    },
    Notes: {
        type: STRING,
        allowNull: false
    }
}, {
    sequelize,
    modelName: "AdminUser",
    freezeTableName: true,
    tableName: "AdminUser",
    timestamps: false
});

AdminUser.hasMany(AdminUserPermission);
AdminUserPermission.belongsTo(AdminUser);

const AdminUserSchema = new mongoose.Schema({
    Username: {
        type: String
    },
    Password: {
        type: String
    },
    FirstName: {
        type: String
    },
    LastName: {
        type: String
    },
    Email: {
        type: String
    },
    Enabled: {
        type: Boolean
    },
    Notes: {
        type: String
    },
    OldId: {
        type: Number
    },
    AdminUserPermissions: [AdminUserPermissionSchema]
}, {
    versionKey: false,
    collection: "AdminUsers"
});
const db = mongoose.connection.useDb(IDENTITY_DB);
const AdminUserCollection = db.model("AdminUsers", AdminUserSchema);

export { AdminUser, AdminUserCollection, AdminUserSchema };
