import { INTEGER, DATE, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { RATING_DB } from "../config";

class LTLPackageSpecificRate extends ModelWithTenantId {
    OriginRegionId: number;
    DestinationRegionId: number;
    VendorRatingId: number;
}

LTLPackageSpecificRate.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    VendorRatingId: {
        type: BIGINT,
    },
    EffectiveDate: {
        type: DATE,
    },
    OriginRegionId: {
        type: BIGINT,
    },
    DestinationRegionId: {
        type: BIGINT,
    },
    PackageQuantity: {
        type: INTEGER,
    },
    Rate: {
        type: DECIMAL,
    },
    RatePriority: {
        type: INTEGER,
    },
}, {
    sequelize,
    modelName: "LTLPackageSpecificRate",
    freezeTableName: true,
    tableName: "LTLPackageSpecificRate",
    timestamps: false
});


const LTLPackageSpecificRateSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    },
    VendorRatingId: {
        type: mongoose.Types.ObjectId
    },
    EffectiveDate: {
        type: Date
    },
    OriginRegionId: {
        type: mongoose.Types.ObjectId
    },
    DestinationRegionId: {
        type: mongoose.Types.ObjectId
    },
    PackageQuantity: {
        type: Number
    },
    Rate: {
        type: Number
    },
    RatePriority: {
        type: Number
    },
}, {
    versionKey: false,
    collection: "LTLPackageSpecificRates"
});

const db = mongoose.connection.useDb(RATING_DB);
const LTLPackageSpecificRateCollection = db
    .model("LTLPackageSpecificRates", LTLPackageSpecificRateSchema);

export { LTLPackageSpecificRate, LTLPackageSpecificRateCollection, LTLPackageSpecificRateSchema };
