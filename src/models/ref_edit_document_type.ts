import { STRING, INTEGER } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import {BaseModel } from "./contracts/base_models";
import {  OPERATIONS_DB } from "../config";

class RefEdiDocumentType extends BaseModel {
   EdiDocumentTypeIndex: number; 
}

RefEdiDocumentType.init({
 
    EdiDocumentTypeIndex: {
        type: INTEGER,
        primaryKey: true
    },
    EdiDocumentTypeText: {
        type: STRING
    },
  
}, {
    sequelize,
    modelName: "RefEdiDocumentType",
    freezeTableName: true,
    tableName: "RefEdiDocumentType",
    timestamps: false
});


const RefEdiDocumentTypeSchema = new mongoose.Schema({
    EdiDocumentTypeIndex: {
        type:Number
    },
    EdiDocumentTypeText: {
        type:String
    }, 
}, {
    versionKey: false,
    collection: "RefEdiDocumentTypes"
});


const db = mongoose.connection.useDb(OPERATIONS_DB);
const RefEdiDocumentTypeCollection = db.model(
    "RefEdiDocumentTypes",
    RefEdiDocumentTypeSchema
);

export { RefEdiDocumentType, RefEdiDocumentTypeCollection, RefEdiDocumentTypeSchema };
