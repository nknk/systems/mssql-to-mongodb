import { BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { RATING_DB } from "../config";
class FuelIndex extends ModelWithTenantId {
    FuelTableId: number;
}

FuelIndex.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    LowerBound: {
        type: DECIMAL
    },
    UpperBound: {
        type: DECIMAL
    },
    Surcharge: {
        type: DECIMAL
    },
    FuelTableId: {
        type: BIGINT
    }
}, {
    sequelize,
    modelName: "FuelIndex",
    freezeTableName: true,
    tableName: "FuelIndex",
    timestamps: false
});


const FuelIndexSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    LowerBound: {
        type: Number
    },
    UpperBound: {
        type: Number
    },
    Surcharge: {
        type: Number
    },
    FuelTableId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "FuelIndexs"
});


const db = mongoose.connection.useDb(RATING_DB);
const FuelIndexCollection = db.model("FuelIndexs", FuelIndexSchema);

export {
    FuelIndex,
    FuelIndexCollection,
    FuelIndexSchema
};
