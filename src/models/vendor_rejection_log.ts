import { INTEGER, STRING, DATE, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";

class VendorRejectionLog extends ModelWithTenantId {
    VendorId: number;
    UserId: number;
    CustomerId: number;
}

VendorRejectionLog.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    LoadShipmentDateCreated: {
        type: DATE
    },
    LoadShipmentNumber: {
        type: STRING
    },
    DateCreated: {
        type: DATE
    },
    VendorId: {
        type: BIGINT
    },
    UserId: {
        type: BIGINT
    },
    OriginCity: {
        type: STRING
    },
    OriginState: {
        type: STRING
    },
    OriginPostalCode: {
        type: STRING
    },
    OriginCountryCode: {
        type: STRING
    },
    DestinationCity: {
        type: STRING
    },
    DestinationState: {
        type: STRING
    },
    DestinationPostalCode: {
        type: STRING
    },
    DestinationCountryCode: {
        type: STRING
    },
    CustomerId: {
        type: BIGINT
    },
    ServiceMode: {
        type: INTEGER
    }    
}, {
    sequelize,
    modelName: "VendorRejectionLog",
    freezeTableName: true,
    tableName: "VendorRejectionLog",
    timestamps: false
});


const VendorRejectionLogSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    LoadShipmentDateCreated: {
        type: Date
    },
    LoadShipmentNumber: {
        type: String
    },
    DateCreated: {
        type: Date
    },
    VendorId: {
        type: mongoose.Types.ObjectId
    },
    UserId: {
        type: mongoose.Types.ObjectId
    },
    OriginCity: {
        type: String
    },
    OriginState: {
        type: String
    },
    OriginPostalCode: {
        type: String
    },
    OriginCountryCode: {
        type: String
    },
    DestinationCity: {
        type: String
    },
    DestinationState: {
        type: String
    },
    DestinationPostalCode: {
        type: String
    },
    DestinationCountryCode: {
        type: String
    },
    CustomerId: {
        type: mongoose.Types.ObjectId
    },
    ServiceMode: {
        type: Number
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "VendorRejectionLogs"
});


const db = mongoose.connection.useDb(FINANCE_DB);
const VendorRejectionLogCollection = db.model(
    "VendorRejectionLogs",
    VendorRejectionLogSchema
);

export { VendorRejectionLog, VendorRejectionLogCollection, VendorRejectionLogSchema };
