import { STRING, BOOLEAN, INTEGER, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { REGISTRY_DB } from "../config";
class DocumentTemplate extends ModelWithTenantId { }

DocumentTemplate.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    TemplatePath: {
        type: STRING,
        allowNull: false,
    },
    Code: {
        type: STRING,
        allowNull: false
    },
    Description: {
        type: STRING,
        allowNull: false
    },
    Category: {
        type: INTEGER,
        allowNull: false
    },
    ServiceMode: {
        type: INTEGER,
        allowNull: false
    },
    Primary: {
        type: BOOLEAN,
        allowNull: false
    },
    WeightDecimals: {
        type: STRING,
        allowNull: false
    },
    DimensionDecimals: {
        type: STRING,
        allowNull: false
    },
    CurrencyDecimals: {
        type: STRING,
        allowNull: false
    },
}, {
    sequelize,
    modelName: "DocumentTemplate",
    freezeTableName: true,
    tableName: "DocumentTemplate",
    timestamps: false
});


const DocumentTemplateSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    TemplatePath: {
        type: String,
    },
    Code: {
        type: String,
    },
    Description: {
        type: String,
    },
    Category: {
        type: Number,
    },
    ServiceMode: {
        type: Number,
    },
    Primary: {
        type: Boolean,
    },
    WeightDecimals: {
        type: String,
    },
    DimensionDecimals: {
        type: String,
    },
    CurrencyDecimals: {
        type: String,
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "DocumentTemplates"
});

const db = mongoose.connection.useDb(REGISTRY_DB);
const DocumentTemplateCollection = db.model(
    "DocumentTemplates",
    DocumentTemplateSchema
);

export { DocumentTemplate, DocumentTemplateCollection, DocumentTemplateSchema };
