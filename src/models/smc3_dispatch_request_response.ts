import { STRING, DATE, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { BaseModel } from "./contracts/base_models";
import { RATING_DB } from "../config";

class SMC3DispatchRequestResponse extends BaseModel {
    ShipmentId: number;
}

SMC3DispatchRequestResponse.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    ShipmentId: {
        type: BIGINT
    },
    RequestData: {
        type: STRING
    },
    TransactionId: {
        type: STRING
    },
    ResponseData: {
        type: STRING
    },
    DateCreated: {
        type: DATE
    },
    Scac: {
        type: STRING
    }
}, {
    sequelize,
    modelName: "SMC3DispatchRequestResponse",
    freezeTableName: true,
    tableName: "SMC3DispatchRequestResponse",
    timestamps: false
});


const SMC3DispatchRequestResponseSchema = new mongoose.Schema({
    ShipmentId: {
        type: mongoose.Types.ObjectId
    },
    RequestData: {
        type: String
    },
    TransactionId: {
        type: String
    },
    ResponseData: {
        type: String
    },
    DateCreated: {
        type: Date
    },
    Scac: {
        type: String
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "SMC3DispatchRequestResponses"
});

const db = mongoose.connection.useDb(RATING_DB);
const SMC3DispatchRequestResponseCollection = db
    .model("SMC3DispatchRequestResponses", SMC3DispatchRequestResponseSchema);

export { SMC3DispatchRequestResponse, SMC3DispatchRequestResponseCollection, SMC3DispatchRequestResponseSchema };
