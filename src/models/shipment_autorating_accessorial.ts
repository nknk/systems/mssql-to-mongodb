import { INTEGER, BOOLEAN, STRING, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class ShipmentAutoRatingAccessorial extends ModelWithTenantId {
    ShipmentId: number;
}

ShipmentAutoRatingAccessorial.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    ServiceDescription: {
        type: STRING
    },
    RateType: {
        type: INTEGER
    },
    Rate: {
        type: DECIMAL
    },
    BuyFloor: {
        type: DECIMAL
    },
    BuyCeiling: {
        type: DECIMAL
    },
    SellFloor: {
        type: DECIMAL
    },
    SellCeiling: {
        type: DECIMAL
    },
    MarkupValue: {
        type: DECIMAL
    },
    MarkupPercent: {
        type: DECIMAL
    },
    UseLowerSell: {
        type: BOOLEAN
    },
    ShipmentId: {
        type: BIGINT
    }
}, {
    sequelize,
    modelName: "ShipmentAutoRatingAccessorial",
    freezeTableName: true,
    tableName: "ShipmentAutoRatingAccessorial",
    timestamps: false
});


const ShipmentAutoRatingAccessorialSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    ServiceDescription: {
        type: String
    },
    RateType: {
        type: Number
    },
    Rate: {
        type: Number
    },
    BuyFloor: {
        type: Number
    },
    BuyCeiling: {
        type: Number
    },
    SellFloor: {
        type: Number
    },
    SellCeiling: {
        type: Number
    },
    MarkupValue: {
        type: Number
    },
    MarkupPercent: {
        type: Number
    },
    UseLowerSell: {
        type: Boolean
    },
    ShipmentId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "ShipmentAutoRatingAccessorials"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const ShipmentAutoRatingAccessorialCollection = db.model(
    "ShipmentAutoRatingAccessorials",
    ShipmentAutoRatingAccessorialSchema
);

export {
    ShipmentAutoRatingAccessorial,
    ShipmentAutoRatingAccessorialCollection,
    ShipmentAutoRatingAccessorialSchema
};
