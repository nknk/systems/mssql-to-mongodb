import { STRING, BOOLEAN, DATE, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";

class VendorDocument extends ModelWithTenantId {
    VendorId: number;
    DocumentTagId: number;

}

VendorDocument.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    VendorId: {
        type: BIGINT
    },
    LocationPath: {
        type: STRING
    },
    DocumentTagId: {
        type: BIGINT
    },
    Name: {
        type: STRING
    },
    Description: {
        type: STRING
    },
    IsInternal: {
        type: BOOLEAN
    },
    DateCreated: {
        type: DATE
    }
}, {
    sequelize,
    modelName: "VendorDocument",
    freezeTableName: true,
    tableName: "VendorDocument",
    timestamps: false
});


const VendorDocumentSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    VendorId: {
        type: mongoose.Types.ObjectId
    },
    LocationPath: {
        type: String
    },
    DocumentTagId: {
        type: mongoose.Types.ObjectId
    },
    Name: {
        type: String
    },
    Description: {
        type: String
    },
    IsInternal: {
        type: Boolean
    },
    DateCreated: {
        type: Date
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "VendorDocuments"
});


const db = mongoose.connection.useDb(FINANCE_DB);
const VendorDocumentCollection = db.model(
    "VendorDocuments",
    VendorDocumentSchema
);

export { VendorDocument, VendorDocumentCollection, VendorDocumentSchema };
