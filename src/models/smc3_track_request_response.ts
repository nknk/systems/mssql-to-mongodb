import { STRING, DATE, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { BaseModel } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class SMC3TrackRequestResponse extends BaseModel {
    ShipmentId: number;
}

SMC3TrackRequestResponse.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    ShipmentId: {
        type: BIGINT
    },
    RequestData: {
        type: STRING
    },
    TransactionId: {
        type: STRING
    },
    ResponseData: {
        type: STRING
    },
    DateCreated: {
        type: DATE
    },
    Scac: {
        type: STRING
    }
}, {
    sequelize,
    modelName: "SMC3TrackRequestResponse",
    freezeTableName: true,
    tableName: "SMC3TrackRequestResponse",
    timestamps: false
});


const SMC3TrackRequestResponseSchema = new mongoose.Schema({
    ShipmentId: {
        type: mongoose.Types.ObjectId
    },
    RequestData: {
        type: String
    },
    TransactionId: {
        type: String
    },
    ResponseData: {
        type: String
    },
    DateCreated: {
        type: Date
    },
    Scac: {
        type: String
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "SMC3TrackRequestResponses"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const SMC3TrackRequestResponseCollection = db
    .model("SMC3TrackRequestResponses", SMC3TrackRequestResponseSchema);

export { SMC3TrackRequestResponse, SMC3TrackRequestResponseCollection, SMC3TrackRequestResponseSchema };
