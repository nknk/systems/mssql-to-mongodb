import { BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";

class CustomerGroupMap extends ModelWithTenantId {
    CustomerId: number;
    CustomerGroupId: number;
}

CustomerGroupMap.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    CustomerGroupId:{
        type:BIGINT
    },
    CustomerId:{
        type:BIGINT
    },
}, {
    sequelize,
    modelName: "CustomerGroupMap",
    freezeTableName: true,
    tableName: "CustomerGroupMap",
    timestamps: false
});


const CustomerGroupMapSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    CustomerGroupId:{
        type:mongoose.Types.ObjectId
    },
    CustomerId:{
        type:mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "CustomerGroupMaps"
});

const db = mongoose.connection.useDb(FINANCE_DB);
const CustomerGroupMapCollection = db.model("CustomerGroupMaps", CustomerGroupMapSchema);

export {
    CustomerGroupMap,
    CustomerGroupMapCollection,
    CustomerGroupMapSchema
};
