import { STRING, BOOLEAN, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class LoadOrderContact extends ModelWithTenantId {
    ContactTypeId: number;
    LoadOrderLocationId: number;
}

LoadOrderContact.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    LoadOrderLocationId: {
        type: BIGINT
    },
    Name: {
        type: STRING
    },
    Phone: {
        type: STRING
    },
    Mobile: {
        type: STRING
    },
    Fax: {
        type: STRING
    },
    Email: {
        type: STRING
    },
    Comment: {
        type: STRING
    },
    Primary: {
        type: BOOLEAN
    },
    ContactTypeId: {
        type: BIGINT
    }

}, {
    sequelize,
    modelName: "LoadOrderContact",
    freezeTableName: true,
    tableName: "LoadOrderContact",
    timestamps: false
});

const LoadOrderContactSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    LoadOrderLocationId: {
        type: mongoose.Types.ObjectId
    },
    Name: {
        type: String
    },
    Phone: {
        type: String
    },
    Mobile: {
        type: String
    },
    Fax: {
        type: String
    },
    Email: {
        type: String
    },
    Comment: {
        type: String
    },
    Primary: {
        type: Boolean
    },
    ContactTypeId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "LoadOrderContacts"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const LoadOrderContactCollection = db.model("LoadOrderContacts", LoadOrderContactSchema);

export { LoadOrderContact, LoadOrderContactCollection, LoadOrderContactSchema };
