import { STRING, DATE, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class JobDocument extends ModelWithTenantId {
    JobId: number;
    DocumentTagId: number;
}

JobDocument.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    JobId: {
        type: BIGINT,
    },
    LocationPath: {
        type: STRING,
    },
    DocumentTagId: {
        type: BIGINT,
    },
    Name: {
        type: STRING,
    },
    Description: {
        type: STRING,
    },
    DateCreated: {
        type: DATE,
    }
}, {
    sequelize,
    modelName: "JobDocument",
    freezeTableName: true,
    tableName: "JobDocument",
    timestamps: false
});


const JobDocumentSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    JobId: {
        type: mongoose.Types.ObjectId
    },
    LocationPath: {
        type: String
    },
    DocumentTagId: {
        type: mongoose.Types.ObjectId
    },
    Name: {
        type: String
    },
    Description: {
        type: String
    },
    DateCreated: {
        type: Date
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "JobDocuments"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const JobDocumentCollection = db.model("JobDocuments", JobDocumentSchema);

export { JobDocument, JobDocumentCollection, JobDocumentSchema };
