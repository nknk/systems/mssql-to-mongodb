import { STRING, BOOLEAN, DATE, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class ServiceTicketDocument extends ModelWithTenantId {
    ServiceTicketId: number;
    DocumentTagId: number;
    VendorBillId: number;
}

ServiceTicketDocument.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    ServiceTicketId: {
        type: BIGINT
    },
    LocationPath: {
        type: STRING
    },
    DocumentTagId: {
        type: BIGINT
    },
    Name: {
        type: STRING
    },
    Description: {
        type: STRING
    },
    IsInternal: {
        type: BOOLEAN
    },
    FtpDelivered: {
        type: BOOLEAN
    },
    VendorBillId: {
        type: BIGINT
    },
    DateCreated: {
        type: DATE
    }
}, {
    sequelize,
    modelName: "ServiceTicketDocument",
    freezeTableName: true,
    tableName: "ServiceTicketDocument",
    timestamps: false
});


const ServiceTicketDocumentSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    ServiceTicketId: {
        type: mongoose.Types.ObjectId
    },
    LocationPath: {
        type: String
    },
    DocumentTagId: {
        type: mongoose.Types.ObjectId
    },
    Name: {
        type: String
    },
    Description: {
        type: String
    },
    IsInternal: {
        type: Boolean
    },
    FtpDelivered: {
        type: Boolean
    },
    VendorBillId: {
        type: mongoose.Types.ObjectId
    },
    DateCreated: {
        type: Date
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "ServiceTicketDocuments"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const ServiceTicketDocumentCollection = db.model("ServiceTicketDocuments", ServiceTicketDocumentSchema);

export { ServiceTicketDocument, ServiceTicketDocumentCollection, ServiceTicketDocumentSchema };
