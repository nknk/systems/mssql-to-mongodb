import { STRING, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";

class VendorGroup extends ModelWithTenantId {
}

VendorGroup.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    Name: {
        type: STRING
    },
    Description: {
        type: STRING
    }
}, {
    sequelize,
    modelName: "VendorGroup",
    freezeTableName: true,
    tableName: "VendorGroup",
    timestamps: false
});


const VendorGroupSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    Name: {
        type: String
    },
    Description: {
        type: String
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "VendorGroups"
});


const db = mongoose.connection.useDb(FINANCE_DB);
const VendorGroupCollection = db.model(
    "VendorGroups",
    VendorGroupSchema
);

export { VendorGroup, VendorGroupCollection, VendorGroupSchema };
