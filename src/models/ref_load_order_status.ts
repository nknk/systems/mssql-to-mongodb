import { STRING, INTEGER } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import {BaseModel } from "./contracts/base_models";
import {  OPERATIONS_DB } from "../config";

class RefLoadOrderStatus extends BaseModel {
   LoadOrderStatusIndex: number; 
}

RefLoadOrderStatus.init({
 
    LoadOrderStatusIndex: {
        type: INTEGER,
        primaryKey: true
    },
    LoadOrderStatusText: {
        type: STRING
    },
  
}, {
    sequelize,
    modelName: "RefLoadOrderStatus",
    freezeTableName: true,
    tableName: "RefLoadOrderStatus",
    timestamps: false
});


const RefLoadOrderStatusSchema = new mongoose.Schema({
    LoadOrderStatusIndex: {
        type: Number,
    },
    LoadOrderStatusText: {
        type:String
    },
}, {
    versionKey: false,
    collection: "RefLoadOrderStatuses"
});


const db = mongoose.connection.useDb(OPERATIONS_DB);
const RefLoadOrderStatusCollection = db.model(
    "RefLoadOrderStatuses",
    RefLoadOrderStatusSchema
);

export { RefLoadOrderStatus, RefLoadOrderStatusCollection, RefLoadOrderStatusSchema };
