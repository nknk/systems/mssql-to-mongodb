import { STRING, INTEGER, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";

class InvoiceDetail extends ModelWithTenantId {
    AccountBucketId: number;
    ChargeCodeId: number;
    InvoiceId: number;
}

InvoiceDetail.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    Quantity: {
        type: INTEGER
    },
    UnitSell: {
        type: DECIMAL
    },
    UnitDiscount: {
        type: DECIMAL
    },
    ReferenceNumber: {
        type: STRING
    },
    ReferenceType: {
        type: INTEGER
    },
    Comment: {
        type: STRING
    },
    ChargeCodeId: {
        type: BIGINT
    },
    InvoiceId: {
        type: BIGINT
    },
    AccountBucketId: {
        type: BIGINT
    }
}, {
    sequelize,
    modelName: "InvoiceDetail",
    freezeTableName: true,
    tableName: "InvoiceDetail",
    timestamps: false
});

const InvoiceDetailSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    Quantity: {
        type: Number
    },
    UnitSell: {
        type: Number
    },
    UnitDiscount: {
        type: Number
    },
    ReferenceNumber: {
        type: String
    },
    ReferenceType: {
        type: Number
    },
    Comment: {
        type: String
    },
    ChargeCodeId: {
        type: mongoose.Types.ObjectId
    },
    InvoiceId: {
        type: mongoose.Types.ObjectId
    },
    AccountBucketId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "InvoiceDetails"
});

const db = mongoose.connection.useDb(FINANCE_DB);
const InvoiceDetailCollection = db.model("InvoiceDetails", InvoiceDetailSchema);

export { InvoiceDetail, InvoiceDetailCollection, InvoiceDetailSchema };
