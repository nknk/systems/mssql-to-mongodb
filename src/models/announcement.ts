import { STRING, BOOLEAN, INTEGER, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { UTILITIES_DB } from "../config";
class Announcement extends ModelWithTenantId { }

Announcement.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    Enabled: {
        type: BOOLEAN,
        allowNull: false
    },
    Name: {
        type: STRING,
        allowNull: false
    },
    Type: {
        type: INTEGER,
        allowNull: false
    },
    Message: {
        type: STRING,
        allowNull: false
    },
}, {
    sequelize,
    modelName: "Announcement",
    freezeTableName: true,
    tableName: "Announcement",
    timestamps: false
});

const AnnouncementSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    },
    Enabled: {
        type: Boolean
    },
    Name: {
        type: String
    },
    Type: {
        type: Number
    },
    Message: {
        type: String
    },
}, {
    versionKey: false,
    collection: "Announcements"
});

const db = mongoose.connection.useDb(UTILITIES_DB);
const AnnouncementCollection = db.model("Announcements", AnnouncementSchema);

export { Announcement, AnnouncementCollection, AnnouncementSchema };
