import { STRING, BOOLEAN, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class ShipmentContact extends ModelWithTenantId {
    ShipmentLocationId: number;
    ContactTypeId: number;
}

ShipmentContact.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    ShipmentLocationId: {
        type: BIGINT
    },
    Name: {
        type: STRING
    },
    Phone: {
        type: STRING
    },
    Mobile: {
        type: STRING
    },
    Fax: {
        type: STRING
    },
    Email: {
        type: STRING
    },
    Comment: {
        type: STRING
    },
    Primary: {
        type: BOOLEAN
    },
    ContactTypeId: {
        type: BIGINT
    }
}, {
    sequelize,
    modelName: "ShipmentContact",
    freezeTableName: true,
    tableName: "ShipmentContact",
    timestamps: false
});


const ShipmentContactSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    ShipmentLocationId: {
        type: mongoose.Types.ObjectId
    },
    Name: {
        type: String
    },
    Phone: {
        type: String
    },
    Mobile: {
        type: String
    },
    Fax: {
        type: String
    },
    Email: {
        type: String
    },
    Comment: {
        type: String
    },
    Primary: {
        type: Boolean
    },
    ContactTypeId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "ShipmentContacts"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const ShipmentContactCollection = db.model(
    "ShipmentContacts",
    ShipmentContactSchema
);

export { ShipmentContact, ShipmentContactCollection, ShipmentContactSchema };
