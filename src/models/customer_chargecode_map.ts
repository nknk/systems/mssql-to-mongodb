import { STRING, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";
class CustomerChargeCodeMap extends ModelWithTenantId {
    ChargeCodeId: string;
    CustomerId: string;
}

CustomerChargeCodeMap.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    ChargeCodeId: {
        type: BIGINT,
        allowNull: false
    },
    CustomerId: {
        type: BIGINT,
        allowNull: false
    },
    ExternalCode: {
        type: STRING,
        allowNull: false
    },
    ExternalDescription: {
        type: STRING,
        allowNull: false
    },
}, {
    sequelize,
    modelName: "CustomerChargeCodeMap",
    freezeTableName: true,
    tableName: "CustomerChargeCodeMap",
    timestamps: false
});

const CustomerChargeCodeMapSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    ChargeCodeId: {
        type: mongoose.Types.ObjectId
    },
    CustomerId: {
        type: mongoose.Types.ObjectId
    },
    ExternalCode: {
        type: String
    },
    ExternalDescription: {
        type: String
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "CustomerChargeCodeMaps"
});

const db = mongoose.connection.useDb(FINANCE_DB);
const CustomerChargeCodeMapCollection = db.model(
    "CustomerChargeCodeMaps",
    CustomerChargeCodeMapSchema,
);

export {
    CustomerChargeCodeMap,
    CustomerChargeCodeMapCollection,
    CustomerChargeCodeMapSchema
};
