import { STRING, DATE, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { AUDITLOG_DB } from "../config";

class BackgroundReportRunRecord extends ModelWithTenantId {
    UserId: number;
}

BackgroundReportRunRecord.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    UserId: {
        type: BIGINT,
        allowNull: false
    },
    Query: {
        type: STRING,
        allowNull: false
    },
    Parameters: {
        type: STRING,
        allowNull: false
    },
    DateCreated: {
        type: DATE,
        allowNull: false
    },
    ExpirationDate: {
        type: DATE,
        allowNull: false
    },
    ReportCustomization: {
        type: STRING,
        allowNull: false
    },
    CompletedReportFileName: {
        type: STRING,
        allowNull: false
    },
    ReportName: {
        type: STRING,
        allowNull: false
    },
}, {
    sequelize,
    modelName: "BackgroundReportRunRecord",
    freezeTableName: true,
    tableName: "BackgroundReportRunRecord",
    timestamps: false
});


const BackgroundReportRunRecordSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    },
    UserId: {
        type: mongoose.Types.ObjectId
    },
    Query: {
        type: String
    },
    Parameters: {
        type: String
    },
    DateCreated: {
        type: Date
    },
    ExpirationDate: {
        type: Date
    },
    ReportCustomization: {
        type: String
    },
    CompletedReportFileName: {
        type: String
    },
    ReportName: {
        type: String
    },
}, {
    versionKey: false,
    collection: "BackgroundReportRunRecords"
});

const db = mongoose.connection.useDb(AUDITLOG_DB);
const BackgroundReportRunRecordCollection = db
    .model("BackgroundReportRunRecords",
        BackgroundReportRunRecordSchema);

export {
    BackgroundReportRunRecord,
    BackgroundReportRunRecordCollection,
    BackgroundReportRunRecordSchema,
};
