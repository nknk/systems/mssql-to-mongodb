import { BOOLEAN, DATE, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";
class CustomerServiceMarkup extends ModelWithTenantId {
    CustomerRatingId: number;
    ServiceId: number;
}

CustomerServiceMarkup.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    MarkupPercent: {
        type: DECIMAL,
        allowNull: false,
    },
    MarkupValue: {
        type: DECIMAL,
        allowNull: false,
    },
    UseMinimum: {
        type: BOOLEAN,
        allowNull: false,
    },
    EffectiveDate: {
        type: DATE,
        allowNull: false,
    },
    Active: {
        type: BOOLEAN,
        allowNull: false,
    },
    ServiceChargeCeiling: {
        type: DECIMAL,
        allowNull: false,
    },
    ServiceChargeFloor: {
        type: DECIMAL,
        allowNull: false,
    },
    CustomerRatingId: {
        type: BIGINT,
        allowNull: false,
    },
    ServiceId: {
        type: BIGINT,
        allowNull: false,
    },
}, {
    sequelize,
    modelName: "CustomerServiceMarkup",
    freezeTableName: true,
    tableName: "CustomerServiceMarkup",
    timestamps: false
});


const CustomerServiceMarkupSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    },
    MarkupPercent: {
        type: Number,
    },
    MarkupValue: {
        type: Number,
    },
    UseMinimum: {
        type: Boolean,
    },
    EffectiveDate: {
        type: Date,
    },
    Active: {
        type: Boolean,
    },
    ServiceChargeCeiling: {
        type: Number,
    },
    ServiceChargeFloor: {
        type: Number,
    },
    CustomerRatingId: {
        type: mongoose.Types.ObjectId,
    },
    ServiceId: {
        type: mongoose.Types.ObjectId,
    },
}, {
    versionKey: false,
    collection: "CustomerServiceMarkups"
});

const db = mongoose.connection.useDb(FINANCE_DB);
const CustomerServiceMarkupCollection = db.model(
    "CustomerServiceMarkups",
    CustomerServiceMarkupSchema
);

export {
    CustomerServiceMarkup,
    CustomerServiceMarkupCollection,
    CustomerServiceMarkupSchema
};
