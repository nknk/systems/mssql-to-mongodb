import mongoose from "mongoose";
import { AUDITLOG_DB } from "../config";

const ObjectAssociationSchema = new mongoose.Schema({
    EntityId: {
        type: mongoose.Types.ObjectId
    },
    EntityName: {
        type: String,
    },
    Occurrences: {
        type: Number,
    },
    Status: {
        type: Number,
    },
}, {
    versionKey: false,
    collection: "ObjectAssociations"
});

const db = mongoose.connection.useDb(AUDITLOG_DB);
const ObjectAssociationCollection = db.model("ObjectAssociations", ObjectAssociationSchema);

export { ObjectAssociationCollection };
