import { STRING, INTEGER } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import {BaseModel } from "./contracts/base_models";
import {  OPERATIONS_DB } from "../config";

class RefValuePercentageType extends BaseModel {
   ValuePercentageTypeIndex: number; 
}

RefValuePercentageType.init({
 
    ValuePercentageTypeIndex: {
        type: INTEGER,
        primaryKey: true
    },
    ValuePercentageTypeText: {
        type: STRING
    },
  
}, {
    sequelize,
    modelName: "RefValuePercentageType",
    freezeTableName: true,
    tableName: "RefValuePercentageType",
    timestamps: false
});


const RefValuePercentageTypeSchema = new mongoose.Schema({
    ValuePercentageTypeIndex: {
        type: Number,
    },
    ValuePercentageTypeText: {
        type:String
    },
   
}, {
    versionKey: false,
    collection: "RefValuePercentageTypes"
});


const db = mongoose.connection.useDb(OPERATIONS_DB);
const RefValuePercentageTypeCollection = db.model(
    "RefValuePercentageTypes",
    RefValuePercentageTypeSchema
);

export { RefValuePercentageType, RefValuePercentageTypeCollection, RefValuePercentageTypeSchema };
