import { STRING, INTEGER, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";

class VendorBillDetail extends ModelWithTenantId {
    ChargeCodeId: number;
    VendorBillId: number;
    AccountBucketId: number;

}

VendorBillDetail.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    VendorBillId: {
        type: BIGINT
    },
    ChargeCodeId: {
        type: BIGINT
    },
    Quantity: {
        type: INTEGER
    },
    UnitBuy: {
        type: DECIMAL
    },
    ReferenceNumber: {
        type: STRING
    },
    ReferenceType: {
        type: INTEGER
    },
    AccountBucketId: {
        type: BIGINT
    },
    Note: {
        type: STRING
    }
}, {
    sequelize,
    modelName: "VendorBillDetail",
    freezeTableName: true,
    tableName: "VendorBillDetail",
    timestamps: false
});


const VendorBillDetailSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    VendorBillId: {
        type: mongoose.Types.ObjectId
    },
    ChargeCodeId: {
        type: mongoose.Types.ObjectId
    },
    Quantity: {
        type: Number
    },
    UnitBuy: {
        type: Number
    },
    ReferenceNumber: {
        type: String
    },
    ReferenceType: {
        type: Number
    },
    AccountBucketId: {
        type: mongoose.Types.ObjectId
    },
    Note: {
        type: String
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "VendorBillDetails"
});


const db = mongoose.connection.useDb(FINANCE_DB);
const VendorBillDetailCollection = db.model(
    "VendorBillDetails",
    VendorBillDetailSchema
);

export { VendorBillDetail, VendorBillDetailCollection, VendorBillDetailSchema };
