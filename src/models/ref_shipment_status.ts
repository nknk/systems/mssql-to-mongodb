import { STRING, INTEGER } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import {BaseModel } from "./contracts/base_models";
import {  OPERATIONS_DB } from "../config";

class RefShipmentStatus extends BaseModel {
   ShipmentStatusIndex: number; 
}

RefShipmentStatus.init({
 
    ShipmentStatusIndex: {
        type: INTEGER,
        primaryKey: true
    },
    ShipmentStatusText: {
        type: STRING
    },
  
}, {
    sequelize,
    modelName: "RefShipmentStatus",
    freezeTableName: true,
    tableName: "RefShipmentStatus",
    timestamps: false
});


const RefShipmentStatusSchema = new mongoose.Schema({
    ShipmentStatusIndex: {
        type: Number,
    },
    ShipmentStatusText: {
        type:String
    },
}, {
    versionKey: false,
    collection: "RefShipmentStatuses"
});


const db = mongoose.connection.useDb(OPERATIONS_DB);
const RefShipmentStatusCollection = db.model(
    "RefShipmentStatuses",
    RefShipmentStatusSchema
);

export { RefShipmentStatus, RefShipmentStatusCollection, RefShipmentStatusSchema };
