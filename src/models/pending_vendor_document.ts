import { STRING, BOOLEAN, DATE, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";

class PendingVendorDocument extends ModelWithTenantId {
    PendingVendorId: number;
    DocumentTagId: number;
}

PendingVendorDocument.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    PendingVendorId: {
        type: BIGINT,
        allowNull: false
    },
    LocationPath: {
        type: STRING,
        allowNull: false
    },
    DocumentTagId: {
        type: BIGINT,
        allowNull: false
    },
    Name: {
        type: STRING,
        allowNull: false
    },
    Description: {
        type: STRING,
        allowNull: false
    },
    IsInternal: {
        type: BOOLEAN,
        allowNull: false
    },
    DateCreated: {
        type: DATE,
        allowNull: false
    },
}, {
    sequelize,
    modelName: "PendingVendorDocument",
    freezeTableName: true,
    tableName: "PendingVendorDocument",
    timestamps: false
});


const PendingVendorDocumentSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    },
    PendingVendorId: {
        type: mongoose.Types.ObjectId

    },
    LocationPath: {
        type: String
    },
    DocumentTagId: {
        type: mongoose.Types.ObjectId
    },
    Name: {
        type: String
    },
    Description: {
        type: String
    },
    IsInternal: {
        type: Boolean
    },
    DateCreated: {
        type: Date
    },
}, {
    versionKey: false,
    collection: "PendingVendorDocuments"
});

const db = mongoose.connection.useDb(FINANCE_DB);
const PendingVendorDocumentCollection = db
    .model("PendingVendorDocuments", PendingVendorDocumentSchema);

export {
    PendingVendorDocument,
    PendingVendorDocumentCollection,
    PendingVendorDocumentSchema
};
