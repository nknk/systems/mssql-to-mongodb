import { BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { OPERATIONS_DB } from "../config";
import { ModelWithTenantId } from "./contracts/base_models";

class VendorGroupMap extends ModelWithTenantId {
    VendorGroupId: number;
    VendorId: number;
}

VendorGroupMap.init({    
    TenantId: {
        type: BIGINT
    },
    VendorGroupId: {
        type: BIGINT
    },
    VendorId: {
        type: BIGINT
    }
}, {
    sequelize,
    modelName: "VendorGroupMap",
    freezeTableName: true,
    tableName: "VendorGroupMap",
    timestamps: false
});


const VendorGroupMapSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    VendorGroupId: {
        type: mongoose.Types.ObjectId
    },
    VendorId: {
        type: mongoose.Types.ObjectId
    }
}, {
    versionKey: false,
    collection: "VendorGroupMaps"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const VendorGroupMapCollection = db.model("VendorGroupMaps", VendorGroupMapSchema);

export { VendorGroupMap, VendorGroupMapCollection, VendorGroupMapSchema };
