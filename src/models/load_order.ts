import { STRING, INTEGER, BOOLEAN, DATE, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";
class LoadOrder extends ModelWithTenantId {
    CustomerId: number;
    PrefixId: number;
    MileageSourceId: number;
    SalesRepresentativeId: number;
    AccountBucketUnitId: number;
    ResellerAdditionId: number;
    CarrierCoordinatorUserId: number;
    CarrierCoordinatorId?: number;
    LoadOrderCoordinatorUserId: number;
    LoadOrderCoordinatorId?: number;
    ShipmentPriorityId: number;
    UserId: number;
    OriginId: number;
    DestinationId: number;
    JobId: number;

}

LoadOrder.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    LoadOrderNumber: {
        type: STRING
    },
    PrefixId: {
        type: BIGINT
    },
    HidePrefix: {
        type: BOOLEAN
    },
    ShipperBol: {
        type: STRING
    },
    PurchaseOrderNumber: {
        type: STRING
    },
    ShipperReference: {
        type: STRING
    },
    HazardousMaterial: {
        type: BOOLEAN
    },
    HazardousMaterialContactName: {
        type: STRING
    },
    HazardousMaterialContactPhone: {
        type: STRING
    },
    HazardousMaterialContactMobile: {
        type: STRING
    },
    HazardousMaterialContactEmail: {
        type: STRING
    },
    DesiredPickupDate: {
        type: DATE
    },
    EarlyPickup: {
        type: STRING
    },
    LatePickup: {
        type: STRING
    },
    EstimatedDeliveryDate: {
        type: DATE
    },
    EarlyDelivery: {
        type: STRING
    },
    LateDelivery: {
        type: STRING
    },
    Status: {
        type: INTEGER
    },
    DateCreated: {
        type: DATE
    },
    ServiceMode: {
        type: INTEGER
    },
    IsPartialTruckload: {
        type: BOOLEAN
    },
    DeclineInsurance: {
        type: BOOLEAN
    },
    LinearFootRuleBypassed: {
        type: BOOLEAN
    },
    GeneralBolComments: {
        type: STRING
    },
    CriticalBolComments: {
        type: STRING
    },
    MiscField1: {
        type: STRING
    },
    MiscField2: {
        type: STRING
    },
    EmptyMileage: {
        type: DECIMAL
    },
    Mileage: {
        type: DECIMAL
    },
    MileageSourceId: {
        type: BIGINT
    },
    SalesRepresentativeId: {
        type: BIGINT
    },
    SalesRepresentativeCommissionPercent: {
        type: DECIMAL
    },
    ResellerAdditionId: {
        type: BIGINT
    },
    AccountBucketUnitId: {
        type: BIGINT
    },
    BillReseller: {
        type: BOOLEAN
    },
    CarrierCoordinatorUserId: {
        type: BIGINT
    },
    LoadOrderCoordinatorUserId: {
        type: BIGINT
    },
    UserId: {
        type: BIGINT
    },
    CustomerId: {
        type: BIGINT
    },
    OriginId: {
        type: BIGINT
    },
    DestinationId: {
        type: BIGINT
    },
    ShipmentPriorityId: {
        type: BIGINT
    },
    NotAvailableToLoadboards: {
        type: BOOLEAN
    },
    Description: {
        type: STRING
    },
    SalesRepAddlEntityName: {
        type: STRING
    },
    SalesRepAddlEntityCommPercent: {
        type: DECIMAL
    },
    SalesRepMinCommValue: {
        type: DECIMAL
    },
    SalesRepMaxCommValue: {
        type: DECIMAL
    },
    DriverName: {
        type: STRING
    },
    DriverPhoneNumber: {
        type: STRING
    },
    DriverTrailerNumber: {
        type: STRING
    },
    JobId: {
        type: BIGINT
    },
    JobStep: {
        type: INTEGER
    },
    RMANumber: {
        type: STRING
    }

}, {
    sequelize,
    modelName: "LoadOrder",
    freezeTableName: true,
    tableName: "LoadOrder",
    timestamps: false
});


const LoadOrderSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },

    LoadOrderNumber: {
        type: String
    },
    PrefixId: {
        type: mongoose.Types.ObjectId
    },
    HidePrefix: {
        type: Boolean
    },
    ShipperBol: {
        type: String
    },
    PurchaseOrderNumber: {
        type: String
    },
    ShipperReference: {
        type: String
    },
    HazardousMaterial: {
        type: Boolean
    },
    HazardousMaterialContactName: {
        type: String
    },
    HazardousMaterialContactPhone: {
        type: String
    },
    HazardousMaterialContactMobile: {
        type: String
    },
    HazardousMaterialContactEmail: {
        type: String
    },
    DesiredPickupDate: {
        type: Date
    },
    EarlyPickup: {
        type: String
    },
    LatePickup: {
        type: String
    },
    EstimatedDeliveryDate: {
        type: Date
    },
    EarlyDelivery: {
        type: String
    },
    LateDelivery: {
        type: String
    },
    Status: {
        type: Number
    },
    DateCreated: {
        type: Date
    },
    ServiceMode: {
        type: Number
    },
    IsPartialTruckload: {
        type: Boolean
    },
    DeclineInsurance: {
        type: Boolean
    },
    LinearFootRuleBypassed: {
        type: Boolean
    },
    GeneralBolComments: {
        type: String
    },
    CriticalBolComments: {
        type: String
    },
    MiscField1: {
        type: String
    },
    MiscField2: {
        type: String
    },
    EmptyMileage: {
        type: Number
    },
    Mileage: {
        type: Number
    },
    MileageSourceId: {
        type: mongoose.Types.ObjectId
    },
    SalesRepresentativeId: {
        type: mongoose.Types.ObjectId
    },
    SalesRepresentativeCommissionPercent: {
        type: Number
    },
    ResellerAdditionId: {
        type: mongoose.Types.ObjectId
    },
    AccountBucketUnitId: {
        type: mongoose.Types.ObjectId
    },
    BillReseller: {
        type: Boolean
    },
    CarrierCoordinatorId: {
        type: mongoose.Types.ObjectId
    },
    LoadOrderCoordinatorId: {
        type: mongoose.Types.ObjectId
    },
    UserId: {
        type: mongoose.Types.ObjectId
    },
    CustomerId: {
        type: mongoose.Types.ObjectId
    },
    OriginId: {
        type: mongoose.SchemaTypes.Mixed
    },
    DestinationId: {
        type: mongoose.SchemaTypes.Mixed
    },
    ShipmentPriorityId: {
        type: mongoose.Types.ObjectId
    },
    NotAvailableToLoadboards: {
        type: Boolean
    },
    Description: {
        type: String
    },
    SalesRepAddlEntityName: {
        type: String
    },
    SalesRepAddlEntityCommPercent: {
        type: Number
    },
    SalesRepMinCommValue: {
        type: Number
    },
    SalesRepMaxCommValue: {
        type: Number
    },
    DriverName: {
        type: String
    },
    DriverPhoneNumber: {
        type: String
    },
    DriverTrailerNumber: {
        type: String
    },
    JobId: {
        type: mongoose.Types.ObjectId
    },
    JobStep: {
        type: Number
    },
    RMANumber: {
        type: String
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "LoadOrders"
});

export interface ILoadOrderDoc extends mongoose.Document {
    OriginId: string;
    DestinationId: string;
    CarrierCoordinatorId: string;
    LoadOrderCoordinatorId: string;
}

const db = mongoose.connection.useDb(OPERATIONS_DB);
const LoadOrderCollection = db.model<ILoadOrderDoc>("LoadOrders", LoadOrderSchema);

export { LoadOrder, LoadOrderCollection, LoadOrderSchema };
