import { STRING, BOOLEAN, INTEGER, DECIMAL, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { RATING_DB } from "../config";

class AdditionalChargeIndex extends ModelWithTenantId {
    CountryId: number;
    LTLAdditionalChargeId: number;
    RegionId: number;
}

AdditionalChargeIndex.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    PostalCode: {
        type: STRING
    },
    CountryId: {
        type: BIGINT
    },
    UsePostalCode: {
        type: BOOLEAN
    },
    ApplyOnOrigin: {
        type: BOOLEAN
    },
    ApplyOnDestination: {
        type: BOOLEAN
    },
    RateType: {
        type: INTEGER,
    },
    Rate: {
        type: DECIMAL
    },
    ChargeFloor: {
        type: DECIMAL
    },
    ChargeCeiling: {
        type: DECIMAL
    },
    LTLAdditionalChargeId: {
        type: BIGINT
    },
    RegionId: {
        type: BIGINT
    }
}, {
    sequelize,
    modelName: "AdditionalChargeIndex",
    freezeTableName: true,
    tableName: "AdditionalChargeIndex",
    timestamps: false
});

const AdditionalChargeIndexSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.SchemaTypes.ObjectId,
    },
    PostalCode: {
        type: String
    },
    CountryId: {
        type: mongoose.SchemaTypes.ObjectId,
    },
    UsePostalCode: {
        type: Boolean,
    },
    ApplyOnOrigin: {
        type: Boolean,
    },
    ApplyOnDestination: {
        type: Boolean,
    },
    RateType: {
        type: Number,
    },
    Rate: {
        type: Number,
    },
    ChargeFloor: {
        type: Number,
    },
    ChargeCeiling: {
        type: Number,
    },
    LTLAdditionalChargeId: {
        type: mongoose.SchemaTypes.ObjectId
    },
    RegionId: {
        type: mongoose.SchemaTypes.ObjectId
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "AdditionalChargeIndices"
});

const db = mongoose.connection.useDb(RATING_DB);
const AdditionalChargeIndexCollection = db
    .model(
        "AdditionalChargeIndices",
        AdditionalChargeIndexSchema
    );

export {
    AdditionalChargeIndex,
    AdditionalChargeIndexCollection,
    AdditionalChargeIndexSchema
};
