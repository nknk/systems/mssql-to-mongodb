import { STRING, INTEGER } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import {BaseModel } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class RefXmlConnectStatus extends BaseModel {
   XmlConnectStatusIndex: number; 
}

RefXmlConnectStatus.init({
 
    XmlConnectStatusIndex: {
        type: INTEGER,
        primaryKey: true
    },
    XmlConnectStatusText: {
        type: STRING
    },
  
}, {
    sequelize,
    modelName: "RefXmlConnectStatus",
    freezeTableName: true,
    tableName: "RefXmlConnectStatus",
    timestamps: false
});


const RefXmlConnectStatusSchema = new mongoose.Schema({
    XmlConnectStatusIndex: {
        type: Number,
    },
    XmlConnectStatusText: {
        type:String
    }
}, {
    versionKey: false,
    collection: "RefXmlConnectStatuses"
});


const db = mongoose.connection.useDb(OPERATIONS_DB);
const RefXmlConnectStatusCollection = db.model(
    "RefXmlConnectStatuses",
    RefXmlConnectStatusSchema
);

export { RefXmlConnectStatus, RefXmlConnectStatusCollection, RefXmlConnectStatusSchema };
