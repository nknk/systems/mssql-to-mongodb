import { STRING, INTEGER } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { BaseModel } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class RefNotificationMethod extends BaseModel {
    NotificationMethodIndex: number;
}

RefNotificationMethod.init({

    NotificationMethodIndex: {
        type: INTEGER,
        primaryKey: true
    },
    NotificationMethodText: {
        type: STRING
    },

}, {
    sequelize,
    modelName: "RefNotificationMethod",
    freezeTableName: true,
    tableName: "RefNotificationMethod",
    timestamps: false
});


const RefNotificationMethodSchema = new mongoose.Schema({
    NotificationMethodIndex: {
        type: Number,
    },
    NotificationMethodText: {
        type: String
    },
}, {
    versionKey: false,
    collection: "RefNotificationMethods"
});


const db = mongoose.connection.useDb(OPERATIONS_DB);
const RefNotificationMethodCollection = db.model(
    "RefNotificationMethods",
    RefNotificationMethodSchema
);

export { RefNotificationMethod, RefNotificationMethodCollection, RefNotificationMethodSchema };
