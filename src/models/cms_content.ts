import { STRING, DATE, BOOLEAN, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { BaseModel } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class CmsContent extends BaseModel { }

CmsContent.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    ContentKey: {
        type: STRING,
    },
    FullText: {
        type: STRING,
    },
    IdentifierCode: {
        type: STRING,
    },
    DateCreated: {
        type: DATE,
    },
    Current: {
        type: BOOLEAN,
    },
    ShortName: {
        type: STRING,
    },
    ShortText: {
        type: STRING,
    },
}, {
    sequelize,
    modelName: "CmsContent",
    freezeTableName: true,
    tableName: "CmsContent",
    timestamps: false
});


const CmsContentSchema = new mongoose.Schema({
    ContentKey: {
        type: String,
    },
    FullText: {
        type: String,
    },
    IdentifierCode: {
        type: String,
    },
    DateCreated: {
        type: Date,
    },
    Current: {
        type: Boolean,
    },
    ShortName: {
        type: String,
    },
    ShortText: {
        type: String,
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "CmsContents"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const CmsContentCollection = db.model("CmsContents", CmsContentSchema);

export {
    CmsContent,
    CmsContentCollection,
    CmsContentSchema
};
