import { STRING, DATE, BOOLEAN, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class ShipmentDocument extends ModelWithTenantId {
    ShipmentId: number;
    DocumentTagId: number;
    VendorBillId: number;
}

ShipmentDocument.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    ShipmentId: {
        type: BIGINT
    },
    LocationPath: {
        type: STRING
    },
    DocumentTagId: {
        type: BIGINT
    },
    Name: {
        type: STRING
    },
    Description: {
        type: STRING
    },
    IsInternal: {
        type: BOOLEAN
    },
    FtpDelivered: {
        type: BOOLEAN
    },
    VendorBillId: {
        type: BIGINT
    },
    DateCreated: {
        type: DATE
    }    
}, {
    sequelize,
    modelName: "ShipmentDocument",
    freezeTableName: true,
    tableName: "ShipmentDocument",
    timestamps: false
});


const ShipmentDocumentSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    ShipmentId: {
        type: mongoose.Types.ObjectId
    },
    LocationPath: {
        type: String
    },
    DocumentTagId: {
        type: mongoose.Types.ObjectId
    },
    Name: {
        type: String
    },
    Description: {
        type: String
    },
    IsInternal: {
        type: Boolean
    },
    FtpDelivered: {
        type: Boolean
    },
    VendorBillId: {
        type: mongoose.Types.ObjectId
    },
    DateCreated: {
        type: Date
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "ShipmentDocuments"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const ShipmentDocumentCollection = db.model(
    "ShipmentDocuments",
    ShipmentDocumentSchema
);

export { ShipmentDocument, ShipmentDocumentCollection, ShipmentDocumentSchema };
