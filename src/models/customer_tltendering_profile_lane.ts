import { STRING, DATE, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";
class CustomerTLTenderingProfileLane extends ModelWithTenantId {
    CustomerId: number;
    OriginCountryId: number;
    DestinationCountryId: number;
    TLTenderingProfileId: number;
    ThirdPreferredVendorId: number;
    SecondPreferredVendorId: number;
    FirstPreferredVendorId: number;

}

CustomerTLTenderingProfileLane.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },

    TenantId: {
        type: BIGINT,
        allowNull: false
    },

    DateCreated: {
        type: DATE
    },

    OriginCity: {
        type: STRING
    },

    OriginState: {
        type: STRING
    },

    OriginCountryId: {
        type: BIGINT
    },

    OriginPostalCode: {
        type: STRING
    },

    DestinationCity: {
        type: STRING
    },

    DestinationState: {
        type: STRING
    },

    DestinationCountryId: {
        type: BIGINT
    },

    DestinationPostalCode: {
        type: STRING
    },

    FirstPreferredVendorId: {
        type: BIGINT
    },

    SecondPreferredVendorId: {
        type: BIGINT
    },

    ThirdPreferredVendorId: {
        type: BIGINT
    },
    TLTenderingProfileId: {
        type: BIGINT
    },
}, {
    sequelize,
    modelName: "CustomerTLTenderingProfileLane",
    freezeTableName: true,
    tableName: "CustomerTLTenderingProfileLane",
    timestamps: false
});


const CustomerTLTenderingProfileLaneSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },

    DateCreated: {
        type: Date
    },

    OriginCity: {
        type: String
    },

    OriginState: {
        type: String
    },

    OriginCountryId: {
        type: mongoose.Types.ObjectId
    },

    OriginPostalCode: {
        type: String
    },

    DestinationCity: {
        type: String
    },

    DestinationState: {
        type: String
    },

    DestinationCountryId: {
        type: mongoose.Types.ObjectId
    },

    DestinationPostalCode: {
        type: String
    },
    FirstPreferredVendorId: {
        type: mongoose.Types.ObjectId
    },
    SecondPreferredVendorId: {
        type: mongoose.Types.ObjectId
    },
    ThirdPreferredVendorId: {
        type: mongoose.Types.ObjectId
    },
    TLTenderingProfileId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "CustomerTLTenderingProfileLanes"
});


const db = mongoose.connection.useDb(FINANCE_DB);
const CustomerTLTenderingProfileLaneCollection = db.model(
    "CustomerTLTenderingProfileLanes",
    CustomerTLTenderingProfileLaneSchema
);

export {
    CustomerTLTenderingProfileLane,
    CustomerTLTenderingProfileLaneCollection,
    CustomerTLTenderingProfileLaneSchema
};
