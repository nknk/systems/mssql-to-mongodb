import { STRING, BOOLEAN, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { REGISTRY_DB } from "../config";
class AccountBucketUnit extends ModelWithTenantId {
    AccountBucketId: number;
}

AccountBucketUnit.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    Name: {
        type: STRING,
        allowNull: false,
    },
    Description: {
        type: STRING,
        allowNull: false,
    },
    Active: {
        type: BOOLEAN,
        allowNull: false,
    },
    AccountBucketId: {
        type: BIGINT,
        allowNull: false,
    },
}, {
    sequelize,
    modelName: "AccountBucketUnit",
    freezeTableName: true,
    tableName: "AccountBucketUnit",
    timestamps: false
});

const AccountBucketUnitSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    Name: {
        type: String
    },
    Description: {
        type: String
    },
    Active: {
        type: Boolean
    },
    OldId: {
        type: Number
    },
    AccountBucketId: {
        type: mongoose.Types.ObjectId
    }
}, {
    versionKey: false,
    collection: "AccountBucketUnits"
});

const db = mongoose.connection.useDb(REGISTRY_DB);
const AccountBucketUnitCollection = db.model("AccountBucketUnits", AccountBucketUnitSchema);

export { AccountBucketUnit, AccountBucketUnitCollection, AccountBucketUnitSchema };
