import { STRING, INTEGER, DATE, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class MacroPointOrder extends ModelWithTenantId { 
    UserId: number;
}

MacroPointOrder.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    UserId: {
        type: BIGINT,
    },
    IdNumber: {
        type: STRING,
    },
    TrackDurationHours: {
        type: INTEGER,
    },
    TrackIntervalMinutes: {
        type: INTEGER,
    },
    TrackCost: {
        type: DECIMAL,
    },
    OrderId: {
        type: STRING,
    },
    TrackingStatusCode: {
        type: STRING,
    },
    TrackingStatusDescription: {
        type: STRING,
    },
    DateCreated: {
        type: DATE,
    },
    NumberType: {
        type: STRING,
    },
    Number: {
        type: STRING,
    },
    DateStopRequested: {
        type: DATE,
    },
    StartTrackDateTime: {
        type: DATE,
    },
    Notes: {
        type: STRING,
    },
    EmailCopiesOfUpdatesTo: {
        type: STRING,
    },
}, {
    sequelize,
    modelName: "MacroPointOrder",
    freezeTableName: true,
    tableName: "MacroPointOrder",
    timestamps: false
});


const MacroPointOrderSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    UserId: {
        type: mongoose.Types.ObjectId,
    },
    IdNumber: {
        type: String,
    },
    TrackDurationHours: {
        type: Number,
    },
    TrackIntervalMinutes: {
        type: Number,
    },
    TrackCost: {
        type: Number,
    },
    OrderId: {
        type: String,
    },
    TrackingStatusCode: {
        type: String,
    },
    TrackingStatusDescription: {
        type: String,
    },
    DateCreated: {
        type: Date,
    },
    NumberType: {
        type: String,
    },
    Number: {
        type: String,
    },
    DateStopRequested: {
        type: Date,
    },
    StartTrackDateTime: {
        type: Date,
    },
    Notes: {
        type: String,
    },
    EmailCopiesOfUpdatesTo: {
        type: String,
    },
    OldId: {
        type: Number
    },
}, {
    versionKey: false,
    collection: "MacroPointOrders"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const MacroPointOrderCollection = db.model("MacroPointOrders", MacroPointOrderSchema);

export {
    MacroPointOrder,
    MacroPointOrderCollection,
    MacroPointOrderSchema
};
