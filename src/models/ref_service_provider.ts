import { STRING, INTEGER } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import {BaseModel } from "./contracts/base_models";
import {  OPERATIONS_DB } from "../config";

class RefServiceProvider extends BaseModel {
   ServiceProviderIndex: number; 
}

RefServiceProvider.init({
 
    ServiceProviderIndex: {
        type: INTEGER,
        primaryKey: true
    },
    ServiceProviderText: {
        type: STRING
    },
  
}, {
    sequelize,
    modelName: "RefServiceProvider",
    freezeTableName: true,
    tableName: "RefServiceProvider",
    timestamps: false
});


const RefServiceProviderSchema = new mongoose.Schema({
    ServiceProviderIndex: {
        type: Number,
        primaryKey: true
    },
    ServiceProviderText: {
        type:String
    },
}, {
    versionKey: false,
    collection: "RefServiceProviders"
});


const db = mongoose.connection.useDb(OPERATIONS_DB);
const RefServiceProviderCollection = db.model(
    "RefServiceProviders",
    RefServiceProviderSchema
);

export { RefServiceProvider, RefServiceProviderCollection, RefServiceProviderSchema };
