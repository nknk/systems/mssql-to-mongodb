import { STRING, BOOLEAN, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { REGISTRY_DB } from "../config";

class DocumentTag extends ModelWithTenantId {}

DocumentTag.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    Code: {
        type: STRING,
        allowNull: false
    },
    Description: {
        type: STRING,
        allowNull: false
    },
    NonEmployeeVisible: {
        type: BOOLEAN,
        allowNull: false
    }
}, {
    sequelize,
    modelName: "DocumentTag",
    freezeTableName: true,
    tableName: "DocumentTag",
    timestamps: false
});


const DocumentTagSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    Code: {
        type: String
    },
    Description: {
        type: String
    },
    NonEmployeeVisible: {
        type: Boolean
    },
    OldId: {
        type: Number
    },
}, {
    versionKey: false,
    collection: "DocumentTags"
});

const db = mongoose.connection.useDb(REGISTRY_DB);
const DocumentTagCollection = db.model("DocumentTags", DocumentTagSchema);

export { DocumentTag, DocumentTagCollection, DocumentTagSchema };
