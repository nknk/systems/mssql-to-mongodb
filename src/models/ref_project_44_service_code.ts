import { STRING, INTEGER } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { BaseModel } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class RefProject44ServiceCode extends BaseModel {
    Project44ServiceCodeIndex: number;
}

RefProject44ServiceCode.init({

    Project44ServiceCodeIndex: {
        type: INTEGER,
        primaryKey: true
    },
    Project44ServiceCodeText: {
        type: STRING
    },
    Project44ServiceCodeDescription: {
        type: STRING
    }
}, {
    sequelize,
    modelName: "RefProject44ServiceCode",
    freezeTableName: true,
    tableName: "RefProject44ServiceCode",
    timestamps: false
});


const RefProject44ServiceCodeSchema = new mongoose.Schema({
    Project44ServiceCodeIndex: {
        type: Number,
    },
    Project44ServiceCodeText: {
        type: String
    },
    Project44ServiceCodeDescription: {
        type: String
    },
}, {
    versionKey: false,
    collection: "RefProject44ServiceCodes"
});


const db = mongoose.connection.useDb(OPERATIONS_DB);
const RefProject44ServiceCodeCollection = db.model(
    "RefProject44ServiceCodes",
    RefProject44ServiceCodeSchema
);

export { RefProject44ServiceCode, RefProject44ServiceCodeCollection, RefProject44ServiceCodeSchema };
