import { INTEGER, STRING, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { RATING_DB } from "../config";

class SmallPackageServiceMap extends ModelWithTenantId {
    ServiceId: number;
}

SmallPackageServiceMap.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    SmallPackageEngine: {
        type: INTEGER
    },
    ServiceId: {
        type: BIGINT
    },
    SmallPackEngineService: {
        type: STRING
    }
}, {
    sequelize,
    modelName: "SmallPackageServiceMap",
    freezeTableName: true,
    tableName: "SmallPackageServiceMap",
    timestamps: false
});


const SmallPackageServiceMapSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    SmallPackageEngine: {
        type: Number
    },
    ServiceId: {
        type: mongoose.Types.ObjectId
    },
    SmallPackEngineService: {
        type: String
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "SmallPackageServiceMaps"
});

const db = mongoose.connection.useDb(RATING_DB);
const SmallPackageServiceMapCollection = db.model("SmallPackageServiceMaps", SmallPackageServiceMapSchema);

export { SmallPackageServiceMap, SmallPackageServiceMapCollection, SmallPackageServiceMapSchema };
