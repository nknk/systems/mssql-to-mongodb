import { STRING, DATE, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { BaseModel } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class Project44TrackingData extends BaseModel {
}

Project44TrackingData.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    DateCreated: {
        type: DATE,
    },
    Project44Id: {
        type: STRING,
    },
    ResponseData: {
        type: STRING,
    },
}, {
    sequelize,
    modelName: "Project44TrackingData",
    freezeTableName: true,
    tableName: "Project44TrackingData",
    timestamps: false
});


const Project44TrackingDataSchema = new mongoose.Schema({
    OldId: {
        type: Number
    },
    DateCreated: {
        type: Date
    },
    Project44Id: {
        type: String
    },
    ResponseData: {
        type: String
    },
}, {
    versionKey: false,
    collection: "Project44TrackingDatas"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const Project44TrackingDataCollection = db.model("Project44TrackingDatas", Project44TrackingDataSchema);

export { Project44TrackingData, Project44TrackingDataSchema, Project44TrackingDataCollection };
