import { STRING, BOOLEAN, DATE, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";

class VendorDispute extends ModelWithTenantId {
    VendorId: number;
    CreatedByUserId: number;
    ResolvedByUserId: number;

}

VendorDispute.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    VendorId: {
        type: BIGINT
    },
    CreatedByUserId: {
        type: BIGINT
    },
    ResolvedByUserId: {
        type: BIGINT
    },
    DateCreated: {
        type: DATE
    },
    IsResolved: {
        type: BOOLEAN
    },
    ResolutionDate: {
        type: DATE
    },
    ResolutionComments: {
        type: STRING
    },
    DisputeComments: {
        type: STRING
    },
    DisputerReferenceNumber: {
        type: STRING
    }
}, {
    sequelize,
    modelName: "VendorDispute",
    freezeTableName: true,
    tableName: "VendorDispute",
    timestamps: false
});


const VendorDisputeSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    VendorId: {
        type: mongoose.Types.ObjectId
    },
    CreatedByUserId: {
        type: mongoose.Types.ObjectId
    },
    ResolvedByUserId: {
        type: mongoose.Types.ObjectId
    },
    DateCreated: {
        type: Date
    },
    IsResolved: {
        type: Boolean
    },
    ResolutionDate: {
        type: Date
    },
    ResolutionComments: {
        type: String
    },
    DisputeComments: {
        type: String
    },
    DisputerReferenceNumber: {
        type: String
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "VendorDisputes"
});


const db = mongoose.connection.useDb(FINANCE_DB);
const VendorDisputeCollection = db.model(
    "VendorDisputes",
    VendorDisputeSchema
);

export { VendorDispute, VendorDisputeCollection, VendorDisputeSchema };
