import { STRING, BOOLEAN, INTEGER, DATE, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class ServiceTicket extends ModelWithTenantId {
    PrefixId: number;
    AccountBucketId: number;
    UserId: number;
    CustomerId: number;
    SalesRepresentativeId: number;
    ResellerAdditionId: number;
    AccountBucketUnitId: number;
    OverrideCustomerLocationId: number;
    JobId: number;
}

ServiceTicket.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    ServiceTicketNumber: {
        type: STRING
    },
    PrefixId: {
        type: BIGINT
    },
    HidePrefix: {
        type: BOOLEAN
    },
    Status: {
        type: INTEGER
    },
    AccountBucketId: {
        type: BIGINT
    },
    UserId: {
        type: BIGINT
    },
    CustomerId: {
        type: BIGINT
    },
    DateCreated: {
        type: DATE
    },
    TicketDate: {
        type: DATE
    },
    SalesRepresentativeId: {
        type: BIGINT
    },
    SalesRepresentativeCommissionPercent: {
        type: DECIMAL
    },
    ResellerAdditionId: {
        type: BIGINT
    },
    BillReseller: {
        type: BOOLEAN
    },
    AccountBucketUnitId: {
        type: BIGINT
    },
    AuditedForInvoicing: {
        type: BOOLEAN
    },
    SalesRepAddlEntityName: {
        type: STRING
    },
    SalesRepAddlEntityCommPercent: {
        type: DECIMAL
    },
    SalesRepMinCommValue: {
        type: DECIMAL
    },
    SalesRepMaxCommValue: {
        type: DECIMAL
    },
    OverrideCustomerLocationId: {
        type: BIGINT
    },
    ExternalReference1: {
        type: STRING
    },
    ExternalReference2: {
        type: STRING
    },
    JobId: {
        type: BIGINT
    },
    JobStep: {
        type: INTEGER
    }    
}, {
    sequelize,
    modelName: "ServiceTicket",
    freezeTableName: true,
    tableName: "ServiceTicket",
    timestamps: false
});


const ServiceTicketSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    ServiceTicketNumber: {
        type: String
    },
    PrefixId: {
        type: mongoose.Types.ObjectId
    },
    HidePrefix: {
        type: Boolean
    },
    Status: {
        type: Number
    },
    AccountBucketId: {
        type: mongoose.Types.ObjectId
    },
    UserId: {
        type: mongoose.Types.ObjectId
    },
    CustomerId: {
        type: mongoose.Types.ObjectId
    },
    DateCreated: {
        type: Date
    },
    TicketDate: {
        type: Date
    },
    SalesRepresentativeId: {
        type: mongoose.Types.ObjectId
    },
    SalesRepresentativeCommissionPercent: {
        type: Number
    },
    ResellerAdditionId: {
        type: mongoose.Types.ObjectId
    },
    BillReseller: {
        type: Boolean
    },
    AccountBucketUnitId: {
        type: mongoose.Types.ObjectId
    },
    AuditedForInvoicing: {
        type: Boolean
    },
    SalesRepAddlEntityName: {
        type: String
    },
    SalesRepAddlEntityCommPercent: {
        type: Number
    },
    SalesRepMinCommValue: {
        type: Number
    },
    SalesRepMaxCommValue: {
        type: Number
    },
    OverrideCustomerLocationId: {
        type: mongoose.Types.ObjectId
    },
    ExternalReference1: {
        type: String
    },
    ExternalReference2: {
        type: String
    },
    JobId: {
        type: mongoose.Types.ObjectId
    },
    JobStep: {
        type: Number
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "ServiceTickets"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const ServiceTicketCollection = db.model("ServiceTickets", ServiceTicketSchema);

export { ServiceTicket, ServiceTicketCollection, ServiceTicketSchema };
