import { STRING, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { BaseModel } from "./contracts/base_models";
import { REGISTRY_DB } from "../config";

class ChangeLogImage extends BaseModel {
    ChangelogId: number;
    ImagePath: string;
}

ChangeLogImage.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    ChangelogId: {
        type: BIGINT,
    },
    ImagePath: {
        type: STRING,
        allowNull: false,
    }
}, {
    sequelize,
    modelName: "ChangeLogImage",
    freezeTableName: true,
    tableName: "ChangeLogImage",
    timestamps: false
});

const ChangeLogImageSchema = new mongoose.Schema({
    ChangelogId: {
        type: mongoose.SchemaTypes.ObjectId,
    },
    ImagePath: {
        type: String,
    },
    OldId: {
        type: Number
    },
}, {
    versionKey: false,
    collection: "ChangeLogImages"
});

const db = mongoose.connection.useDb(REGISTRY_DB);
const ChangeLogImageCollection = db.model("ChangeLogImages", ChangeLogImageSchema);

export { ChangeLogImage, ChangeLogImageCollection, ChangeLogImageSchema };
