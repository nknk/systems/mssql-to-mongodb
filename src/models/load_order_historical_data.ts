import { BOOLEAN, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { BaseModel } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";
class LoadOrderHistoricalData extends BaseModel {
        LoadOrderId: number;
}

LoadOrderHistoricalData.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    LoadOrderId:{
        type: BIGINT
    },
    WasOffered:{
        type: BOOLEAN
    },
    WasQuoted:{
        type: BOOLEAN
    },
    WasCancelled:{
        type: BOOLEAN
    },
    WasCovered:{
        type: BOOLEAN
    },
    WasLost:{
        type: BOOLEAN
    },
    WasAccepted:{
        type: BOOLEAN
    },
    WasShipment:{
        type: BOOLEAN
    }
}, {
    sequelize,
    modelName: "LoadOrderHistoricalData",
    freezeTableName: true,
    tableName: "LoadOrderHistoricalData",
    timestamps: false
});

const LoadOrderHistoricalDataSchema = new mongoose.Schema({
    LoadOrderId:{
        type: mongoose.Types.ObjectId
    },
    WasOffered:{
        type: Boolean
    },
    WasQuoted:{
        type: Boolean
    },
    WasCancelled:{
        type: Boolean
    },
    WasCovered:{
        type: Boolean
    },
    WasLost:{
        type: Boolean
    },
    WasAccepted:{
        type: Boolean
    },
    WasShipment:{
        type: Boolean
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "LoadOrderHistoricalDatas"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const LoadOrderHistoricalDataCollection = db.model("LoadOrderHistoricalDatas", LoadOrderHistoricalDataSchema);

export { LoadOrderHistoricalData, LoadOrderHistoricalDataCollection, LoadOrderHistoricalDataSchema };
