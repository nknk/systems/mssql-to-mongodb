import { STRING, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";
class AddressBook extends ModelWithTenantId {
    CustomerId: number;
    CountryId: number;
}

AddressBook.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    OriginSpecialInstruction: {
        type: STRING,
        allowNull: false
    },
    DestinationSpecialInstruction: {
        type: STRING,
        allowNull: false
    },
    CustomerId: {
        type: BIGINT,
        allowNull: false
    },
    Street1: {
        type: STRING,
        allowNull: false
    },
    Street2: {
        type: STRING,
        allowNull: false
    },
    City: {
        type: STRING,
        allowNull: false
    },
    State: {
        type: STRING,
        allowNull: false
    },
    CountryId: {
        type: BIGINT,
        allowNull: false
    },
    PostalCode: {
        type: STRING,
        allowNull: false
    },
    Description: {
        type: STRING,
        allowNull: false
    },
    GeneralInfo: {
        type: STRING,
        allowNull: false
    },
    Direction: {
        type: STRING,
        allowNull: false
    },
}, {
    sequelize,
    modelName: "AddressBook",
    freezeTableName: true,
    tableName: "AddressBook",
    timestamps: false
});

const AddressBookSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    },
    OriginSpecialInstruction: {
        type: String

    },
    DestinationSpecialInstruction: {
        type: String

    },
    CustomerId: {
        type: mongoose.Types.ObjectId

    },
    Street1: {
        type: String

    },
    Street2: {
        type: String

    },
    City: {
        type: String

    },
    State: {
        type: String

    },
    CountryId: {
        type: mongoose.Types.ObjectId

    },
    PostalCode: {
        type: String

    },
    Description: {
        type: String

    },
    GeneralInfo: {
        type: String

    },
    Direction: {
        type: String

    },
}, {
    versionKey: false,
    collection: "AddressBooks"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const AddressBookCollection = db.model("AddressBooks", AddressBookSchema);

export { AddressBook, AddressBookCollection, AddressBookSchema };
