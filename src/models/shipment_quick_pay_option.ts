import { STRING, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class ShipmentQuickPayOption extends ModelWithTenantId {
    ShipmentId: number;
}

ShipmentQuickPayOption.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    Description: {
        type: STRING
    },
    DiscountPercent: {
        type: DECIMAL
    },
    ShipmentId: {
        type: BIGINT
    }     
}, {
    sequelize,
    modelName: "ShipmentQuickPayOption",
    freezeTableName: true,
    tableName: "ShipmentQuickPayOption",
    timestamps: false
});


const ShipmentQuickPayOptionSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    Description: {
        type: String
    },
    DiscountPercent: {
        type: Number
    },
    ShipmentId: {
        type: mongoose.Types.ObjectId
    } ,
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "ShipmentQuickPayOptions"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const ShipmentQuickPayOptionCollection = db.model("ShipmentQuickPayOptions", ShipmentQuickPayOptionSchema);

export { ShipmentQuickPayOption, ShipmentQuickPayOptionCollection, ShipmentQuickPayOptionSchema };
