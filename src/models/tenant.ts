import { STRING, BOOLEAN, DOUBLE, DECIMAL, INTEGER, BIGINT } from "sequelize";
import mongoose from "mongoose";

import sequelize from "../config/sequelize";
import { IDENTITY_DB } from "../config";
import { BaseModel } from "./contracts/base_models";

class Tenant extends BaseModel {
    BillingLocationId: string;
    MailingLocationId: string;
    HazardousMaterialServiceId: string;
    InsuranceChargeCodeId: string;
    InsuranceProviderVendorId: string;
    BorderCrossingServiceId: string;
    GuaranteedDeliveryServiceId: string;
    DefaultUnmappedChargeCodeId: string;
    DefaultSchedulingContactTypeId: string;
    DefaultPackagingTypeId: string;
    AvailableLoadsContactTypeId: string;
    DefaultSystemUserId: string;
    MacroPointChargeCodeId: string;
    PodDocumentTagId: string;
    BolDocumentTagId: string;
    WniDocumentTagId: string;
    VendorBillDocumentTagId: string;
    OtherDocumentTagId: string;
    DefaultCollectChargeCodeId: string;
    DefaultCountryId: string;
}

Tenant.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    Code: {
        type: STRING,
        allowNull: false
    },
    Active: {
        type: BOOLEAN
    },
    Name: {
        type: STRING
    },
    TenantScac: {
        type: STRING
    },
    LogoUrl: {
        type: STRING
    },
    BillingLocationId: {
        type: BIGINT
    },
    MailingLocationId: {
        type: BIGINT
    },
    SMCRateWareParameterFile: {
        type: STRING
    },
    SMCRateWareEnabled: {
        type: BOOLEAN
    },
    SMCCarrierConnectParameterFile: {
        type: STRING
    },
    SMCCarrierConnectEnabled: {
        type: BOOLEAN
    },
    FedExSmallPackParameterFile: {
        type: STRING
    },
    FedExSmallPackEnabled: {
        type: BOOLEAN
    },
    UpsSmallPackParameterFile: {
        type: STRING
    },
    UpsSmallPackEnabled: {
        type: BOOLEAN
    },
    MacroPointParameterFile: {
        type: STRING
    },
    MacroPointEnabled: {
        type: BOOLEAN
    },
    TermsAndConditionsFile: {
        type: STRING
    },
    AutoRatingNoticeText: {
        type: STRING
    },
    HazardousMaterialServiceId: {
        type: DOUBLE
    },
    InsuranceChargeCodeId: {
        type: DOUBLE
    },
    InsuranceProviderVendorId: {
        type: DOUBLE
    },
    BorderCrossingServiceId: {
        type: DOUBLE
    },
    GuaranteedDeliveryServiceId: {
        type: BIGINT
    },
    DefaultUnmappedChargeCodeId: {
        type: BIGINT
    },
    DefaultSchedulingContactTypeId: {
        type: BIGINT
    },
    DefaultPackagingTypeId: {
        type: DOUBLE
    },
    AvailableLoadsContactTypeId: {
        type: BIGINT
    },
    DefaultSystemUserId: {
        type: DOUBLE
    },
    MacroPointChargeCodeId: {
        type: DOUBLE
    },
    RatingNotice: {
        type: STRING
    },
    BatchRatingAnalysisStartTime: {
        type: STRING
    },
    BatchRatingAnalysisEndTime: {
        type: STRING
    },
    DefaultMileageEngine: {
        type: INTEGER
    },
    TruckloadPickupTolerance: {
        type: INTEGER
    },
    TruckloadDeliveryTolerance: {
        type: INTEGER
    },
    PodDocumentTagId: {
        type: DOUBLE
    },
    BolDocumentTagId: {
        type: DOUBLE
    },
    WniDocumentTagId: {
        type: DOUBLE
    },
    VendorBillDocumentTagId: {
        type: BIGINT
    },
    OtherDocumentTagId: {
        type: BIGINT
    },
    ShipmentDocImgRtrvAllowance: {
        type: INTEGER
    },
    PaymentGatewayType: {
        type: INTEGER
    },
    PaymentGatewayLoginId: {
        type: STRING
    },
    PaymentGatewaySecret: {
        type: STRING
    },
    PaymentGatewayTransactionId: {
        type: STRING
    },
    AutoNotificationSubjectPrefix: {
        type: STRING
    },
    PaymentGatewayInTestMode: {
        type: BOOLEAN
    },
    DefaultCollectChargeCodeId: {
        type: BIGINT
    },
    DefaultCountryId: {
        type: BIGINT
    },
    ShippersInsuranceBuyRate: {
        type: DECIMAL
    },
    ShippersInsuranceSellRate: {
        type: DECIMAL
    },
    ShippersInsuranceSellRateMinCharge: {
        type: DECIMAL,
    },
    InsuranceEnabled: {
        type: BOOLEAN,
    },
}, {
    sequelize,
    modelName: "Tenant",
    freezeTableName: true,
    tableName: "Tenant",
    timestamps: false
});

const TenantSchema = new mongoose.Schema({
    Code: {
        type: String,
    },
    Active: {
        type: Boolean,
    },
    Name: {
        type: String
    },
    TenantScac: {
        type: String
    },
    LogoUrl: {
        type: String
    },
    BillingLocationId: {
        type: mongoose.SchemaTypes.Mixed,
    },
    MailingLocationId: {
        type: mongoose.SchemaTypes.Mixed,
    },
    SMCRateWareParameterFile: {
        type: String
    },
    SMCRateWareEnabled: {
        type: Boolean,
    },
    SMCCarrierConnectParameterFile: {
        type: String
    },
    SMCCarrierConnectEnabled: {
        type: Boolean,
    },
    FedExSmallPackParameterFile: {
        type: String
    },
    FedExSmallPackEnabled: {
        type: Boolean,
    },
    UpsSmallPackParameterFile: {
        type: String
    },
    UpsSmallPackEnabled: {
        type: Boolean,
    },
    MacroPointParameterFile: {
        type: String
    },
    MacroPointEnabled: {
        type: Boolean,
    },
    TermsAndConditionsFile: {
        type: String
    },
    AutoRatingNoticeText: {
        type: String
    },
    HazardousMaterialServiceId: {
        type: mongoose.SchemaTypes.Mixed,
    },
    InsuranceChargeCodeId: {
        type: mongoose.SchemaTypes.Mixed,
    },
    InsuranceProviderVendorId: {
        type: mongoose.SchemaTypes.Mixed,
    },
    BorderCrossingServiceId: {
        type: mongoose.SchemaTypes.Mixed,
    },
    GuaranteedDeliveryServiceId: {
        type: mongoose.SchemaTypes.Mixed,
    },
    DefaultUnmappedChargeCodeId: {
        type: mongoose.SchemaTypes.Mixed,
    },
    DefaultSchedulingContactTypeId: {
        type: mongoose.SchemaTypes.Mixed,
    },
    DefaultPackagingTypeId: {
        type: mongoose.SchemaTypes.Mixed,
    },
    AvailableLoadsContactTypeId: {
        type: mongoose.SchemaTypes.Mixed,
    },
    DefaultSystemUserId: {
        type: mongoose.Schema.Types.Mixed,
    },
    MacroPointChargeCodeId: {
        type: mongoose.SchemaTypes.Mixed,
    },
    RatingNotice: {
        type: String
    },
    BatchRatingAnalysisStartTime: {
        type: String
    },
    BatchRatingAnalysisEndTime: {
        type: String
    },
    DefaultMileageEngine: {
        type: Number,
    },
    TruckloadPickupTolerance: {
        type: Number,
    },
    TruckloadDeliveryTolerance: {
        type: Number,
    },
    PodDocumentTagId: {
        type: mongoose.SchemaTypes.Mixed,
    },
    BolDocumentTagId: {
        type: mongoose.SchemaTypes.Mixed,
    },
    WniDocumentTagId: {
        type: mongoose.SchemaTypes.Mixed,
    },
    VendorBillDocumentTagId: {
        type: mongoose.SchemaTypes.Mixed,
    },
    OtherDocumentTagId: {
        type: mongoose.SchemaTypes.Mixed,
    },
    ShipmentDocImgRtrvAllowance: {
        type: Number,
    },
    PaymentGatewayType: {
        type: Number,
    },
    PaymentGatewayLoginId: {
        type: String
    },
    PaymentGatewaySecret: {
        type: String
    },
    PaymentGatewayTransactionId: {
        type: String
    },
    AutoNotificationSubjectPrefix: {
        type: String
    },
    PaymentGatewayInTestMode: {
        type: Boolean,
    },
    DefaultCollectChargeCodeId: {
        type: mongoose.SchemaTypes.Mixed,
    },
    DefaultCountryId: {
        type: mongoose.SchemaTypes.Mixed,
    },
    ShippersInsuranceBuyRate: {
        type: Number,
    },
    ShippersInsuranceSellRate: {
        type: Number,
    },
    ShippersInsuranceSellRateMinCharge: {
        type: Number,
    },
    InsuranceEnabled: {
        type: Boolean,
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "Tenants"
});

declare interface ITenantDoc extends mongoose.Document {

    BillingLocationId: string;
    MailingLocationId: string;
    HazardousMaterialServiceId: string;
    InsuranceChargeCodeId: string;
    InsuranceProviderVendorId: string;
    BorderCrossingServiceId: string;
    GuaranteedDeliveryServiceId: string;
    DefaultUnmappedChargeCodeId: string;
    DefaultSchedulingContactTypeId: string;
    DefaultPackagingTypeId: string;
    AvailableLoadsContactTypeId: string;
    DefaultSystemUserId: string;
    MacroPointChargeCodeId: string;
    PodDocumentTagId: string;
    BolDocumentTagId: string;
    WniDocumentTagId: string;
    VendorBillDocumentTagId: string;
    OtherDocumentTagId: string;
    DefaultCollectChargeCodeId: string;
    DefaultCountryId: string;
}

const db = mongoose.connection.useDb(IDENTITY_DB);
const TenantCollection = db.model<ITenantDoc>("Tenants", TenantSchema);

export { Tenant, TenantCollection, TenantSchema };
