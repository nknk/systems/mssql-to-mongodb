import { BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { OPERATIONS_DB } from "../config";
import { ModelWithTenantId } from "./contracts/base_models";

class VendorEquipment extends ModelWithTenantId {
    EquipmentTypeId: number;
    VendorId: number;
}

VendorEquipment.init({    
    TenantId: {
        type: BIGINT
    },
    EquipmentTypeId: {
        type: BIGINT
    },
    VendorId: {
        type: BIGINT
    }
}, {
    sequelize,
    modelName: "VendorEquipment",
    freezeTableName: true,
    tableName: "VendorEquipment",
    timestamps: false
});


const VendorEquipmentSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    EquipmentTypeId: {
        type: mongoose.Types.ObjectId
    },
    VendorId: {
        type: mongoose.Types.ObjectId
    }
}, {
    versionKey: false,
    collection: "VendorEquipments"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const VendorEquipmentCollection = db.model("VendorEquipments", VendorEquipmentSchema);

export { VendorEquipment, VendorEquipmentCollection, VendorEquipmentSchema };
