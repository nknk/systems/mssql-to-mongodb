import { STRING, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { RATING_DB } from "../config";

class FuelTable extends ModelWithTenantId {
}

FuelTable.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    Name: {
        type: STRING
    }
}, {
    sequelize,
    modelName: "FuelTable",
    freezeTableName: true,
    tableName: "FuelTable",
    timestamps: false
});


const FuelTableSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    Name: {
        type: String
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "FuelTables"
});

const db = mongoose.connection.useDb(RATING_DB);
const FuelTableCollection = db.model(
    "FuelTables",
    FuelTableSchema
);

export { FuelTable, FuelTableCollection, FuelTableSchema };
