import { STRING, INTEGER } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { BaseModel } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class RefChargeCodeCategory extends BaseModel {
    ChargeCodeCategoryIndex: number;
}

RefChargeCodeCategory.init({

    ChargeCodeCategoryIndex: {
        type: INTEGER,
        primaryKey: true
    },
    ChargeCodeCategoryText: {
        type: STRING
    },

}, {
    sequelize,
    modelName: "RefChargeCodeCategory",
    freezeTableName: true,
    tableName: "RefChargeCodeCategory",
    timestamps: false
});


const RefChargeCodeCategorySchema = new mongoose.Schema({
    ChargeCodeCategoryIndex: {
        type: Number,
    },
    ChargeCodeCategoryText: {
        type: String
    },
}, {
    versionKey: false,
    collection: "RefChargeCodeCategories"
});


const db = mongoose.connection.useDb(OPERATIONS_DB);
const RefChargeCodeCategoryCollection = db.model(
    "RefChargeCodeCategories",
    RefChargeCodeCategorySchema
);

export {
    RefChargeCodeCategory,
    RefChargeCodeCategoryCollection,
    RefChargeCodeCategorySchema,
};
