import { INTEGER, STRING, BOOLEAN, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class ServiceTicketNote extends ModelWithTenantId {
    ServiceTicketId: number;
    UserId: number;
}

ServiceTicketNote.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    ServiceTicketId: {
        type: BIGINT
    },
    Message: {
        type: STRING
    },
    Type: {
        type: INTEGER
    },
    Archived: {
        type: BOOLEAN
    },
    Classified: {
        type: BOOLEAN
    },
    UserId: {
        type: BIGINT
    }
}, {
    sequelize,
    modelName: "ServiceTicketNote",
    freezeTableName: true,
    tableName: "ServiceTicketNote",
    timestamps: false
});


const ServiceTicketNoteSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    ServiceTicketId: {
        type: mongoose.Types.ObjectId
    },
    Message: {
        type: String
    },
    Type: {
        type: Number
    },
    Archived: {
        type: Boolean
    },
    Classified: {
        type: Boolean
    },
    UserId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "ServiceTicketNotes"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const ServiceTicketNoteCollection = db.model("ServiceTicketNotes", ServiceTicketNoteSchema);

export { ServiceTicketNote, ServiceTicketNoteCollection, ServiceTicketNoteSchema };
