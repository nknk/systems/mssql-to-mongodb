import { BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";

class CollectApprovedVendor extends ModelWithTenantId {
    CustomerRatingId: number;
    VendorId: number;
}

CollectApprovedVendor.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    CustomerRatingId: {
        type: BIGINT,
    },
    VendorId: {
        type: BIGINT,
    },
}, {
    sequelize,
    modelName: "CollectApprovedVendor",
    freezeTableName: true,
    tableName: "CollectApprovedVendor",
    timestamps: false
});


const CollectApprovedVendorSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    CustomerRatingId: {
        type: mongoose.SchemaTypes.ObjectId,
    },
    VendorId: {
        type: mongoose.SchemaTypes.ObjectId,
    },
    OldId: {
        type: Number,
    }
}, {
    versionKey: false,
    collection: "CollectApprovedVendors"
});

const db = mongoose.connection.useDb(FINANCE_DB);
const CollectApprovedVendorCollection = db.model("CollectApprovedVendors", CollectApprovedVendorSchema);

export { CollectApprovedVendor, CollectApprovedVendorSchema, CollectApprovedVendorCollection };
