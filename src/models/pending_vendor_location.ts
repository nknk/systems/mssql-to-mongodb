import { STRING, BOOLEAN, DATE, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";

class PendingVendorLocation extends ModelWithTenantId {
    PendingVendorId: number;
    CountryId: number;
}

PendingVendorLocation.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    LocationNumber: {
        type: STRING,
        allowNull: false
    },
    RemitToLocation: {
        type: BOOLEAN,
        allowNull: false
    },
    MainRemitToLocation: {
        type: BOOLEAN,
        allowNull: false
    },
    Primary: {
        type: BOOLEAN,
        allowNull: false
    },
    DateCreated: {
        type: DATE,
        allowNull: false
    },
    PendingVendorId: {
        type: BIGINT,
        allowNull: false
    },
    Street1: {
        type: STRING,
        allowNull: false
    },
    Street2: {
        type: STRING,
        allowNull: false
    },
    City: {
        type: STRING,
        allowNull: false
    },
    State: {
        type: STRING,
        allowNull: false
    },
    CountryId: {
        type: BIGINT,
        allowNull: false
    },
    PostalCode: {
        type: STRING,
        allowNull: false
    },
}, {
    sequelize,
    modelName: "PendingVendorLocation",
    freezeTableName: true,
    tableName: "PendingVendorLocation",
    timestamps: false
});


const PendingVendorLocationSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    },
    LocationNumber: {
        type: String
    },
    RemitToLocation: {
        type: Boolean
    },
    MainRemitToLocation: {
        type: Boolean
    },
    Primary: {
        type: Boolean
    },
    DateCreated: {
        type: Date
    },
    PendingVendorId: {
        type: mongoose.Types.ObjectId
    },
    Street1: {
        type: String
    },
    Street2: {
        type: String
    },
    City: {
        type: String

    },
    State: {
        type: String
    },
    CountryId: {
        type: mongoose.Types.ObjectId
    },
    PostalCode: {
        type: String

    },
}, {
    versionKey: false,
    collection: "PendingVendorLocations"
});

const db = mongoose.connection.useDb(FINANCE_DB);
const PendingVendorLocationCollection = db
    .model(
        "PendingVendorLocations",
        PendingVendorLocationSchema
    );

export {
    PendingVendorLocation,
    PendingVendorLocationCollection,
    PendingVendorLocationSchema
};
