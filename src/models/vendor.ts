import { STRING, BOOLEAN, INTEGER, DATE, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";

class Vendor extends ModelWithTenantId {
    CommunicationId: number;
    VendorServiceRepUserId: number;
    VendorServiceRepId?: string;
    LastAuditedByUserId: number;
}

Vendor.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    VendorNumber: {
        type: STRING,
        allowNull: false
    },
    Name: {
        type: STRING,
        allowNull: false
    },
    Notation: {
        type: STRING,
        allowNull: false
    },
    Active: {
        type: BOOLEAN,
        allowNull: false
    },
    DateCreated: {
        type: DATE,
        allowNull: false
    },
    TSACertified: {
        type: BOOLEAN,
        allowNull: false
    },
    CTPATCertified: {
        type: BOOLEAN,
        allowNull: false
    },
    SmartWayCertified: {
        type: BOOLEAN,
        allowNull: false
    },
    PIPCertified: {
        type: BOOLEAN,
        allowNull: false
    },
    CTPATNumber: {
        type: STRING,
        allowNull: false
    },
    PIPNumber: {
        type: STRING,
        allowNull: false
    },
    Scac: {
        type: STRING,
        allowNull: false
    },
    FederalIDNumber: {
        type: STRING,
        allowNull: false
    },
    BrokerReferenceNumber: {
        type: STRING,
        allowNull: false
    },
    BrokerReferenceNumberType: {
        type: INTEGER,
        allowNull: false
    },
    MC: {
        type: STRING,
        allowNull: false
    },
    DOT: {
        type: STRING,
        allowNull: false
    },
    TrackingUrl: {
        type: STRING,
        allowNull: false
    },
    HandlesLessThanTruckload: {
        type: BOOLEAN,
        allowNull: false
    },
    HandlesTruckload: {
        type: BOOLEAN,
        allowNull: false
    },
    HandlesAir: {
        type: BOOLEAN,
        allowNull: false
    },
    HandlesPartialTruckload: {
        type: BOOLEAN,
        allowNull: false
    },
    HandlesRail: {
        type: BOOLEAN,
        allowNull: false
    },
    HandlesSmallPack: {
        type: BOOLEAN,
        allowNull: false
    },
    IsCarrier: {
        type: BOOLEAN,
        allowNull: false
    },
    IsAgent: {
        type: BOOLEAN,
        allowNull: false
    },
    IsBroker: {
        type: BOOLEAN,
        allowNull: false
    },
    CommunicationId: {
        type: BIGINT,
        allowNull: false
    },
    Notes: {
        type: STRING,
        allowNull: false
    },
    VendorServiceRepUserId: {
        type: BIGINT,
        allowNull: false
    },
    LastAuditDate: {
        type: DATE,
        allowNull: false
    },
    LastAuditedByUserId: {
        type: BIGINT,
        allowNull: false
    },
    ExcludeFromAvailableLoadsBlast: {
        type: BOOLEAN,
        allowNull: false
    },
    LogoUrl: {
        type: STRING,
        allowNull: false
    },
    CustomField1: {
        type: STRING,
        allowNull: false
    },
    CustomField2: {
        type: STRING,
        allowNull: false
    },
    CustomField3: {
        type: STRING,
        allowNull: false
    },
    CustomField4: {
        type: STRING,
        allowNull: false
    },
}, {
    sequelize,
    modelName: "Vendor",
    freezeTableName: true,
    tableName: "Vendor",
    timestamps: false
});

const VendorSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    },
    VendorNumber: {
        type: String,
    },
    Name: {
        type: String,
    },
    Notation: {
        type: String,
    },
    Active: {
        type: Boolean,
    },
    DateCreated: {
        type: Date,
    },
    TSACertified: {
        type: Boolean,
    },
    CTPATCertified: {
        type: Boolean,
    },
    SmartWayCertified: {
        type: Boolean,
    },
    PIPCertified: {
        type: Boolean,
    },
    CTPATNumber: {
        type: String,
    },
    PIPNumber: {
        type: String,
    },
    Scac: {
        type: String,
    },
    FederalIDNumber: {
        type: String,
    },
    BrokerReferenceNumber: {
        type: String,
    },
    BrokerReferenceNumberType: {
        type: Number,
    },
    MC: {
        type: String,
    },
    DOT: {
        type: String,
    },
    TrackingUrl: {
        type: String,
    },
    HandlesLessThanTruckload: {
        type: Boolean,
    },
    HandlesTruckload: {
        type: Boolean,
    },
    HandlesAir: {
        type: Boolean,
    },
    HandlesPartialTruckload: {
        type: Boolean,
    },
    HandlesRail: {
        type: Boolean,
    },
    HandlesSmallPack: {
        type: Boolean,
    },
    IsCarrier: {
        type: Boolean,
    },
    IsAgent: {
        type: Boolean,
    },
    IsBroker: {
        type: Boolean,
    },
    CommunicationId: {
        type: mongoose.Types.ObjectId,
    },
    Notes: {
        type: String,
    },
    VendorServiceRepId: {
        type: mongoose.Types.ObjectId,
    },
    LastAuditDate: {
        type: Date,
    },
    LastAuditedByUserId: {
        type: mongoose.Types.ObjectId,
    },
    ExcludeFromAvailableLoadsBlast: {
        type: Boolean,
    },
    LogoUrl: {
        type: String,
    },
    CustomField1: {
        type: String,
    },
    CustomField2: {
        type: String,
    },
    CustomField3: {
        type: String,
    },
    CustomField4: {
        type: String,
    }
}, {
    versionKey: false,
    collection: "Vendors"
});

export interface IVendorDocument extends mongoose.Document {
    OldId: number;
    CommunicationId: string;
    VendorServiceRepId: string;
}

const db = mongoose.connection.useDb(FINANCE_DB);
const VendorCollection = db.model<IVendorDocument>("Vendors", VendorSchema);

export { Vendor, VendorCollection, VendorSchema };
