import { BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { REGISTRY_DB } from "../config";
class DocumentPrefixMap extends ModelWithTenantId {
    PrefixId: string;
    DocumentTemplateId: string;
}

DocumentPrefixMap.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    PrefixId: {
        type: BIGINT,
        allowNull: false,
    },
    DocumentTemplateId: {
        type: BIGINT,
        allowNull: false,
    },
}, {
    sequelize,
    modelName: "DocumentPrefixMap",
    freezeTableName: true,
    tableName: "DocumentPrefixMap",
    timestamps: false
});


const DocumentPrefixMapSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    PrefixId: {
        type: mongoose.Types.ObjectId
    },
    DocumentTemplateId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "DocumentPrefixMaps"
});

const db = mongoose.connection.useDb(REGISTRY_DB);
const DocumentPrefixMapCollection = db.model("DocumentPrefixMaps", DocumentPrefixMapSchema);

export { DocumentPrefixMap, DocumentPrefixMapCollection, DocumentPrefixMapSchema };
