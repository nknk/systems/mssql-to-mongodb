import { STRING, BOOLEAN, INTEGER, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { BaseModel } from "./contracts/base_models";
import { IDENTITY_DB } from "../config";

class Country extends BaseModel {}

Country.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    Code: {
        type: STRING,
        allowNull: false
    },
    Name: {
        type: STRING,
        allowNull: false
    },
    EmploysPostalCodes: {
        type: BOOLEAN,
        allowNull: false
    },
    SortWeight: {
        type: INTEGER,
        allowNull: false
    },
    PostalCodeValidation: {
        type: STRING,
        allowNull: false
    },
    Project44CountryCode: {
        type: INTEGER,
        allowNull: false
    },
}, {
    sequelize,
    modelName: "Country",
    freezeTableName: true,
    tableName: "Country",
    timestamps: false
});


const CountrySchema = new mongoose.Schema({
    Code: {
        type: String,
    },
    Name: {
        type: String,
    },
    EmploysPostalCodes: {
        type: Boolean,
    },
    SortWeight: {
        type: Number,
    },
    PostalCodeValidation: {
        type: String,
    },
    Project44CountryCode: {
        type: Number,
    },
    OldId: {
        type: Number
    },
}, {
    versionKey: false,
    collection: "Countries"
});

declare interface ICountryCollection extends mongoose.Document {
    OldId: number;
}

const db = mongoose.connection.useDb(IDENTITY_DB);
const CountryCollection = db.model<ICountryCollection>("Countries", CountrySchema);

export { Country, CountryCollection, CountrySchema };
