import { BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { IDENTITY_DB } from "../config";
import { ModelWithTenantId } from "./contracts/base_models";

class UserShipAs extends ModelWithTenantId {
    CustomerId: number;
    UserId: number;
}

UserShipAs.init({    
    TenantId: {
        type: BIGINT
    },
    CustomerId: {
        type: BIGINT
    },
    UserId: {
        type: BIGINT
    }
}, {
    sequelize,
    modelName: "UserShipAs",
    freezeTableName: true,
    tableName: "UserShipAs",
    timestamps: false
});


const UserShipAsSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    CustomerId: {
        type: mongoose.Types.ObjectId
    },
    UserId: {
        type: mongoose.Types.ObjectId
    }
}, {
    versionKey: false,
    collection: "UserShipAs"
});

const db = mongoose.connection.useDb(IDENTITY_DB);
const UserShipAsCollection = db.model("UserShipAs", UserShipAsSchema);

export { UserShipAs, UserShipAsCollection, UserShipAsSchema };
