import { BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { OPERATIONS_DB } from "../config";
import { ModelWithTenantId } from "./contracts/base_models";

class ShipmentService extends ModelWithTenantId {
    ServiceId: number;
    ShipmentId: number;
}

ShipmentService.init({    
    TenantId: {
        type: BIGINT
    },
    ServiceId: {
        type: BIGINT
    },
    ShipmentId: {
        type: BIGINT
    }
}, {
    sequelize,
    modelName: "ShipmentService",
    freezeTableName: true,
    tableName: "ShipmentService",
    timestamps: false
});


const ShipmentServiceSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    ServiceId: {
        type: mongoose.Types.ObjectId
    },
    ShipmentId: {
        type: mongoose.Types.ObjectId
    }
}, {
    versionKey: false,
    collection: "ShipmentServices"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const ShipmentServiceCollection = db.model("ShipmentServices", ShipmentServiceSchema);

export { ShipmentService, ShipmentServiceCollection, ShipmentServiceSchema };
