import { BOOLEAN, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class ShipmentAccountBucket extends ModelWithTenantId {
    ShipmentId: number;
    AccountBucketId: number;
}

ShipmentAccountBucket.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    ShipmentId: {
        type: BIGINT
    },
    AccountBucketId: {
        type: BIGINT
    },
    Primary: {
        type: BOOLEAN
    }
}, {
    sequelize,
    modelName: "ShipmentAccountBucket",
    freezeTableName: true,
    tableName: "ShipmentAccountBucket",
    timestamps: false
});


const ShipmentAccountBucketSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    ShipmentId: {
        type: mongoose.Types.ObjectId
    },
    AccountBucketId: {
        type: mongoose.Types.ObjectId
    },
    Primary: {
        type: Boolean
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "ShipmentAccountBuckets"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const ShipmentAccountBucketCollection = db.model(
    "ShipmentAccountBuckets",
    ShipmentAccountBucketSchema
);

export { ShipmentAccountBucket, ShipmentAccountBucketCollection, ShipmentAccountBucketSchema };
