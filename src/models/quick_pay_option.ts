import { STRING, BOOLEAN,  BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { REGISTRY_DB } from "../config";

class QuickPayOption extends ModelWithTenantId { }

QuickPayOption.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    Code: {
        type: STRING,
        allowNull: false
    },
    Description: {
        type: STRING,
        allowNull: false
    },
    Active: {
        type: BOOLEAN,
        allowNull: false
    },
    DiscountPercent: {
        type: DECIMAL,
        allowNull: false
    }
}, {
    sequelize,
    modelName: "QuickPayOption",
    freezeTableName: true,
    tableName: "QuickPayOption",
    timestamps: false
});


const QuickPayOptionSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    Code: {
        type: String
    },
    Description: {
        type: String
    },
    Active: {
        type: Boolean
    },
    OldId: {
        type: Number
    },
    DiscountPercent: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "QuickPayOptions"
});

const db = mongoose.connection.useDb(REGISTRY_DB);
const QuickPayOptionCollection = db.model("QuickPayOptions", QuickPayOptionSchema);

export { QuickPayOption, QuickPayOptionSchema, QuickPayOptionCollection };
