import { STRING, INTEGER, FLOAT, BOOLEAN, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { RATING_DB } from "../config";
class CustomerRating extends ModelWithTenantId {
    InsuranceChargeCodeId: number;
    ResellerAdditionId: number
    CustomerId: number;
    TLFuelChargeCodeId: number;
    TLFreightChargeCodeId: number;

}

CustomerRating.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    InsuranceEnabled: {
        type: BOOLEAN,
        allowNull: false,
    },
    InsurancePurchaseFloor: {
        type: DECIMAL,
        allowNull: false,
    },
    InsuranceChargeCodeId: {
        type: BIGINT,
        allowNull: false,
    },
    NoLineHaulProfit: {
        type: BOOLEAN,
        allowNull: false,
    },
    LineHaulProfitCeilingType: {
        type: INTEGER,
        allowNull: false,
    },
    LineHaulProfitCeilingValue: {
        type: DECIMAL,
        allowNull: false,
    },
    LineHaulProfitCeilingPercentage: {
        type: DECIMAL,
        allowNull: false,
    },
    UseLineHaulMinimumCeiling: {
        type: BOOLEAN,
        allowNull: false,
    },
    LineHaulProfitFloorType: {
        type: INTEGER,
        allowNull: false,
    },
    LineHaulProfitFloorValue: {
        type: DECIMAL,
        allowNull: false,
    },
    LineHaulProfitFloorPercentage: {
        type: DECIMAL,
        allowNull: false,
    },
    UseLineHaulMinimumFloor: {
        type: BOOLEAN,
        allowNull: false,
    },
    NoFuelProfit: {
        type: BOOLEAN,
        allowNull: false,
    },
    FuelProfitCeilingType: {
        type: INTEGER,
        allowNull: false,
    },
    FuelProfitCeilingValue: {
        type: DECIMAL,
        allowNull: false,
    },
    FuelProfitCeilingPercentage: {
        type: DECIMAL,
        allowNull: false,
    },
    UseFuelMinimumCeiling: {
        type: BOOLEAN,
        allowNull: false,
    },
    FuelProfitFloorType: {
        type: INTEGER,
        allowNull: false,
    },
    FuelProfitFloorValue: {
        type: DECIMAL,
        allowNull: false,
    },
    FuelProfitFloorPercentage: {
        type: DECIMAL,
        allowNull: false,
    },
    UseFuelMinimumFloor: {
        type: BOOLEAN,
        allowNull: false,
    },
    NoAccessorialProfit: {
        type: BOOLEAN,
        allowNull: false,
    },
    AccessorialProfitCeilingType: {
        type: INTEGER,
        allowNull: false,
    },
    AccessorialProfitCeilingValue: {
        type: DECIMAL,
        allowNull: false,
    },
    AccessorialProfitCeilingPercentage: {
        type: DECIMAL,
        allowNull: false,
    },
    UseAccessorialMinimumCeiling: {
        type: BOOLEAN,
        allowNull: false,
    },
    AccessorialProfitFloorType: {
        type: INTEGER,
        allowNull: false,
    },
    AccessorialProfitFloorValue: {
        type: DECIMAL,
        allowNull: false,
    },
    AccessorialProfitFloorPercentage: {
        type: DECIMAL,
        allowNull: false,
    },
    UseAccessorialMinimumFloor: {
        type: BOOLEAN,
        allowNull: false,
    },
    NoServiceProfit: {
        type: BOOLEAN,
        allowNull: false,
    },
    ServiceProfitCeilingType: {
        type: INTEGER,
        allowNull: false,
    },
    ServiceProfitCeilingValue: {
        type: DECIMAL,
        allowNull: false,
    },
    ServiceProfitCeilingPercentage: {
        type: DECIMAL,
        allowNull: false,
    },
    UseServiceMinimumCeiling: {
        type: BOOLEAN,
        allowNull: false,
    },
    ServiceProfitFloorType: {
        type: INTEGER,
        allowNull: false,
    },
    ServiceProfitFloorValue: {
        type: DECIMAL,
        allowNull: false,
    },
    ServiceProfitFloorPercentage: {
        type: DECIMAL,
        allowNull: false,
    },
    UseServiceMinimumFloor: {
        type: BOOLEAN,
        allowNull: false,
    },
    BillReseller: {
        type: BOOLEAN,
        allowNull: false,
    },
    ResellerAdditionId: {
        type: BIGINT,
        allowNull: false,
    },
    CustomerId: {
        type: BIGINT,
        allowNull: false,
    },
    TLFuelRateType: {
        type: INTEGER,
        allowNull: false,
    },
    TLFuelRate: {
        type: DECIMAL,
        allowNull: false,
    },
    TLFuelChargeCodeId: {
        type: BIGINT,
        allowNull: false,
    },
    TLFreightChargeCodeId: {
        type: BIGINT,
        allowNull: false,
    },
    Project44AccountGroup: {
        type: STRING,
        allowNull: false,
    },
    InsuranceSellRateOverride: {
        type: DECIMAL,
        allowNull: false,
    },
    InsuranceOverrideEnabled: {
        type: BOOLEAN,
        allowNull: false,
    },
    Project44Timeout: {
        type: INTEGER,
        allowNull: false,
    },
}, {
    sequelize,
    modelName: "CustomerRating",
    freezeTableName: true,
    tableName: "CustomerRating",
    timestamps: false
});


const CustomerRatingSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    InsuranceEnabled: {
        type: Boolean,
    },
    InsurancePurchaseFloor: {
        type: Number,
    },
    InsuranceChargeCodeId: {
        type: mongoose.Types.ObjectId,
    },
    NoLineHaulProfit: {
        type: Boolean,
    },
    LineHaulProfitCeilingType: {
        type: Number,
    },
    LineHaulProfitCeilingValue: {
        type: Number,
    },
    LineHaulProfitCeilingPercentage: {
        type: Number,
    },
    UseLineHaulMinimumCeiling: {
        type: Boolean,
    },
    LineHaulProfitFloorType: {
        type: Number,
    },
    LineHaulProfitFloorValue: {
        type: Number,
    },
    LineHaulProfitFloorPercentage: {
        type: Number,
    },
    UseLineHaulMinimumFloor: {
        type: Boolean,
    },
    NoFuelProfit: {
        type: Boolean,
    },
    FuelProfitCeilingType: {
        type: Number,
    },
    FuelProfitCeilingValue: {
        type: Number,
    },
    FuelProfitCeilingPercentage: {
        type: Number,
    },
    UseFuelMinimumCeiling: {
        type: Boolean,
    },
    FuelProfitFloorType: {
        type: Number,
    },
    FuelProfitFloorValue: {
        type: Number,
    },
    FuelProfitFloorPercentage: {
        type: Number,
    },
    UseFuelMinimumFloor: {
        type: Boolean,
    },
    NoAccessorialProfit: {
        type: Boolean,
    },
    AccessorialProfitCeilingType: {
        type: Number,
    },
    AccessorialProfitCeilingValue: {
        type: Number,
    },
    AccessorialProfitCeilingPercentage: {
        type: Number,
    },
    UseAccessorialMinimumCeiling: {
        type: Boolean,
    },
    AccessorialProfitFloorType: {
        type: Number,
    },
    AccessorialProfitFloorValue: {
        type: Number,
    },
    AccessorialProfitFloorPercentage: {
        type: Number,
    },
    UseAccessorialMinimumFloor: {
        type: Boolean,
    },
    NoServiceProfit: {
        type: Boolean,
    },
    ServiceProfitCeilingType: {
        type: Number,
    },
    ServiceProfitCeilingValue: {
        type: Number,
    },
    ServiceProfitCeilingPercentage: {
        type: Number,
    },
    UseServiceMinimumCeiling: {
        type: Boolean,
    },
    ServiceProfitFloorType: {
        type: Number,
    },
    ServiceProfitFloorValue: {
        type: Number,
    },
    ServiceProfitFloorPercentage: {
        type: Number,
    },
    UseServiceMinimumFloor: {
        type: Boolean,
    },
    BillReseller: {
        type: Boolean,
    },
    ResellerAdditionId: {
        type: mongoose.Types.ObjectId,
    },
    CustomerId: {
        type: mongoose.Types.ObjectId,
    },
    TLFuelRateType: {
        type: Number,
    },
    TLFuelRate: {
        type: Number,
    },
    TLFuelChargeCodeId: {
        type: mongoose.Types.ObjectId,
    },
    TLFreightChargeCodeId: {
        type: mongoose.Types.ObjectId,
    },
    Project44AccountGroup: {
        type: String,
    },
    InsuranceSellRateOverride: {
        type: Number,
    },
    InsuranceOverrideEnabled: {
        type: Boolean,
    },
    Project44Timeout: {
        type: Number,
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "CustomerRatings"
});

declare interface ICustomerRatingCollection extends mongoose.Document {
    CustomerId: string;
}

const db = mongoose.connection.useDb(RATING_DB);
const CustomerRatingCollection = db.model<ICustomerRatingCollection>(
    "CustomerRatings",
    CustomerRatingSchema,
);

export {
    CustomerRating,
    CustomerRatingCollection,
    CustomerRatingSchema
};