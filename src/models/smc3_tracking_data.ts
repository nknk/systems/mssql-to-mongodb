import { INTEGER, DATE, STRING, BOOLEAN, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { BaseModel } from "./contracts/base_models";
import { RATING_DB } from "../config";

class SMC3TrackingData extends BaseModel {
}

SMC3TrackingData.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TransactionId: {
        type: STRING
    },
    AccountToken: {
        type: STRING
    },
    ReferenceNumber: {
        type: STRING
    },
    ReferenceType: {
        type: STRING
    },
    SCAC: {
        type: STRING
    },
    Pro: {
        type: STRING
    },
    Bol: {
        type: STRING
    },
    PO: {
        type: STRING,
    },
    StatusCode: {
        type: STRING
    },
    StatusEventType: {
        type: STRING
    },
    StatusDate: {
        type: STRING
    },
    StatusDescription: {
        type: STRING
    },
    StatusTime: {
        type: STRING
    },
    StatusCity: {
        type: STRING
    },
    StatusCountry: {
        type: STRING
    },
    StatusStateProvince: {
        type: STRING,
    },
    StatusPostalCode: {
        type: STRING
    },
    AppointmentScheduled: {
        type: STRING
    },
    AppointmentDate: {
        type: STRING
    },
    AppointmentTime: {
        type: STRING
    },
    AppointmentType: {
        type: STRING
    },
    PickupDate: {
        type: STRING,
    },
    PickupTime: {
        type: STRING
    },
    DeliveryDate: {
        type: STRING
    },
    DeliveryTime: {
        type: STRING
    },
    ETADestinationDate: {
        type: STRING
    },
    ETADestinationTime: {
        type: STRING
    },
    CCXLDestinationDate: {
        type: STRING
    },
    CCXLDestinationTime: {
        type: STRING,
    },
    CCXLCalendarDays: {
        type: STRING
    },
    InterlineContactPhone: {
        type: STRING
    },
    InterlinePartnerName: {
        type: STRING
    },
    InterlineNotes: {
        type: STRING
    },
    InterlinePro: {
        type: STRING
    },
    InterlineSCAC: {
        type: STRING,
    },
    OriginTerminalCode: {
        type: STRING
    },
    OriginTerminalPhone: {
        type: STRING
    },
    OriginTerminalFax: {
        type: STRING
    },
    OriginTerminalTollFree: {
        type: STRING
    },
    OriginTerminalContactName: {
        type: STRING
    },
    OriginTerminalCity: {
        type: STRING
    },
    OriginTerminalCountry: {
        type: STRING,
    },
    OriginTerminalStateProvince: {
        type: STRING
    },
    OriginTerminalPostalCode: {
        type: STRING
    },
    DestinationTerminalCode: {
        type: STRING
    },
    DestinationTerminalPhone: {
        type: STRING
    },
    DestinationTerminalFax: {
        type: STRING
    },
    DestinationTerminalTollFree: {
        type: STRING,
    },
    DestinationTerminalContactName: {
        type: STRING
    },
    DestinationTerminalCity: {
        type: STRING
    },
    DestinationTerminalCountry: {
        type: STRING
    },
    DestinationTerminalStateProvince: {
        type: STRING
    },
    DestinationTerminalPostalCode: {
        type: STRING
    },
    EquipmentId: {
        type: STRING,
    },
    EquipmentType: {
        type: STRING
    },
    Weight: {
        type: DECIMAL
    },
    WeightType: {
        type: STRING
    },
    WeightUnit: {
        type: STRING
    },
    Pieces: {
        type: INTEGER
    },
    PackagingType: {
        type: STRING
    },
    HazardousMaterial: {
        type: BOOLEAN,
    },
    BillToAccount: {
        type: STRING
    },
    BillToAddress1: {
        type: STRING
    },
    BillToAddress2: {
        type: STRING
    },
    BillToCity: {
        type: STRING
    },
    BillToCountry: {
        type: STRING
    },
    BillToName: {
        type: STRING,
    },
    BillToPhone: {
        type: STRING
    },
    BillToStateProvince: {
        type: STRING
    },
    BillToPostalCode: {
        type: STRING
    },
    PaymentTerms: {
        type: STRING,
    },
    DestinationAccount: {
        type: STRING
    },
    DestinationAddress1: {
        type: STRING
    },
    DestinationAddress2: {
        type: STRING
    },
    DestinationCity: {
        type: STRING
    },
    DestinationCountry: {
        type: STRING
    },
    DestinationName: {
        type: STRING,
    },
    DestinationPhone: {
        type: STRING
    },
    DestinationStateProvince: {
        type: STRING
    },
    DestinationPostalCode: {
        type: STRING
    },
    OriginAccount: {
        type: STRING
    },
    OriginAddress1: {
        type: STRING
    },
    OriginAddress2: {
        type: STRING
    },
    OriginCity: {
        type: STRING,
    },
    OriginCountry: {
        type: STRING
    },
    OriginName: {
        type: STRING
    },
    OriginPhone: {
        type: STRING
    },
    OriginStateProvince: {
        type: STRING
    },
    OriginPostalCode: {
        type: STRING
    },
    ResponseStatus: {
        type: STRING,
    },
    ResponseCode: {
        type: STRING
    },
    ResponseMessage: {
        type: STRING
    },
    DateCreated: {
        type: DATE
    },
    OriginTerminalEmail: {
        type: STRING
    },
    DestinationTerminalEmail: {
        type: STRING
    }
}, {
    sequelize,
    modelName: "SMC3TrackingData",
    freezeTableName: true,
    tableName: "SMC3TrackingData",
    timestamps: false
});


const SMC3TrackingDataSchema = new mongoose.Schema({
    TransactionId: {
        type: String
    },
    AccountToken: {
        type: String
    },
    ReferenceNumber: {
        type: String
    },
    ReferenceType: {
        type: String
    },
    SCAC: {
        type: String
    },
    Pro: {
        type: String
    },
    Bol: {
        type: String
    },
    PO: {
        type: String,
    },
    StatusCode: {
        type: String
    },
    StatusEventType: {
        type: String
    },
    StatusDate: {
        type: String
    },
    StatusDescription: {
        type: String
    },
    StatusTime: {
        type: String
    },
    StatusCity: {
        type: String
    },
    StatusCountry: {
        type: String
    },
    StatusStateProvince: {
        type: String,
    },
    StatusPostalCode: {
        type: String
    },
    AppointmentScheduled: {
        type: String
    },
    AppointmentDate: {
        type: String
    },
    AppointmentTime: {
        type: String
    },
    AppointmentType: {
        type: String
    },
    PickupDate: {
        type: String,
    },
    PickupTime: {
        type: String
    },
    DeliveryDate: {
        type: String
    },
    DeliveryTime: {
        type: String
    },
    ETADestinationDate: {
        type: String
    },
    ETADestinationTime: {
        type: String
    },
    CCXLDestinationDate: {
        type: String
    },
    CCXLDestinationTime: {
        type: String,
    },
    CCXLCalendarDays: {
        type: String
    },
    InterlineContactPhone: {
        type: String
    },
    InterlinePartnerName: {
        type: String
    },
    InterlineNotes: {
        type: String
    },
    InterlinePro: {
        type: String
    },
    InterlineSCAC: {
        type: String,
    },
    OriginTerminalCode: {
        type: String
    },
    OriginTerminalPhone: {
        type: String
    },
    OriginTerminalFax: {
        type: String
    },
    OriginTerminalTollFree: {
        type: String
    },
    OriginTerminalContactName: {
        type: String
    },
    OriginTerminalCity: {
        type: String
    },
    OriginTerminalCountry: {
        type: String,
    },
    OriginTerminalStateProvince: {
        type: String
    },
    OriginTerminalPostalCode: {
        type: String
    },
    DestinationTerminalCode: {
        type: String
    },
    DestinationTerminalPhone: {
        type: String
    },
    DestinationTerminalFax: {
        type: String
    },
    DestinationTerminalTollFree: {
        type: String,
    },
    DestinationTerminalContactName: {
        type: String
    },
    DestinationTerminalCity: {
        type: String
    },
    DestinationTerminalCountry: {
        type: String
    },
    DestinationTerminalStateProvince: {
        type: String
    },
    DestinationTerminalPostalCode: {
        type: String
    },
    EquipmentId: {
        type: String,
    },
    EquipmentType: {
        type: String
    },
    Weight: {
        type: Number
    },
    WeightType: {
        type: String
    },
    WeightUnit: {
        type: String
    },
    Pieces: {
        type: Number
    },
    PackagingType: {
        type: String
    },
    HazardousMaterial: {
        type: Boolean,
    },
    BillToAccount: {
        type: String
    },
    BillToAddress1: {
        type: String
    },
    BillToAddress2: {
        type: String
    },
    BillToCity: {
        type: String
    },
    BillToCountry: {
        type: String
    },
    BillToName: {
        type: String,
    },
    BillToPhone: {
        type: String
    },
    BillToStateProvince: {
        type: String
    },
    BillToPostalCode: {
        type: String
    },
    PaymentTerms: {
        type: String,
    },
    DestinationAccount: {
        type: String
    },
    DestinationAddress1: {
        type: String
    },
    DestinationAddress2: {
        type: String
    },
    DestinationCity: {
        type: String
    },
    DestinationCountry: {
        type: String
    },
    DestinationName: {
        type: String,
    },
    DestinationPhone: {
        type: String
    },
    DestinationStateProvince: {
        type: String
    },
    DestinationPostalCode: {
        type: String
    },
    OriginAccount: {
        type: String
    },
    OriginAddress1: {
        type: String
    },
    OriginAddress2: {
        type: String
    },
    OriginCity: {
        type: String,
    },
    OriginCountry: {
        type: String
    },
    OriginName: {
        type: String
    },
    OriginPhone: {
        type: String
    },
    OriginStateProvince: {
        type: String
    },
    OriginPostalCode: {
        type: String
    },
    ResponseStatus: {
        type: String,
    },
    ResponseCode: {
        type: String
    },
    ResponseMessage: {
        type: String
    },
    DateCreated: {
        type: Date
    },
    OriginTerminalEmail: {
        type: String
    },
    DestinationTerminalEmail: {
        type: String
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "SMC3TrackingDatas"
});

const db = mongoose.connection.useDb(RATING_DB);
const SMC3TrackingDataCollection = db
    .model("SMC3TrackingDatas", SMC3TrackingDataSchema);

export { SMC3TrackingData, SMC3TrackingDataCollection, SMC3TrackingDataSchema };
