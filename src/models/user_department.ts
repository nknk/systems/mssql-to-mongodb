import { STRING,  BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { REGISTRY_DB } from "../config";

class UserDepartment extends ModelWithTenantId { }

UserDepartment.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: STRING,
        allowNull: false
    },
    Code: {
        type: STRING,
        allowNull: false
    },
    Description: {
        type: STRING,
        allowNull: false
    },
    Phone: {
        type: STRING,
        allowNull: false
    },
    Fax: {
        type: STRING,
        allowNull: false
    },
    Email: {
        type: STRING,
        allowNull: false
    },
    Comments: {
        type: STRING,
        allowNull: false
    }
}, {
    sequelize,
    modelName: "UserDepartment",
    freezeTableName: true,
    tableName: "UserDepartment",
    timestamps: false
});


const UserDepartmentSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    Code: {
        type: String
    },
    Description: {
        type: String
    },
    OldId: {
        type: Number
    },
    Phone: {
        type: String,
    },
    Fax: {
        type: String,
    },
    Email: {
        type: String,
    },
    Comments: {
        type: String,
    }
}, {
    versionKey: false,
    collection: "UserDepartments"
});

const db = mongoose.connection.useDb(REGISTRY_DB);
const UserDepartmentCollection = db.model("UserDepartments", UserDepartmentSchema);

export { UserDepartment, UserDepartmentSchema, UserDepartmentCollection };
