import { INTEGER, STRING, FLOAT, BOOLEAN, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class ShipmentItem extends ModelWithTenantId {
    PackageTypeId: number;
    ShipmentId: number;
}

ShipmentItem.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    Description: {
        type: STRING
    },
    ActualFreightClass: {
        type: FLOAT
    },
    RatedFreightClass: {
        type: FLOAT
    },
    Comment: {
        type: STRING
    },
    ActualWeight: {
        type: DECIMAL,
    },
    ActualLength: {
        type: DECIMAL,
    },
    ActualWidth: {
        type: DECIMAL,
    },
    ActualHeight: {
        type: DECIMAL,
    },
    Quantity: {
        type: INTEGER
    },
    PieceCount: {
        type: INTEGER
    },
    PackageTypeId: {
        type: BIGINT
    },
    Pickup: {
        type: INTEGER
    },
    Delivery: {
        type: INTEGER
    },
    NMFCCode: {
        type: STRING
    },
    HTSCode: {
        type: STRING
    },
    ShipmentId: {
        type: BIGINT
    },
    Value: {
        type: DECIMAL
    },
    IsStackable: {
        type: BOOLEAN
    },
    HazardousMaterial: {
        type: BOOLEAN
    }    
}, {
    sequelize,
    modelName: "ShipmentItem",
    freezeTableName: true,
    tableName: "ShipmentItem",
    timestamps: false
});


const ShipmentItemSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    Description: {
        type: String
    },
    ActualFreightClass: {
        type: Number
    },
    RatedFreightClass: {
        type: Number
    },
    Comment: {
        type: String
    },
    ActualWeight: {
        type: Number
    },
    ActualLength: {
        type: Number
    },
    ActualWidth: {
        type: Number
    },
    ActualHeight: {
        type: Number
    },
    Quantity: {
        type: Number
    },
    PieceCount: {
        type: Number
    },
    PackageTypeId: {
        type: mongoose.Types.ObjectId
    },
    Pickup: {
        type: Number
    },
    Delivery: {
        type: Number
    },
    NMFCCode: {
        type: String
    },
    HTSCode: {
        type: String
    },
    ShipmentId: {
        type: mongoose.Types.ObjectId
    },
    Value: {
        type: Number
    },
    IsStackable: {
        type: Boolean
    },
    HazardousMaterial: {
        type: Boolean
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "ShipmentItems"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const ShipmentItemCollection = db.model("ShipmentItems", ShipmentItemSchema);

export { ShipmentItem, ShipmentItemCollection, ShipmentItemSchema };
