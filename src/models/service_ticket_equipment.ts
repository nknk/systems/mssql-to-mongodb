import { BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { OPERATIONS_DB } from "../config";
import { ModelWithTenantId } from "./contracts/base_models";

class ServiceTicketEquipment extends ModelWithTenantId {
    EquipmentTypeId: number;
    ServiceTicketId: number;
}

ServiceTicketEquipment.init({    
    TenantId: {
        type: BIGINT
    },
    EquipmentTypeId: {
        type: BIGINT
    },
    ServiceTicketId: {
        type: BIGINT
    }
}, {
    sequelize,
    modelName: "ServiceTicketEquipment",
    freezeTableName: true,
    tableName: "ServiceTicketEquipment",
    timestamps: false
});


const ServiceTicketEquipmentSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    EquipmentTypeId: {
        type: mongoose.Types.ObjectId
    },
    ServiceTicketId: {
        type: mongoose.Types.ObjectId
    }
}, {
    versionKey: false,
    collection: "ServiceTicketEquipments"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const ServiceTicketEquipmentCollection = db.model("ServiceTicketEquipments", ServiceTicketEquipmentSchema);

export { ServiceTicketEquipment, ServiceTicketEquipmentCollection, ServiceTicketEquipmentSchema };
