import { BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";

class PendingVendorEquipment extends ModelWithTenantId {
    PendingVendorId: number;
    EquipmentTypeId: number;
}

PendingVendorEquipment.init({
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    EquipmentTypeId: {
        type: BIGINT,
        allowNull: false
    },
    PendingVendorId: {
        type: BIGINT,
        allowNull: false
    }
}, {
    sequelize,
    modelName: "PendingVendorEquipment",
    freezeTableName: true,
    tableName: "PendingVendorEquipment",
    timestamps: false
});


const PendingVendorEquipmentSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    EquipmentTypeId: {
        type: mongoose.Types.ObjectId
    },
    PendingVendorId: {
        type: mongoose.Types.ObjectId
    }
}, {
    versionKey: false,
    collection: "PendingVendorEquipments"
});

const db = mongoose.connection.useDb(FINANCE_DB);
const PendingVendorEquipmentCollection = db
    .model("PendingVendorEquipments", PendingVendorEquipmentSchema);

export {
    PendingVendorEquipment,
    PendingVendorEquipmentCollection,
    PendingVendorEquipmentSchema
};
