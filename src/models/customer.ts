import { STRING, INTEGER, DATE, BOOLEAN, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";
class Customer extends ModelWithTenantId {
    DefaultAccountBucketId: number;
    PrefixId: number;
    RequiredMileageSourceId: number;
    CommunicationId: number;
    RatingId: number;
    SalesRepresentativeId: number;
    TierId: number;
}

Customer.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    CustomerNumber: {
        type: STRING,
        allowNull: false,
    },
    Name: {
        type: STRING,
        allowNull: false,
    },
    DefaultAccountBucketId: {
        type: BIGINT,
        allowNull: false,
    },
    PrefixId: {
        type: BIGINT,
        allowNull: false,
    },
    HidePrefix: {
        type: BOOLEAN,
        allowNull: false,
    },
    Active: {
        type: BOOLEAN,
        allowNull: false,
    },
    RequiredMileageSourceId: {
        type: BIGINT,
        allowNull: false,
    },
    CustomerType: {
        type: INTEGER,
        allowNull: false,
    },
    AdditionalBillOfladingText: {
        type: STRING,
        allowNull: false,
    },
    InvoiceTerms: {
        type: INTEGER,
        allowNull: false,
    },
    InvoiceRequiresCarrierProNumber: {
        type: BOOLEAN,
        allowNull: false,
    },
    InvoiceRequiresShipperReference: {
        type: BOOLEAN,
        allowNull: false,
    },
    InvoiceRequiresControlAccount: {
        type: BOOLEAN,
        allowNull: false,
    },
    ShipmentRequiresPurchaseOrderNumber: {
        type: BOOLEAN,
        allowNull: false,
    },
    ValidatePurchaseOrderNumber: {
        type: BOOLEAN,
        allowNull: false,
    },
    InvalidPurchaseOrderNumberMessage: {
        type: STRING,
        allowNull: false,
    },
    CustomVendorSelectionMessage: {
        type: STRING,
        allowNull: false,
    },
    ShipmentRequiresNMFC: {
        type: BOOLEAN,
        allowNull: false,
    },
    DateCreated: {
        type: DATE,
        allowNull: false,
    },
    LogoUrl: {
        type: STRING,
        allowNull: false,
    },
    ShipperBillOfLadingSeed: {
        type: BIGINT,
        allowNull: false,
    },
    EnableShipperBillOfLading: {
        type: BOOLEAN,
        allowNull: false,
    },
    ShipperBillOfLadingPrefix: {
        type: STRING,
        allowNull: false,
    },
    ShipperBillOfLadingSuffix: {
        type: STRING,
        allowNull: false,
    },
    CreditLimit: {
        type: DECIMAL,
        allowNull: false,
    },
    CommunicationId: {
        type: BIGINT,
        allowNull: false,
    },
    RatingId: {
        type: BIGINT,
        allowNull: false,
    },
    SalesRepresentativeId: {
        type: BIGINT,
        allowNull: false,
    },
    TierId: {
        type: BIGINT,
        allowNull: false,
    },
    Notes: {
        type: STRING,
        allowNull: false,
    },
    AuditInstructions: {
        type: STRING,
        allowNull: false,
    },
    CareOfAddressFormatEnabled: {
        type: BOOLEAN,
        allowNull: false,
    },
    TruckloadPickupTolerance: {
        type: INTEGER,
        allowNull: false,
    },
    TruckloadDeliveryTolerance: {
        type: INTEGER,
        allowNull: false,
    },
    CustomerToleranceEnabled: {
        type: BOOLEAN,
        allowNull: false,
    },
    CustomField1: {
        type: STRING,
        allowNull: false,
    },
    CustomField2: {
        type: STRING,
        allowNull: false,
    },
    CustomField3: {
        type: STRING,
        allowNull: false,
    },
    CustomField4: {
        type: STRING,
        allowNull: false,
    },
    PaymentGatewayKey: {
        type: STRING,
        allowNull: false,
    },
    IsCashOnly: {
        type: BOOLEAN,
        allowNull: false,
    },
    CanPayByCreditCard: {
        type: BOOLEAN,
        allowNull: false,
    },
    RateAndScheduleInDemo: {
        type: BOOLEAN,
        allowNull: false,
    },
    CanInvoiceNonDeliveredShipment: {
        type: BOOLEAN,
        allowNull: false,
    },
    PurgeExpiredQuotes: {
        type: BOOLEAN,
        allowNull: false,
    },
    AllowLocationContactNotification: {
        type: BOOLEAN,
        allowNull: false,
    },
}, {
    sequelize,
    modelName: "Customer",
    freezeTableName: true,
    tableName: "Customer",
    timestamps: false
});


const CustomerSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    CustomerNumber: {
        type: String,
    },
    Name: {
        type: String,
    },
    DefaultAccountBucketId: {
        type: mongoose.Types.ObjectId,
    },
    PrefixId: {
        type: mongoose.Types.ObjectId,
    },
    HidePrefix: {
        type: Boolean,
    },
    Active: {
        type: Boolean,
    },
    RequiredMileageSourceId: {
        type: mongoose.Types.ObjectId,
    },
    CustomerType: {
        type: Number,
    },
    AdditionalBillOfladingText: {
        type: String,
    },
    InvoiceTerms: {
        type: Number,
    },
    InvoiceRequiresCarrierProNumber: {
        type: Boolean,
    },
    InvoiceRequiresShipperReference: {
        type: Boolean,
    },
    InvoiceRequiresControlAccount: {
        type: Boolean,
    },
    ShipmentRequiresPurchaseOrderNumber: {
        type: Boolean,
    },
    ValidatePurchaseOrderNumber: {
        type: Boolean,
    },
    InvalidPurchaseOrderNumberMessage: {
        type: String,
    },
    CustomVendorSelectionMessage: {
        type: String,
    },
    ShipmentRequiresNMFC: {
        type: Boolean,
    },
    DateCreated: {
        type: Date,
    },
    LogoUrl: {
        type: String,
    },
    ShipperBillOfLadingSeed: {
        type: Number,
    },
    EnableShipperBillOfLading: {
        type: Boolean,
    },
    ShipperBillOfLadingPrefix: {
        type: String,
    },
    ShipperBillOfLadingSuffix: {
        type: String,
    },
    CreditLimit: {
        type: Number,
    },
    CommunicationId: {
        type: mongoose.Types.ObjectId,
    },
    RatingId: {
        type: mongoose.Types.ObjectId,
    },
    SalesRepresentativeId: {
        type: mongoose.Types.ObjectId,
    },
    TierId: {
        type: mongoose.Types.ObjectId,
    },
    Notes: {
        type: String,
    },
    AuditInstructions: {
        type: String,
    },
    CareOfAddressFormatEnabled: {
        type: Boolean,
    },
    TruckloadPickupTolerance: {
        type: Number,
    },
    TruckloadDeliveryTolerance: {
        type: Number,
    },
    CustomerToleranceEnabled: {
        type: Boolean,
    },
    CustomField1: {
        type: String,
    },
    CustomField2: {
        type: String,
    },
    CustomField3: {
        type: String,
    },
    CustomField4: {
        type: String,
    },
    PaymentGatewayKey: {
        type: String,
    },
    IsCashOnly: {
        type: Boolean,
    },
    CanPayByCreditCard: {
        type: Boolean,
    },
    RateAndScheduleInDemo: {
        type: Boolean,
    },
    CanInvoiceNonDeliveredShipment: {
        type: Boolean,
    },
    PurgeExpiredQuotes: {
        type: Boolean,
    },
    AllowLocationContactNotification: {
        type: Boolean,
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "Customers"
});

declare interface ICustomerCollection extends mongoose.Document {
    RatingId: string;
    CommunicationId: string;
}

const db = mongoose.connection.useDb(FINANCE_DB);
const CustomerCollection = db.model<ICustomerCollection>("Customers", CustomerSchema);

export {
    Customer,
    CustomerCollection,
    CustomerSchema
};
