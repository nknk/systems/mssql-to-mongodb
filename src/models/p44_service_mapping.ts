import { INTEGER, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { REGISTRY_DB } from "../config";

class P44ServiceMapping extends ModelWithTenantId {
    ServiceId: number;
}

P44ServiceMapping.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    ServiceId: {
        type: BIGINT,
        allowNull: false
    },
    Project44Code: {
        type: INTEGER,
        allowNull: false
    }
}, {
    sequelize,
    modelName: "P44ServiceMapping",
    freezeTableName: true,
    tableName: "P44ServiceMapping",
    timestamps: false
});


const P44ServiceMappingSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    },
    ServiceId: {
        type: mongoose.Types.ObjectId
    },
    Project44Code: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "P44ServiceMappings"
});

const db = mongoose.connection.useDb(REGISTRY_DB);
const P44ServiceMappingCollection = db.model("P44ServiceMappings", P44ServiceMappingSchema);

export { P44ServiceMapping, P44ServiceMappingSchema, P44ServiceMappingCollection };
