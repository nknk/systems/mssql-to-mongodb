import { STRING, INTEGER, BOOLEAN, DATE, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { REPORTING_DB } from "../config";

class ReportSchedule extends ModelWithTenantId {

    ReportConfigurationId: number;
    UserId: number;
    CustomerGroupId: number;
    VendorGroupId: number;
}

ReportSchedule.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    ScheduleInterval: {
        type: STRING,
    },
    Start: {
        type: DATE,
    },
    End: {
        type: DATE,
    },
    LastRun: {
        type: DATE,
    },
    Enabled: {
        type: BOOLEAN,
    },
    Time: {
        type: STRING,
    },
    ExcludeSunday: {
        type: BOOLEAN,
    },
    ExcludeMonday: {
        type: BOOLEAN,
    },
    ExcludeTuesday: {
        type: BOOLEAN,
    },
    ExcludeWednesday: {
        type: BOOLEAN,
    },
    ExcludeThursday: {
        type: BOOLEAN,
    },
    ExcludeFriday: {
        type: BOOLEAN,
    },
    ExcludeSaturday: {
        type: BOOLEAN,
    },
    NotifyEmails: {
        type: STRING,
    },
    ReportConfigurationId: {
        type: BIGINT,
    },
    Notify: {
        type: BOOLEAN,
    },
    UserId: {
        type: BIGINT,
    },
    CustomerGroupId: {
        type: BIGINT,
    },
    VendorGroupId: {
        type: BIGINT,
    },
    Extension: {
        type: INTEGER,
    },
    FtpEnabled: {
        type: BOOLEAN,
    },
    FtpUrl: {
        type: STRING,
    },
    SecureFtp: {
        type: BOOLEAN,
    },
    FtpUsername: {
        type: STRING,
    },
    FtpPassword: {
        type: STRING,
    },
    FtpDefaultFolder: {
        type: STRING,
    },
    SuppressNotificationForEmptyReport: {
        type: BOOLEAN,
    },
    DeliveryFilename: {
        type: STRING,
    },
    SSHFtp: {
        type: BOOLEAN,
    },
}, {
    sequelize,
    modelName: "ReportSchedule",
    freezeTableName: true,
    tableName: "ReportSchedule",
    timestamps: false
});


const ReportScheduleSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    },
    ScheduleInterval: {
        type: String
    },
    Start: {
        type: Date
    },
    End: {
        type: Date
    },
    LastRun: {
        type: Date
    },
    Enabled: {
        type: Boolean
    },
    Time: {
        type: String
    },
    ExcludeSunday: {
        type: Boolean
    },
    ExcludeMonday: {
        type: Boolean
    },
    ExcludeTuesday: {
        type: Boolean
    },
    ExcludeWednesday: {
        type: Boolean
    },
    ExcludeThursday: {
        type: Boolean
    },
    ExcludeFriday: {
        type: Boolean
    },
    ExcludeSaturday: {
        type: Boolean
    },
    NotifyEmails: {
        type: String
    },
    ReportConfigurationId: {
        type: mongoose.Types.ObjectId
    },
    Notify: {
        type: Boolean
    },
    UserId: {
        type: mongoose.Types.ObjectId
    },
    CustomerGroupId: {
        type: mongoose.Types.ObjectId
    },
    VendorGroupId: {
        type: mongoose.Types.ObjectId
    },
    Extension: {
        type: Number
    },
    FtpEnabled: {
        type: Boolean
    },
    FtpUrl: {
        type: String
    },
    SecureFtp: {
        type: Boolean
    },
    FtpUsername: {
        type: String
    },
    FtpPassword: {
        type: String
    },
    FtpDefaultFolder: {
        type: String
    },
    SuppressNotificationForEmptyReport: {
        type: Boolean
    },
    DeliveryFilename: {
        type: String
    },
    SSHFtp: {
        type: Boolean
    },
}, {
    versionKey: false,
    collection: "ReportSchedules"
});

const db = mongoose.connection.useDb(REPORTING_DB);
const ReportScheduleCollection = db.model("ReportSchedules", ReportScheduleSchema);

export { ReportSchedule, ReportScheduleCollection, ReportScheduleSchema };
