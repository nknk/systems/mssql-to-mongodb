import { STRING, DATE, BIGINT, DECIMAL, INTEGER, BOOLEAN } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";

class MiscReceipt extends ModelWithTenantId {
    UserId: number;
    ShipmentId: number;
    CustomerId: number;
    OriginalMiscReceiptId: number;
    LoadOrderId: number;
    PaymentProfileId: string;
}

MiscReceipt.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    ShipmentId: {
        type: BIGINT,
    },
    UserId: {
        type: BIGINT,
    },
    CustomerId: {
        type: BIGINT,
    },
    AmountPaid: {
        type: DECIMAL(18, 4),
    },
    PaymentDate: {
        type: DATE,
    },
    GatewayTransactionId: {
        type: STRING,
    },
    PaymentGatewayType: {
        type: INTEGER,
    },
    Reversal: {
        type: BOOLEAN,
    },
    OriginalMiscReceiptId: {
        type: BIGINT,
    },
    NameOnCard: {
        type: STRING,
    },
    LoadOrderId: {
        type: BIGINT,
    },
    PaymentProfileId: {
        type: STRING,
    }
}, {
    sequelize,
    modelName: "MiscReceipt",
    freezeTableName: true,
    tableName: "MiscReceipt",
    timestamps: false
});


const MiscReceiptSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    ShipmentId: {
        type: mongoose.Types.ObjectId,
    },
    UserId: {
        type: mongoose.Types.ObjectId,
    },
    CustomerId: {
        type: mongoose.Types.ObjectId,
    },
    AmountPaid: {
        type: Number,
    },
    PaymentDate: {
        type: Date,
    },
    GatewayTransactionId: {
        type: String,
    },
    PaymentGatewayType: {
        type: Number,
    },
    Reversal: {
        type: Boolean,
    },
    OriginalMiscReceiptId: {
        type: mongoose.SchemaTypes.Mixed,
    },
    NameOnCard: {
        type: String,
    },
    LoadOrderId: {
        type: mongoose.Types.ObjectId,
    },
    PaymentProfileId: {
        type: String
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "MiscReceipts"
});

declare interface IMiscReceiptDoc extends mongoose.Document {
    UserId: string;
    ShipmentId: string;
    CustomerId: string;
    OriginalMiscReceiptId: string;
    LoadOrderId: string;
    PaymentProfileId: string;
}

const db = mongoose.connection.useDb(FINANCE_DB);
const MiscReceiptCollection = db.model<IMiscReceiptDoc>(
    "MiscReceipts",
    MiscReceiptSchema
);

export {
    MiscReceipt,
    MiscReceiptCollection,
    MiscReceiptSchema
};
