import { STRING, INTEGER, FLOAT, BOOLEAN, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";
class LoadOrderItem extends ModelWithTenantId {
    PackageTypeId: number;
    LoadOrderId: number;
}

LoadOrderItem.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    Description: {
        type: STRING
    },
    FreightClass: {
        type: FLOAT
    },
    Comment: {
        type: STRING
    },
    Weight: {
        type: DECIMAL
    },
    Length: {
        type: DECIMAL
    },
    Width: {
        type: DECIMAL
    },
    Height: {
        type: DECIMAL
    },
    Quantity: {
        type: INTEGER
    },
    PieceCount: {
        type: INTEGER
    },
    IsStackable: {
        type: BOOLEAN
    },
    PackageTypeId: {
        type: BIGINT
    },
    Pickup: {
        type: INTEGER
    },
    Delivery: {
        type: INTEGER
    },
    NMFCCode: {
        type: STRING
    },
    HTSCode: {
        type: STRING
    },
    Value: {
        type: DECIMAL
    },
    LoadOrderId: {
        type: INTEGER
    },
    HazardousMaterial: {
        type: BOOLEAN
    }

}, {
    sequelize,
    modelName: "LoadOrderItem",
    freezeTableName: true,
    tableName: "LoadOrderItem",
    timestamps: false
});

const LoadOrderItemSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    Description: {
        type: String
    },
    FreightClass: {
        type: Number
    },
    Comment: {
        type: String
    },
    Weight: {
        type: Number
    },
    Length: {
        type: Number
    },
    Width: {
        type: Number
    },
    Height: {
        type: Number
    },
    Quantity: {
        type: Number
    },
    PieceCount: {
        type: Number
    },
    IsStackable: {
        type: Boolean
    },
    PackageTypeId: {
        type: mongoose.Types.ObjectId
    },
    Pickup: {
        type: Number
    },
    Delivery: {
        type: Number
    },
    NMFCCode: {
        type: String
    },
    HTSCode: {
        type: String
    },
    Value: {
        type: Number
    },
    LoadOrderId: {
        type: mongoose.Types.ObjectId
    },
    HazardousMaterial: {
        type: Boolean
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "LoadOrderItems"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const LoadOrderItemCollection = db.model("LoadOrderItems", LoadOrderItemSchema);

export { LoadOrderItem, LoadOrderItemCollection, LoadOrderItemSchema };
