import { STRING, BOOLEAN, DATE, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";

class CustomerLocation extends ModelWithTenantId {
    CustomerId: number;
    CountryId: number;
}

CustomerLocation.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    LocationNumber: {
        type: STRING,
    },
    BillToLocation: {
        type: BOOLEAN,
    },
    MainBillToLocation: {
        type: BOOLEAN,
    },
    Primary: {
        type: BOOLEAN,
    },
    DateCreated: {
        type: DATE,
    },
    CustomerId: {
        type: BIGINT,
    },
    Street1: {
        type: STRING,
    },
    Street2: {
        type: STRING,
    },
    City: {
        type: STRING,
    },
    State: {
        type: STRING,
    },
    CountryId: {
        type: BIGINT,
    },
    PostalCode: {
        type: STRING,
    }
}, {
    sequelize,
    modelName: "CustomerLocation",
    freezeTableName: true,
    tableName: "CustomerLocation",
    timestamps: false
});


const CustomerLocationSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    },
    LocationNumber: {
        type: String,
    },
    BillToLocation: {
        type: Boolean,
    },
    MainBillToLocation: {
        type: Boolean,
    },
    Primary: {
        type: Boolean,
    },
    DateCreated: {
        type: Date,
    },
    CustomerId: {
        type: mongoose.Types.ObjectId,
    },
    Street1: {
        type: String,
    },
    Street2: {
        type: String,
    },
    City: {
        type: String,
    },
    State: {
        type: String,
    },
    CountryId: {
        type: mongoose.Types.ObjectId,
    },
    PostalCode: {
        type: String,
    }
}, {
    versionKey: false,
    collection: "CustomerLocations"
});

const db = mongoose.connection.useDb(FINANCE_DB);
const CustomerLocationCollection = db.model("CustomerLocations", CustomerLocationSchema);

export { CustomerLocation, CustomerLocationCollection, CustomerLocationSchema };
