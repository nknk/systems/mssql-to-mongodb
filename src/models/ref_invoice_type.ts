import { STRING, INTEGER } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import {BaseModel } from "./contracts/base_models";
import {  OPERATIONS_DB } from "../config";

class RefInvoiceType extends BaseModel {
   InvoiceTypeIndex: number; 
}

RefInvoiceType.init({
 
    InvoiceTypeIndex: {
        type: INTEGER,
        primaryKey: true
    },
    InvoiceTypeText: {
        type: STRING
    },
  
}, {
    sequelize,
    modelName: "RefInvoiceType",
    freezeTableName: true,
    tableName: "RefInvoiceType",
    timestamps: false
});


const RefInvoiceTypeSchema = new mongoose.Schema({
    InvoiceTypeIndex: {
        type: Number,
    },
    InvoiceTypeText: {
        type:String
    },
}, {
    versionKey: false,
    collection: "RefInvoiceTypes"
});


const db = mongoose.connection.useDb(OPERATIONS_DB);
const RefInvoiceTypeCollection = db.model(
    "RefInvoiceTypes",
    RefInvoiceTypeSchema
);

export { RefInvoiceType, RefInvoiceTypeCollection, RefInvoiceTypeSchema };
