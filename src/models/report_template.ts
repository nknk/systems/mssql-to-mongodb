import { STRING, BOOLEAN, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { BaseModel } from "./contracts/base_models";
import { REPORTING_DB } from "../config";

class ReportTemplate extends BaseModel {

}

ReportTemplate.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    Name: {
        type: STRING,
    },
    Description: {
        type: STRING,
    },
    CanFilterByCustomerGroup: {
        type: BOOLEAN,
    },
    CanFilterByVendorGroup: {
        type: BOOLEAN,
    },
    DefaultCustomization: {
        type: STRING,
    },
    Query: {
        type: STRING,
    },
}, {
    sequelize,
    modelName: "ReportTemplate",
    freezeTableName: true,
    tableName: "ReportTemplate",
    timestamps: false
});


const ReportTemplateSchema = new mongoose.Schema({
    OldId: {
        type: Number
    },
    Name: {
        type: String
    },
    Description: {
        type: String
    },
    CanFilterByCustomerGroup: {
        type: Boolean
    },
    CanFilterByVendorGroup: {
        type: Boolean
    },
    DefaultCustomization: {
        type: String
    },
    Query: {
        type: String
    },
}, {
    versionKey: false,
    collection: "ReportTemplates"
});

const db = mongoose.connection.useDb(REPORTING_DB);
const ReportTemplateCollection = db.model("ReportTemplates", ReportTemplateSchema);

export { ReportTemplate, ReportTemplateCollection, ReportTemplateSchema };
