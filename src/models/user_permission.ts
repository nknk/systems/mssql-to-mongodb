import { Model, STRING, BOOLEAN, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { IDENTITY_DB } from "../config";

class UserPermission extends Model { }

UserPermission.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    UserId: {
        type: BIGINT
    },
    Code: {
        type: STRING
    },
    Description: {
        type: STRING
    },
    Grant: {
        type: BOOLEAN
    },
    Deny: {
        type: BOOLEAN
    },
    Modify: {
        type: BOOLEAN
    },
    Delete: {
        type: BOOLEAN
    },
    ModifyApplicable: {
        type: BOOLEAN
    },
    DeleteApplicable: {
        type: BOOLEAN
    }

}, {
    sequelize,
    modelName: "UserPermission",
    freezeTableName: true,
    tableName: "UserPermission",
    timestamps: false
});

const UserPermissionSchema = new mongoose.Schema({
    Code: {
        type: String
    },
    Description: {
        type: String
    },
    Grant: {
        type: Boolean
    },
    Deny: {
        type: Boolean
    },
    Modify: {
        type: Boolean
    },
    Delete: {
        type: Boolean
    },
    ModifyApplicable: {
        type: Boolean
    },
    DeleteApplicable: {
        type: Boolean
    }
}, {
    versionKey: false,
    collection: "UserPermissions",
    _id: false
});

const db = mongoose.connection.useDb(IDENTITY_DB);
const UserPermissionCollection = db.model("UserPermissions", UserPermissionSchema);

export { UserPermission, UserPermissionCollection, UserPermissionSchema };
