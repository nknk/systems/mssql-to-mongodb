import { STRING, INTEGER, BOOLEAN, DATE, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";

class Invoice extends ModelWithTenantId {
    CustomerLocationId: number;
    OriginalInvoiceId: string;
    OldOriginalInvoiceId: string;
    UserId: number;
    PrefixId: number;
}

Invoice.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    InvoiceNumber: {
        type: STRING
    },
    InvoiceDate: {
        type: DATE
    },
    DueDate: {
        type: DATE
    },
    SpecialInstruction: {
        type: STRING
    },
    InvoiceType: {
        type: INTEGER
    },
    Posted: {
        type: BOOLEAN
    },
    Exported: {
        type: BOOLEAN
    },
    PostDate: {
        type: DATE
    },
    ExportDate: {
        type: DATE
    },
    PaidAmount: {
        type: DECIMAL
    },
    CustomerLocationId: {
        type: BIGINT
    },
    UserId: {
        type: BIGINT
    },
    OriginalInvoiceId: {
        type: BIGINT
    },
    CustomerControlAccountNumber: {
        type: STRING
    },
    PrefixId: {
        type: BIGINT
    },
    HidePrefix: {
        type: BOOLEAN
    },
    FtpDelivered: {
        type: BOOLEAN
    }
}, {
    sequelize,
    modelName: "Invoice",
    freezeTableName: true,
    tableName: "Invoice",
    timestamps: false
});


const InvoiceSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    InvoiceNumber: {
        type: String
    },
    InvoiceDate: {
        type: Date
    },
    DueDate: {
        type: Date
    },
    SpecialInstruction: {
        type: String
    },
    InvoiceType: {
        type: Number
    },
    Posted: {
        type: Boolean
    },
    Exported: {
        type: Boolean
    },
    PostDate: {
        type: Date
    },
    ExportDate: {
        type: Date
    },
    PaidAmount: {
        type: Number
    },
    CustomerLocationId: {
        type: mongoose.Types.ObjectId
    },
    UserId: {
        type: mongoose.Types.ObjectId
    },
    OriginalInvoiceId: {
        type: mongoose.Types.ObjectId
    },
    OldOriginalInvoiceId: {
        type: String
    },
    CustomerControlAccountNumber: {
        type: String
    },
    PrefixId: {
        type: mongoose.Types.ObjectId
    },
    HidePrefix: {
        type: Boolean
    },
    FtpDelivered: {
        type: Boolean
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "Invoices"
});

export interface IInvoiceDocument extends mongoose.Document {
    OriginalInvoiceId: string;
    OldOriginalInvoiceId?: string;
}

const db = mongoose.connection.useDb(FINANCE_DB);
const InvoiceCollection = db.model<IInvoiceDocument>("Invoices", InvoiceSchema);

export { Invoice, InvoiceCollection, InvoiceSchema };
