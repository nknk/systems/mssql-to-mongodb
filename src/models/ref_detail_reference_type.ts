import { STRING, INTEGER } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { BaseModel } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class RefDetailReferenceType extends BaseModel {
    DetailReferenceTypeIndex: number;
}

RefDetailReferenceType.init({

    DetailReferenceTypeIndex: {
        type: INTEGER,
        primaryKey: true
    },
    DetailReferenceTypeText: {
        type: STRING
    },

}, {
    sequelize,
    modelName: "RefDetailReferenceType",
    freezeTableName: true,
    tableName: "RefDetailReferenceType",
    timestamps: false
});


const RefDetailReferenceTypeSchema = new mongoose.Schema({
    DetailReferenceTypeIndex: {
        type: Number
    },
    DetailReferenceTypeText: {
        type: String
    },
}, {
    versionKey: false,
    collection: "RefDetailReferenceTypes"
});


const db = mongoose.connection.useDb(OPERATIONS_DB);
const RefDetailReferenceTypeCollection = db.model(
    "RefDetailReferenceTypes",
    RefDetailReferenceTypeSchema
);

export { RefDetailReferenceType, RefDetailReferenceTypeCollection, RefDetailReferenceTypeSchema };
