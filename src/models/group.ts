import { STRING, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { IDENTITY_DB } from "../config";

class Group extends ModelWithTenantId {}

Group.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    Name: {
        type: STRING,
        allowNull: false
    },
    Description: {
        type: STRING,
        allowNull: false
    },
}, {
    sequelize,
    modelName: "Group",
    freezeTableName: true,
    tableName: "Group",
    timestamps: false
});


const GroupSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    Name: {
        type: String,
    },
    Description: {
        type: String,
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "Groups"
});

const db = mongoose.connection.useDb(IDENTITY_DB);
const GroupCollection = db.model("Groups", GroupSchema);

export { Group, GroupCollection, GroupSchema };
