import { STRING, BOOLEAN, INTEGER, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { REGISTRY_DB } from "../config";

class Service extends ModelWithTenantId {
    ChargeCodeId: number;
}

Service.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    Code: {
        type: STRING,
        allowNull: false
    },
    Description: {
        type: STRING,
        allowNull: false
    },
    Category: {
        type: INTEGER,
        allowNull: false
    },
    ChargeCodeId: {
        type: BIGINT,
        allowNull: false
    },
    DispatchFlag: {
        type: INTEGER,
        allowNull: false
    },
    ApplicableAtPickup: {
        type: BOOLEAN,
        allowNull: false
    },
    ApplicableAtDelivery: {
        type: BOOLEAN,
        allowNull: false
    },
    Project44Code: {
        type: INTEGER,
        allowNull: false
    },
}, {
    sequelize,
    modelName: "Service",
    freezeTableName: true,
    tableName: "Service",
    timestamps: false
});


const ServiceSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    Code: {
        type: String
    },
    Description: {
        type: String
    },
    OldId: {
        type: Number
    },
    Category: {
        type: Number
    },
    ChargeCodeId: {
        type: mongoose.Types.ObjectId
    },
    DispatchFlag: {
        type: Number
    },
    ApplicableAtPickup: {
        type: Boolean
    },
    ApplicableAtDelivery: {
        type: Boolean
    },
    Project44Code: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "Services"
});

const db = mongoose.connection.useDb(REGISTRY_DB);
const ServiceCollection = db.model("Services", ServiceSchema);

export { Service, ServiceSchema, ServiceCollection };
