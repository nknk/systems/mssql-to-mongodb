import { STRING, INTEGER, DATE, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class Job extends ModelWithTenantId {
    CreatedByUserId: number;
    CustomerId: number;
}

Job.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    JobNumber: {
        type: STRING
    },
    DateCreated: {
        type: DATE
    },
    Status: {
        type: INTEGER
    },
    ExternalReference1: {
        type: STRING
    },
    ExternalReference2: {
        type: STRING
    },
    CustomerId: {
        type: BIGINT
    },
    CreatedByUserId: {
        type: BIGINT
    }
}, {
    sequelize,
    modelName: "Job",
    freezeTableName: true,
    tableName: "Job",
    timestamps: false
});


const JobSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    JobNumber: {
        type: String
    },
    DateCreated: {
        type: Date
    },
    Status: {
        type: Number
    },
    ExternalReference1: {
        type: String
    },
    ExternalReference2: {
        type: String
    },
    CustomerId: {
        type: mongoose.Types.ObjectId
    },
    CreatedByUserId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "Jobs"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const JobCollection = db.model("Jobs", JobSchema);

export { Job, JobCollection, JobSchema };
