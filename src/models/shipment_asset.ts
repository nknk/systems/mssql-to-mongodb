import { INTEGER, BOOLEAN, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class ShipmentAsset extends ModelWithTenantId {
    ShipmentId: number;
    DriverAssetId: number;
    TrailerAssetId: number;
    TractorAssetId: number;
}

ShipmentAsset.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    ShipmentId: {
        type: BIGINT
    },
    DriverAssetId: {
        type: BIGINT
    },
    TractorAssetId: {
        type: BIGINT
    },
    TrailerAssetId: {
        type: BIGINT
    },
    MilesRun: {
        type: DECIMAL
    },
    Primary: {
        type: BOOLEAN
    },
    MileageEngine: {
        type: INTEGER
    }
}, {
    sequelize,
    modelName: "ShipmentAsset",
    freezeTableName: true,
    tableName: "ShipmentAsset",
    timestamps: false
});


const ShipmentAssetSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    ShipmentId: {
        type: mongoose.Types.ObjectId
    },
    DriverAssetId: {
        type: mongoose.Types.ObjectId
    },
    TractorAssetId: {
        type: mongoose.Types.ObjectId
    },
    TrailerAssetId: {
        type: mongoose.Types.ObjectId
    },
    MilesRun: {
        type: Number
    },
    Primary: {
        type: Boolean
    },
    MileageEngine: {
        type: Number
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "ShipmentAssets"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const ShipmentAssetCollection = db.model(
    "ShipmentAssets",
    ShipmentAssetSchema
);

export { ShipmentAsset, ShipmentAssetCollection, ShipmentAssetSchema };
