import { BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class ClaimServiceTicket extends ModelWithTenantId {
    ServiceTicketId: number;
    ClaimId: number;
}

ClaimServiceTicket.init({
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    ServiceTicketId: {
        type: BIGINT,
    },
    ClaimId: {
        type: BIGINT,
    },
}, {
    sequelize,
    modelName: "ClaimServiceTicket",
    freezeTableName: true,
    tableName: "ClaimServiceTicket",
    timestamps: false
});


const ClaimServiceTicketSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    ServiceTicketId: {
        type: mongoose.Types.ObjectId,
    },
    ClaimId: {
        type: mongoose.Types.ObjectId,
    },
}, {
    versionKey: false,
    collection: "ClaimServiceTickets"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const ClaimServiceTicketCollection = db
    .model("ClaimServiceTickets", ClaimServiceTicketSchema);

export {
    ClaimServiceTicket,
    ClaimServiceTicketCollection,
    ClaimServiceTicketSchema
};
