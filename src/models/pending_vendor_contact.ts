import { STRING, BOOLEAN, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";

class PendingVendorContact extends ModelWithTenantId {
    PendingVendorLocationId: number;
    ContactTypeId: number;
}

PendingVendorContact.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    PendingVendorLocationId: {
        type: BIGINT,
        allowNull: false
    },
    Name: {
        type: STRING,
        allowNull: false
    },
    Phone: {
        type: STRING,
        allowNull: false
    },
    Mobile: {
        type: STRING,
        allowNull: false
    },
    Fax: {
        type: STRING,
        allowNull: false
    },
    Email: {
        type: STRING,
        allowNull: false
    },
    Comment: {
        type: STRING,
        allowNull: false
    },
    Primary: {
        type: BOOLEAN,
        allowNull: false
    },
    ContactTypeId: {
        type: BIGINT,
        allowNull: false
    },
}, {
    sequelize,
    modelName: "PendingVendorContact",
    freezeTableName: true,
    tableName: "PendingVendorContact",
    timestamps: false
});


const PendingVendorContactSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    },
    PendingVendorLocationId: {
        type: mongoose.Types.ObjectId
    },
    Name: {
        type: String
    },
    Phone: {
        type: String
    },
    Mobile: {
        type: String
    },
    Fax: {
        type: String
    },
    Email: {
        type: String
    },
    Comment: {
        type: String
    },
    Primary: {
        type: Boolean
    },
    ContactTypeId: {
        type: mongoose.Types.ObjectId

    },
}, {
    versionKey: false,
    collection: "PendingVendorContacts"
});

const db = mongoose.connection.useDb(FINANCE_DB);
const PendingVendorContactCollection = db
    .model(
        "PendingVendorContacts",
        PendingVendorContactSchema
    );

export {
    PendingVendorContact,
    PendingVendorContactCollection,
    PendingVendorContactSchema
};
