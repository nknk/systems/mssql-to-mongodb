import { STRING, INTEGER, DATE, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";
class LoadOrderLocation extends ModelWithTenantId {
    LoadOrderId: number;
    CountryId: number;
}

LoadOrderLocation.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    Description: {
        type: STRING
    },
    SpecialInstructions: {
        type: STRING
    },
    GeneralInfo: {
        type: STRING
    },
    Direction: {
        type: STRING
    },
    StopOrder: {
        type: INTEGER
    },
    AppointmentDateTime: {
        type: DATE
    },
    LoadOrderId: {
        type: BIGINT
    },
    Street1: {
        type: STRING
    },
    Street2: {
        type: STRING
    },
    City: {
        type: STRING
    },
    State: {
        type: STRING
    },
    CountryId: {
        type: BIGINT
    },
    PostalCode: {
        type: STRING
    },
    MilesFromPreceedingStop: {
        type: DECIMAL
    }
}, {
    sequelize,
    modelName: "LoadOrderLocation",
    freezeTableName: true,
    tableName: "LoadOrderLocation",
    timestamps: false
});

const LoadOrderLocationSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    Description: {
        type: String
    },
    SpecialInstructions: {
        type: String
    },
    GeneralInfo: {
        type: String
    },
    Direction: {
        type: String
    },
    StopOrder: {
        type: Number
    },
    AppointmentDateTime: {
        type: Date
    },
    LoadOrderId: {
        type: mongoose.Types.ObjectId
    },
    Street1: {
        type: String
    },
    Street2: {
        type: String
    },
    City: {
        type: String
    },
    State: {
        type: String
    },
    CountryId: {
        type: mongoose.SchemaTypes.ObjectId
    },
    PostalCode: {
        type: String
    },
    MilesFromPreceedingStop: {
        type: Number
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "LoadOrderLocations"
});

export interface ILoadOrderLocationDoc extends mongoose.Document {
    LoadOrderId: string;
}

const db = mongoose.connection.useDb(OPERATIONS_DB);
const LoadOrderLocationCollection = db
    .model<ILoadOrderLocationDoc>(
        "LoadOrderLocations",
        LoadOrderLocationSchema
    );

export { LoadOrderLocation, LoadOrderLocationCollection, LoadOrderLocationSchema };
