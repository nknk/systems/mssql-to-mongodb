import { DATE, BIGINT, DECIMAL } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";

class MiscReceiptApplication extends ModelWithTenantId {
    MiscReceiptId: number;
    InvoiceId: number;
}

MiscReceiptApplication.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    MiscReceiptId: {
        type: BIGINT,
    },
    InvoiceId: {
        type: BIGINT,
    },
    ApplicationDate: {
        type: DATE,
    },
    Amount: {
        type: DECIMAL,
    },
}, {
    sequelize,
    modelName: "MiscReceiptApplication",
    freezeTableName: true,
    tableName: "MiscReceiptApplication",
    timestamps: false
});


const MiscReceiptApplicationSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    MiscReceiptId: {
        type: mongoose.Types.ObjectId,
    },
    InvoiceId: {
        type: mongoose.Types.ObjectId,
    },
    ApplicationDate: {
        type: Date,
    },
    Amount: {
        type: Number,
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "MiscReceiptApplications"
});

declare interface IMiscReceiptApplicationCollection extends mongoose.Document {
    MiscReceiptId: string;
    InvoiceId: string;
}

const db = mongoose.connection.useDb(FINANCE_DB);
const MiscReceiptApplicationCollection = db
    .model<IMiscReceiptApplicationCollection>(
        "MiscReceiptApplications",
        MiscReceiptApplicationSchema
    );

export {
    MiscReceiptApplication,
    MiscReceiptApplicationCollection,
    MiscReceiptApplicationSchema
};
