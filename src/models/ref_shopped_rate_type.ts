import { STRING, INTEGER } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import {BaseModel } from "./contracts/base_models";
import {  OPERATIONS_DB } from "../config";

class RefShoppedRateType extends BaseModel {
   ShoppedRateTypeIndex: number; 
}

RefShoppedRateType.init({
 
    ShoppedRateTypeIndex: {
        type: INTEGER,
        primaryKey: true
    },
    ShoppedRateTypeText: {
        type: STRING
    },
  
}, {
    sequelize,
    modelName: "RefShoppedRateType",
    freezeTableName: true,
    tableName: "RefShoppedRateType",
    timestamps: false
});


const RefShoppedRateTypeSchema = new mongoose.Schema({
    ShoppedRateTypeIndex: {
        type: Number,
    },
    ShoppedRateTypeText: {
        type:String
    },
}, {
    versionKey: false,
    collection: "RefShoppedRateTypes"
});


const db = mongoose.connection.useDb(OPERATIONS_DB);
const RefShoppedRateTypeCollection = db.model(
    "RefShoppedRateTypes",
    RefShoppedRateTypeSchema
);

export { RefShoppedRateType, RefShoppedRateTypeCollection, RefShoppedRateTypeSchema };
