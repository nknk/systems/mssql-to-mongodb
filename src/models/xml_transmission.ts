import { INTEGER, STRING, BOOLEAN, DATE, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class XMLTransmission extends ModelWithTenantId {
    CommunicationReferenceId: number;
    UserId: number;
    CommunicationReferenceType: number;
}

XMLTransmission.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    TransmissionKey: {
        type: STRING
    },
    Xml: {
        type: STRING
    },
    DocumentType: {
        type: STRING
    },
    CommunicationReferenceType: {
        type: INTEGER
    },
    CommunicationReferenceId: {
        type: BIGINT
    },
    TransmissionDateTime: {
        type: DATE
    },
    AcknowledgementDateTime: {
        type: DATE
    },
    AcknowledgementMessage: {
        type: STRING
    },
    ReferenceNumber: {
        type: STRING
    },
    ReferenceNumberType: {
        type: INTEGER
    },
    NotificationMethod: {
        type: INTEGER
    },
    UserId: {
        type: BIGINT
    },
    SendOkay: {
        type: BOOLEAN
    },
    Direction: {
        type: INTEGER
    }
}, {
    sequelize,
    modelName: "XMLTransmission",
    freezeTableName: true,
    tableName: "XMLTransmission",
    timestamps: false
});

const XMLTransmissionSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    TransmissionKey: {
        type: String
    },
    Xml: {
        type: String
    },
    DocumentType: {
        type: String
    },
    CommunicationReferenceType: {
        type: Number
    },
    CommunicationReferenceId: {
        type: mongoose.Types.ObjectId
    },
    TransmissionDateTime: {
        type: Date
    },
    AcknowledgementDateTime: {
        type: Date
    },
    AcknowledgementMessage: {
        type: String
    },
    ReferenceNumber: {
        type: String
    },
    ReferenceNumberType: {
        type: Number
    },
    NotificationMethod: {
        type: Number
    },
    UserId: {
        type: mongoose.Types.ObjectId
    },
    SendOkay: {
        type: Boolean
    },
    Direction: {
        type: Number
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "XMLTransmissions"
});

const db = mongoose.connection.useDb(OPERATIONS_DB);
const XMLTransmissionCollection = db
    .model("XMLTransmissions", XMLTransmissionSchema);

export { XMLTransmission, XMLTransmissionCollection, XMLTransmissionSchema };
