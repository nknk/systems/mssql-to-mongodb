import { STRING, INTEGER } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { BaseModel } from "./contracts/base_models";
import { OPERATIONS_DB } from "../config";

class RefBrokerReferenceNumberType extends BaseModel {
    BrokerReferenceNumberTypeIndex: number;
}

RefBrokerReferenceNumberType.init({

    BrokerReferenceNumberTypeIndex: {
        type: INTEGER,
        primaryKey: true
    },
    BrokerReferenceNumberTypeText: {
        type: STRING
    },

}, {
    sequelize,
    modelName: "RefBrokerReferenceNumberType",
    freezeTableName: true,
    tableName: "RefBrokerReferenceNumberType",
    timestamps: false
});


const RefBrokerReferenceNumberTypeSchema = new mongoose.Schema({
    BrokerReferenceNumberTypeIndex: {
        type: Number,
    },
    BrokerReferenceNumberTypeText: {
        type: String
    }
}, {
    versionKey: false,
    collection: "RefBrokerReferenceNumberTypes"
});


const db = mongoose.connection.useDb(OPERATIONS_DB);
const RefBrokerReferenceNumberTypeCollection = db.model(
    "RefBrokerReferenceNumberTypes",
    RefBrokerReferenceNumberTypeSchema
);

export {
    RefBrokerReferenceNumberType,
    RefBrokerReferenceNumberTypeCollection,
    RefBrokerReferenceNumberTypeSchema
};
