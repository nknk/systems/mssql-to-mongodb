import { STRING, INTEGER, BOOLEAN, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";

class CustomerNotification extends ModelWithTenantId {
    CustomerCommunicationId: number;
}

CustomerNotification.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },

    TenantId: {
        type: BIGINT,
        allowNull: false
    },

    CustomerCommunicationId: {
        type: BIGINT
    },

    Milestone: {
        type: INTEGER
    },

    NotificationMethod: {
        type: INTEGER
    },

    Email: {
        type: STRING
    },

    Fax: {
        type: STRING
    },

    Enabled: {
        type: BOOLEAN
    }
}, {
    sequelize,
    modelName: "CustomerNotification",
    freezeTableName: true,
    tableName: "CustomerNotification",
    timestamps: false
});


const CustomerNotificationSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },

    CustomerCommunicationId: {
        type: mongoose.Types.ObjectId
    },

    Milestone: {
        type: Number
    },

    NotificationMethod: {
        type: Number
    },

    Email: {
        type: String
    },

    Fax: {
        type: String
    },

    Enabled: {
        type: Boolean
    },

    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "CustomerNotifications"
});


const db = mongoose.connection.useDb(FINANCE_DB);
const CustomerNotificationCollection = db.model("CustomerNotifications", CustomerNotificationSchema);

export {
    CustomerNotification,
    CustomerNotificationCollection,
    CustomerNotificationSchema
};
