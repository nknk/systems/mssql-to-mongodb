import { STRING, INTEGER, DATE, BOOLEAN, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { UTILITIES_DB } from "../config";

class DocDeliveryLog extends ModelWithTenantId {
    DocumentTagId: number;
    UserId: number;
    EntityId: number;
    EntityType: string;
}

DocDeliveryLog.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    EntityId: {
        type: BIGINT
    },
    EntityType: {
        type: INTEGER
    },
    LocationPath: {
        type: STRING
    },
    DocumentName: {
        type: STRING
    },
    LogDateTime: {
        type: DATE
    },
    DeliveryWasSuccessful: {
        type: BOOLEAN
    },
    FailedDeliveryMessage: {
        type: STRING
    },
    DocumentTagId: {
        type: BIGINT
    },
    UserId: {
        type: BIGINT
    }

}, {
    sequelize,
    modelName: "DocDeliveryLog",
    freezeTableName: true,
    tableName: "DocDeliveryLog",
    timestamps: false
});

const DocDeliveryLogSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    EntityId: {
        type: mongoose.Types.ObjectId
    },
    EntityType: {
        type: Number
    },
    LocationPath: {
        type: String
    },
    DocumentName: {
        type: String
    },
    LogDateTime: {
        type: Date
    },
    DeliveryWasSuccessful: {
        type: Boolean
    },
    FailedDeliveryMessage: {
        type: String
    },
    DocumentTagId: {
        type: mongoose.Types.ObjectId
    },
    UserId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "DocDeliveryLogs"
});

const db = mongoose.connection.useDb(UTILITIES_DB);
const DocDeliveryLogCollection = db.model("DocDeliveryLogs", DocDeliveryLogSchema);

export { DocDeliveryLog, DocDeliveryLogCollection, DocDeliveryLogSchema };
