import { STRING, DATE, BOOLEAN, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { UTILITIES_DB } from "../config";
class DeveloperAccessRequest extends ModelWithTenantId {
    CustomerId: number;
}

DeveloperAccessRequest.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
        allowNull: false
    },
    ContactName: {
        type: STRING
    },
    ContactEmail: {
        type: STRING
    },
    DateCreated: {
        type: DATE
    },
    CustomerId: {
        type: BIGINT
    },
    TestInformation: {
        type: STRING
    },
    ProductionAccess: {
        type: BOOLEAN
    },
    AccessGranted: {
        type: BOOLEAN
    }
}, {
    sequelize,
    modelName: "DeveloperAccessRequest",
    freezeTableName: true,
    tableName: "DeveloperAccessRequest",
    timestamps: false
});


const DeveloperAccessRequestSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    ContactName: {
        type: String
    },
    ContactEmail: {
        type: String
    },
    DateCreated: {
        type: Date
    },
    CustomerId: {
        type: mongoose.Types.ObjectId
    },
    TestInformation: {
        type: String
    },
    ProductionAccess: {
        type: Boolean
    },
    AccessGranted: {
        type: Boolean
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "DeveloperAccessRequests"
});


const db = mongoose.connection.useDb(UTILITIES_DB);
const DeveloperAccessRequestCollection = db.model("DeveloperAccessRequests", DeveloperAccessRequestSchema);

export {
    DeveloperAccessRequest,
    DeveloperAccessRequestCollection,
    DeveloperAccessRequestSchema
};
