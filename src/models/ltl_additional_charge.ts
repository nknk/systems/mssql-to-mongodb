import { DATE, STRING, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { RATING_DB } from "../config";

class LTLAdditionalCharge extends ModelWithTenantId {
    ChargeCodeId: number;
    VendorRatingId: number;
}

LTLAdditionalCharge.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    Name: {
        type: STRING,
    },
    ChargeCodeId: {
        type: BIGINT,
    },
    EffectiveDate: {
        type: DATE,
    },
    VendorRatingId: {
        type: BIGINT,
    },
}, {
    sequelize,
    modelName: "LTLAdditionalCharge",
    freezeTableName: true,
    tableName: "LTLAdditionalCharge",
    timestamps: false
});


const LTLAdditionalChargeSchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    OldId: {
        type: Number
    },
    Name: {
        type: String
    },
    ChargeCodeId: {
        type: mongoose.Types.ObjectId
    },
    EffectiveDate: {
        type: Date
    },
    VendorRatingId: {
        type: mongoose.Types.ObjectId
    },
}, {
    versionKey: false,
    collection: "LTLAdditionalCharges"
});

const db = mongoose.connection.useDb(RATING_DB);
const LTLAdditionalChargeCollection = db
    .model("LTLAdditionalCharges", LTLAdditionalChargeSchema);

export { LTLAdditionalCharge, LTLAdditionalChargeCollection, LTLAdditionalChargeSchema };
