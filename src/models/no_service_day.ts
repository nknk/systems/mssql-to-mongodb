import { STRING, DATE, BIGINT } from "sequelize";
import sequelize from "../config/sequelize";
import mongoose from "mongoose";
import { ModelWithTenantId } from "./contracts/base_models";
import { FINANCE_DB } from "../config";

class NoServiceDay extends ModelWithTenantId {
}

NoServiceDay.init({
    Id: {
        type: BIGINT,
        primaryKey: true
    },
    TenantId: {
        type: BIGINT,
    },
    DateOfNoService: {
        type: DATE
    },
    Description: {
        type: STRING
    }
}, {
    sequelize,
    modelName: "NoServiceDay",
    freezeTableName: true,
    tableName: "NoServiceDay",
    timestamps: false
});


const NoServiceDaySchema = new mongoose.Schema({
    TenantId: {
        type: mongoose.Types.ObjectId
    },
    DateOfNoService: {
        type: Date
    },
    Description: {
        type: String
    },
    OldId: {
        type: Number
    }
}, {
    versionKey: false,
    collection: "NoServiceDays"
});

const db = mongoose.connection.useDb(FINANCE_DB);
const NoServiceDayCollection = db.model(
    "NoServiceDays",
    NoServiceDaySchema
);

export { NoServiceDay, NoServiceDayCollection, NoServiceDaySchema };
