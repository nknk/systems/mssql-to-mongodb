export interface IOldKey {
    Id: number;
    OldId: number;
}

export interface IWithTenantId extends IOldKey {
    TenantId: string;
}