import { Model, FindOptions, QueryTypes } from "sequelize";
import { IWithTenantId, IOldKey } from "./interfaces";
import { Pagination } from "../../config";
import { consoleColor } from "../../util";
import sequelize from "../../config/sequelize";
import { cpus } from "os";

declare interface IPaginateOptions extends FindOptions {
    itemsPerRoll?: number;
}

declare interface IPaginators {
    paginator: Pagination;
    lastPage: number;
}

export class BaseModel extends Model implements IOldKey {
    static paginationOptions: IPaginateOptions;
    static paginationCallback: (collection: BaseModel[]) => void;

    Id: number;
    OldId: number;

    public static async rollByRow<M extends BaseModel>(
        this: { new(): M } & typeof BaseModel,
        orderField: string,
        callback: (collection: M[]) => void, options?: IPaginateOptions
    ) {

        this.paginationOptions = options || {};
        this.paginationCallback = callback;

        const paginators = await this.getPaginators();

        const promises = [];

        for (const paginator of paginators) {
            promises.push(
                this.runPaginated(paginator, orderField)
            );
        }
        await Promise.all(promises);
    }

    private static getPaginatedResults<M extends BaseModel>(
        this: { new(): M } & typeof BaseModel,
        paginator: Pagination,
    ) {
        return this.findAll<M>({
            limit: paginator.itemsPerPage,
            offset: paginator.getSkip(),
            raw: true,
            ...this.paginationOptions
        });
    }

    private static getRolledResults<M extends BaseModel>(paginator: Pagination, rollField: string) {
        const query =
            `SELECT ROW_NUMBER() OVER (ORDER BY ${rollField}) row_num, * FROM ${this.tableName} 
            ORDER BY row_num 
            OFFSET ${paginator.getSkip()} 
            ROWS FETCH NEXT ${paginator.itemsPerPage} ROWS ONLY`;

        return sequelize.query<M>(query, {
            type: QueryTypes.SELECT,
        });
    }

    private static async runPaginated(
        params: { paginator: Pagination; lastPage: number },
        rollField?: string,
    ) {
        const {
            paginator,
            lastPage,
        } = params;

        while (paginator.getCurrentPage() <= lastPage) {

            console.log(consoleColor("cyan"));
            console.log(`Paginating for ${this.name} : ${paginator.getCurrentPage()}/${lastPage}`);
            console.log(consoleColor("white"));

            let results;

            if (rollField) {
                results = await this.getRolledResults(paginator, rollField);
            } else {
                results = await this.getPaginatedResults(paginator);
            }

            await this.paginationCallback(results);
            paginator.currentPage += 1;
        }
    }

    private static async getPaginators() {
        const where = this.paginationOptions.where || {};
        const totalItems = await this.count({where});
        const itemsPerPage = 1000;
        const paginator = new Pagination({ currentPage: 1, totalItems, itemsPerPage });
        const totalPages = paginator.getTotalPages();
        const totalCpus = cpus().length;
        const perThreadPages = totalPages < totalCpus ? Math.ceil(totalCpus/totalPages) : Math.ceil(totalPages / totalCpus);
        const paginators: IPaginators[] = [];

        if (totalPages > perThreadPages) {
            for (let i = 0; i <= totalPages; i += perThreadPages) {
                const currentPage = i + 1;
                let lastPage = i + perThreadPages;
                lastPage = lastPage > totalPages ? totalPages : lastPage;

                const pagination = new Pagination({
                    currentPage, totalItems, itemsPerPage
                });

                paginators.push({ paginator: pagination, lastPage });
            }
        } else {
            paginators.push({ paginator, lastPage: totalPages });
        }

        return paginators;
    }

    /**
       * Paginate and execute callback for each set of collection.
       *
       *
       * Model.rollPagination((collection) => { }, {
       * itemsPerRoll: 100
       * }))
       *
       */
    public static async rollPagination<M extends BaseModel>(
        this: { new(): M } & typeof BaseModel,
        callback: (collection: M[]) => void, options?: IPaginateOptions
    ) {

        this.paginationOptions = options || {};
        this.paginationCallback = callback;

        const paginators = await this.getPaginators();

        const promises = [];

        for (const paginator of paginators) {
            promises.push(
                this.runPaginated(paginator)
            );
        }

        await Promise.all(promises);
    }
}

export class ModelWithTenantId extends BaseModel implements IWithTenantId {
    TenantId: string;
}