import { consoleColor } from "./util";
import { connectMsSql, connectMongo } from "./config";
import { readdir as fsReadDir } from "fs";
import { join, resolve } from "path";
import { promisify } from "util";
import { MigrationCollection } from "./models";
const readdir = promisify(fsReadDir);

export class Migration {

    private sequelize: any;
    private mongoose: Record<string, any>;
    private completed = {};

    constructor() {
        this.sequelize = null;
        this.mongoose = null;
    }

    private async _connectToDatabases() {
        if (!this.sequelize || !this.mongoose || this.mongoose.connection.readyState != 1) {
            console.log("Connecting to databases ... !");

            this.sequelize = await connectMsSql();
            this.mongoose = await connectMongo();
        }
    }

    private async _consoleLogStart() {
        const migrations = await this._getMigrations();
        console.log(consoleColor("magenta"), `Total migrations: ${migrations.length}`);
        console.log(consoleColor("white"));
    }

    private async _run() {
        await this._upMigrations();
    }

    private async _drop() {
        await this._downMigrations();
    }

    private async _upMigrations() {
        const migrations = await this._getMigrations();
        const completed = this._getTotalCompleted();
        const totalMigrations = migrations.length + completed;
        let counter = (completed) ? completed: 1;
        for await (const migration of migrations) {
            console.log(consoleColor("green"));
            console.log(`Running migration ${counter}/${totalMigrations}}`);
            console.log(consoleColor("magenta"));
            console.log(`Running ${migration.fileName}`, consoleColor("white"));
            await migration.module.up(this.mongoose);
            await MigrationCollection.create({ file: migration.fileName });
            console.log(consoleColor("green"));
            console.log(`Done ${migration.fileName}`);
            console.log(consoleColor("white"));
            counter++;
        }
    }

    private async _downMigrations() {
        const migrations = await this._getMigrations();
        migrations.reverse();
        const completed = this._getTotalCompleted();
        const totalMigrations = migrations.length + completed;
        let counter = (completed) ? completed: 1;
        for await (const migration of migrations) {
            console.log(consoleColor("green"));
            console.log(`Running migration ${counter}/${totalMigrations}`);
            console.log(consoleColor("magenta"));
            console.log(`Starting undo ${migration.fileName}`, consoleColor("white"));
            await migration.module.down(this.mongoose);
            console.log(consoleColor("green"));
            console.log(`Done Undo ${migration.fileName}`);
            console.log(consoleColor("white"));
            counter++;
        }
    }

    private async _getMigrations() {
        const dir = join(__dirname, "/migrations");
        let fileNames = await readdir(dir);
        fileNames = fileNames.filter((fileName) => {
            const validExtensions = [".ts", ".js"];
            const extension = fileName.slice(-3);
            const isCompleted = this.completed[fileName] || false;
            return validExtensions.includes(extension) && !isCompleted;
        });
        return fileNames.map(async (fileName) => {
            const importPath = resolve(dir, fileName);
            const module = await import(importPath);
            return { fileName, module };
        });
    }

    private async _runMigration() {
        return this._run();
    }

    public async up() {
        const start = new Date().getTime();

        await this._connectToDatabases();
        await this._loadCompleted();
        await this._consoleLogStart();
        await this._runMigration();

        console.log(consoleColor("green"));
        console.log("MigrationUpTime:", new Date().getTime() - start);
        console.log(consoleColor("white"));
    }

    public async down() {

        const start = new Date().getTime();
        console.time("MigrationDownTime");

        await this._connectToDatabases();
        await this._loadCompleted();
        await this._drop();

        console.log(consoleColor("green"));
        console.log("MigrationDownTime:", new Date().getTime() - start);
        console.log(consoleColor("white"));
    }

    private async _loadCompleted() {
        if (!this._getTotalCompleted()) {
            const migrations = await MigrationCollection.find({});
            for (const migration of migrations) {
                this.completed[migration.file] = 1;
            }
        }
    }

    private _getTotalCompleted() {
        return Object.keys(this.completed).length;
    }

}
