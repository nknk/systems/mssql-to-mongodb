import { Migration } from "./migration";
import { Cosmos } from "./config";
import dotenv from "dotenv";
dotenv.config();

const cosmos = new Cosmos();
const migration = new Migration();

async function runMigrations() {
  console.time("runMigrations");
  if (process.env.COSMOS_URI) {
    await cosmos.setup();
  }
  await migration.down();
  await migration.up();

  console.timeEnd("runMigrations");
}

runMigrations().then(() => {
  console.log("!!DONE!!");
  process.exit();
});