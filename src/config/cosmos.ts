import dotenv from "dotenv";
import { CosmosClient, CosmosClientOptions, ContainerDefinition } from "@azure/cosmos";
import {
    AUDITLOG_DB,
    FINANCE_DB,
    IDENTITY_DB,
    OPERATIONS_DB,
    RATING_DB,
    REGISTRY_DB,
    REPORTING_DB,
    UTILITIES_DB
} from "./database";

dotenv.config();

const Databases: Array<Record<any, any>> = [
    {
        name: AUDITLOG_DB,
        collections: {
            AdminAuditLogs: ["/EntityCode"],
            AppPerformanceApplicationLogs: ["/AppLogType"],
        },
    }, {
        name: FINANCE_DB,
        collections: {
            AverageWeeklyFuels: ["/TenantId"],
            CommissionPayments: ["/Type"],
            CustomFields: ["/Name"],
            CustomerChargeCodeMaps: ["/ChargeCodeId"],
            CustomerCommunications: ["/CustomerId"],
            CustomerContacts: ["/CustomerLocationId"],
            CustomerControlAccounts: ["/CustomerId"],
            CustomerDocuments: ["/CustomerId"],
            CustomerGroupMaps: ["/CustomerGroupId"],
            CustomerGroups: ["/TenantId"],
            CustomerLocations: ["/CustomerId"],
            CustomerNotifications: ["/CustomerCommunicationId"],
            CustomerPurchaseOrders: ["/CustomerId"],
            CustomerServiceMarkups: ["/CustomerRatingId"],
            CustomerServiceRepresentatives: ["/CustomerId"],
            CustomerTLTenderingProfileLanes: ["/TenantId"],
            CustomerTLTenderingProfileVendors: ["/VendorId"],
            CustomerTLTenderingProfiles: ["/CustomerId"],
            Customers: ["/Name"],
            InvoiceDetails: ["/InvoiceId"],
            InvoicePrintLogs: ["/InvoiceId"],
            Invoices: ["/UserId"],
            PendingVendorContacts: ["/PendingVendorLocationId"],
            PendingVendorDocuments: ["/PendingVendorId"],
            PendingVendorEquipments: ["/PendingVendorId"],
            PendingVendorInsurances: ["/PendingVendorId"],
            PendingVendorLocations: ["/PendingVendorId"],
            PendingVendorServices: ["/PendingVendorId"],
            PendingVendors: ["/TenantId"],
            RequiredInvoiceDocumentTags: ["/DocumentTagId"],
            SalesRepCommTiers: ["/CustomerId"],
            SalesRepresentatives: ["/Name"],
            Tiers: ["/Name"],
            VendorBillDetails: ["/VendorBillId"],
            VendorBills: ["/VendorLocationId"],
            VendorCommunications: ["/VendorId"],
            VendorContacts: ["/VendorLocationId"],
            VendorDisputeDetails: ["/DisputedId"],
            VendorDisputes: ["/VendorId"],
            VendorDocuments: ["/VendorId"],
            VendorGroups: ["/Name"],
            VendorInsuranceLiabilityTiers: ["/VendorRatingId"],
            VendorInsurances: ["/VendorId"],
            VendorLocations: ["/VendorId"],
            VendorNotifications: ["/VendorCommunicationId"],
            VendorPackageCustomMappings: ["/VendorId"],
            VendorRejectionLogs: ["/VendorId"],
            VendorServices: ["/VendorId"],
            VendorTerminals: ["/VendorId"],
            Vendors: ["/TenantId"],
            CollectApprovedVendors: ["/VendorId"],
        },
    }, {
        name: IDENTITY_DB,
        collections: {
            AdminUsers: ["/Username"],
            AutoNumbers: ["/Code"],
            Countries: ["/Code"],
            GroupPermissions: ["/GroupId"],
            GroupUsers: ["/GroupId"],
            Groups: ["/Name"],
            PostalCodes: ["/Code"],
            TenantContacts: ["/TenantLocationId"],
            TenantLocations: ["/TenantId"],
            Tenants: ["/Code"],
            UserShipAs: ["/CustomerId"],
            Users: ["/Username"],
            EntityLocks: ["/LockKey"],
        },
    }, {
        name: OPERATIONS_DB,
        collections: {
            AddressBookContacts: ["/AddressBookId"],
            AddressBooks: ["/CustomerId"],
            AssetLogs: ["/AssetLogs"],
            Assets: ["/Code"],
            ClaimDocuments: ["/ClaimId"],
            ClaimNotes: ["/ClaimId"],
            ClaimShipments: ["/ClaimId"],
            ClaimVendors: ["/VendorId"],
            Claims: ["/CustomerId"],
            DatLoadBoardAssetPostings: ["/AssetId"],
            JobDocuments: ["/JobId"],
            Jobs: ["/CustomerId"],
            LibraryItems: ["/CustomerId"],
            LoadOrderAccountBuckets: ["/AccountBucketId"],
            LoadOrderCharges: ["/LoadOrderId"],
            LoadOrderContacts: ["/ContactTypeId"],
            LoadOrderDocuments: ["/LoadOrderId"],
            LoadOrderEquipments: ["/LoadOrderId"],
            LoadOrderHistoricalDatas: ["/LoadOrderId"],
            LoadOrderItems: ["/LoadOrderId"],
            LoadOrderLocations: ["/LoadOrderId"],
            LoadOrderNotes: ["/LoadOrderId"],
            LoadOrderReferences: ["/LoadOrderId"],
            LoadOrderServices: ["/LoadOrderId"],
            LoadOrderVendors: ["/LoadOrderId"],
            LoadOrders: ["/CustomerId"],
            Project44DispatchResponses: ["/UserId"],
            Project44ImagesResponses: ["/UserId"],
            Project44RatingResponses: ["/UserId"],
            Project44TrackingDatas: ["/Project44Id"],
            Project44TrackingResponses: ["/UserId"],
            RestTransmissionLogs: ["/UserId"],
            SearchProfileDefaults: ["/UserId"],
            ServiceTicketAssets: ["/ServiceTicketId"],
            ServiceTicketCharges: ["/VendorId"],
            ServiceTicketDocuments: ["/ServiceTicketId"],
            ServiceTicketEquipments: ["/ServiceTicketId"],
            ServiceTicketItems: ["/ServiceTicketId"],
            ServiceTicketNotes: ["/ServiceTicketId"],
            ServiceTicketVendors: ["/ServiceTicketId"],
            ShipmentAccountBuckets: ["/ShipmentId"],
            ShipmentAssets: ["/ShipmentId"],
            ShipmentAutoRatingAccessorials: ["/ShipmentId"],
            ShipmentCharges: ["/ShipmentId"],
            ShipmentContacts: ["/ContactTypeId"],
            ShipmentDocRtrvLogs: ["/UserId"],
            ShipmentDocuments: ["/ShipmentId"],
            ShipmentEquipments: ["/ShipmentId"],
            ShipmentHistoricalDatas: ["/ShipmentId"],
            ShipmentItems: ["/ShipmentId"],
            ShipmentLocations: ["/ShipmentId"],
            ShipmentNotes: ["/ShipmentId"],
            ShipmentQuickPayOptions: ["/ShipmentId"],
            ShipmentReferences: ["/ShipmentId"],
            ShipmentServices: ["/ShipmentId"],
            ShipmentVendors: ["/ShipmentId"],
            Shipments: ["/CustomerId"],
            ShoppedRateServices: ["/ServiceId"],
            ShoppedRates: ["/UserId"],
            VendorEquipments: ["/VendorId"],
            VendorGroupMaps: ["/VendorId"],
            VendorNoServiceDays: ["/VendorId"],
            VendorPreferredLanes: ["/VendorId"],
            XMLConnects: ["/TenantId"],
            XMLTransmissions: ["/UserId"],
            ProcQueObjs: ["/ObjType"],
            ChangeLogImages: ["/ChangelogId"],
            CheckCalls: ["/UserId"],
            ClaimServiceTickets: ["/ClaimId"],
            CmsContents: ["/ContentKey"],
            CmsNews: ["/NewsKey"],
            MacroPointOrders: ["/UserId"],
        },
    }, {
        name: RATING_DB,
        collections: {
            AdditionalChargeIndices: ["/LTLAdditionalChargeId"],
            Areas: ["/RegionId"],
            BatchRateData: ["/VendorRatingId"],
            CustomerRatings: ["/CustomerId"],
            DiscountTiers: ["/VendorRatingId"],
            FuelIndexs: ["/FuelTableId"],
            FuelTables: ["/TenantId"],
            LTLAccessorials: ["/VendorRatingId"],
            LTLAdditionalCharges: ["/VendorRatingId"],
            LTLCubicFootCapacityRules: ["/VendorRatingId"],
            LTLGuaranteedCharges: ["/VendorRatingId"],
            LTLOverlengthRules: ["/VendorRatingId"],
            LTLPackageSpecificRates: ["/VendorRatingId"],
            LTLPcfToFcConversions: ["/VendorRatingId"],
            LTLSellRates: ["/VendorRatingId"],
            LTLTerminalInfos: ["/ShipmentId"],
            Regions: ["/TenantId"],
            ResellerAdditions: ["/Name"],
            SMC3DispatchRequestResponses: ["/ShipmentId"],
            SMC3TrackRequestResponses: ["/ShipmentId"],
            SMC3TrackingDatas: ["/OriginTerminalCity"],
            SmallPackRates: ["/CustomerRatingId"],
            SmallPackageServiceMaps: ["/ServiceId"],
            SmallPackagingMaps: ["/PackageTypeId"],
            TLSellRates: ["/EquipmentTypeId"],
            TruckloadBids: ["/LoadOrderId"],
            VendorRatings: ["/VendorId"],
        },
    }, {
        name: REGISTRY_DB,
        collections: {
            AccountBucketUnits: ["/AccountBucketId"],
            AccountBuckets: ["/Code"],
            ChangeLogs: ["/UserId"],
            ChargeCodes: ["/Code"],
            ContactTypes: ["/Code"],
            DocumentPrefixMaps: ["/PrefixId"],
            DocumentTags: ["/Code"],
            DocumentTemplates: ["/Code"],
            EquipmentTypes: ["/Code"],
            FailureCodes: ["/Code"],
            InsuranceTypes: ["/Code"],
            MileageSources: ["/Code"],
            NoServiceDays: ["/TenantId"],
            P44ChargeCodeMappings: ["/Project44Code"],
            P44ServiceMappings: ["/ServiceId"],
            PackageTypes: ["/TypeName"],
            PermissionDetails: ["/TenantId"],
            Prefixes: ["/Code"],
            QuickPayOptions: ["/Code"],
            Services: ["/Code"],
            Settings: ["/Settings"],
            ShipmentPriorities: ["/TenantId"],
            UserDepartments: ["/Code"],
            YrMthBusDays: ["/Year"],
        },
    }, {
        name: REPORTING_DB,
        collections: {
            ReportConfigurations: ["/UserId"],
            ReportCustomizationGroups: ["/GroupId"],
            ReportCustomizationUsers: ["/UserId"],
            ReportSchedules: ["/UserId"],
            ReportTemplates: ["/Name"],
        },
    }, {
        name: UTILITIES_DB,
        collections: {
            Announcements: ["/TenantId"],
            DeveloperAccessRequests: ["/CustomerId"],
            DocDeliveryDocTags: ["/DocumentTagId"],
            EmailLogs: ["/TenantId"],
            FaxTransmissions: ["/TenantId"],
        }
    }];

export class Cosmos {
    private options: CosmosClientOptions;
    private throughput: number;
    private client: CosmosClient;

    constructor() {
        this.throughput = parseInt(process.env.COSMOS_THROUGHPUT);

        this.options = {
            endpoint: process.env.COSMOS_URI,
            key: process.env.COSMOS_PRIMARY_KEY
        };

        this.client = new CosmosClient(this.options);
    }

    private async _createDatabase(offerThroughput: number, databaseId: string) {
        const { database } = await this.client.databases.createIfNotExists({ id: databaseId }, { offerThroughput });
        console.log(`Created database:\n${database.id}\n`);
    }

    private async _createContainer(
        offerThroughput: number,
        databaseId: string,
        containerId: string,
        partitionKeyPaths: string[],
    ) {

        const containerRequest: ContainerDefinition = {
            id: containerId,
            partitionKey: { paths: partitionKeyPaths }
        };

        const { container } = await this.client.database(databaseId).containers
            .createIfNotExists(containerRequest, { offerThroughput });
        console.log(`Created container:\n${container.id}\n`);
    }
    private exit(message: string) {
        console.log(message);
        console.log("Press any key to exit");
        process.stdin.setRawMode(true);
        process.stdin.resume();
        process.stdin.on("data", process.exit.bind(process, 0));
    };

    public async setup() {
        for (const db of Databases) {
            await this._createDatabase(this.throughput, db.name);

            for (const containerId in db.collections) {
                const partitionKeyPaths = db.collections[containerId];
                await this._createContainer(
                    this.throughput,
                    db.name,
                    containerId,
                    partitionKeyPaths,
                );
            }
        }
    }
}
