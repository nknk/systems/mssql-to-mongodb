import {Sequelize} from "sequelize";
import { sqlConfig } from "./app_config";

const { username, password, database } = sqlConfig;
export default new Sequelize(database, username, password, sqlConfig);