import dotenv from "dotenv";
import { Options } from "sequelize";
import { ConnectionOptions } from "mongoose";
dotenv.config();


const mongoConnectionOptions: ConnectionOptions = {
  useNewUrlParser: true,
  useCreateIndex: true,
  poolSize: 60,
  ssl: process.env.MONGO_SSL === "true",
};

export const mongoConfig = {
  auth: {
    host: process.env.MONGO_HOST || "localhost",
    port: process.env.MONGO_PORT || 27017,
    database: process.env.MONGO_DB || "",
    username: process.env.MONGO_USERNAME || "",
    password: process.env.MONGO_PASSWORD || "",
  },
  options: mongoConnectionOptions
};

export const sqlConfig: Options = {
  username: process.env.SQL_USERNAME || "",
  password: process.env.SQL_PASSWORD || "",
  database: process.env.SQL_DB || "",
  host: process.env.SQL_HOST || "localhost",
  port: parseInt(process.env.SQL_PORT) || 1433,
  pool: {max: 60},
  dialect: "mssql",
  dialectOptions: {
    options: {
      requestTimeout: 300000,
      encrypt: true,
    }
  }
};