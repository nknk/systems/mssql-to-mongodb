import dotenv from "dotenv";
dotenv.config();

const IDENTITY_DB = process.env.IDENTITY_DB || "IdentityDB";
const REGISTRY_DB = process.env.REGISTRY_DB || "RegistryDB";
const FINANCE_DB = process.env.FINANCE_DB || "FinanceDB";
const RATING_DB = process.env.RATING_DB || "RatingDB";
const OPERATIONS_DB = process.env.OPERATIONS_DB || "OperationsDB";
const UTILITIES_DB = process.env.UTILITIES_DB || "UtilitiesDB";
const AUDITLOG_DB = process.env.AUDITLOG_DB || "AuditLogDB";
const REPORTING_DB = process.env.REPORTING_DB || "ReportingDB";

export {
    IDENTITY_DB,
    REGISTRY_DB,
    FINANCE_DB,
    RATING_DB,
    OPERATIONS_DB,
    UTILITIES_DB,
    AUDITLOG_DB,
    REPORTING_DB,
};
