import mongoose from "mongoose";
import { consoleColor } from "../util/console-color";
import { mongoConfig } from "./app_config";

let previousQueryString = "";

const buildQueryString = () => {
 const {
  username, password, database, host, port,
 } = mongoConfig.auth;
  const authenticationString = (username && password) ? `${username}:${password}@` : "";
  if (!database) throw Error("Should specify a databse which you want to connect");

  return `mongodb://${authenticationString}${host}:${port}/${database}`;
};

export const connectMongo = () => {
  return new Promise((res, rej) => {
    console.log(consoleColor("yellow"), "Connecting to mongo...", consoleColor("white"));
    const queryString = buildQueryString();

    if (mongoose.connection.readyState == 1 && previousQueryString === queryString) {
      console.log(consoleColor("green"), "Connected to mongo Successfully!", consoleColor("white"));
      return res(mongoose);
    }
    mongoose.connect(queryString, mongoConfig.options);
    const db = mongoose.connection;

    db.once("open", () => {
      console.log(consoleColor("green"), "Connected to mongo Successfully!", consoleColor("white"));
      previousQueryString = queryString;
      res(mongoose);
    });

    db.on("error", (er) => {
      console.log(consoleColor("red"), "Connection to mongo error!", consoleColor("white"));
      rej(er);
    });
  });
};
