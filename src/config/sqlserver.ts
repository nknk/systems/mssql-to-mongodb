import { consoleColor } from "../util/console-color";
import sequelize from "./sequelize";

export const connectMsSql = () => {
  return new Promise((res, rej) => {
    console.log(consoleColor("yellow"), "Connecting to mssql...", consoleColor("white"));
    sequelize
      .authenticate()
      .then(() => {
        console.log(consoleColor("green"), "Connected to mssql successfully!", consoleColor("white"));
        res(sequelize);
      })
      .catch((err: any) => {
        console.log(consoleColor("red"), "Error connecting to mssql!", consoleColor("white"));
        console.log(consoleColor("red"), err, consoleColor("white"));
        rej(err);
      });
  });
};
