import { RefSmallPackageEngine, RefSmallPackageEngineCollection } from "../models";

export const up = async () => {
    await RefSmallPackageEngine
        .rollPagination(async (results) => {
            await RefSmallPackageEngineCollection
                .insertMany(results);
        });
};

export const down = async () => {
    await RefSmallPackageEngineCollection.deleteMany({});
};