import { MileageSource, MileageSourceCollection, TenantCollection } from "../models";

export const up = async () => {
    const results = await MileageSource.findAll(
        {
            raw: true,
            attributes: ["*", ["Id", "OldId"]]
        }
    );

    for (const result of results) {
        const tenant = await TenantCollection.findOne({ OldId: result.TenantId });
        result.TenantId = tenant._id;
    };

    await MileageSourceCollection.insertMany(results);
};

export const down = async () => {
    await MileageSourceCollection.deleteMany({});
};

