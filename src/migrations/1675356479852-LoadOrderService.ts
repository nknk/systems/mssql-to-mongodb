import {
    TenantCollection,
    LoadOrderCollection,
    ServiceCollection,
    LoadOrderService,
    LoadOrderServiceCollection,
} from "../models";

export const up = async () => {
    await LoadOrderService
        .rollByRow("LoadOrderId", async (loadOrderServices) => {
            for (const loadOrderService of loadOrderServices) {
                const tenant = await TenantCollection
                    .findOne({ OldId: loadOrderService.TenantId });
                const loadOrder = await LoadOrderCollection
                    .findOne({ OldId: loadOrderService.LoadOrderId });
                const service = await ServiceCollection
                    .findOne({ OldId: loadOrderService.ServiceId });

                loadOrderService.LoadOrderId = loadOrder && loadOrder._id || null;
                loadOrderService.ServiceId = service && service._id || null;
                loadOrderService.TenantId = tenant._id;
            };
            await LoadOrderServiceCollection
                .insertMany(loadOrderServices);
        });
};

export const down = async () => {
    await LoadOrderServiceCollection.deleteMany({});
};

