import {
    TenantCollection,
    BackgroundReportRunRecord,
    BackgroundReportRunRecordCollection,
    UserCollection,
} from "../models";

export const up = async () => {
    await BackgroundReportRunRecord.rollPagination(
        async (records) => {
            for (const record of records) {
                const tenant = await TenantCollection
                    .findOne({ OldId: record.TenantId });

                const user = await UserCollection
                    .findOne({ OldId: record.UserId });

                record.TenantId = tenant._id;
                record.UserId = user ? user._id : null;
                record.OldId = record.Id;
            };
            await BackgroundReportRunRecordCollection
                .insertMany(records);
        }
    );

};

export const down = async () => {
    await BackgroundReportRunRecordCollection.deleteMany({});
};

