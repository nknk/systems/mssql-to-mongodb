import {
    TenantCollection,
    QlikSheet,
    ReportConfigurationCollection,
    QlikSheetCollection
} from "../models";

export const up = async () => {
    await QlikSheet
        .rollPagination(async (results) => {

            for (const result of results) {

                const tenant = await TenantCollection
                    .findOne({ OldId: result.TenantId });

                result.TenantId = tenant ? tenant._id : null;

                if (result.ReportConfigurationId.toString() === "0") {
                    result.ReportConfigurationId = null;
                } else {
                    const reportConfig = await ReportConfigurationCollection
                        .findOne({ OldId: result.ReportConfigurationId });
                    result.ReportConfigurationId = reportConfig ? reportConfig._id : null;
                }

            }

            await QlikSheetCollection
                .insertMany(results);
        }, {
            attributes: ["*", ["Id", "OldId"]]
        });

};

export const down = async () => {
    await QlikSheetCollection
        .deleteMany({});
};
