import {
    ClaimDocument,
    TenantCollection,
    ClaimDocumentCollection,
    ClaimCollection,
    DocumentTagCollection
} from "../models";

const associateRelations = async (result: ClaimDocument) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const claim = await ClaimCollection
        .findOne({ OldId: result.ClaimId });
    const documentTag = await DocumentTagCollection
        .findOne({ OldId: result.DocumentTagId });
    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.ClaimId = claim && claim._id || null;
    result.DocumentTagId = documentTag && documentTag._id || null;
};

export const up = async () => {
    await ClaimDocument.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await ClaimDocumentCollection.insertMany(results);
    });
};

export const down = async () => {
    await ClaimDocumentCollection.deleteMany({});
};