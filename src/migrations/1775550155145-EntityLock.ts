import {
    TenantCollection,
    ChangeLogCollection,
    CustomerCollection,
    CustomerGroupCollection,
    GroupCollection,
    RegionCollection,
    ShipmentCollection,
    VendorDisputeCollection,
    EntityLock,
    UserCollection,
    EntityLockCollection,
} from "../models";
import { Document, Model } from "mongoose";

const lockKeyCollection = {
    "Changelog": ChangeLogCollection,
    "Customer": CustomerCollection,
    "Customer Group": CustomerGroupCollection,
    "Group": GroupCollection,
    "Region": RegionCollection,
    "Shipment": ShipmentCollection,
    "Vendor Dispute": VendorDisputeCollection,
};

export const up = async () => {
    await EntityLock
        .rollPagination(async (results) => {
            for (const result of results) {
                const tenant = await TenantCollection
                    .findOne({ OldId: result.TenantId });
                const user = await UserCollection
                    .findOne({ OldId: result.LockUserId });

                const entityCollection: Model<Document, {}> = lockKeyCollection[result.LockKey] || null;

                const entity = entityCollection ? await entityCollection
                    .findOne({ OldId: result.EntityId }) : null;

                result.EntityId = entity ? entity._id : null;
                result.TenantId = tenant ? tenant._id : null;
                result.LockUserId = user ? user._id : null;
            }
            await EntityLockCollection
                .insertMany(results);
        }, {
            attributes: ["*", ["Id", "OldId"]]
        });
};

export const down = async () => {
    await EntityLockCollection
        .deleteMany({});
};

