import { RefProject44ChargeCode, RefProject44ChargeCodeCollection } from "../models";

export const up = async () => {
    await RefProject44ChargeCode
        .rollPagination(async (results) => {
            await RefProject44ChargeCodeCollection
                .insertMany(results);
        });
};

export const down = async () => {
    await RefProject44ChargeCodeCollection
        .deleteMany({});
};