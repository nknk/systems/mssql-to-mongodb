import {
    ResellerAddition,
    TenantCollection,
    CustomerCollection,
    ResellerAdditionCollection,
} from "../models";

export const up = async () => {
    const results = await ResellerAddition.findAll(
        {
            raw: true,
            attributes: ["*", ["Id", "OldId"]]
        }
    );

    for (const result of results) {
        const tenant = await TenantCollection
            .findOne({ OldId: result.TenantId });
        const customer = await CustomerCollection
            .findOne({ OldId: result.ResellerCustomerAccountId });
        result.TenantId = tenant._id;
        result.ResellerCustomerAccountId = customer? customer._id: null;
    };

    await ResellerAdditionCollection.insertMany(results);
};

export const down = async () => {
    await ResellerAdditionCollection.deleteMany({});
};

