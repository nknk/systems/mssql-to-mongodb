import { Tier, TenantCollection, TierCollection } from "../models";

export const up = async () => {
    await Tier
        .rollPagination(async (results) => {
            for (const result of results) {
                const tenant = await TenantCollection
                    .findOne({ OldId: result.TenantId });
                result.TenantId = tenant._id;
                result.OldId = result.Id;
            };

            await TierCollection.insertMany(results);
        });
};

export const down = async () => {
    await TierCollection.deleteMany({});
};

