import {
    ShipmentDocument,
    TenantCollection,
    ShipmentDocumentCollection,
    ShipmentCollection,
    DocumentTagCollection,
    VendorBillCollection
} from "../models";

const associateRelations = async (result: ShipmentDocument) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const shipment = await ShipmentCollection
        .findOne({ OldId: result.ShipmentId });
    const documentTag = await DocumentTagCollection
        .findOne({ OldId: result.DocumentTagId });
    const vendorBill = await VendorBillCollection
        .findOne({ OldId: result.VendorBillId });


    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.ShipmentId = shipment && shipment._id || null;
    result.DocumentTagId = documentTag && documentTag._id || null;
    result.VendorBillId = vendorBill && vendorBill._id || null;
};

export const up = async () => {
    await ShipmentDocument.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await ShipmentDocumentCollection.insertMany(results);
    });
};

export const down = async () => {
    await ShipmentDocumentCollection.deleteMany({});
};