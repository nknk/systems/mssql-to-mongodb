import {
    ShipmentCharge,
    TenantCollection,
    ShipmentChargeCollection,
    ShipmentCollection,
    ChargeCodeCollection,
    VendorCollection,
    VendorBillCollection
} from "../models";

const associateRelations = async (result: ShipmentCharge) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const vendor = await VendorCollection
        .findOne({ OldId: result.VendorId });
    const shipment = await ShipmentCollection
        .findOne({ OldId: result.ShipmentId });
    const chargeCode = await ChargeCodeCollection
        .findOne({ OldId: result.ChargeCodeId });
    const vendorBill = await VendorBillCollection
        .findOne({ OldId: result.VendorBillId });

    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.VendorId = vendor && vendor._id || null;
    result.ShipmentId = shipment && shipment._id || null;
    result.ChargeCodeId = chargeCode && chargeCode._id || null;
    result.VendorBillId = vendorBill && vendorBill._id || null;
};

export const up = async () => {
    await ShipmentCharge.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await ShipmentChargeCollection.insertMany(results);
    });
};

export const down = async () => {
    await ShipmentChargeCollection.deleteMany({});
};