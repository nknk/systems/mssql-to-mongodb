import {
    ShipmentDocRtrvLog,
    TenantCollection,
    ShipmentDocRtrvLogCollection,
    VendorCommunicationCollection,
    UserCollection
} from "../models";

const associateRelations = async (result: ShipmentDocRtrvLog) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const vendorCommunication = await VendorCommunicationCollection
        .findOne({ OldId: result.CommunicationId });
    const user = await UserCollection
        .findOne({ OldId: result.UserId });
    

    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.CommunicationId = vendorCommunication && vendorCommunication._id || null;
    result.UserId = user && user._id || null;
};

export const up = async () => {
    await ShipmentDocRtrvLog.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await ShipmentDocRtrvLogCollection.insertMany(results);
    });
};

export const down = async () => {
    await ShipmentDocRtrvLogCollection.deleteMany({});
};