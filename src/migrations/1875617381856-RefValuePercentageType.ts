import { RefValuePercentageType, RefValuePercentageTypeCollection } from "../models";

export const up = async () => {
    await RefValuePercentageType
        .rollPagination(async (results) => {
            await RefValuePercentageTypeCollection
                .insertMany(results);
        });
};

export const down = async () => {
    await RefValuePercentageTypeCollection.deleteMany({});
};