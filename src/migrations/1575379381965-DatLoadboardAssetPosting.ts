import {
    TenantCollection,
    UserCollection,
    DatLoadBoardAssetPosting,
    DatLoadBoardAssetPostingCollection,
} from "../models";

export const up = async () => {
    await DatLoadBoardAssetPosting
        .rollPagination(async (datloadboards) => {
            for (const datloadboard of datloadboards) {
                const tenant = await TenantCollection
                    .findOne({ OldId: datloadboard.TenantId });

                const user = await UserCollection
                    .findOne({ OldId: datloadboard.UserId });

                datloadboard.UserId = user && user._id || null;
                datloadboard.TenantId = tenant._id;
                datloadboard.OldId = datloadboard.Id;
            }
            await DatLoadBoardAssetPostingCollection
                .insertMany(datloadboards);
        });
};

export const down = async () => {
    await DatLoadBoardAssetPostingCollection.deleteMany({});
};

