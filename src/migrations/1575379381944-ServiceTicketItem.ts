import {
    ServiceTicketItem,
    TenantCollection,
    ServiceTicketItemCollection,
    ServiceTicketCollection
} from "../models";

const associateRelations = async (result: ServiceTicketItem) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const serviceTicket = await ServiceTicketCollection
        .findOne({ OldId: result.ServiceTicketId });

    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.ServiceTicketId = serviceTicket && serviceTicket._id || null;
};


export const up = async () => {
    await ServiceTicketItem.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await ServiceTicketItemCollection.insertMany(results);
    });
};

export const down = async () => {
    await ServiceTicketItemCollection.deleteMany({});
};