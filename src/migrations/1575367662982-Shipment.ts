import {
    TenantCollection,
    CustomerCollection,
    Shipment,
    PrefixCollection,
    UserCollection,
    MileageSourceCollection,
    SalesRepresentativeCollection,
    ShipmentCollection,
    LTLTerminalInfoCollection,
    IShipmentDoc,
    ResellerAdditionCollection,
    ShipmentLocationCollection,
    AccountBucketUnitCollection,
    ShipmentPriorityCollection,
    JobCollection
} from "../models";

const updateRelations = async (shipment: Shipment) => {

    const {
        TenantId,
        PrefixId,
        CustomerId,
        UserId,
        MileageSourceId,
        SalesRepresentativeId,
        ResellerAdditionId,
        AccountBucketUnitId,
        CarrierCoordinatorUserId,
        ShipmentCoordinatorUserId,
        ShipmentPriorityId,
        JobId,
    } = shipment;

    const tenant = await TenantCollection
        .findOne({ OldId: TenantId });

    const prefix = await PrefixCollection
        .findOne({ OldId: PrefixId });

    const customer = await CustomerCollection
        .findOne({ OldId: CustomerId });

    const user = await UserCollection
        .findOne({ OldId: UserId });

    const mileageSource = await MileageSourceCollection
        .findOne({ OldId: MileageSourceId });

    const salesRepresentative = await SalesRepresentativeCollection
        .findOne({ OldId: SalesRepresentativeId });

    const resellerAddition = await ResellerAdditionCollection
        .findOne({ OldId: ResellerAdditionId });

    const bucketUnit = await AccountBucketUnitCollection
        .findOne({ OldId: AccountBucketUnitId });

    const carrierCoordinatorUser = await UserCollection
        .findOne({ OldId: CarrierCoordinatorUserId });

    const shipmentCoordinatorUser = await UserCollection
        .findOne({ OldId: ShipmentCoordinatorUserId });

    const shipmentPriority = await ShipmentPriorityCollection
        .findOne({ OldId: ShipmentPriorityId });
    
    const job = await JobCollection.findOne({ OldId: JobId });


    shipment.TenantId = tenant._id;
    shipment.PrefixId = prefix && prefix._id || null;
    shipment.CustomerId = customer && customer._id || null;
    shipment.UserId = user && user._id || null;
    shipment.MileageSourceId = mileageSource && mileageSource._id || null;;
    shipment.SalesRepresentativeId = salesRepresentative && salesRepresentative._id || null;
    shipment.ResellerAdditionId = resellerAddition && resellerAddition._id || null;
    shipment.AccountBucketUnitId = bucketUnit ? bucketUnit._id : null;
    shipment.CarrierCoordinatorUserId = carrierCoordinatorUser ? carrierCoordinatorUser._id : null;
    shipment.ShipmentCoordinatorUserId = shipmentCoordinatorUser ? shipmentCoordinatorUser._id : null;
    shipment.ShipmentPriorityId = shipmentPriority ? shipmentPriority._id : null;
    shipment.JobId = job ? job._id : null;

};

const associateTerminals = async (shipment: IShipmentDoc) => {

    const destinationTerminal = await LTLTerminalInfoCollection.findOne({
        OldId: shipment.DestinationTerminalInfoId
    });

    shipment.DestinationTerminalInfoId = destinationTerminal ?
        destinationTerminal._id : null;

    const originTerminal = await LTLTerminalInfoCollection.findOne({
        OldId: shipment.OriginTerminalInfoId
    });

    shipment.OriginTerminalInfoId = originTerminal ?
        originTerminal._id : null;

    const origin = await ShipmentLocationCollection
        .findOne({ OldId: shipment.OriginId });

    shipment.OriginId = origin ?
        origin._id : null;

    const destination = await ShipmentLocationCollection
        .findOne({ OldId: shipment.DestinationId });
    shipment.DestinationId = destination ?
        destination._id : null;

    await shipment.save();

    if (originTerminal) {
        originTerminal.ShipmentId = shipment._id;
        await originTerminal.save();
    }

    if (destinationTerminal) {
        destinationTerminal.ShipmentId = shipment._id;
        await destinationTerminal.save();
    }

    if (origin) {
        origin.ShipmentId = shipment._id;
        await origin.save();
    }

    if (destination) {
        destination.ShipmentId = shipment._id;
        await destination.save();
    }
};

export const up = async () => {

    await Shipment.rollPagination(async (shipments) => {
        for (const shipment of shipments) {
            await updateRelations(shipment);
        };
        const savedCollection = await ShipmentCollection.insertMany(shipments);
        for (const shipment of savedCollection) {
            await associateTerminals(shipment);
        }
    }, {
        attributes: [
            "*",
            ["Id", "OldId"],
        ],
    });

};

export const down = async () => {
    await ShipmentCollection.deleteMany({});
};

