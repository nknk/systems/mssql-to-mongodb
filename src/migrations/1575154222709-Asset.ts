import { TenantCollection, Asset, AssetCollection } from "../models";

export const up = async () => {

    await Asset
        .rollPagination(async (assets) => {
            for (const asset of assets) {
                const tenant = await TenantCollection
                    .findOne({ OldId: asset.TenantId });
                asset.TenantId = tenant._id;
            };
            await AssetCollection
                .insertMany(assets);
        }, {
            attributes: ["*", ["Id", "OldId"]]
        });

};

export const down = async () => {
    await AssetCollection.deleteMany({});
};

