import {
    VendorTerminal,
    TenantCollection,
    VendorTerminalCollection,
    VendorCollection,
    CountryCollection
} from "../models";

const associateRelations = async (result: VendorTerminal) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const vendor = await VendorCollection
        .findOne({ OldId: result.VendorId });
    const country = await CountryCollection
        .findOne({ OldId: result.CountryId });

    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.VendorId = vendor && vendor._id || null;
    result.CountryId = country && country._id || null;
};

export const up = async () => {
    await VendorTerminal.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await VendorTerminalCollection.insertMany(results);
    });
};

export const down = async () => {
    await VendorTerminalCollection.deleteMany({});
};