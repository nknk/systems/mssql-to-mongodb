import {
    ShipmentLocation,
    TenantCollection,
    ShipmentLocationCollection,
    CountryCollection
} from "../models";

const associateRelations = async (result: ShipmentLocation) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const country = await CountryCollection
        .findOne({ OldId: result.CountryId });
    

    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.ShipmentId = null;
    result.CountryId = country && country._id || null;
};

export const up = async () => {
    await ShipmentLocation.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await ShipmentLocationCollection.insertMany(results);
    });
};

export const down = async () => {
    await ShipmentLocationCollection.deleteMany({});
};