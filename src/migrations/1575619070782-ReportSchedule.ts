import {
    ReportConfigurationCollection,
    TenantCollection,
    CustomerGroupCollection,
    VendorGroupCollection,
    UserCollection,
    ReportSchedule,
    ReportScheduleCollection
} from "../models";

const associateRelations = async (result: ReportSchedule) => {
    const tenant = await TenantCollection
    .findOne({ OldId: result.TenantId });
    const reportConfiguration = await ReportConfigurationCollection
    .findOne({ OldId: result.ReportConfigurationId });
    const customerGroup = await CustomerGroupCollection
    .findOne({ OldId: result.CustomerGroupId });
    const vendorGroup = await VendorGroupCollection
    .findOne({ OldId: result.VendorGroupId });
    const user = await UserCollection
    .findOne({ OldId: result.UserId });
    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.ReportConfigurationId = reportConfiguration? reportConfiguration._id: null;
    result.CustomerGroupId = customerGroup? customerGroup._id: null;
    result.VendorGroupId = vendorGroup? vendorGroup._id: null;
    result.UserId = user? user._id: null;

};

export const up = async () => {
    await ReportSchedule.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await ReportScheduleCollection.insertMany(results);
    });
};

export const down = async () => {
    await ReportScheduleCollection.deleteMany({});
};