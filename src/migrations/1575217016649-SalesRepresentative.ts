import {
    SalesRepresentative,
    SalesRepresentativeCollection,
    TenantCollection,
    CountryCollection,
} from "../models";

export const up = async () => {
    const results = await SalesRepresentative.findAll(
        {
            raw: true,
            attributes: ["*", ["Id", "OldId"]]
        }
    );

    for (const result of results) {
        const tenant = await TenantCollection.findOne({ OldId: result.TenantId });
        const country = await CountryCollection.findOne({ OldId: result.CountryId });
        result.TenantId = tenant._id;
        result.CountryId = country._id;
    };

    await SalesRepresentativeCollection.insertMany(results);
};

export const down = async () => {
    await SalesRepresentativeCollection.deleteMany({});
};

