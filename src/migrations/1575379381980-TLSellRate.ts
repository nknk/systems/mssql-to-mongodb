import {
    TLSellRate,
    TenantCollection,
    TLSellRateCollection,
    EquipmentTypeCollection,
    CountryCollection,
    CustomerRatingCollection,
    MileageSourceCollection
} from "../models";

const associateRelations = async (result: TLSellRate) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const equipmentType = await EquipmentTypeCollection
        .findOne({ OldId: result.EquipmentTypeId });
    const originCountry = await CountryCollection
        .findOne({ OldId: result.OriginCountryId });
    const destinationCountry = await CountryCollection
        .findOne({ OldId: result.DestinationCountryId });
    const customerRating = await CustomerRatingCollection
        .findOne({ OldId: result.CustomerRatingId });
    const mileageSource = await MileageSourceCollection
        .findOne({ OldId: result.MileageSourceId });

    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.EquipmentTypeId = equipmentType && equipmentType._id || null;
    result.OriginCountryId = originCountry && originCountry._id || null;
    result.DestinationCountryId = destinationCountry && destinationCountry._id || null;
    result.CustomerRatingId = customerRating && customerRating._id || null;
    result.MileageSourceId = mileageSource && mileageSource._id || null;
};

export const up = async () => {
    await TLSellRate.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await TLSellRateCollection.insertMany(results);
    });
};

export const down = async () => {
    await TLSellRateCollection.deleteMany({});
};