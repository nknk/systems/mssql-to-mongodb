import {
    ReportTemplateCollection,
    ReportConfiguration,
    ReportConfigurationCollection,
    TenantCollection,
    CustomerGroupCollection,
    VendorGroupCollection,
    UserCollection
} from "../models";

const associateRelations = async (result: ReportConfiguration) => {
    const tenant = await TenantCollection
    .findOne({ OldId: result.TenantId });
    const reportTemplate = await ReportTemplateCollection
    .findOne({ OldId: result.ReportTemplateId });
    const customerGroup = await CustomerGroupCollection
    .findOne({ OldId: result.CustomerGroupId });
    const vendorGroup = await VendorGroupCollection
    .findOne({ OldId: result.VendorGroupId });
    const user = await UserCollection
    .findOne({ OldId: result.UserId });
    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.ReportTemplateId = reportTemplate? reportTemplate._id: null;
    result.CustomerGroupId = customerGroup? customerGroup._id: null;
    result.VendorGroupId = vendorGroup? vendorGroup._id: null;
    result.UserId = user? user._id: null;

};

export const up = async () => {
    await ReportConfiguration.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await ReportConfigurationCollection.insertMany(results);
    });
};

export const down = async () => {
    await ReportConfigurationCollection.deleteMany({});
};