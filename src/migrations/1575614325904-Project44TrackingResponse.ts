import { Project44TrackingResponse, Project44TrackingResponseCollection, ShipmentCollection, UserCollection } from "../models";

export const up = async () => {
    await Project44TrackingResponse.rollPagination(async (results) => {

        for (const result of results) {
            const shipment = await ShipmentCollection.findOne({ OldId: result.ShipmentId });
            const user = await UserCollection.findOne({ OldId: result.UserId });
            result.OldId = result.Id;
            result.ShipmentId = shipment ? shipment._id : null;
            result.UserId = user ? user._id : null;
        };

        await Project44TrackingResponseCollection.insertMany(results);
    });
};

export const down = async () => {
    await Project44TrackingResponseCollection.deleteMany({});
};

