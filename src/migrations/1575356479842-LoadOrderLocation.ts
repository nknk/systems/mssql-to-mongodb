import {
    TenantCollection,
    LoadOrderLocation,
    CountryCollection,
    LoadOrderLocationCollection,
} from "../models";

export const up = async () => {
    await LoadOrderLocation
        .rollPagination(async (locations) => {
            for (const location of locations) {
                const tenant = await TenantCollection
                    .findOne({ OldId: location.TenantId });
                const country = await CountryCollection
                    .findOne({ OldId: location.CountryId });

                location.LoadOrderId = null;
                location.CountryId = country && country._id || null;
                location.TenantId = tenant._id;
                location.OldId = location.Id;
            };
            await LoadOrderLocationCollection
                .insertMany(locations);
        });
};

export const down = async () => {
    await LoadOrderLocationCollection.deleteMany({});
};

