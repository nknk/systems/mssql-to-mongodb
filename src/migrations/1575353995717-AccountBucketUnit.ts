import {
    TenantCollection,
    AccountBucketUnit,
    AccountBucketUnitCollection,
    AccountBucketCollection,
} from "../models";

export const up = async () => {
    const results = await AccountBucketUnit.findAll({ raw: true });

    for (const result of results) {
        const tenant = await TenantCollection
            .findOne({ OldId: result.TenantId });
        const accountBucket = await AccountBucketCollection
            .findOne({ OldId: result.AccountBucketId });
        result.TenantId = tenant._id;
        result.OldId = result.Id;
        result.AccountBucketId = accountBucket && accountBucket._id || null;
    };

    await AccountBucketUnitCollection.insertMany(results);
};

export const down = async () => {
    await AccountBucketUnitCollection.deleteMany({});
};

