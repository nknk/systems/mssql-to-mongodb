import {
    CmsNews,
    CmsNewsCollection,
} from "../models";

export const up = async () => {
    await CmsNews
        .rollPagination(async (results) => {
            await CmsNewsCollection
                .insertMany(results);
        }, {
            attributes: ["*", ["Id", "OldId"]]
        });
};

export const down = async () => {
    await CmsNewsCollection
        .deleteMany({});
};

