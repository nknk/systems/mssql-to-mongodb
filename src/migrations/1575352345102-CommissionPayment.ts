import {
    TenantCollection,
    CommissionPayment,
    CommissionPaymentCollection,
    SalesRepresentativeCollection,
} from "../models";

export const up = async () => {
    await CommissionPayment
        .rollPagination(async (payments) => {
            for (const payment of payments) {
                const tenant = await TenantCollection.findOne({ OldId: payment.TenantId });
                const salesRepresentative = await SalesRepresentativeCollection
                    .findOne({ OldId: payment.SalesRepresentativeId });
                payment.TenantId = tenant._id;
                payment.SalesRepresentativeId = salesRepresentative && salesRepresentative._id || null;
            };
            await CommissionPaymentCollection.insertMany(payments);

        }, { attributes: ["*", ["Id", "OldId"]] });

};

export const down = async () => {
    await CommissionPaymentCollection.deleteMany({});
};

