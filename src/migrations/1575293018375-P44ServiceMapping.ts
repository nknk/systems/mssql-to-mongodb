import {
    TenantCollection,
    P44ServiceMapping,
    P44ServiceMappingCollection,
    ServiceCollection,
} from "../models";

export const up = async () => {
    const results = await P44ServiceMapping.findAll({ raw: true });

    for (const result of results) {
        const tenant = await TenantCollection.findOne({ OldId: result.TenantId });
        const service = await ServiceCollection.findOne({ OldId: result.ServiceId });
        result.TenantId = tenant._id;
        result.OldId = result.Id;
        result.ServiceId = service._id;
    };

    await P44ServiceMappingCollection.insertMany(results);
};

export const down = async () => {
    await P44ServiceMappingCollection.deleteMany({});
};

