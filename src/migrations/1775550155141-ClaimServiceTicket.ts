import {
    ClaimServiceTicket,
    ServiceTicketCollection,
    ClaimCollection,
    TenantCollection,
    ClaimServiceTicketCollection,
} from "../models";

export const up = async () => {
    await ClaimServiceTicket
        .rollByRow("ServiceTicketId", async (results) => {
            for (const result of results) {
                const serviceTicket = await ServiceTicketCollection
                    .findOne({ OldId: result.ServiceTicketId });
                const claim = await ClaimCollection
                    .findOne({ OldId: result.ClaimId });
                const tenant = await TenantCollection
                    .findOne({ OldId: result.TenantId });
                result.TenantId = tenant ? tenant._id : null;
                result.ServiceTicketId = serviceTicket ? serviceTicket._id : null;
                result.ClaimId = claim ? claim._id : null;
            }
            await ClaimServiceTicketCollection
                .insertMany(results);
        });
};

export const down = async () => {
    await ClaimServiceTicketCollection
        .deleteMany({});
};

