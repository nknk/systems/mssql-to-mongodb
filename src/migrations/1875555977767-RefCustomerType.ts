import { RefCustomerType, RefCustomerTypeCollection } from "../models";

export const up = async () => {
    await RefCustomerType
        .rollPagination(async (results) => {
            await RefCustomerTypeCollection.insertMany(results);
        });
};

export const down = async () => {
    await RefCustomerTypeCollection.deleteMany({});
};