import { RefXmlConnectStatus, RefXmlConnectStatusCollection } from "../models";

export const up = async () => {
    await RefXmlConnectStatus
        .rollPagination(async (results) => {
            await RefXmlConnectStatusCollection.insertMany(results);
        });
};

export const down = async () => {
    await RefXmlConnectStatusCollection.deleteMany({});
};