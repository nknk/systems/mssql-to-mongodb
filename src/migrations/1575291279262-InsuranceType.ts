import { TenantCollection, InsuranceType, InsuranceTypeCollection } from "../models";

export const up = async () => {
    const results = await InsuranceType.findAll({ raw: true });
 
    for (const result of results) {
        const tenant = await TenantCollection.findOne({ OldId: result.TenantId });
        result.TenantId = tenant._id;
        result.OldId = result.Id;
    };

    await InsuranceTypeCollection.insertMany(results);
};

export const down = async () => {
    await InsuranceTypeCollection.deleteMany({});
};

