import { TenantCollection, PackageType, PackageTypeCollection } from "../models";

export const up = async () => {
    const results = await PackageType.findAll({ raw: true });

    for (const result of results) {
        const tenant = await TenantCollection
            .findOne({ OldId: result.TenantId });
        result.TenantId = tenant._id;
        result.OldId = result.Id;
    }

    await PackageTypeCollection.insertMany(results);

    const tenants = await TenantCollection.find({});
    for (const tenant of tenants) {
        const packageType = await PackageTypeCollection
            .findOne({ OldId: tenant.DefaultPackagingTypeId });
        tenant.DefaultPackagingTypeId = packageType ? packageType._id : null;
        await tenant.save();
    }
};

export const down = async () => {
    await PackageTypeCollection.deleteMany({});
};

