import {
    ServiceTicketNote,
    TenantCollection,
    ServiceTicketNoteCollection,
    ServiceTicketCollection,
    UserCollection
} from "../models";

const associateRelations = async (result: ServiceTicketNote) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const serviceTicket = await ServiceTicketCollection
        .findOne({ OldId: result.ServiceTicketId });
    const user = await UserCollection
        .findOne({ OldId: result.UserId });

    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.ServiceTicketId = serviceTicket && serviceTicket._id || null;
    result.UserId = user && user._id || null;
};


export const up = async () => {
    await ServiceTicketNote.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await ServiceTicketNoteCollection.insertMany(results);
    });
};

export const down = async () => {
    await ServiceTicketNoteCollection.deleteMany({});
};