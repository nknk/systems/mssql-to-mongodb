import {
    TenantCollection,
    GroupCollection,
    GroupUser,
    GroupUserCollection,
    UserCollection,
} from "../models";


export const up = async () => {
    await GroupUser
        .rollByRow("UserId", async (groupUsers) => {
            for (const groupUser of groupUsers) {

                const tenant = await TenantCollection
                    .findOne({ OldId: groupUser.TenantId });
                const group = await GroupCollection
                    .findOne({ OldId: groupUser.GroupId });
                const user = await UserCollection.findOne({
                    OldId: groupUser.UserId
                });

                groupUser.GroupId = group && group._id || null;
                groupUser.TenantId = tenant._id;
                groupUser.UserId = user ? user._id : null;
                groupUser.OldId = groupUser.Id;
            }
            await GroupUserCollection.insertMany(groupUsers);
        });


};

export const down = async () => {
    await GroupUserCollection.deleteMany({});
};

