import { ProcQueObj, ProcQueObjCollection } from "../models";

export const up = async () => {
    await ProcQueObj
        .rollPagination(async (results) => {
            await ProcQueObjCollection
                .insertMany(results);
        }, {
            attributes: ["*", ["Id", "OldId"]]
        });
};

export const down = async () => {
    await ProcQueObjCollection.deleteMany({});
};