import {
    CmsContentCollection,
    CmsContent,
} from "../models";

export const up = async () => {
    await CmsContent
        .rollPagination(async (results) => {
            await CmsContentCollection
                .insertMany(results);
        }, {
            attributes: ["*", ["Id", "OldId"]]
        });
};

export const down = async () => {
    await CmsContentCollection
        .deleteMany({});
};

