import { Group, GroupCollection, TenantCollection } from "../models";

export const up = async () => {
    const results = await Group.findAll(
        {
            raw: true,
            attributes: ["*", ["Id", "OldId"]]
        }
    );

    for (const result of results) {
        const tenant = await TenantCollection
            .findOne({ OldId: result.TenantId });
        result.TenantId = tenant._id;
    };

    await GroupCollection.insertMany(results);
};

export const down = async () => {
    await GroupCollection.deleteMany({});
};

