import {
    QlikUserAttributeCollection,
    QlikUserAttribute
} from "../models";

export const up = async () => {
    await QlikUserAttribute
        .rollByRow("userid", async (results) => {
            await QlikUserAttributeCollection
                .insertMany(results);
        });
};

export const down = async () => {
    await QlikUserAttributeCollection
        .deleteMany({});
};
