import { RefRateType, RefRateTypeCollection } from "../models";

export const up = async () => {
    await RefRateType
        .rollPagination(async (results) => {
            await RefRateTypeCollection
                .insertMany(results);
        });
};

export const down = async () => {
    await RefRateTypeCollection.deleteMany({});
};