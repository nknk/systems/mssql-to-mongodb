import { Tenant, TenantCollection } from "../models";
export const up = async () => {
    const results = await Tenant.findAll({ raw: true });
    results.map((result) => { result.OldId = result.Id; });
    await TenantCollection.insertMany(results);
};

export const down = async () => {
    await TenantCollection.deleteMany({});
};

