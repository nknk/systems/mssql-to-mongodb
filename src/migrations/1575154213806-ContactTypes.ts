import { ContactType, TenantCollection, ContactTypeCollection } from "../models";

export const up = async () => {
    const results = await ContactType.findAll({ raw: true });
    for (const result of results) {
        const tenant = await TenantCollection.findOne({ OldId: result.TenantId });
        result.TenantId = tenant._id;
        result.OldId = result.Id;
    };

    await ContactTypeCollection.insertMany(results);

    const tenants = await TenantCollection.find({});
    for (const tenant of tenants) {
        const contactType = await ContactTypeCollection
            .findOne({ OldId: tenant.AvailableLoadsContactTypeId });
        const schedulingContactType = await ContactTypeCollection
            .findOne({ OldId: tenant.DefaultSchedulingContactTypeId });

        tenant.AvailableLoadsContactTypeId = contactType ? contactType._id : null;
        tenant.DefaultSchedulingContactTypeId = schedulingContactType ? schedulingContactType._id : null;
        await tenant.save();
    }
};

export const down = async () => {
    await ContactTypeCollection.deleteMany({});
};

