import {
    TenantCollection,
    CustomerCollection,
    LibraryItem,
    LibraryItemCollection,
    PackageTypeCollection,
} from "../models";


export const up = async () => {
    await LibraryItem
        .rollPagination(async (items) => {
            for (const item of items) {
                const tenant = await TenantCollection
                    .findOne({ OldId: item.TenantId });
                const customer = await CustomerCollection
                    .findOne({ OldId: item.CustomerId });
                const packageType = await PackageTypeCollection
                    .findOne({ OldId: item.PackageTypeId });

                item.CustomerId = customer && customer._id || null;
                item.TenantId = tenant._id;
                item.OldId = item.Id;
                item.PackageTypeId = packageType && packageType._id || null;
            }
            await LibraryItemCollection
                .insertMany(items);
        });
};

export const down = async () => {
    await LibraryItemCollection.deleteMany({});
};

