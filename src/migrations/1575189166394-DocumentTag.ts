import { DocumentTag, TenantCollection, DocumentTagCollection } from "../models";

export const up = async () => {
    const results = await DocumentTag
        .findAll({ raw: true });

    for (const result of results) {
        const tenant = await TenantCollection
            .findOne({ OldId: result.TenantId });
        result.TenantId = tenant._id;
        result.OldId = result.Id;
    };

    await DocumentTagCollection.insertMany(results);

    const tenants = await TenantCollection.find({});
    for (const tenant of tenants) {

        const bolDocumentTag = await DocumentTagCollection
            .findOne({ OldId: tenant.BolDocumentTagId });

        const podDocumentTag = await DocumentTagCollection
            .findOne({ OldId: tenant.PodDocumentTagId });

        const wniDocumentTag = await DocumentTagCollection
            .findOne({ OldId: tenant.WniDocumentTagId });

        const otherDocumentTag = await DocumentTagCollection
            .findOne({ OldId: tenant.OtherDocumentTagId });

        const vendorBillDocumentTag = await DocumentTagCollection
            .findOne({ OldId: tenant.VendorBillDocumentTagId });

        tenant.BolDocumentTagId = bolDocumentTag ? bolDocumentTag._id : null;
        tenant.PodDocumentTagId = podDocumentTag ? podDocumentTag._id : null;
        tenant.WniDocumentTagId = wniDocumentTag ? wniDocumentTag._id : null;
        tenant.OtherDocumentTagId = otherDocumentTag ? otherDocumentTag._id : null;
        tenant.VendorBillDocumentTagId = vendorBillDocumentTag ? vendorBillDocumentTag._id : null;
        await tenant.save();
    }
};

export const down = async () => {
    await DocumentTagCollection.deleteMany({});
};

