import { UserCollection, Project44RatingResponse, Project44RatingResponseCollection } from "../models";

export const up = async () => {
    await Project44RatingResponse.rollPagination(async (results) => {

        for (const result of results) {
            const user = await UserCollection.findOne({ OldId: result.UserId });
            result.OldId = result.Id;
            result.UserId = user ? user._id : null;
        };

        await Project44RatingResponseCollection.insertMany(results);
    });
};

export const down = async () => {
    await Project44RatingResponseCollection.deleteMany({});
};

