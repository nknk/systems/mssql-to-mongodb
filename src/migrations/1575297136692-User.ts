import {
    TenantCollection,
    CountryCollection,
    User,
    CustomerCollection,
    UserDepartmentCollection,
    UserCollection,
} from "../models";
import { UserPermission } from "../models/user_permission";

const updateRelations = async (user: User) => {
    const tenant = await TenantCollection
        .findOne({ OldId: user.TenantId });
    const country = await CountryCollection
        .findOne({ OldId: user.CountryId });
    const customer = await CustomerCollection
        .findOne({ OldId: user.CustomerId });
    const userDepartment = await UserDepartmentCollection
        .findOne({ OldId: user.UserDepartmentId });

    user.TenantId = tenant._id;
    user.CountryId = country && country._id || null;
    user.CustomerId = customer && customer._id || null;
    user.UserDepartmentId = userDepartment && userDepartment._id || null;
    if (user.VendorPortalVendorId.toString() === "0") {
        user.VendorPortalVendorId = null;
    }
};

export const up = async () => {
    await User.rollPagination(async (users) => {
        const jsonResults: Array<any> = [];
        for (const user of users) {
            await updateRelations(user);
            const jsonUser: { [key: string]: any } = user.toJSON();
            jsonUser.OldId = jsonUser.Id;
            jsonResults.push(jsonUser);
        };

        await UserCollection.insertMany(jsonResults);
    }, {
        raw: false,
        include: [
            {
                model: UserPermission,
                attributes: [
                    "Code",
                    "Description",
                    "Grant",
                    "Deny",
                    "Modify",
                    "Delete",
                    "ModifyApplicable",
                    "DeleteApplicable",
                ]
            }
        ]
    });

    const tenants = await TenantCollection.find({});
    for (const tenant of tenants) {
        const user = await UserCollection
            .findOne({ OldId: tenant.DefaultSystemUserId });
        tenant.DefaultSystemUserId = user ? user._id : null;
        await tenant.save();
    }

};

export const down = async () => {
    await UserCollection.deleteMany({});
};

