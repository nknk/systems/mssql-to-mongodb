import {
    ShipmentAccountBucket,
    TenantCollection,
    ShipmentAccountBucketCollection,
    ShipmentCollection,
    AccountBucketCollection
} from "../models";

const associateRelations = async (result: ShipmentAccountBucket) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const shipment = await ShipmentCollection
        .findOne({ OldId: result.ShipmentId });
    const accountBucket = await AccountBucketCollection
        .findOne({ OldId: result.AccountBucketId });

    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.ShipmentId = shipment && shipment._id || null;
    result.AccountBucketId = accountBucket && accountBucket._id || null;
};

export const up = async () => {
    await ShipmentAccountBucket.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await ShipmentAccountBucketCollection.insertMany(results);
    });
};

export const down = async () => {
    await ShipmentAccountBucketCollection.deleteMany({});
};