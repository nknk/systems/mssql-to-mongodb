import {
    ChangeLogImage,
    ChangeLogCollection,
    ChangeLogImageCollection
} from "../models";

export const up = async () => {
    await ChangeLogImage
        .rollPagination(async (results) => {
            for (const result of results) {
                const changeLog = await ChangeLogCollection
                    .findOne({ OldId: result.ChangelogId });
                result.ChangelogId = changeLog ? changeLog._id : null;
                result.OldId = result.Id;
            }
            await ChangeLogImageCollection.insertMany(results);
        });
};

export const down = async () => {
    await ChangeLogImageCollection.deleteMany({});
};

