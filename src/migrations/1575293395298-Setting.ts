import { TenantCollection, Setting, SettingCollection } from "../models";

export const up = async () => {
    const results = await Setting.findAll({ raw: true });
 
    for (const result of results) {
        const tenant = await TenantCollection.findOne({ OldId: result.TenantId });
        result.TenantId = tenant._id;
        result.OldId = result.Id;
    };

    await SettingCollection.insertMany(results);
};

export const down = async () => {
    await SettingCollection.deleteMany({});
};

