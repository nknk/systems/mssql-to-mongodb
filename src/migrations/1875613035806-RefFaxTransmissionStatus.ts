import {
    RefFaxTransmissionStatus,
    RefFaxTransmissionStatusCollection
} from "../models";

export const up = async () => {
    await RefFaxTransmissionStatus
        .rollPagination(async (results) => {
            await RefFaxTransmissionStatusCollection
                .insertMany(results);
        });
};

export const down = async () => {
    await RefFaxTransmissionStatusCollection
        .deleteMany({});
};