import {
    LoadOrderCollection,
    LoadOrderHistoricalData,
    LoadOrderHistoricalDataCollection,
} from "../models";

export const up = async () => {
    await LoadOrderHistoricalData
        .rollPagination(async (data) => {
            for (const datum of data) {
                const loadOrder = await LoadOrderCollection
                    .findOne({ OldId: datum.LoadOrderId });
                datum.LoadOrderId = loadOrder && loadOrder._id || null;
                datum.OldId = datum.Id;
            };
            await LoadOrderHistoricalDataCollection
                .insertMany(data);
        });
};

export const down = async () => {
    await LoadOrderHistoricalDataCollection.deleteMany({});
};

