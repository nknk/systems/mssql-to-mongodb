import {
    AccountBucket,
    AccountBucketCollection,
    ObjectAssociationCollection,
} from "../models";
import sequelize from "../config/sequelize";
import { QueryTypes } from "sequelize";


export const up = async () => {

    const associations: any[] = [];

    await AccountBucket
        .rollPagination(async (records) => {
            for (const bucket of records) {
                const bucketDoc = await AccountBucketCollection
                    .findOne({ OldId: bucket.Id });

                if (!bucketDoc) {
                    continue;
                }

                const result: any = await sequelize
                    .query(
                        `SELECT dbo.AccountBucketIdUsageCount(:Id) 
                        AS UsageCount;`,
                        {
                            replacements: { Id: bucket.Id },
                            type: QueryTypes.SELECT,
                            plain: true
                        });

                const Occurrences = parseInt(result.UsageCount);

                if (Occurrences > 0) {
                    associations.push({
                        EntityId: bucketDoc._id,
                        EntityName: "LogisticsPlus.Eship.RestClient.Models.AccountBucket",
                        Occurrences,
                        Status: 0,
                    });
                }
            }

        });


    if (associations.length) {
        await ObjectAssociationCollection
            .insertMany(associations);
    }
};

export const down = async () => {
    ObjectAssociationCollection.deleteMany({
        EntityName: "LogisticsPlus.Eship.RestClient.Models.AccountBucket",
    });
};

