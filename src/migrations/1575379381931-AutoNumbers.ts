import { AutoNumber, AutoNumberCollection, TenantCollection } from "../models";

export const up = async () => {
    const results = await AutoNumber
        .findAll({ raw: true });
    for (const result of results) {
        const tenant = await TenantCollection
            .findOne({ OldId: result.TenantId });
        result.TenantId = tenant._id;
        result.OldId = result.Id;
    };
    await AutoNumberCollection.insertMany(results);
};

export const down = async () => {
    await AutoNumberCollection.deleteMany({});
};

