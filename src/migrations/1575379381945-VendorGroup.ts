import {
    VendorGroup,
    TenantCollection,
    VendorGroupCollection
} from "../models";

const associateRelations = async (result: VendorGroup) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });  

    result.TenantId = tenant._id;
    result.OldId = result.Id;
};

export const up = async () => {
    await VendorGroup.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await VendorGroupCollection.insertMany(results);
    });
};

export const down = async () => {
    await VendorGroupCollection.deleteMany({});
};