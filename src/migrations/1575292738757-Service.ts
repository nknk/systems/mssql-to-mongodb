import {
    TenantCollection,
    Service,
    ServiceCollection,
    ChargeCodeCollection,
} from "../models";

export const up = async () => {
    const results = await Service.findAll({ raw: true });

    for (const result of results) {
        const tenant = await TenantCollection.findOne({ OldId: result.TenantId });
        const chargeCode = await ChargeCodeCollection.findOne({ OldId: result.ChargeCodeId });
        result.TenantId = tenant._id;
        result.OldId = result.Id;
        result.ChargeCodeId = chargeCode._id;
    };
    await ServiceCollection.insertMany(results);

    const tenants = await TenantCollection.find({});
    for (const tenant of tenants) {
        const hazardousMaterialService = await ServiceCollection
            .findOne({ OldId: tenant.HazardousMaterialServiceId });

        const borderCrossingService = await ServiceCollection
            .findOne({ OldId: tenant.BorderCrossingServiceId });

        const gDeliveryService = await ServiceCollection
            .findOne({ OldId: tenant.GuaranteedDeliveryServiceId });

        tenant.HazardousMaterialServiceId = hazardousMaterialService ? hazardousMaterialService._id : null;
        tenant.BorderCrossingServiceId = borderCrossingService ? borderCrossingService._id : null;
        tenant.GuaranteedDeliveryServiceId = gDeliveryService ? gDeliveryService._id : null;
        await tenant.save();
    }
};

export const down = async () => {
    await ServiceCollection.deleteMany({});
};

