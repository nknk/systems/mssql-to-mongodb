import { TenantCollection, ReportConfigurationCollection, ReportCustomizationUserCollection, ReportCustomizationUser, UserCollection } from "../models";

export const up = async () => {
    await ReportCustomizationUser
        .rollByRow("UserId", async (results) => {
            for (const result of results) {
                const tenant = await TenantCollection.findOne({ OldId: result.TenantId });
                const user = await UserCollection.findOne({ OldId: result.UserId });
                const reportConfiguration = await ReportConfigurationCollection
                    .findOne({ OldId: result.ReportConfigurationId });
                result.TenantId = tenant ? tenant._id : null;
                result.UserId = user ? user._id : null;
                result.ReportConfigurationId = reportConfiguration ?
                    reportConfiguration._id : null;
            };

            await ReportCustomizationUserCollection.insertMany(results);
        });



};

export const down = async () => {
    await ReportCustomizationUserCollection.deleteMany({});
};

