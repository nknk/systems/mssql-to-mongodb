import {
    TenantCollection,
    ShipmentCollection,
    UserCollection,
    DocDeliveryLogCollection,
    ShipmentDocumentCollection,
    InvoiceCollection,
    DocDeliveryLog,
    DocumentTagCollection,
} from "../models";
import { Document, Model } from "mongoose";

const entityTypes = {
    "0": ShipmentCollection,
    "1": ShipmentDocumentCollection,
    "2": ShipmentCollection,
    "3": InvoiceCollection,
};

export const up = async () => {
    await DocDeliveryLog
        .rollPagination(async (results) => {
            for (const result of results) {
                const tenant = await TenantCollection
                    .findOne({ OldId: result.TenantId });
                const user = await UserCollection
                    .findOne({ OldId: result.UserId });

                const entityCollection: Model<Document, {}> = entityTypes[result.EntityType] || null;

                const entity = entityCollection ? await entityCollection
                    .findOne({ OldId: result.EntityId }) : null;

                result.EntityId = entity ? entity._id : null;
                result.TenantId = tenant ? tenant._id : null;
                result.UserId = user ? user._id : null;

                if (result.DocumentTagId.toString() === "0") {
                    result.DocumentTagId = null;
                } else {
                    const documentTag = await DocumentTagCollection
                        .findOne({ OldId: result.DocumentTagId });
                    result.DocumentTagId = documentTag ? documentTag._id : null;
                }
            }
            await DocDeliveryLogCollection
                .insertMany(results);
        }, {
            attributes: ["*", ["Id", "OldId"]]
        });
};

export const down = async () => {
    await DocDeliveryLogCollection
        .deleteMany({});
};

