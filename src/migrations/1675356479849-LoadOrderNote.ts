import {
    TenantCollection,
    LoadOrderCollection,
    LoadOrderNote,
    UserCollection,
    LoadOrderNoteCollection,
} from "../models";

export const up = async () => {
    await LoadOrderNote
        .rollPagination(async (notes) => {
            for (const note of notes) {
                const tenant = await TenantCollection
                    .findOne({ OldId: note.TenantId });
                const loadOrder = await LoadOrderCollection
                    .findOne({ OldId: note.LoadOrderId });
                const user = await UserCollection
                    .findOne({ OldId: note.UserId });

                note.LoadOrderId = loadOrder && loadOrder._id || null;
                note.UserId = user && user._id || null;
                note.TenantId = tenant._id;
                note.OldId = note.Id;
            };
            await LoadOrderNoteCollection
            .insertMany(notes);
        });
};

export const down = async () => {
    await LoadOrderNoteCollection.deleteMany({});
};

