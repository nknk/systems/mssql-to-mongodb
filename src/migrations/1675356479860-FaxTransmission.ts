import {
    TenantCollection,
    FaxTransmission,
    FaxTransmissionCollection,
} from "../models";

export const up = async () => {
    await FaxTransmission
        .rollPagination(async (transmissions) => {
            for (const transmission of transmissions) {
                const tenant = await TenantCollection
                    .findOne({ OldId: transmission.TenantId });

                transmission.TenantId = tenant._id;
                transmission.OldId = transmission.Id;
            };
            await FaxTransmissionCollection
                .insertMany(transmissions);
        });
};

export const down = async () => {
    await FaxTransmissionCollection.deleteMany({});
};

