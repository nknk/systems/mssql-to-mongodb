import { RefNoteType, RefNoteTypeCollection } from "../models";

export const up = async () => {
    await RefNoteType
    .rollPagination(async (results) => {
        await RefNoteTypeCollection
        .insertMany(results);
    });
    
};

export const down = async () => {
    await RefNoteTypeCollection.deleteMany({});
};