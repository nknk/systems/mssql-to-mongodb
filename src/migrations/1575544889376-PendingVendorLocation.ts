import {
    TenantCollection,
    PendingVendorLocation,
    PendingVendorLocationCollection,
    PendingVendorCollection,
    CountryCollection
} from "../models";

export const up = async () => {
    const results = await PendingVendorLocation
        .findAll({ raw: true });

    for (const result of results) {
        const tenant = await TenantCollection
            .findOne({ OldId: result.TenantId });
        const pendingVendor = await PendingVendorCollection
            .findOne({ OldId: result.PendingVendorId });
        const country = await CountryCollection
            .findOne({ OldId: result.CountryId });
        result.TenantId = tenant._id;
        result.OldId = result.Id;
        result.PendingVendorId = pendingVendor ? pendingVendor._id : null;
        result.CountryId = country ? country._id : null;
    };

    await PendingVendorLocationCollection
        .insertMany(results);
};

export const down = async () => {
    await PendingVendorLocationCollection.deleteMany({});
};

