import { RefShipmentStatus, RefShipmentStatusCollection } from "../models";

export const up = async () => {
    await RefShipmentStatus
        .rollPagination(async (results) => {
            await RefShipmentStatusCollection
                .insertMany(results);
        });
};

export const down = async () => {
    await RefShipmentStatusCollection.deleteMany({});
};