import {
    TenantCollection,
    CustomerCollection,
    LoadOrder,
    ResellerAdditionCollection,
    SalesRepresentativeCollection,
    ShipmentPriorityCollection,
    UserCollection,
    PrefixCollection,
    MileageSourceCollection,
    LoadOrderCollection,
    LoadOrderLocationCollection,
    ILoadOrderDoc,
    AccountBucketUnitCollection,
} from "../models";
import { JobCollection } from "../models/job";

const associateRelations = async (order: LoadOrder) => {
    const tenant = await TenantCollection
        .findOne({ OldId: order.TenantId });
    const customer = await CustomerCollection
        .findOne({ OldId: order.CustomerId });
    const resellerAddition = await ResellerAdditionCollection
        .findOne({ OldId: order.ResellerAdditionId });
    const salesRepresentative = await SalesRepresentativeCollection
        .findOne({ OldId: order.SalesRepresentativeId });
    const shipmentPriority = await ShipmentPriorityCollection
        .findOne({ OldId: order.ShipmentPriorityId });
    const user = await UserCollection
        .findOne({ OldId: order.UserId });
    const prefixId = await PrefixCollection
        .findOne({ OldId: order.PrefixId });
    const mileageSource = await MileageSourceCollection
        .findOne({ OldId: order.MileageSourceId });
    const job = await JobCollection
        .findOne({ OldId: order.JobId });
    const loadOrderCoordinatorUser = await UserCollection
        .findOne({ OldId: order.LoadOrderCoordinatorUserId });
    const carrierCoordinatorUser = await UserCollection
        .findOne({ OldId: order.CarrierCoordinatorUserId });
    const accountBucketUnit = await AccountBucketUnitCollection
        .findOne({ OldId: order.AccountBucketUnitId });

    order.CustomerId = customer && customer._id || null;
    if(order.OriginId.toString() === "0") {
        order.OriginId = null;
    }
    if(order.DestinationId.toString() === "0") {
        order.DestinationId = null;
    }
    order.CarrierCoordinatorId = carrierCoordinatorUser && carrierCoordinatorUser._id || null;
    order.AccountBucketUnitId = accountBucketUnit && accountBucketUnit._id || null;
    order.JobId = job && job._id || null;
    order.MileageSourceId = mileageSource && mileageSource._id || null;
    order.PrefixId = prefixId && prefixId._id || null;
    order.ShipmentPriorityId = shipmentPriority && shipmentPriority._id || null;
    order.SalesRepresentativeId = salesRepresentative && salesRepresentative._id || null;
    order.ResellerAdditionId = resellerAddition && resellerAddition._id || null;
    order.LoadOrderCoordinatorId = loadOrderCoordinatorUser && loadOrderCoordinatorUser._id || null;
    order.UserId = user && user._id || null;
    order.TenantId = tenant._id;
    order.OldId = order.Id;
};

const associateLocation = async (order: ILoadOrderDoc) => {
    const originLocation = await LoadOrderLocationCollection.findOne({
        OldId: order.OriginId
    });
    const destinationLocation = await LoadOrderLocationCollection.findOne({
        OldId: order.DestinationId
    });

    order.DestinationId = destinationLocation ?
        destinationLocation._id : null;
    order.OriginId = originLocation ?
        originLocation._id : null;
    await order.save();

    if (originLocation) {
        originLocation.LoadOrderId = order._id;
        await originLocation.save();
    }

    if (destinationLocation) {
        destinationLocation.LoadOrderId = order._id;
        await destinationLocation.save();
    }
};

export const up = async () => {
    await LoadOrder
        .rollPagination(async (orders) => {
            for (const order of orders) {
                await associateRelations(order);
            };
            const savedCollections = await LoadOrderCollection
                .insertMany(orders);
            for (const order of savedCollections) {
                await associateLocation(order);
            }
        });
};

export const down = async () => {
    await LoadOrderCollection.deleteMany({});
};

