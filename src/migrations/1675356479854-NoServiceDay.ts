import {
    TenantCollection,
    NoServiceDay,
    NoServiceDayCollection,
} from "../models";

export const up = async () => {
    await NoServiceDay
        .rollPagination(async (serviceDays) => {
            for (const serviceDay of serviceDays) {
                const tenant = await TenantCollection
                    .findOne({ OldId: serviceDay.TenantId });

                serviceDay.TenantId = tenant._id;
                serviceDay.OldId = serviceDay.Id;
            };
            await NoServiceDayCollection
            .insertMany(serviceDays);
        });
};

export const down = async () => {
    await NoServiceDayCollection.deleteMany({});
};

