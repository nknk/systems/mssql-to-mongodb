import {
    ShipmentItem,
    TenantCollection,
    ShipmentItemCollection,
    ShipmentCollection,
    PackageTypeCollection
} from "../models";

const associateRelations = async (result: ShipmentItem) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const shipment = await ShipmentCollection
        .findOne({ OldId: result.ShipmentId });
    const packageType = await PackageTypeCollection
        .findOne({ OldId: result.PackageTypeId });

    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.ShipmentId = shipment && shipment._id || null;
    result.PackageTypeId = packageType && packageType._id || null;
};

export const up = async () => {
    await ShipmentItem.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await ShipmentItemCollection.insertMany(results);
    });
};

export const down = async () => {
    await ShipmentItemCollection.deleteMany({});
};