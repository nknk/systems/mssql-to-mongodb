import {
    DocumentPrefixMap,
    TenantCollection,
    PrefixCollection,
    DocumentTemplateCollection,
    DocumentPrefixMapCollection,
} from "../models";

export const up = async () => {
    const results = await DocumentPrefixMap.findAll({ raw: true });
    for (const result of results) {
        const tenant = await TenantCollection.findOne({ OldId: result.TenantId });
        const prefix = await PrefixCollection.findOne({ OldId: result.PrefixId });
        const docTemplate = await DocumentTemplateCollection
            .findOne({ OldId: result.DocumentTemplateId });
        result.OldId = result.Id;
        result.TenantId = tenant._id;
        result.PrefixId = prefix._id;
        result.DocumentTemplateId = docTemplate._id;
    };

    await DocumentPrefixMapCollection.insertMany(results);
};

export const down = async () => {
    await DocumentPrefixMapCollection.deleteMany({});
};

