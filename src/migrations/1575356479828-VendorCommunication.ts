import {
    TenantCollection,
    VendorCommunication,
    VendorCommunicationCollection,
    VendorCollection,
} from "../models";

export const up = async () => {
    await VendorCommunication
        .rollPagination(async (communications) => {
            for (const communication of communications) {
                const tenant = await TenantCollection
                    .findOne({ OldId: communication.TenantId });

                const vendor = await VendorCollection
                    .findOne({ OldId: communication.VendorId });

                communication.TenantId = tenant._id;
                communication.VendorId = vendor ? vendor._id : null;
                communication.OldId = communication.Id;
            };

            const savedCollection = await VendorCommunicationCollection
                .insertMany(communications);

            for (const communication of savedCollection) {
                const vendor = await VendorCollection.findOne({
                    _id: communication.VendorId
                });
                if (vendor) {
                    vendor.CommunicationId = communication._id;
                    await vendor.save();
                }

            }
        });
};

export const down = async () => {
    await VendorCommunicationCollection.deleteMany({});
};

