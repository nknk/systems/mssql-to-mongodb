import {
    TenantCollection,
    VendorLocation,
    VendorCollection,
    VendorLocationCollection,
    CountryCollection,
} from "../models";

export const up = async () => {
    await VendorLocation
        .rollPagination(async (vendorLocations) => {
            for (const vendorLocation of vendorLocations) {
                const tenant = await TenantCollection
                    .findOne({ OldId: vendorLocation.TenantId });

                const vendor = await VendorCollection
                    .findOne({ OldId: vendorLocation.VendorId });

                const country = await CountryCollection
                    .findOne({ OldId: vendorLocation.CountryId });

                vendorLocation.TenantId = tenant._id;
                vendorLocation.OldId = vendorLocation.Id;
                vendorLocation.VendorId = vendor ? vendor._id : null;
                vendorLocation.CountryId = country ? country._id : null;
            };

            await VendorLocationCollection
                .insertMany(vendorLocations);
        });
};

export const down = async () => {
    await VendorLocationCollection.deleteMany({});
};

