import {
    RefInDisputeReason,
    RefInDisputeReasonCollection
} from "../models";

export const up = async () => {
    await RefInDisputeReason
        .rollPagination(async (results) => {
            await RefInDisputeReasonCollection
                .insertMany(results);
        });
};

export const down = async () => {
    await RefInDisputeReasonCollection
        .deleteMany({});
};