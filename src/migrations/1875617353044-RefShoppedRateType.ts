import { RefShoppedRateType, RefShoppedRateTypeCollection } from "../models";

export const up = async () => {
    await RefShoppedRateType
        .rollPagination(async (results) => {
            await RefShoppedRateTypeCollection
                .insertMany(results);
        });
};

export const down = async () => {
    await RefShoppedRateTypeCollection.deleteMany({});
};