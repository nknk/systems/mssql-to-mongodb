import {
    TenantContact,
    TenantCollection,
    TenantContactCollection,
    ContactTypeCollection,
    TenantLocationCollection
} from "../models";

const associateRelations = async (result: TenantContact) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const contactType = await ContactTypeCollection
        .findOne({ OldId: result.ContactTypeId });
    const tenantLocation = await TenantLocationCollection
        .findOne({ OldId: result.TenantLocationId });

    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.ContactTypeId = contactType && contactType._id || null;
    result.TenantLocationId = tenantLocation && tenantLocation._id || null;
};

export const up = async () => {
    await TenantContact.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await TenantContactCollection.insertMany(results);
    });
};

export const down = async () => {
    await TenantContactCollection.deleteMany({});
};