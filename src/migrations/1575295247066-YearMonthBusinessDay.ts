import { TenantCollection, YearMonthBusinessDay, YearMonthBusinessDayCollection } from "../models";

export const up = async () => {
    const results = await YearMonthBusinessDay.findAll({ raw: true });

    for (const result of results) {
        const tenant = await TenantCollection
            .findOne({ OldId: result.TenantId });
        result.TenantId = tenant._id;
        result.OldId = result.Id;
    };

    await YearMonthBusinessDayCollection.insertMany(results);
};

export const down = async () => {
    await YearMonthBusinessDayCollection.deleteMany({});
};

