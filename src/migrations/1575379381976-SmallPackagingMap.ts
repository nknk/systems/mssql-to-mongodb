import {
    SmallPackagingMap,
    TenantCollection,
    SmallPackagingMapCollection,
    PackageTypeCollection
} from "../models";

const associateRelations = async (result: SmallPackagingMap) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const packageType = await PackageTypeCollection
        .findOne({ OldId: result.PackageTypeId });

    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.PackageTypeId = packageType && packageType._id || null;
};

export const up = async () => {
    await SmallPackagingMap.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await SmallPackagingMapCollection.insertMany(results);
    });
};

export const down = async () => {
    await SmallPackagingMapCollection.deleteMany({});
};