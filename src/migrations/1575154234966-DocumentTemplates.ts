import { DocumentTemplate , DocumentTemplateCollection, TenantCollection } from "../models";

export const up = async () => {
    const results = await DocumentTemplate.findAll({ raw: true });

    for (const result of results) {
        const tenant = await TenantCollection.findOne({ OldId: result.TenantId });
        result.TenantId = tenant._id;
        result.OldId = result.Id;
    };
    await DocumentTemplateCollection.insertMany(results);
};

export const down = async () => {
    await DocumentTemplateCollection.deleteMany({});
};

