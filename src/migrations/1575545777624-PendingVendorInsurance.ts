import {
    TenantCollection,
    PendingVendorCollection,
    PendingVendorInsurance,
    PendingVendorInsuranceCollection,
    InsuranceTypeCollection
} from "../models";

export const up = async () => {
    const results = await PendingVendorInsurance.findAll({ raw: true });

    for (const result of results) {
        const tenant = await TenantCollection
            .findOne({ OldId: result.TenantId });
        const pendingVendor = await PendingVendorCollection
            .findOne({ OldId: result.PendingVendorId });
        const insuranceType = await InsuranceTypeCollection
            .findOne({ OldId: result.InsuranceTypeId });
        result.TenantId = tenant._id;
        result.OldId = result.Id;
        result.InsuranceTypeId = insuranceType ? insuranceType._id : null;
        result.PendingVendorId = pendingVendor ? pendingVendor._id : null;
    };

    await PendingVendorInsuranceCollection.insertMany(results);
};

export const down = async () => {
    await PendingVendorInsuranceCollection.deleteMany({});
};

