import {
    XMLTransmission,
    TenantCollection,
    XMLTransmissionCollection,
    VendorCommunicationCollection,
    CustomerCommunicationCollection,
    UserCollection
} from "../models";

const associateRelations = async (result: XMLTransmission) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const user = await UserCollection
        .findOne({ OldId: result.UserId });

    if (result.CommunicationReferenceType == 0) {
        const vendorCommunication = await VendorCommunicationCollection
            .findOne({ OldId: result.CommunicationReferenceId });
        result.CommunicationReferenceId = vendorCommunication
            && vendorCommunication._id || null;
    }
    else if (result.CommunicationReferenceType == 1) {
        const customerCommunication = await CustomerCommunicationCollection
            .findOne({ OldId: result.CommunicationReferenceId });
        result.CommunicationReferenceId = customerCommunication
            && customerCommunication._id || null;
    }
    else{
        result.CommunicationReferenceId = null;
    }

    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.UserId = user && user._id || null;
};

export const up = async () => {
    await XMLTransmission.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await XMLTransmissionCollection.insertMany(results);
    });
};

export const down = async () => {
    await XMLTransmissionCollection.deleteMany({});
};