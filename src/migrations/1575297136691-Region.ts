import {
    TenantCollection,
    Region,
    RegionCollection,
} from "../models";

export const up = async () => {
    await Region
        .rollPagination(async (results) => {

            for (const result of results) {
                const tenant = await TenantCollection
                    .findOne({ OldId: result.TenantId });
                result.TenantId = tenant._id;
            };

            await RegionCollection.insertMany(results);
        },
            {
                attributes: ["*", ["Id", "OldId"]]
            }
        );
};

export const down = async () => {
    await RegionCollection.deleteMany({});
};

