import {
    TenantCollection,
    LoadOrderCollection,
    LoadOrderCharge,
    ChargeCodeCollection,
    VendorCollection,
    LoadOrderChargeCollection,
} from "../models";

export const up = async () => {
    await LoadOrderCharge
        .rollPagination(async (charges) => {
            for (const charge of charges) {
                const tenant = await TenantCollection
                    .findOne({ OldId: charge.TenantId });
                const loadOrder = await LoadOrderCollection
                    .findOne({ OldId: charge.LoadOrderId });
                const chargeCode = await ChargeCodeCollection
                    .findOne({ OldId: charge.ChargeCodeId });
                const vendor = await VendorCollection
                    .findOne({ OldId: charge.VendorId });

                
                charge.LoadOrderId = loadOrder && loadOrder._id || null;
                charge.ChargeCodeId = chargeCode && chargeCode._id || null;
                charge.VendorId = vendor && vendor._id || null;
                charge.TenantId = tenant._id;
                charge.OldId = charge.Id;
            };
            await LoadOrderChargeCollection
            .insertMany(charges);
        });
};

export const down = async () => {
    await LoadOrderChargeCollection.deleteMany({});
};

