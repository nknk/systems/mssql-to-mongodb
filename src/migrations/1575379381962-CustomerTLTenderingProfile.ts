import {
    TenantCollection,
    CustomerTLTenderingProfile,
    CustomerTLTenderingProfileCollection,
    CustomerCollection,
} from "../models";

export const up = async () => {
    await CustomerTLTenderingProfile
        .rollPagination(async (profiles) => {
            for (const profile of profiles) {
                const tenant = await TenantCollection
                    .findOne({ OldId: profile.TenantId });

                const customer = await CustomerCollection
                    .findOne({ OldId: profile.CustomerId });

                profile.CustomerId = customer && customer._id || null;

                profile.TenantId = tenant._id;
                profile.OldId = profile.Id;
            };

            await CustomerTLTenderingProfileCollection.insertMany(profiles);
        });
};

export const down = async () => {
    await CustomerTLTenderingProfileCollection.deleteMany({});
};

