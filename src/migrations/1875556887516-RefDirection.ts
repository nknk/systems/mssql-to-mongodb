import { RefDirection, RefDirectionCollection } from "../models";

export const up = async () => {
    await RefDirection
        .rollPagination(async (results) => {
            await RefDirectionCollection
                .insertMany(results);
        });
};

export const down = async () => {
    await RefDirectionCollection.deleteMany({});
};