import {
    TenantCollection,
    CustomField,
    CustomerCollection,
    CustomFieldCollection,
} from "../models";

export const up = async () => {
    await CustomField
        .rollPagination(async (fields) => {
            for (const field of fields) {
                const tenant = await TenantCollection
                    .findOne({ OldId: field.TenantId });
                const customer = await CustomerCollection
                    .findOne({ OldId: field.CustomerId });

                field.TenantId = tenant._id;
                field.CustomerId = customer? customer._id: null;
                field.OldId = field.Id;
            };
            await CustomFieldCollection
            .insertMany(fields);
        });
};

export const down = async () => {
    await CustomFieldCollection.deleteMany({});
};

