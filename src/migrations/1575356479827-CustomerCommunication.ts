import {
    TenantCollection,
    CustomerCommunication,
    CustomerCollection,
    CustomerCommunicationCollection,
} from "../models";

export const up = async () => {
    await CustomerCommunication
        .rollPagination(async (communications) => {
            for (const communication of communications) {
                const tenant = await TenantCollection
                    .findOne({ OldId: communication.TenantId });

                const customer = await CustomerCollection
                    .findOne({ OldId: communication.CustomerId });

                communication.TenantId = tenant._id;
                communication.CustomerId = customer ? customer._id : null;
                communication.OldId = communication.Id;
            };

            const savedCollection = await CustomerCommunicationCollection
                .insertMany(communications);

            for (const communication of savedCollection) {
                const customer = await CustomerCollection.findOne({
                    _id: communication.CustomerId
                });
                if (customer) {
                    customer.CommunicationId = communication._id;
                    await customer.save();
                }

            }
        });
};

export const down = async () => {
    await CustomerCommunicationCollection.deleteMany({});
};

