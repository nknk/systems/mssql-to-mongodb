import { RefProject44ServiceCode, RefProject44ServiceCodeCollection } from "../models";

export const up = async () => {
    await RefProject44ServiceCode
        .rollPagination(async (results) => {
            await RefProject44ServiceCodeCollection
                .insertMany(results);
        });


};

export const down = async () => {
    await RefProject44ServiceCodeCollection.deleteMany({});
};