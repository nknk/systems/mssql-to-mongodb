import {
    TenantCollection,
    DocDeliveryDocTag,
    CustomerCommunicationCollection,
    DocumentTagCollection,
    DocDeliveryDocTagCollection,
} from "../models";


export const up = async () => {
    await DocDeliveryDocTag
        .rollByRow("DocumentTagId", async (tags) => {
            for (const tag of tags) {
                const tenant = await TenantCollection
                    .findOne({ OldId: tag.TenantId });

                const customerCommunication = await CustomerCommunicationCollection
                    .findOne({ OldId: tag.CustomerCommunicationId });

                const documentTag = await DocumentTagCollection
                    .findOne({ OldId: tag.DocumentTagId });

                tag.CustomerCommunicationId = customerCommunication && customerCommunication._id || null;
                tag.DocumentTagId = documentTag && documentTag._id || null;
                tag.TenantId = tenant._id;
                tag.OldId = tag.Id;
            }
            await DocDeliveryDocTagCollection
                .insertMany(tags);
        });


};

export const down = async () => {
    await DocDeliveryDocTagCollection.deleteMany({});
};

