import {
    VendorNoServiceDay,
    TenantCollection,
    VendorNoServiceDayCollection,
    NoServiceDayCollection,
    VendorCollection
} from "../models";

const associateRelations = async (result: VendorNoServiceDay) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const noServiceDay = await NoServiceDayCollection
        .findOne({ OldId: result.NoServiceDayId });
    const vendor = await VendorCollection
        .findOne({ OldId: result.VendorId });
    result.TenantId = tenant._id;
    result.NoServiceDayId = noServiceDay && noServiceDay._id || null;
    result.VendorId = vendor && vendor._id || null;
};

export const up = async () => {
    const results = await VendorNoServiceDay
        .findAll({
            raw: true,
            attributes: ["TenantId", "NoServiceDayId", "VendorId"]
        });
    for (const result of results) {
        await associateRelations(result);
    };
    await VendorNoServiceDayCollection.insertMany(results);
};

export const down = async () => {
    await VendorNoServiceDayCollection.deleteMany({});
};