import {
    TenantCollection,
    PendingVendorCollection,
    PendingVendorEquipment,
    PendingVendorEquipmentCollection,
    EquipmentTypeCollection
} from "../models";

export const up = async () => {
    const results = await PendingVendorEquipment
        .findAll({
            raw: true,
            attributes: ["TenantId", "EquipmentTypeId", "PendingVendorId"]
        });

    for (const result of results) {
        const tenant = await TenantCollection
            .findOne({ OldId: result.TenantId });
        const pendingVendor = await PendingVendorCollection
            .findOne({ OldId: result.PendingVendorId });
        const equipmentType = await EquipmentTypeCollection
            .findOne({ OldId: result.EquipmentTypeId });
        result.TenantId = tenant._id;
        result.EquipmentTypeId = equipmentType ? equipmentType._id : null;
        result.PendingVendorId = pendingVendor ? pendingVendor._id : null;
    };

    await PendingVendorEquipmentCollection.insertMany(results);
};

export const down = async () => {
    await PendingVendorEquipmentCollection.deleteMany({});
};

