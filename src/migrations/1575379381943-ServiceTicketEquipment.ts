import {
    ServiceTicketEquipment,
    TenantCollection,
    ServiceTicketEquipmentCollection,
    ServiceTicketCollection,
    EquipmentTypeCollection
} from "../models";

const associateRelations = async (result: ServiceTicketEquipment) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const serviceTicket = await ServiceTicketCollection
        .findOne({ OldId: result.ServiceTicketId });
    const equipmentType = await EquipmentTypeCollection
        .findOne({ OldId: result.EquipmentTypeId });
    result.TenantId = tenant._id;
    result.ServiceTicketId = serviceTicket && serviceTicket._id || null;
    result.EquipmentTypeId = equipmentType && equipmentType._id || null;
};

export const up = async () => {
    const results = await ServiceTicketEquipment
        .findAll({
            raw: true,
            attributes: ["TenantId", "EquipmentTypeId", "ServiceTicketId"]
        });
    for (const result of results) {
        await associateRelations(result);
    };
    await ServiceTicketEquipmentCollection.insertMany(results);

};

export const down = async () => {
    await ServiceTicketEquipmentCollection.deleteMany({});
};