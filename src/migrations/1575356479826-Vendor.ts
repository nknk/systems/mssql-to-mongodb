import {
    TenantCollection,
    Vendor,
    UserCollection,
    VendorCollection,
} from "../models";

export const up = async () => {
    await Vendor
        .rollPagination(async (vendors) => {
            for (const vendor of vendors) {
                const tenant = await TenantCollection
                    .findOne({ OldId: vendor.TenantId });

                const serviceUser = await UserCollection
                    .findOne({ OldId: vendor.VendorServiceRepUserId });

                const AuditUser = await UserCollection
                    .findOne({ OldId: vendor.LastAuditedByUserId });

                vendor.TenantId = tenant._id;
                vendor.CommunicationId = null;
                vendor.LastAuditedByUserId = AuditUser ? AuditUser._id : null;
                vendor.VendorServiceRepId = serviceUser ? serviceUser._id : null;
                vendor.OldId = vendor.Id;
            };

            await VendorCollection
                .insertMany(vendors);
        });


    const users = await UserCollection
        .find({ VendorPortalVendorId: { "$ne": null } });

    for (const user of users) {
        const vendor = await VendorCollection
            .findOne({ OldId: user.VendorPortalVendorId });
        user.VendorPortalVendorId = vendor ? vendor._id : null;
        await user.save();
    }

    const tenants = await TenantCollection.find({});
    for (const tenant of tenants) {
        const vendor = await VendorCollection
            .findOne({ OldId: tenant.InsuranceProviderVendorId });
        tenant.InsuranceProviderVendorId = vendor ? vendor._id : null;
        await tenant.save();
    }


};

export const down = async () => {
    await VendorCollection.deleteMany({});
};

