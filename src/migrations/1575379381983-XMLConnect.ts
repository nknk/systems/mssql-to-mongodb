import {
    XMLConnect,
    TenantCollection,
    XMLConnectCollection
} from "../models";

const associateRelations = async (result: XMLConnect) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });    

    result.TenantId = tenant._id;
    result.OldId = result.Id;
};

export const up = async () => {
    await XMLConnect.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await XMLConnectCollection.insertMany(results);
    });
};

export const down = async () => {
    await XMLConnectCollection.deleteMany({});
};