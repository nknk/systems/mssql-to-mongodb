import { TenantCollection, Announcement, AnnouncementCollection } from "../models";

export const up = async () => {
    await Announcement
        .rollPagination(async (announcements) => {
            for (const announcement of announcements) {
                const tenant = await TenantCollection
                    .findOne({ OldId: announcement.TenantId });
                announcement.TenantId = tenant._id;
            };
            await AnnouncementCollection.insertMany(announcements);
        }, {
            attributes: ["*", ["Id", "OldId"]]
        });
};

export const down = async () => {
    await AnnouncementCollection.deleteMany({});
};

