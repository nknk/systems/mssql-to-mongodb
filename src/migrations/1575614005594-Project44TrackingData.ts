import { Project44TrackingData, Project44TrackingDataCollection } from "../models";

export const up = async () => {
    await Project44TrackingData.rollPagination(async (results) => {

        for (const result of results) {
            result.OldId = result.Id;
        };

        await Project44TrackingDataCollection.insertMany(results);
    });
};

export const down = async () => {
    await Project44TrackingDataCollection.deleteMany({});
};

