import { RefNotificationMethod, RefNotificationMethodCollection } from "../models";

export const up = async () => {
    await RefNotificationMethod
        .rollPagination(async (results) => {
            await RefNotificationMethodCollection
                .insertMany(results);
        });
};

export const down = async () => {
    await RefNotificationMethodCollection
        .deleteMany({});
};