import {
    ShipmentAsset,
    TenantCollection,
    ShipmentAssetCollection,
    ShipmentCollection,
    AssetCollection
} from "../models";

const associateRelations = async (result: ShipmentAsset) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const shipment = await ShipmentCollection
        .findOne({ OldId: result.ShipmentId });
    const driverAsset = await AssetCollection
        .findOne({ OldId: result.DriverAssetId });
    const trailerAsset = await AssetCollection
        .findOne({ OldId: result.TrailerAssetId });
    const tractorAsset = await AssetCollection
        .findOne({ OldId: result.TractorAssetId });

    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.ShipmentId = shipment && shipment._id || null;
    result.DriverAssetId = driverAsset && driverAsset._id || null;
    result.TrailerAssetId = trailerAsset && trailerAsset._id || null;
    result.TractorAssetId = tractorAsset && tractorAsset._id || null;
};

export const up = async () => {
    await ShipmentAsset.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await ShipmentAssetCollection.insertMany(results);
    });
};

export const down = async () => {
    await ShipmentAssetCollection.deleteMany({});
};