import { RefServiceMode, RefServiceModeCollection } from "../models";

export const up = async () => {
    await RefServiceMode
        .rollPagination(async (results) => {
            await RefServiceModeCollection
                .insertMany(results);
        });
};

export const down = async () => {
    await RefServiceModeCollection.deleteMany({});
};