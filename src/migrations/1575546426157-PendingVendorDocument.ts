import {
    TenantCollection,
    PendingVendorCollection,
    PendingVendorDocument,
    PendingVendorDocumentCollection,
    DocumentTagCollection
} from "../models";

export const up = async () => {
    const results = await PendingVendorDocument.findAll({ raw: true });

    for (const result of results) {
        const tenant = await TenantCollection
            .findOne({ OldId: result.TenantId });
        const pendingVendor = await PendingVendorCollection
            .findOne({ OldId: result.PendingVendorId });
        const documentTag = await DocumentTagCollection
            .findOne({ OldId: result.DocumentTagId });
        result.TenantId = tenant._id;
        result.OldId = result.Id;
        result.DocumentTagId = documentTag ? documentTag._id : null;
        result.PendingVendorId = pendingVendor ? pendingVendor._id : null;
    };

    await PendingVendorDocumentCollection.insertMany(results);
};

export const down = async () => {
    await PendingVendorDocumentCollection.deleteMany({});
};

