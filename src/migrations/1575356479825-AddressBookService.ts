import {
    TenantCollection,
    AddressBookCollection,
    AddressBookService,
    AddressBookServiceCollection,
    ServiceCollection,
} from "../models";

export const up = async () => {

    const addressBookServices = await AddressBookService.
        findAll({
            raw: true,
            attributes: ["*"]
        });

    for (const addressBookService of addressBookServices) {
        const tenant = await TenantCollection
            .findOne({ OldId: addressBookService.TenantId });
        const addressBook = await AddressBookCollection
            .findOne({ OldId: addressBookService.AddressBookId });
        const service = await ServiceCollection
            .findOne({ OldId: addressBookService.ServiceId });
        addressBookService.TenantId = tenant._id;
        addressBookService.ServiceId = service && service._id || null;
        addressBookService.AddressBookId = addressBook && addressBook._id || null;
    }

    await AddressBookServiceCollection
        .insertMany(addressBookServices);

};

export const down = async () => {
    await AddressBookServiceCollection.deleteMany({});
};

