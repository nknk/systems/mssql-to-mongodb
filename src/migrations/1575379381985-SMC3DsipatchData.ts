import {
    SMC3DispatchData,    
    SMC3DispatchDataCollection
} from "../models";

export const up = async () => {
    await SMC3DispatchData.rollPagination(async (results) => { 
        for (const result of results) {
            result.OldId = result.Id;
        };       
        await SMC3DispatchDataCollection.insertMany(results);
    });
};

export const down = async () => {
    await SMC3DispatchDataCollection.deleteMany({});
};