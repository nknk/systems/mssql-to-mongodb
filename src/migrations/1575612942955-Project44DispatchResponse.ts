import { UserCollection, ShipmentCollection, Project44DispatchResponseCollection, Project44DispatchResponse } from "../models";

export const up = async () => {
    await Project44DispatchResponse.rollPagination(async (results) => {

        for (const result of results) {
            const shipment = await ShipmentCollection.findOne({ OldId: result.ShipmentId });
            const user = await UserCollection.findOne({ OldId: result.UserId });
            result.OldId = result.Id;
            result.ShipmentId = shipment ? shipment._id : null;
            result.UserId = user ? user._id : null;
        };

        await Project44DispatchResponseCollection.insertMany(results);
    });
};

export const down = async () => {
    await Project44DispatchResponseCollection.deleteMany({});
};

