import {
    VendorEquipment,
    TenantCollection,
    VendorEquipmentCollection,
    EquipmentTypeCollection,
    VendorCollection
} from "../models";

const associateRelations = async (result: VendorEquipment) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const equipmentType = await EquipmentTypeCollection
        .findOne({ OldId: result.EquipmentTypeId });
    const vendor = await VendorCollection
        .findOne({ OldId: result.VendorId });
    result.TenantId = tenant._id;
    result.EquipmentTypeId = equipmentType && equipmentType._id || null;
    result.VendorId = vendor && vendor._id || null;
};

export const up = async () => {
    await VendorEquipment
        .rollByRow("EquipmentTypeId", async (results) => {
            for (const result of results) {
                await associateRelations(result);
            };
            await VendorEquipmentCollection.insertMany(results);
        });
};

export const down = async () => {
    await VendorEquipmentCollection.deleteMany({});
};