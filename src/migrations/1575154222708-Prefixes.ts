import { Prefix, PrefixCollection, TenantCollection } from "../models";

export const up = async () => {
    const results = await Prefix.findAll({ raw: true });
    for (const result of results) {
        const tenant = await TenantCollection.findOne({ OldId: result.TenantId });
        result.TenantId = tenant._id;
        result.OldId = result.Id;
    };

    await PrefixCollection.insertMany(results);
};

export const down = async () => {
    await PrefixCollection.deleteMany({});
};

