import {
    SMC3TrackRequestResponse,    
    SMC3TrackRequestResponseCollection,
    ShipmentCollection
} from "../models";

const associateRelations = async (result: SMC3TrackRequestResponse) => {
    const shipment = await ShipmentCollection
        .findOne({ OldId: result.ShipmentId });
    result.OldId = result.Id;
    result.ShipmentId = shipment && shipment._id || null;
};

export const up = async () => {
    await SMC3TrackRequestResponse.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        }
        await SMC3TrackRequestResponseCollection.insertMany(results);
    });
};

export const down = async () => {
    await SMC3TrackRequestResponseCollection.deleteMany({});
};