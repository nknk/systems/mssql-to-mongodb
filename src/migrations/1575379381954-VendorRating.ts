import {
    VendorRating,
    TenantCollection,
    VendorRatingCollection,
    ChargeCodeCollection,
    FuelTableCollection,
    VendorCollection,
    PackageTypeCollection
} from "../models";

const associateRelations = async (result: VendorRating) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const fuelChargeCode = await ChargeCodeCollection
        .findOne({ OldId: result.FuelChargeCodeId });
    const fuelTable = await FuelTableCollection
        .findOne({ OldId: result.FuelTableId });
    const vendor = await VendorCollection
        .findOne({ OldId: result.VendorId });
    const ltlPackageSpecificRatePackageType = await PackageTypeCollection
        .findOne({ OldId: result.LtlPackageSpecificRatePackageTypeId });



    const ltlPackageSpecificFreightChargeCode = await ChargeCodeCollection
        .findOne({ OldId: result.LTLPackageSpecificFreightChargeCodeId });

    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.FuelChargeCodeId = fuelChargeCode && fuelChargeCode._id || null;
    result.FuelTableId = fuelTable && fuelTable._id || null;
    result.VendorId = vendor && vendor._id || null;
    result.LTLPackageSpecificFreightChargeCodeId = ltlPackageSpecificFreightChargeCode ?
        ltlPackageSpecificFreightChargeCode._id : null;

    delete result.LtlPackageSpecificRatePackageTypeId;
    result.LTLPackageSpecificRatePackageTypeId = ltlPackageSpecificRatePackageType ?
        ltlPackageSpecificRatePackageType._id : null;
    result.EnableLTLPackageSpecificRates = result.EnableLtlPackageSpecificRates;
    delete result.EnableLtlPackageSpecificRates;

};

export const up = async () => {
    await VendorRating.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await VendorRatingCollection.insertMany(results);
    });
};

export const down = async () => {
    await VendorRatingCollection.deleteMany({});
};