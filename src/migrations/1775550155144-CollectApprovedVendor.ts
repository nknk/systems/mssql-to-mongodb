import {
    CollectApprovedVendor,
    CollectApprovedVendorCollection,
    VendorCollection,
    CustomerRatingCollection,
    TenantCollection,
} from "../models";
export const up = async () => {
    await CollectApprovedVendor
        .rollPagination(async (results) => {
            for (const result of results) {
                const tenant = await TenantCollection
                    .findOne({ OldId: result.TenantId });
                const vendor = await VendorCollection
                    .findOne({ OldId: result.VendorId });

                const customerRating = await CustomerRatingCollection
                    .findOne({ OldId: result.CustomerRatingId });

                result.TenantId = tenant ? tenant._id : null;
                result.VendorId = vendor ? vendor._id : null;
                result.CustomerRatingId = customerRating ? customerRating._id : null;
            }
            await CollectApprovedVendorCollection
                .insertMany(results);
        }, {
            attributes: ["*", ["Id", "OldId"]]
        });
};

export const down = async () => {
    await CollectApprovedVendorCollection
        .deleteMany({});
};

