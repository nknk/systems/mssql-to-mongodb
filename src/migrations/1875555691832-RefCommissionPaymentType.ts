import { RefClaimStatus, RefClaimStatusCollection } from "../models";

export const up = async () => {
    await RefClaimStatus
        .rollPagination(async (results) => {
            await RefClaimStatusCollection.insertMany(results);
        });
};

export const down = async () => {
    await RefClaimStatusCollection.deleteMany({});
};