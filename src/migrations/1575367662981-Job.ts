import {
    Job,
    TenantCollection,
    JobCollection,
    CustomerCollection,
    UserCollection
} from "../models";

const associateRelations = async (result: Job) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const customer = await CustomerCollection
        .findOne({ OldId: result.CustomerId });
    const user = await UserCollection
        .findOne({ OldId: result.CreatedByUserId });
    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.CustomerId = customer && customer._id || null;
    result.CreatedByUserId = user && user._id || null;
};

export const up = async () => {
    await Job.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await JobCollection.insertMany(results);
    });
};

export const down = async () => {
    await JobCollection.deleteMany({});
};