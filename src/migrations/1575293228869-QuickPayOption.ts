import { TenantCollection, QuickPayOption, QuickPayOptionCollection } from "../models";

export const up = async () => {
    const results = await QuickPayOption.findAll({ raw: true });
 
    for (const result of results) {
        const tenant = await TenantCollection.findOne({ OldId: result.TenantId });
        result.TenantId = tenant._id;
        result.OldId = result.Id;
    };

    await QuickPayOptionCollection.insertMany(results);
};

export const down = async () => {
    await QuickPayOptionCollection.deleteMany({});
};

