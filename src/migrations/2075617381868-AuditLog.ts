import {
    AuditLogCollection,
    AuditLog,
    AccountBucketCollection,
    AddressBookCollection,
    AnnouncementCollection,
    AssetCollection,
    AssetLogCollection,
    AverageWeeklyFuelCollection,
    BatchRateDataCollection,
    ChangeLogCollection,
    ChargeCodeCollection,
    ClaimCollection,
    ContactTypeCollection,
    CustomerCollection,
    CustomerCommunicationCollection,
    CustomerControlAccountCollection,
    CustomerGroupCollection,
    CustomerPurchaseOrderCollection,
    CustomerRatingCollection,
    CustomerTLTenderingProfileCollection,
    DeveloperAccessRequestCollection,
    DocumentPrefixMapCollection,
    DocumentTagCollection,
    DocumentTemplateCollection,
    EquipmentTypeCollection,
    FailureCodeCollection,
    FaxTransmissionCollection,
    FuelTableCollection,
    GroupCollection,
    InsuranceTypeCollection,
    InvoiceCollection,
    LibraryItemCollection,
    LoadOrderCollection,
    MacroPointOrderCollection,
    MileageSourceCollection,
    MiscReceiptApplicationCollection,
    P44ChargeCodeMappingCollection,
    P44ServiceMappingCollection,
    PackageTypeCollection,
    PendingVendorCollection,
    PermissionDetailCollection,
    PrefixCollection,
    QuickPayOptionCollection,
    RegionCollection,
    ReportConfigurationCollection,
    ReportScheduleCollection,
    ResellerAdditionCollection,
    SalesRepresentativeCollection,
    ServiceCollection,
    ServiceTicketCollection,
    ShipmentCollection,
    ShipmentPriorityCollection,
    SmallPackageServiceMapCollection,
    SmallPackagingMapCollection,
    TenantCollection,
    TierCollection,
    TruckloadBidCollection,
    UserCollection,
    UserDepartmentCollection,
    VendorCollection,
    VendorBillCollection,
    VendorCommunicationCollection,
    VendorDisputeCollection,
    VendorGroupCollection,
    VendorPreferredLaneCollection,
    VendorRatingCollection,
    VendorTerminalCollection,
    XMLConnectCollection,
    YearMonthBusinessDayCollection,
    JobCollection,
    EmailLogCollection,
    CommissionPaymentCollection,
    BackgroundReportRunRecordCollection
} from "../models";

import { Document, Model } from "mongoose";
import { Op } from "sequelize";






const skipEntityTypes = ["Lock", "Vendor Rating Bill To Address"];

const entities = {
    "Account Bucket": AccountBucketCollection,
    "Address Book": AddressBookCollection,
    "Announcement": AnnouncementCollection,
    "Asset": AssetCollection,
    "Asset Log": AssetLogCollection,
    "Average Weekly Fuel": AverageWeeklyFuelCollection,
    "Background Report Run Record": BackgroundReportRunRecordCollection,
    "Batch Rate Data": BatchRateDataCollection,
    "Changelog": ChangeLogCollection,
    "Charge Code": ChargeCodeCollection,
    "Claim": ClaimCollection,
    "Commission Payment": CommissionPaymentCollection,
    "Contact Type": ContactTypeCollection,
    "Customer": CustomerCollection,
    "Customer Communication": CustomerCommunicationCollection,
    "Customer Control Account": CustomerControlAccountCollection,
    "Customer Group": CustomerGroupCollection,
    "Customer Purchase Order": CustomerPurchaseOrderCollection,
    "Customer Rating": CustomerRatingCollection,
    "Customer TLTendering Profile": CustomerTLTenderingProfileCollection,
    "Developer Access Request": DeveloperAccessRequestCollection,
    "Document Prefix Map": DocumentPrefixMapCollection,
    "Document Tag": DocumentTagCollection,
    "Document Template": DocumentTemplateCollection,
    "Email Log": EmailLogCollection,
    "Equipment Type": EquipmentTypeCollection,
    "Failure Code": FailureCodeCollection,
    "Fax Transmission": FaxTransmissionCollection,
    "Fuel Table": FuelTableCollection,
    "Group": GroupCollection,
    "Insurance Type": InsuranceTypeCollection,
    "Invoice": InvoiceCollection,
    "Job": JobCollection,
    "Library Item": LibraryItemCollection,
    "Load Order": LoadOrderCollection,
    "Macro Point Order": MacroPointOrderCollection,
    "Mileage Source": MileageSourceCollection,
    "Misc Receipt Application": MiscReceiptApplicationCollection,
    "P44Charge Code Mapping": P44ChargeCodeMappingCollection,
    "P44Service Mapping": P44ServiceMappingCollection,
    "Package Type": PackageTypeCollection,
    "Pending Vendor": PendingVendorCollection,
    "Permission Detail": PermissionDetailCollection,
    "Prefix": PrefixCollection,
    "Quick Pay Option": QuickPayOptionCollection,
    "Region": RegionCollection,
    "Report Configuration": ReportConfigurationCollection,
    "Report Schedule": ReportScheduleCollection,
    "Reseller Addition": ResellerAdditionCollection,
    "Sales Representative": SalesRepresentativeCollection,
    "Service": ServiceCollection,
    "Service Ticket": ServiceTicketCollection,
    "Shipment": ShipmentCollection,
    "Shipment Priority": ShipmentPriorityCollection,
    "Small Package Service Map": SmallPackageServiceMapCollection,
    "Small Packaging Map": SmallPackagingMapCollection,
    "Tenant": TenantCollection,
    "Tier": TierCollection,
    "Truckload Bid": TruckloadBidCollection,
    "User": UserCollection,
    "User Department": UserDepartmentCollection,
    "Vendor": VendorCollection,
    "Vendor Bill": VendorBillCollection,
    "Vendor Communication": VendorCommunicationCollection,
    "Vendor Dispute": VendorDisputeCollection,
    "Vendor Group": VendorGroupCollection,
    "Vendor Preferred Lane": VendorPreferredLaneCollection,
    "Vendor Rating": VendorRatingCollection,
    "Vendor Terminal": VendorTerminalCollection,
    "Xml Connect": XMLConnectCollection,
    "Yr Mth Bus Day": YearMonthBusinessDayCollection
};

export const up = async () => {
    await AuditLog
        .rollPagination(async (results) => {
            for (const result of results) {
                if (skipEntityTypes.includes(result.EntityCode)) {
                    continue;
                }
                const tenant = await TenantCollection
                    .findOne({ OldId: result.TenantId });
                const user = await UserCollection
                    .findOne({ OldId: result.UserId });

                const entityCollection: Model<Document, {}> = entities[result.EntityCode] || null;

                const entity = entityCollection ? await entityCollection
                    .findOne({ OldId: result.EntityId }) : null;

                result.EntityId = entity ? entity._id : null;
                result.TenantId = tenant ? tenant._id : null;
                result.UserId = user ? user._id : null;
            }
            await AuditLogCollection
                .insertMany(results);
        }, {
            attributes: ["*", ["Id", "OldId"]],
            where: {
                EntityId: {
                    [Op.gt]: "0"
                }
            }
        });
};

export const down = async () => {
    return;
    await AuditLogCollection
        .deleteMany({});
};

