import {
    TenantCollection,
    LoadOrderCollection,
    LoadOrderVendor,
    VendorCollection,
    LoadOrderVendorCollection,
} from "../models";

export const up = async () => {
    await LoadOrderVendor
        .rollPagination(async (loadOrderVendors) => {
            for (const loadOrderVendor of loadOrderVendors) {
                const tenant = await TenantCollection
                    .findOne({ OldId: loadOrderVendor.TenantId });
                const loadOrder = await LoadOrderCollection
                    .findOne({ OldId: loadOrderVendor.LoadOrderId });
                const vendor = await VendorCollection
                    .findOne({ OldId: loadOrderVendor.VendorId });

                loadOrderVendor.LoadOrderId = loadOrder && loadOrder._id || null;
                loadOrderVendor.VendorId = vendor && vendor._id || null;
                loadOrderVendor.TenantId = tenant._id;
                loadOrderVendor.OldId = loadOrderVendor.Id;
            };
            await LoadOrderVendorCollection
            .insertMany(loadOrderVendors);
        });
};

export const down = async () => {
    await LoadOrderVendorCollection.deleteMany({});
};

