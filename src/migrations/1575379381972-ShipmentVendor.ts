import {
    ShipmentVendor,
    TenantCollection,
    ShipmentVendorCollection,
    ShipmentCollection,
    VendorCollection,
    FailureCodeCollection
} from "../models";

const associateRelations = async (result: ShipmentVendor) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const shipment = await ShipmentCollection
        .findOne({ OldId: result.ShipmentId });
    const vendor = await VendorCollection
        .findOne({ OldId: result.VendorId });
    const failureCode = await FailureCodeCollection
        .findOne({ OldId: result.FailureCodeId });

    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.ShipmentId = shipment && shipment._id || null;
    result.VendorId = vendor && vendor._id || null;
    result.FailureCodeId = failureCode && failureCode._id || null;
};

export const up = async () => {
    await ShipmentVendor.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await ShipmentVendorCollection.insertMany(results);
    });
};

export const down = async () => {
    await ShipmentVendorCollection.deleteMany({});
};