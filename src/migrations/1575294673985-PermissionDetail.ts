import { TenantCollection, PermissionDetail, PermissionDetailCollection } from "../models";

export const up = async () => {
    const results = await PermissionDetail.findAll({ raw: true });

    for (const result of results) {
        const tenant = await TenantCollection
            .findOne({ OldId: result.TenantId });
        result.TenantId = tenant._id;
        result.OldId = result.Id;
    };

    await PermissionDetailCollection.insertMany(results);
};

export const down = async () => {
    await PermissionDetailCollection.deleteMany({});
};

