import { Country, CountryCollection, TenantCollection } from "../models";

export const up = async () => {
    const results = await Country.findAll(
        {
            raw: true,
            attributes: ["*", ["Id", "OldId"]]
        }
    );

    await CountryCollection.insertMany(results);

    const tenants = await TenantCollection.find({});
    for (const tenant of tenants) {
        const country = await CountryCollection
            .findOne({ OldId: tenant.DefaultCountryId });
        tenant.DefaultCountryId = country ? country._id : null;
        await tenant.save();
    }
};

export const down = async () => {
    await CountryCollection.deleteMany({});
};
