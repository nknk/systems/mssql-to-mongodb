import {
    SMC3TrackingData,    
    SMC3TrackingDataCollection
} from "../models";

export const up = async () => {
    await SMC3TrackingData.rollPagination(async (results) => { 
        for (const result of results) {
            result.OldId = result.Id;
        };       
        await SMC3TrackingDataCollection.insertMany(results);
    });
};

export const down = async () => {
    await SMC3TrackingDataCollection.deleteMany({});
};