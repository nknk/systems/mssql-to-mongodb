import {
    ShipmentReference,
    TenantCollection,
    ShipmentReferenceCollection,
    ShipmentCollection
} from "../models";

const associateRelations = async (result: ShipmentReference) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const shipment = await ShipmentCollection
        .findOne({ OldId: result.ShipmentId });

    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.ShipmentId = shipment && shipment._id || null;
};

export const up = async () => {
    await ShipmentReference.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await ShipmentReferenceCollection.insertMany(results);
    });
};

export const down = async () => {
    await ShipmentReferenceCollection.deleteMany({});
};