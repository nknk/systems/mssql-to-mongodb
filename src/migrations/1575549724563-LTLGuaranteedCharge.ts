import {
    TenantCollection,
    VendorRatingCollection,
    LTLGuaranteedCharge,
    LTLGuaranteedChargeCollection,
    ChargeCodeCollection,
} from "../models";

export const up = async () => {
    await LTLGuaranteedCharge
        .rollPagination(async (results) => {
            for (const result of results) {
                const tenant = await TenantCollection
                    .findOne({ OldId: result.TenantId });
                const vendorRating = await VendorRatingCollection
                    .findOne({ OldId: result.VendorRatingId });
                const chargeCode = await ChargeCodeCollection
                    .findOne({ OldId: result.ChargeCodeId });
                result.TenantId = tenant._id;
                result.OldId = result.Id;
                result.VendorRatingId = vendorRating ? vendorRating._id : null;
                result.ChargeCodeId = chargeCode ? chargeCode._id : null;
            };

            await LTLGuaranteedChargeCollection
                .insertMany(results);
        });


};

export const down = async () => {
    await LTLGuaranteedChargeCollection.deleteMany({});
};

