import {
    AssetLog,
    TenantCollection,
    AssetLogCollection,
    ShipmentCollection,
    CountryCollection
} from "../models";

const associateRelations = async (log: AssetLog) => {
    const tenant = await TenantCollection
        .findOne({ OldId: log.TenantId });
    const asset = await AssetLogCollection
        .findOne({ OldId: log.AssetId });
    const shipment = await ShipmentCollection
        .findOne({ OldId: log.ShipmentId });
    const country = await CountryCollection
        .findOne({ OldId: log.CountryId });
    log.TenantId = tenant._id;
    log.OldId = log.Id;
    log.AssetId = asset && asset._id || null;
    log.ShipmentId = shipment && shipment._id || null;
    log.CountryId = country && country._id || null;
};

export const up = async () => {
    await AssetLog.rollPagination(async (logs) => {
        for (const log of logs) {
            await associateRelations(log);
        };
        await AssetLogCollection.insertMany(logs);
    });
};

export const down = async () => {
    await AssetLogCollection.deleteMany({});
};