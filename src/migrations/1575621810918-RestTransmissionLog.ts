import {
    UserCollection,
    RestTransmissionLog,
    RestTransmissionLogCollection
} from "../models";

const associateRelations = async (result: RestTransmissionLog) => {
    result.OldId = result.Id;
    if (result.UserId.toString() === "0") {
        result.UserId = null;
    } else {
        const user = await UserCollection
            .findOne({ OldId: result.UserId });
        result.UserId = user ? user._id : null;
    }
};

export const up = async () => {
    await RestTransmissionLog.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await RestTransmissionLogCollection.insertMany(results);
    });
};

export const down = async () => {
    await RestTransmissionLogCollection.deleteMany({});
};