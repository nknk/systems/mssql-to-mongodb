import { RefChargeCodeCategory, RefChargeCodeCategoryCollection } from "../models";

export const up = async () => {
    await RefChargeCodeCategory
        .rollPagination(async (results) => {
            await RefChargeCodeCategoryCollection
            .insertMany(results);
        });

};

export const down = async () => {
    await RefChargeCodeCategoryCollection.deleteMany({});
};