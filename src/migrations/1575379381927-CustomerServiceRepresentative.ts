import { TenantCollection, CustomerServiceRepresentative, CustomerServiceRepresentativeCollection, CustomerCollection, UserCollection } from "../models";

export const up = async () => {
    const results = await CustomerServiceRepresentative
        .findAll({
            raw: true,
            attributes: ["CustomerId", "UserId", "TenantId"],
        });
    for (const result of results) {
        const tenant = await TenantCollection.findOne({ OldId: result.TenantId });
        const customer = await CustomerCollection.findOne({ OldId: result.CustomerId });
        const user = await UserCollection.findOne({ OldId: result.UserId });
        result.TenantId = tenant._id;
        result.CustomerId = customer && customer._id || null;
        result.UserId = user && user._id || null;
    };

    await CustomerServiceRepresentativeCollection.insertMany(results);
};

export const down = async () => {
    await CustomerServiceRepresentativeCollection.deleteMany({});
};

