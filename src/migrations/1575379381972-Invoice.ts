import {
    TenantCollection,
    Invoice,
    PrefixCollection,
    UserCollection,
    CustomerLocationCollection,
    InvoiceCollection,
    IInvoiceDocument,
} from "../models";

const addOriginalInvoice = async (doc: IInvoiceDocument) => {
    const originalInvoice = await InvoiceCollection
        .findOne({ OldId: doc.OldOriginalInvoiceId });
    const invoiceId = originalInvoice && originalInvoice._id || null;
    doc.OriginalInvoiceId = invoiceId;
    doc.OldOriginalInvoiceId = undefined;
    await doc.save();
};

export const up = async () => {
    await Invoice
        .rollPagination(async (invoices) => {
            for (const invoice of invoices) {
                const tenant = await TenantCollection
                    .findOne({ OldId: invoice.TenantId });
                const prefix = await PrefixCollection
                    .findOne({ OldId: invoice.PrefixId });
                const user = await UserCollection
                    .findOne({ OldId: invoice.UserId });
                const customerLocation = await CustomerLocationCollection
                    .findOne({ OldId: invoice.CustomerLocationId });
                invoice.PrefixId = prefix && prefix._id || null;
                invoice.CustomerLocationId = customerLocation && customerLocation._id || null;
                invoice.UserId = user && user._id || null;
                invoice.TenantId = tenant._id;
                invoice.OldId = invoice.Id;
                invoice.OriginalInvoiceId = null;
                if (invoice.OldOriginalInvoiceId === "0") {
                    delete invoice.OldOriginalInvoiceId;
                }
            }
            await InvoiceCollection.insertMany(invoices);
        }, {
            raw: true,
            attributes: [
                "*",
                ["Id", "OldId"],
                ["OriginalInvoiceId", "OldOriginalInvoiceId"]
            ]
        });

    const docs = await InvoiceCollection.find({
        OldOriginalInvoiceId: { "$exists": true }
    });
    for (const doc of docs) {
        await addOriginalInvoice(doc);
    }
};

export const down = async () => {
    await InvoiceCollection
        .deleteMany({});
};

