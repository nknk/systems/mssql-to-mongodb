import {
    VendorBill,
    TenantCollection,
    VendorBillCollection,
    UserCollection,
    VendorLocationCollection,
    IVendorBillDocument
} from "../models";

const associateRelations = async (result: VendorBill) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const user = await UserCollection
        .findOne({ OldId: result.UserId });
    const vendorLocation = await VendorLocationCollection
        .findOne({ OldId: result.VendorLocationId });

    if (result.ApplyToDocumentId.toString() === "0") {
        result.ApplyToDocumentId = null;
    }

    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.UserId = user && user._id || null;
    result.VendorLocationId = vendorLocation && vendorLocation._id || null;
};

const applyToDocument = async (doc: IVendorBillDocument) => {
    const vendorBill = await VendorBillCollection
        .findOne({ OldId: doc.ApplyToDocumentId });
    const applyToDocumentId = vendorBill && vendorBill._id || null;
    doc.ApplyToDocumentId = applyToDocumentId;
    await doc.save();
};

export const up = async () => {
    await VendorBill.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await VendorBillCollection.insertMany(results);
    });

    const docs = await VendorBillCollection.find({
        ApplyToDocumentId: {
            "$ne": null
        }
    });
    for (const doc of docs) {
        await applyToDocument(doc);
    };
};

export const down = async () => {
    await VendorBillCollection.deleteMany({});
};