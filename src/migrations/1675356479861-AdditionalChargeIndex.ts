import {
    TenantCollection,
    CountryCollection,
    LTLAdditionalChargeCollection,
    RegionCollection,
    AdditionalChargeIndex,
    AdditionalChargeIndexCollection,
} from "../models";

export const up = async () => {
    await AdditionalChargeIndex
        .rollPagination(async (indices) => {
            for (const indice of indices) {
                const tenant = await TenantCollection
                    .findOne({ OldId: indice.TenantId });
                const country = await CountryCollection
                    .findOne({ OldId: indice.CountryId });
                const ltlAdditionalCharge = await LTLAdditionalChargeCollection
                    .findOne({ OldId: indice.LTLAdditionalChargeId });
                const region = await RegionCollection
                    .findOne({ OldId: indice.RegionId });

                indice.TenantId = tenant._id;
                indice.CountryId = country ? country._id : null;
                indice.LTLAdditionalChargeId = ltlAdditionalCharge ?
                    ltlAdditionalCharge._id : null;
                indice.RegionId = region ? region._id : null;
                indice.OldId = indice.Id;
            };
            await AdditionalChargeIndexCollection
                .insertMany(indices);
        });
};

export const down = async () => {
    await AdditionalChargeIndexCollection.deleteMany({});
};

