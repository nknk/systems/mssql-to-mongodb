import {
    TenantCollection,
    AreaCollection,
    Area,
    CountryCollection,
    RegionCollection,
} from "../models";

const updateRelations = async (area: Area) => {
    const tenant = await TenantCollection
        .findOne({ OldId: area.TenantId });
    const country = await CountryCollection
        .findOne({ OldId: area.CountryId });
    const region = await RegionCollection
        .findOne({ OldId: area.RegionId });
    const subregion = await RegionCollection
        .findOne({ OldId: area.SubRegionId });
    area.TenantId = tenant._id;
    area.CountryId = country && country._id || null;
    area.RegionId = region && region._id || null;
    area.SubRegionId = subregion && subregion._id || null;
};

export const up = async () => {

    await Area.rollPagination(async (areas) => {
        for (const area of areas) {
            await updateRelations(area);
        };
        await AreaCollection.insertMany(areas);
    }, {
        attributes: ["*", ["Id", "OldId"]]
    });

};

export const down = async () => {
    await AreaCollection.deleteMany({});
};

