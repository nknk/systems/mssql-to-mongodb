import {
    ShipmentService,
    TenantCollection,
    ShipmentServiceCollection,
    ShipmentCollection,
    ServiceCollection
} from "../models";

const associateRelations = async (result: ShipmentService) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const shipment = await ShipmentCollection
        .findOne({ OldId: result.ShipmentId });
    const service = await ServiceCollection
        .findOne({ OldId: result.ServiceId });
    result.TenantId = tenant._id;
    result.ShipmentId = shipment && shipment._id || null;
    result.ServiceId = service && service._id || null;
};

export const up = async () => {
    await ShipmentService
        .rollByRow("ServiceId", async (results) => {
            for (const result of results) {
                await associateRelations(result);
            };
            await ShipmentServiceCollection.insertMany(results);
        });
};

export const down = async () => {
    await ShipmentServiceCollection.deleteMany({});
};