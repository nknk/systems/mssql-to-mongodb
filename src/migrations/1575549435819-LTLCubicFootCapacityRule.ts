import {
    TenantCollection,
    VendorRatingCollection,
    LTLCubicFootCapacityRule,
    LTLCubicFootCapacityRuleCollection,
} from "../models";

export const up = async () => {
    await LTLCubicFootCapacityRule
        .rollPagination(async (results) => {
            for (const result of results) {
                const tenant = await TenantCollection
                    .findOne({ OldId: result.TenantId });
                const vendorRating = await VendorRatingCollection
                    .findOne({ OldId: result.VendorRatingId });
                result.TenantId = tenant._id;
                result.OldId = result.Id;
                result.VendorRatingId = vendorRating ? vendorRating._id : null;
            };

            await LTLCubicFootCapacityRuleCollection
                .insertMany(results);
        });


};

export const down = async () => {
    await LTLCubicFootCapacityRuleCollection.deleteMany({});
};

