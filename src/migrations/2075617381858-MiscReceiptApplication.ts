import {
    MiscReceiptApplication,
    TenantCollection,
    MiscReceiptApplicationCollection,
    InvoiceCollection,
    MiscReceiptCollection
} from "../models";

export const up = async () => {
    await MiscReceiptApplication
        .rollPagination(async (results) => {

            for (const result of results) {

                const tenant = await TenantCollection
                    .findOne({ OldId: result.TenantId });

                result.TenantId = tenant ? tenant._id : null;

                if (result.InvoiceId.toString() === "0") {
                    result.InvoiceId = null;
                } else {
                    const invoice = await InvoiceCollection
                        .findOne({ OldId: result.InvoiceId });
                    result.InvoiceId = invoice ? invoice._id : null;
                }

                if (result.MiscReceiptId.toString() === "0") {
                    result.MiscReceiptId = null;
                } else {
                    const miscReceipt = await MiscReceiptCollection
                        .findOne({ OldId: result.MiscReceiptId });
                    result.MiscReceiptId = miscReceipt ? miscReceipt._id : null;
                }

            }

            await MiscReceiptApplicationCollection
                .insertMany(results);
        }, {
            attributes: ["*", ["Id", "OldId"]]
        });

};

export const down = async () => {
    await MiscReceiptApplicationCollection
        .deleteMany({});
};

