import {
    TenantCollection,
    SalesRepCommTier,
    SalesRepCommTierCollection,
    CustomerCollection,
    SalesRepresentativeCollection
} from "../models";

const associateRelations = async (result: SalesRepCommTier) => {
    const tenant = await TenantCollection
    .findOne({ OldId: result.TenantId });
    const customer = await CustomerCollection
    .findOne({ OldId: result.CustomerId });
    const salesRepresentative = await SalesRepresentativeCollection
    .findOne({ OldId: result.SalesRepresentativeId });
    result.OldId = result.Id;
    result.TenantId = tenant? tenant._id: null;
    result.CustomerId = customer? customer._id: null;
    result.SalesRepresentativeId = salesRepresentative? salesRepresentative._id: null;

};

export const up = async () => {
    await SalesRepCommTier.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await SalesRepCommTierCollection.insertMany(results);
    });
};

export const down = async () => {
    await SalesRepCommTierCollection.deleteMany({});
};