import { ChargeCode, TenantCollection, ChargeCodeCollection } from "../models";

export const up = async () => {
    const results = await ChargeCode.findAll({ raw: true });

    for (const result of results) {
        const tenant = await TenantCollection.findOne({ OldId: result.TenantId });
        result.TenantId = tenant._id;
        result.OldId = result.Id;
    };

    await ChargeCodeCollection.insertMany(results);

    const tenants = await TenantCollection.find({});
    for (const tenant of tenants) {
        const insuranceChargeCode = await ChargeCodeCollection
            .findOne({ OldId: tenant.InsuranceChargeCodeId });

        const macroPointChargeCode = await ChargeCodeCollection
            .findOne({ OldId: tenant.MacroPointChargeCodeId });

        const defaultCollectChargeCode = await ChargeCodeCollection
            .findOne({ OldId: tenant.DefaultCollectChargeCodeId });

        const defaultUnmappedChargeCodeId = await ChargeCodeCollection
            .findOne({ OldId: tenant.DefaultUnmappedChargeCodeId });

        tenant.InsuranceChargeCodeId = insuranceChargeCode ? insuranceChargeCode._id : null;
        tenant.MacroPointChargeCodeId = macroPointChargeCode ? macroPointChargeCode._id : null;
        tenant.DefaultCollectChargeCodeId = defaultCollectChargeCode ? defaultCollectChargeCode._id : null;
        tenant.DefaultUnmappedChargeCodeId = defaultUnmappedChargeCodeId ? defaultUnmappedChargeCodeId._id : null;
        await tenant.save();
    }
};

export const down = async () => {
    await ChargeCodeCollection.deleteMany({});
};

