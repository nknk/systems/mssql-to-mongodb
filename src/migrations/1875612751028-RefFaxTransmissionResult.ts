import { RefFaxTransmissionResult, RefFaxTransmissionResultCollection } from "../models";

export const up = async () => {
    await RefFaxTransmissionResult
        .rollPagination(async (results) => {
            await RefFaxTransmissionResultCollection
                .insertMany(results);
        });
};

export const down = async () => {
    await RefFaxTransmissionResultCollection.deleteMany({});
};