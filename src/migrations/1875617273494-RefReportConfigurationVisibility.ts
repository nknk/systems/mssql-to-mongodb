import {
    RefReportConfigurationVisibility,
    RefReportConfigurationVisibilityCollection
} from "../models";

export const up = async () => {
    await RefReportConfigurationVisibility
        .rollPagination(async (results) => {
            await RefReportConfigurationVisibilityCollection
                .insertMany(results);
        });
};

export const down = async () => {
    await RefReportConfigurationVisibilityCollection
        .deleteMany({});
};