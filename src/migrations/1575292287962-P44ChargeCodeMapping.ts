import {
    TenantCollection,
    P44ChargeCodeMapping,
    P44ChargeCodeMappingCollection,
    ChargeCodeCollection
} from "../models";

export const up = async () => {
    const results = await P44ChargeCodeMapping.findAll({ raw: true });

    for (const result of results) {
        const tenant = await TenantCollection.findOne({ OldId: result.TenantId });
        const chargeCode = await ChargeCodeCollection.findOne({ OldId: result.ChargeCodeId });
        result.TenantId = tenant._id;
        result.OldId = result.Id;
        result.ChargeCodeId = chargeCode._id;
    };

    await P44ChargeCodeMappingCollection.insertMany(results);
};

export const down = async () => {
    await P44ChargeCodeMappingCollection.deleteMany({});
};

