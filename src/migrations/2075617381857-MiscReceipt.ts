import {
    MiscReceipt,
    TenantCollection,
    UserCollection,
    ShipmentCollection,
    CustomerCollection,
    LoadOrderCollection,
    MiscReceiptCollection
} from "../models";

const addOriginalReceiptIds = async () => {

    const docs = await MiscReceiptCollection.find({
        OriginalMiscReceiptId: { "$ne": null }
    });

    for (const doc of docs) {
        const original = await MiscReceiptCollection
            .findOne({ OldId: doc.OriginalMiscReceiptId });
        doc.OriginalMiscReceiptId = original ? original._id : null;
        await doc.save();
    }
};

export const up = async () => {
    await MiscReceipt
        .rollPagination(async (results) => {

            for (const result of results) {

                const tenant = await TenantCollection
                    .findOne({ OldId: result.TenantId });

                result.TenantId = tenant ? tenant._id : null;

                if (result.UserId.toString() === "0") {
                    result.UserId = null;
                } else {
                    const user = await UserCollection
                        .findOne({ OldId: result.UserId });
                    result.UserId = user ? user._id : null;
                }

                if (result.ShipmentId.toString() === "0") {
                    result.ShipmentId = null;
                } else {
                    const shipment = await ShipmentCollection
                        .findOne({ OldId: result.ShipmentId });
                    result.ShipmentId = shipment ? shipment._id : null;
                }

                if (result.CustomerId.toString() === "0") {
                    result.CustomerId = null;
                } else {
                    const customer = await CustomerCollection
                        .findOne({ OldId: result.CustomerId });
                    result.CustomerId = customer ? customer._id : null;
                }

                if (result.LoadOrderId.toString() === "0") {
                    result.LoadOrderId = null;
                } else {

                    const loadOrder = await LoadOrderCollection
                        .findOne({ OldId: result.LoadOrderId });
                    result.LoadOrderId = loadOrder ? loadOrder._id : null;
                }

            }

            await MiscReceiptCollection
                .insertMany(results);
        }, {
            attributes: ["*", ["Id", "OldId"]]
        });


    await addOriginalReceiptIds();
};

export const down = async () => {
    await MiscReceiptCollection
        .deleteMany({});
};

