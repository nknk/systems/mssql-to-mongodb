import {
    TenantCollection,
    EmailLog,
    EmailLogCollection,
} from "../models";

export const up = async () => {
    await EmailLog
        .rollPagination(async (logs) => {
            for (const log of logs) {
                const tenant = await TenantCollection
                    .findOne({ OldId: log.TenantId });

                log.TenantId = tenant._id;
                log.OldId = log.Id;
            };
            await EmailLogCollection
            .insertMany(logs);
        });
};

export const down = async () => {
    await EmailLogCollection.deleteMany({});
};

