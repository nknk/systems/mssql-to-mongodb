import {
    TenantCollection,
    CustomerCollection,
    DeveloperAccessRequest,
    DeveloperAccessRequestCollection,
} from "../models";


export const up = async () => {
    await DeveloperAccessRequest
        .rollPagination(async (requests) => {
            for (const request of requests) {
                const tenant = await TenantCollection
                    .findOne({ OldId: request.TenantId });

                const customer = await CustomerCollection
                    .findOne({ OldId: request.CustomerId });

                request.CustomerId = customer && customer._id || null;
                request.TenantId = tenant._id;
                request.OldId = request.Id;
            }
            await DeveloperAccessRequestCollection
                .insertMany(requests);
        });
};

export const down = async () => {
    await DeveloperAccessRequestCollection.deleteMany({});
};

