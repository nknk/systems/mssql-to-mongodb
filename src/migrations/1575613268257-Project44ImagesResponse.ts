import { UserCollection, Project44ImagesResponse, Project44ImagesResponseCollection } from "../models";

export const up = async () => {
    await Project44ImagesResponse.rollPagination(async (results) => {

        for (const result of results) {
            const user = await UserCollection.findOne({ OldId: result.UserId });
            result.OldId = result.Id;
            result.UserId = user ? user._id : null;
        };

        await Project44ImagesResponseCollection.insertMany(results);
    });
};

export const down = async () => {
    await Project44ImagesResponseCollection.deleteMany({});
};

