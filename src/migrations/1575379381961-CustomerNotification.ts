import {
    TenantCollection,
    CustomerNotification,
    CustomerNotificationCollection,
    CustomerCommunicationCollection,
} from "../models";

export const up = async () => {
    await CustomerNotification
        .rollPagination(async (notifications) => {
            for (const notification of notifications) {
                const tenant = await TenantCollection
                    .findOne({ OldId: notification.TenantId });

                const customerCommunication = await CustomerCommunicationCollection
                    .findOne({ OldId: notification.CustomerCommunicationId });

                notification.TenantId = tenant._id;
                notification.CustomerCommunicationId = customerCommunication && customerCommunication._id || null;
                notification.OldId = notification.Id;
            }
            await CustomerNotificationCollection
                .insertMany(notifications);
        });
};

export const down = async () => {
    await CustomerNotificationCollection.deleteMany({});
};

