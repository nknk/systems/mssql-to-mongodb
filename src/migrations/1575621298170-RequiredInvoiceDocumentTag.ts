import { TenantCollection, RequiredInvoiceDocumentTag, RequiredInvoiceDocumentTagCollection, CustomerCollection, DocumentTagCollection } from "../models";

export const up = async () => {
    await RequiredInvoiceDocumentTag
        .rollByRow("DocumentTagId", async (results) => {
            for (const result of results) {
                const tenant = await TenantCollection
                    .findOne({ OldId: result.TenantId });
                const customer = await CustomerCollection
                    .findOne({ OldId: result.CustomerId });
                const documentTag = await DocumentTagCollection
                    .findOne({ OldId: result.DocumentTagId });
                result.TenantId = tenant ? tenant._id : null;
                result.DocumentTagId = documentTag ? documentTag._id : null;
                result.CustomerId = customer ? customer._id : null;
            };

            await RequiredInvoiceDocumentTagCollection.insertMany(results);
        });
};

export const down = async () => {
    await RequiredInvoiceDocumentTagCollection.deleteMany({});
};

