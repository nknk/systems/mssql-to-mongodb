import {
    ShoppedRate,
    TenantCollection,
    ShoppedRateCollection,
    LoadOrderCollection,
    ShipmentCollection,
    CustomerCollection,
    UserCollection,
    CountryCollection
} from "../models";

const associateRelations = async (result: ShoppedRate) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const shipment = await ShipmentCollection
        .findOne({ OldId: result.ShipmentId });
    const customer = await CustomerCollection
        .findOne({ OldId: result.CustomerId });
    const user = await UserCollection
        .findOne({ OldId: result.UserId });
    const originCountry = await CountryCollection
        .findOne({ OldId: result.OriginCountryId });
    const destinationCountry = await CountryCollection
        .findOne({ OldId: result.DestinationCountryId });
    const loadOrder = await LoadOrderCollection
        .findOne({ OldId: result.LoadOrderId });

    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.ShipmentId = shipment && shipment._id || null;
    result.CustomerId = customer && customer._id || null;
    result.UserId = user && user._id || null;
    result.OriginCountryId = originCountry && originCountry._id || null;
    result.DestinationCountryId = destinationCountry && destinationCountry._id || null;
    result.LoadOrderId = loadOrder && loadOrder._id || null;
};

export const up = async () => {
    await ShoppedRate.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await ShoppedRateCollection.insertMany(results);
    });
};

export const down = async () => {
    await ShoppedRateCollection.deleteMany({});
};