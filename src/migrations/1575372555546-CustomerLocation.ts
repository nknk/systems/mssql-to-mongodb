import {
    TenantCollection,
    CustomerLocation,
    CustomerLocationCollection,
    CountryCollection,
    CustomerCollection
} from "../models";


const updateRelations = async (location: CustomerLocation) => {
    const tenant = await TenantCollection
        .findOne({ OldId: location.TenantId });
    const customer = await CustomerCollection
        .findOne({ OldId: location.CustomerId });
    const country = await CountryCollection
        .findOne({ OldId: location.CountryId });
    location.TenantId = tenant && tenant._id || null;
    location.CustomerId = customer && customer._id || null;
    location.CountryId = country && country._id || null;
};


export const up = async () => {
    await CustomerLocation.rollPagination(async (locations) => {
        for (const location of locations) {
            await updateRelations(location);
        };
        await CustomerLocationCollection
            .insertMany(locations);
    }, {
        attributes: ["*", ["Id", "OldId"]]
    });
};

export const down = async () => {
    await CustomerLocationCollection
        .deleteMany({});
};