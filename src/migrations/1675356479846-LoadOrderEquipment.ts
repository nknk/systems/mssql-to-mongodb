import {
    TenantCollection,
    LoadOrderCollection,
    LoadOrderEquipment,
    EquipmentTypeCollection,
    LoadOrderEquipmentCollection,
} from "../models";

export const up = async () => {
    await LoadOrderEquipment
        .rollByRow("EquipmentTypeId", async (equipments) => {
            for (const equipment of equipments) {
                const tenant = await TenantCollection
                    .findOne({ OldId: equipment.TenantId });
                const loadOrder = await LoadOrderCollection
                    .findOne({ OldId: equipment.LoadOrderId });
                const equipmentType = await EquipmentTypeCollection
                    .findOne({ OldId: equipment.EquipmentTypeId });

                equipment.LoadOrderId = loadOrder && loadOrder._id || null;
                equipment.EquipmentTypeId = equipmentType && equipmentType._id || null;
                equipment.TenantId = tenant._id;
            };
            await LoadOrderEquipmentCollection
                .insertMany(equipments);
        });
};

export const down = async () => {
    await LoadOrderEquipmentCollection.deleteMany({});
};

