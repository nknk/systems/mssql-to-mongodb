import {
    VendorDispute,
    TenantCollection,
    VendorDisputeCollection,
    UserCollection,
    VendorCollection
} from "../models";

const associateRelations = async (result: VendorDispute) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const vendor = await VendorCollection
        .findOne({ OldId: result.VendorId });
    const creatingUser = await UserCollection
        .findOne({ OldId: result.CreatedByUserId });
    const resolvingUser = await UserCollection
        .findOne({ OldId: result.ResolvedByUserId });

    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.VendorId = vendor && vendor._id || null;
    result.CreatedByUserId = creatingUser && creatingUser._id || null;
    result.ResolvedByUserId = resolvingUser && resolvingUser._id || null;
};

export const up = async () => {
    await VendorDispute.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await VendorDisputeCollection.insertMany(results);
    });
};

export const down = async () => {
    await VendorDisputeCollection.deleteMany({});
};