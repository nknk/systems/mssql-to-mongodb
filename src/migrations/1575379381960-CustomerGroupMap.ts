import {
    TenantCollection,
    CustomerGroupMap,
    CustomerCollection,
    CustomerGroupMapCollection,
    CustomerGroupCollection,
} from "../models";

export const up = async () => {

    const maps = await CustomerGroupMap.findAll({
        raw: true,
        attributes: ["TenantId", "CustomerGroupId", "CustomerId"]
    });

    for (const map of maps) {
        const tenant = await TenantCollection
            .findOne({ OldId: map.TenantId });

        const customer = await CustomerCollection
            .findOne({ OldId: map.CustomerId });

        const customerGroup = await CustomerGroupCollection
            .findOne({ OldId: map.CustomerGroupId });

        map.CustomerId = customer && customer._id || null;
        map.CustomerGroupId = customerGroup && customerGroup._id || null;
        map.TenantId = tenant._id;
        map.OldId = map.Id;
    };

    await CustomerGroupMapCollection.insertMany(maps);

};

export const down = async () => {
    await CustomerGroupMapCollection.deleteMany({});
};

