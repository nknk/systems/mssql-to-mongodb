import {
    TenantCollection,
    VendorCollection,
    VendorService,
    VendorServiceCollection,
    ServiceCollection
} from "../models";

export const up = async () => {
    await VendorService
        .rollByRow("ServiceId", async (results) => {
            for (const result of results) {
                const tenant = await TenantCollection
                    .findOne({ OldId: result.TenantId });
                const vendor = await VendorCollection
                    .findOne({ OldId: result.VendorId });
                const service = await ServiceCollection
                    .findOne({ OldId: result.ServiceId });
                result.TenantId = tenant._id;
                result.ServiceId = service ? service._id : null;
                result.VendorId = vendor ? vendor._id : null;
            }
            await VendorServiceCollection.insertMany(results);
        });
};

export const down = async () => {
    await VendorServiceCollection.deleteMany({});
};

