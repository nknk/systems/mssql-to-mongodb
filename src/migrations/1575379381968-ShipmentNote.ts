import {
    ShipmentNote,
    TenantCollection,
    ShipmentNoteCollection,
    ShipmentCollection,
    UserCollection
} from "../models";

const associateRelations = async (result: ShipmentNote) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const shipment = await ShipmentCollection
        .findOne({ OldId: result.ShipmentId });
    const user = await UserCollection
        .findOne({ OldId: result.UserId });

    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.ShipmentId = shipment && shipment._id || null;
    result.UserId = user && user._id || null;
};

export const up = async () => {
    await ShipmentNote.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await ShipmentNoteCollection.insertMany(results);
    });
};

export const down = async () => {
    await ShipmentNoteCollection.deleteMany({});
};