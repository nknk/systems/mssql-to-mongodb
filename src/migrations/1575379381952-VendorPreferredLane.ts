import {
    VendorPreferredLane,
    TenantCollection,
    VendorPreferredLaneCollection,
    CountryCollection,
    VendorCollection
} from "../models";

const associateRelations = async (result: VendorPreferredLane) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const originCountry = await CountryCollection
        .findOne({ OldId: result.OriginCountryId });
    const destinationCountry = await CountryCollection
        .findOne({ OldId: result.DestinationCountryId });
    const vendor = await VendorCollection
        .findOne({ OldId: result.VendorId });

    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.OriginCountryId = originCountry && originCountry._id || null;
    result.DestinationCountryId = destinationCountry && destinationCountry._id || null;
    result.VendorId = vendor && vendor._id || null;
};

export const up = async () => {
    await VendorPreferredLane.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await VendorPreferredLaneCollection.insertMany(results);
    });
};

export const down = async () => {
    await VendorPreferredLaneCollection.deleteMany({});
};