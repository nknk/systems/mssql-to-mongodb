import {
    TenantCollection,
    FuelIndex,
    FuelIndexCollection,
    FuelTableCollection,
} from "../models";


export const up = async () => {
    await FuelIndex
        .rollPagination(async (fuelIndices) => {
            for (const fuelIndex of fuelIndices) {
                const tenant = await TenantCollection
                    .findOne({ OldId: fuelIndex.TenantId });
                const fuelTable = await FuelTableCollection
                    .findOne({ OldId: fuelIndex.FuelTableId });

                fuelIndex.FuelTableId = fuelTable && fuelTable._id || null;
                fuelIndex.TenantId = tenant._id;
                fuelIndex.OldId = fuelIndex.Id;
            };
            await FuelIndexCollection
            .insertMany(fuelIndices);
        });
};

export const down = async () => {
    await FuelIndexCollection.deleteMany({});
};

