import { RefDetailReferenceType, RefDetailReferenceTypeCollection } from "../models";

export const up = async () => {
    await RefDetailReferenceType
        .rollPagination(async (results) => {
            await RefDetailReferenceTypeCollection
                .insertMany(results);
        });

};

export const down = async () => {
    await RefDetailReferenceTypeCollection.deleteMany({});
};