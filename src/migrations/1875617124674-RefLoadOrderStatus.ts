import { RefLoadOrderStatus, RefLoadOrderStatusCollection } from "../models";

export const up = async () => {
    await RefLoadOrderStatus
        .rollPagination(async (results) => {
            await RefLoadOrderStatusCollection
                .insertMany(results);
        });
};

export const down = async () => {
    await RefLoadOrderStatusCollection
        .deleteMany({});
};