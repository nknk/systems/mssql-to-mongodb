import {
    SmallPackageServiceMap,
    TenantCollection,
    SmallPackageServiceMapCollection,
    ServiceCollection
} from "../models";

const associateRelations = async (result: SmallPackageServiceMap) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const service = await ServiceCollection
        .findOne({ OldId: result.ServiceId });

    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.ServiceId = service && service._id || null;
};

export const up = async () => {
    await SmallPackageServiceMap.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await SmallPackageServiceMapCollection.insertMany(results);
    });
};

export const down = async () => {
    await SmallPackageServiceMapCollection.deleteMany({});
};