import { PostalCode, CountryCollection, PostalCodeCollection } from "../models";

export const up = async () => {
    const countries = await CountryCollection.find({});
    const countryIds = {};
    for (const country of countries) {
        countryIds[country.OldId] = country._id;
    }
    await PostalCode.rollPagination(async (results) => {
        for (const result of results) {
            result.CountryId = countryIds[result.CountryId];
        };
        await PostalCodeCollection.insertMany(results);
    }, {
        attributes: ["*", ["Id", "OldId"]]
    });

};

export const down = async () => {
    await PostalCodeCollection.deleteMany({});
};

