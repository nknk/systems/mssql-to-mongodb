import {
    ShipmentAutoRatingAccessorial,
    TenantCollection,
    ShipmentAutoRatingAccessorialCollection,
    ShipmentCollection    
} from "../models";

const associateRelations = async (result: ShipmentAutoRatingAccessorial) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const shipment = await ShipmentCollection
        .findOne({ OldId: result.ShipmentId });   

    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.ShipmentId = shipment && shipment._id || null;
};

export const up = async () => {
    await ShipmentAutoRatingAccessorial.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await ShipmentAutoRatingAccessorialCollection.insertMany(results);
    });
};

export const down = async () => {
    await ShipmentAutoRatingAccessorialCollection.deleteMany({});
};