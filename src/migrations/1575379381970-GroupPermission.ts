import {
    TenantCollection,
    GroupPermission,
    GroupCollection,
    GroupPermissionCollection,
} from "../models";


export const up = async () => {
    await GroupPermission
        .rollPagination(async (permissions) => {
            for (const permission of permissions) {
                const tenant = await TenantCollection
                    .findOne({ OldId: permission.TenantId });
                const group = await GroupCollection
                    .findOne({ OldId: permission.GroupId });

                permission.GroupId = group && group._id || null;
                permission.TenantId = tenant._id;
                permission.OldId = permission.Id;
            }
            await GroupPermissionCollection
            .insertMany(permissions);
        });
};

export const down = async () => {
    await GroupPermissionCollection.deleteMany({});
};

