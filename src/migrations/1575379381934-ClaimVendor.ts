import {
    ClaimVendor,
    TenantCollection,
    ClaimVendorCollection,
    ClaimCollection,
    VendorCollection
} from "../models";

const associateRelations = async (result: ClaimVendor) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const vendor = await VendorCollection
        .findOne({ OldId: result.VendorId });
    const claim = await ClaimCollection
        .findOne({ OldId: result.ClaimId });
    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.VendorId = vendor && vendor._id || null;
    result.ClaimId = claim && claim._id || null;
};

export const up = async () => {
    await ClaimVendor.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await ClaimVendorCollection.insertMany(results);
    });
};

export const down = async () => {
    await ClaimVendorCollection.deleteMany({});
};