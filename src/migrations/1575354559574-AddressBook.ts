import {
    TenantCollection,
    AddressBook,
    AddressBookCollection,
    CustomerCollection,
    CountryCollection,
} from "../models";

export const up = async () => {

    await AddressBook.rollPagination(async (addressBooks) => {
        for (const addressBook of addressBooks) {
            const tenant = await TenantCollection
                .findOne({ OldId: addressBook.TenantId });
            const customer = await CustomerCollection
                .findOne({ OldId: addressBook.CustomerId });
            const country = await CountryCollection
                .findOne({ OldId: addressBook.CountryId });
            addressBook.TenantId = tenant._id;
            addressBook.CustomerId = customer && customer._id || null;
            addressBook.CountryId = country && country._id || null;
        };
        await AddressBookCollection.insertMany(addressBooks);

    }, { attributes: ["*", ["Id", "OldId"]] });
};

export const down = async () => {
    await AddressBookCollection.deleteMany({});
};

