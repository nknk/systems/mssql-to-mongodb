import {
    UserShipAs,
    TenantCollection,
    UserShipAsCollection,
    CustomerCollection,
    UserCollection
} from "../models";

const associateRelations = async (result: UserShipAs) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const customer = await CustomerCollection
        .findOne({ OldId: result.CustomerId });
    const user = await UserCollection
        .findOne({ OldId: result.UserId });
    result.TenantId = tenant._id;
    result.CustomerId = customer && customer._id || null;
    result.UserId = user && user._id || null;
};

export const up = async () => {
    await UserShipAs
        .rollByRow("UserId", async (results) => {
            for (const result of results) {
                await associateRelations(result);
            };
            await UserShipAsCollection.insertMany(results);
        });


};

export const down = async () => {
    await UserShipAsCollection.deleteMany({});
};