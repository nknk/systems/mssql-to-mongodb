import {
    TenantCollection,
    PendingVendor,
    PendingVendorCollection,
    UserCollection,
} from "../models";

export const up = async () => {
    await PendingVendor
        .rollPagination(async (vendors) => {
            for (const vendor of vendors) {
                const tenant = await TenantCollection
                    .findOne({ OldId: vendor.TenantId });
                const user = await UserCollection
                    .findOne({ OldId: vendor.CreatedByUserId });
                vendor.TenantId = tenant._id;
                vendor.OldId = vendor.Id;
                vendor.CreatedByUserId = user && user._id || null;
            };
            await PendingVendorCollection.insertMany(vendors);
        });
};

export const down = async () => {
    await PendingVendorCollection.deleteMany({});
};

