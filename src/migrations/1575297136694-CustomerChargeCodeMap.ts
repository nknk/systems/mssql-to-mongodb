import {
    TenantCollection,
    CustomerCollection,
    ChargeCodeCollection,
    CustomerChargeCodeMap,
    CustomerChargeCodeMapCollection,
} from "../models";

const updateRelations = async (csChargeCodeMap: CustomerChargeCodeMap) => {

    const tenant = await TenantCollection
        .findOne({ OldId: csChargeCodeMap.TenantId });
    const chargeCode = await ChargeCodeCollection
        .findOne({ OldId: csChargeCodeMap.ChargeCodeId });
    const customer = await CustomerCollection
        .findOne({ OldId: csChargeCodeMap.CustomerId });

    csChargeCodeMap.TenantId = tenant._id;
    csChargeCodeMap
        .CustomerId = customer && customer._id || null;
    csChargeCodeMap
        .ChargeCodeId = chargeCode && chargeCode._id || null;
};

export const up = async () => {

    await CustomerChargeCodeMap.rollPagination(async (csChargeCodeMaps) => {
        for (const csChargeCodeMap of csChargeCodeMaps) {
            await updateRelations(csChargeCodeMap);
        };
        await CustomerChargeCodeMapCollection
            .insertMany(csChargeCodeMaps);
    }, {
        attributes: ["*", ["Id", "OldId"]],
    });

};

export const down = async () => {
    await CustomerChargeCodeMapCollection
        .deleteMany({});
};

