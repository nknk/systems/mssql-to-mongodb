import {
    Customer,
    TenantCollection,
    AccountBucketCollection,
    PrefixCollection,
    MileageSourceCollection,
    SalesRepresentativeCollection,
    TierCollection, CustomerCollection,
} from "../models";


const updateRelations = async (customer: Customer) => {
    const tenant = await TenantCollection
        .findOne({ OldId: customer.TenantId });
    const bucket = await AccountBucketCollection
        .findOne({ OldId: customer.DefaultAccountBucketId });
    const prefix = await PrefixCollection
        .findOne({ OldId: customer.PrefixId });
    const mileageSource = await MileageSourceCollection
        .findOne({ OldId: customer.RequiredMileageSourceId });
    const saleRep = await SalesRepresentativeCollection
        .findOne({ OldId: customer.SalesRepresentativeId });
    const tier = await TierCollection
        .findOne({ OldId: customer.TierId });

    customer.TenantId = tenant && tenant._id || null;
    customer.DefaultAccountBucketId = bucket && bucket._id || null;
    customer.PrefixId = prefix && prefix._id || null;
    customer.RequiredMileageSourceId = mileageSource && mileageSource._id || null;
    customer.TierId = tier && tier._id || null;
    customer.SalesRepresentativeId = saleRep && saleRep._id || null;
    customer.RatingId = null;
    customer.CommunicationId = null;
};


export const up = async () => {
    await Customer.rollPagination(async (results) => {
        for (const result of results) {
            await updateRelations(result);
        };
        await CustomerCollection.insertMany(results);
    }, {
        attributes: ["*", ["Id", "OldId"]]
    });


};

export const down = async () => {
    await CustomerCollection.deleteMany({});
};