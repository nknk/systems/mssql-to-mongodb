import { RefScheduleInterval, RefScheduleIntervalCollection } from "../models";

export const up = async () => {
    await RefScheduleInterval
        .rollPagination(async (results) => {
            await RefScheduleIntervalCollection
                .insertMany(results);
        });
};

export const down = async () => {
    await RefScheduleIntervalCollection.deleteMany({});
};