import { RefServiceProvider, RefServiceProviderCollection } from "../models";

export const up = async () => {
    await RefServiceProvider
        .rollPagination(async (results) => {
            await RefServiceProviderCollection
                .insertMany(results);
        });
};

export const down = async () => {
    await RefServiceProviderCollection
        .deleteMany({});
};