import {
    VendorPackageCustomMapping,
    TenantCollection,
    VendorPackageCustomMappingCollection,
    PackageTypeCollection,
    VendorCollection
} from "../models";

const associateRelations = async (result: VendorPackageCustomMapping) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const packageType = await PackageTypeCollection
        .findOne({ OldId: result.PackageTypeId });
    const vendor = await VendorCollection
        .findOne({ OldId: result.VendorId });

    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.VendorId = vendor && vendor._id || null;
    result.PackageTypeId = packageType && packageType._id || null;
};

export const up = async () => {
    await VendorPackageCustomMapping.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await VendorPackageCustomMappingCollection.insertMany(results);
    });
};

export const down = async () => {
    await VendorPackageCustomMappingCollection.deleteMany({});
};