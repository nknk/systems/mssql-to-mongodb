import {
    TenantCollection,
    VendorCollection,
    CustomerTLTenderingProfileLane,
    CustomerTLTenderingProfileLaneCollection,
    CustomerTLTenderingProfileCollection,
    CountryCollection,
} from "../models";


const associateRelation = async (lane: CustomerTLTenderingProfileLane) => {
    const tenant = await TenantCollection
        .findOne({ OldId: lane.TenantId });

    const firstPrefferredVendor = await VendorCollection
        .findOne({ OldId: lane.FirstPreferredVendorId });

    const secondPrefferredVendor = await VendorCollection
        .findOne({ OldId: lane.SecondPreferredVendorId });

    const thirdPrefferredVendor = await VendorCollection
        .findOne({ OldId: lane.ThirdPreferredVendorId });

    const originCountryId = await CountryCollection
        .findOne({ OldId: lane.OriginCountryId });

    const destinationCountryId = await CountryCollection
        .findOne({ OldId: lane.DestinationCountryId });

    const tLTenderingProfile = await CustomerTLTenderingProfileCollection
        .findOne({ OldId: lane.TLTenderingProfileId });

    lane.FirstPreferredVendorId = firstPrefferredVendor && firstPrefferredVendor._id || null;
    lane.SecondPreferredVendorId = secondPrefferredVendor && secondPrefferredVendor._id || null;
    lane.ThirdPreferredVendorId = thirdPrefferredVendor && thirdPrefferredVendor._id || null;
    lane.OriginCountryId = originCountryId && originCountryId._id || null;
    lane.DestinationCountryId = destinationCountryId && destinationCountryId._id || null;
    lane.TenantId = tenant._id;
    lane.TLTenderingProfileId = tLTenderingProfile ? tLTenderingProfile._id: null;
    lane.OldId = lane.Id;
};

export const up = async () => {
    await CustomerTLTenderingProfileLane
        .rollPagination(async (lanes) => {
            for (const lane of lanes) {
                await associateRelation(lane);
            };
            await CustomerTLTenderingProfileLaneCollection
                .insertMany(lanes);
        });
};

export const down = async () => {
    await CustomerTLTenderingProfileLaneCollection.deleteMany({});
};

