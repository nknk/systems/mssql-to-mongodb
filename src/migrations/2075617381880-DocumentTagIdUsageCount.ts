import {
    DocumentTag,
    DocumentTagCollection,
    ObjectAssociationCollection,
} from "../models";
import sequelize from "../config/sequelize";
import { QueryTypes } from "sequelize";

export const up = async () => {

    const associations: any[] = [];

    await DocumentTag
        .rollPagination(async (records) => {
            for (const record of records) {
                const recordDoc = await DocumentTagCollection
                    .findOne({ OldId: record.Id });

                if (!recordDoc) {
                    continue;
                }

                const result: any = await sequelize
                    .query(
                        `SELECT dbo.DocumentTagIdUsageCount(:Id) 
                        AS UsageCount;`,
                        {
                            replacements: { Id: record.Id },
                            type: QueryTypes.SELECT,
                            plain: true
                        });

                const Occurrences = parseInt(result.UsageCount);

                if (Occurrences > 0) {
                    associations.push({
                        EntityId: recordDoc._id,
                        EntityName: "LogisticsPlus.Eship.RestClient.Models.DocumentTag",
                        Occurrences,
                        Status: 0,
                    });
                }
            }

        });


    if (associations.length) {
        await ObjectAssociationCollection
            .insertMany(associations);
    }
};

export const down = async () => {
    ObjectAssociationCollection.deleteMany({
        EntityName: "LogisticsPlus.Eship.RestClient.Models.DocumentTag"
    });
};

