import {
    VendorContact,
    TenantCollection,
    VendorContactCollection,
    ContactTypeCollection,
    VendorLocationCollection
} from "../models";

const associateRelations = async (result: VendorContact) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const contactType = await ContactTypeCollection
        .findOne({ OldId: result.ContactTypeId });
    const vendorLocation = await VendorLocationCollection
        .findOne({ OldId: result.VendorLocationId });
    
    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.ContactTypeId = contactType && contactType._id || null;
    result.VendorLocationId = vendorLocation && vendorLocation._id || null;
};

export const up = async () => {
    await VendorContact.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await VendorContactCollection.insertMany(results);
    });
};

export const down = async () => {
    await VendorContactCollection.deleteMany({});
};