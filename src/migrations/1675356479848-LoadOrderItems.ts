import {
    TenantCollection,
    LoadOrderCollection,
    LoadOrderItem,
    PackageTypeCollection,
    LoadOrderItemCollection,
} from "../models";

export const up = async () => {
    await LoadOrderItem
        .rollPagination(async (items) => {
            for (const item of items) {
                const tenant = await TenantCollection
                    .findOne({ OldId: item.TenantId });
                const loadOrder = await LoadOrderCollection
                    .findOne({ OldId: item.LoadOrderId });
                const packageType = await PackageTypeCollection
                    .findOne({ OldId: item.PackageTypeId });

                item.LoadOrderId = loadOrder && loadOrder._id || null;
                item.PackageTypeId = packageType && packageType._id || null;
                item.TenantId = tenant._id;
                item.OldId = item.Id;
            };
            await LoadOrderItemCollection
            .insertMany(items);
        });
};

export const down = async () => {
    await LoadOrderItemCollection.deleteMany({});
};

