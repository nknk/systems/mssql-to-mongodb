import {
    ShipmentContact,
    TenantCollection,
    ShipmentContactCollection,
    ShipmentLocationCollection,
    ContactTypeCollection
} from "../models";

const associateRelations = async (result: ShipmentContact) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const shipmentLocation = await ShipmentLocationCollection
        .findOne({ OldId: result.ShipmentLocationId });
    const contactType = await ContactTypeCollection
        .findOne({ OldId: result.ContactTypeId });
    

    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.ShipmentLocationId = shipmentLocation && shipmentLocation._id || null;
    result.ContactTypeId = contactType && contactType._id || null;
};

export const up = async () => {
    await ShipmentContact.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await ShipmentContactCollection.insertMany(results);
    });
};

export const down = async () => {
    await ShipmentContactCollection.deleteMany({});
};