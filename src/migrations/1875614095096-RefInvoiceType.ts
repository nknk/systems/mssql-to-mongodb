import { RefInvoiceType, RefInvoiceTypeCollection } from "../models";

export const up = async () => {
    await RefInvoiceType
        .rollPagination(async (results) => {
            await RefInvoiceTypeCollection.insertMany(results);
        });
};

export const down = async () => {
    await RefInvoiceTypeCollection.deleteMany({});
};