import { RefServiceTicketStatus, RefServiceTicketStatusCollection } from "../models";

export const up = async () => {
    await RefServiceTicketStatus
        .rollPagination(async (results) => {
            await RefServiceTicketStatusCollection
                .insertMany(results);
        });
};

export const down = async () => {
    await RefServiceTicketStatusCollection.deleteMany({});
};