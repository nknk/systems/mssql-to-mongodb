import {
    TenantCollection,
    VendorRatingCollection,
    LTLSellRate,
    LTLSellRateCollection,
    CustomerRatingCollection,
} from "../models";

export const up = async () => {
    await LTLSellRate
        .rollPagination(async (results) => {
            for (const result of results) {
                const tenant = await TenantCollection
                    .findOne({ OldId: result.TenantId });
                const vendorRating = await VendorRatingCollection
                    .findOne({ OldId: result.VendorRatingId });
                const customerRating = await CustomerRatingCollection
                    .findOne({ OldId: result.CustomerRatingId });
                result.TenantId = tenant._id;
                result.OldId = result.Id;
                result.VendorRatingId = vendorRating ? vendorRating._id : null;
                result.CustomerRatingId = customerRating ? customerRating._id : null;
            };
            await LTLSellRateCollection.insertMany(results);
        });


};

export const down = async () => {
    await LTLSellRateCollection.deleteMany({});
};

