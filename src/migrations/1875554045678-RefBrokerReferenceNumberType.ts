import { RefBrokerReferenceNumberType, RefBrokerReferenceNumberTypeCollection } from "../models";

export const up = async () => {
    await RefBrokerReferenceNumberType
        .rollPagination(async (results) => {
            await RefBrokerReferenceNumberTypeCollection
                .insertMany(results);
        });
};

export const down = async () => {
    await RefBrokerReferenceNumberTypeCollection.deleteMany({});
};