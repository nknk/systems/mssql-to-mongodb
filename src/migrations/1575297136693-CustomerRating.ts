import {
    TenantCollection,
    CustomerCollection,
    ChargeCodeCollection,
    ResellerAdditionCollection,
    CustomerRating,
    CustomerRatingCollection,
} from "../models";

const updateRelations = async (customerRating: CustomerRating) => {

    const tenant = await TenantCollection
        .findOne({ OldId: customerRating.TenantId });
    const insuranceChargeCode = await ChargeCodeCollection
        .findOne({ OldId: customerRating.InsuranceChargeCodeId });
    const resellerAddition = await ResellerAdditionCollection
        .findOne({ OldId: customerRating.ResellerAdditionId });
    const customer = await CustomerCollection
        .findOne({ OldId: customerRating.CustomerId });
    const tlFuelChargeCode = await ChargeCodeCollection
        .findOne({ OldId: customerRating.TLFuelChargeCodeId });
    const tlFreightChargeCode = await ChargeCodeCollection
        .findOne({ OldId: customerRating.TLFreightChargeCodeId });

    customerRating.TenantId = tenant._id;
    customerRating
        .InsuranceChargeCodeId = insuranceChargeCode && insuranceChargeCode._id || null;
    customerRating
        .ResellerAdditionId = resellerAddition && resellerAddition._id || null;
    customerRating
        .CustomerId = customer && customer._id || null;
    customerRating
        .TLFuelChargeCodeId = tlFuelChargeCode && tlFuelChargeCode._id || null;
    customerRating
        .TLFreightChargeCodeId = tlFreightChargeCode && tlFreightChargeCode._id || null;
};

export const up = async () => {
    await CustomerRating.rollPagination(async (customerRatings) => {
        for (const customerRating of customerRatings) {
            await updateRelations(customerRating);
        };
        const savedCollection = await CustomerRatingCollection.insertMany(customerRatings);
        for (const rating of savedCollection) {
            const customer = await CustomerCollection.findOne({
                _id: rating.CustomerId
            });
            if (customer) {
                customer.RatingId = rating ? rating._id : null;
                await customer.save();
            }
        }
    }, {
        attributes: ["*", ["Id", "OldId"]],
    });

};

export const down = async () => {
    await CustomerRatingCollection.deleteMany({});
};

