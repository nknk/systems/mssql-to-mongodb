import {
    VendorDisputeDetail,
    TenantCollection,
    VendorDisputeDetailCollection,
    VendorDisputeCollection,
    ServiceTicketChargeCollection,
    ShipmentChargeCollection
} from "../models";

const associateRelations = async (result: VendorDisputeDetail) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const vendorDispute = await VendorDisputeCollection
        .findOne({ OldId: result.DisputedId });

    if(result.ChargeLineIdType == 0){
        const shipmentCharge = await ShipmentChargeCollection
        .findOne({ OldId: result.ChargeLineId });
        result.ChargeLineId = shipmentCharge && shipmentCharge._id || null; 
    }
    else if(result.ChargeLineIdType == 1){
        const serviceTicketCharge = await ServiceTicketChargeCollection
        .findOne({ OldId: result.ChargeLineId });
        result.ChargeLineId = serviceTicketCharge && serviceTicketCharge._id || null;
    }
    else{
        result.ChargeLineId = null;
    }

    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.DisputedId = vendorDispute && vendorDispute._id || null;    
};

export const up = async () => {
    await VendorDisputeDetail.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await VendorDisputeDetailCollection.insertMany(results);
    });
};

export const down = async () => {
    await VendorDisputeDetailCollection.deleteMany({});
};