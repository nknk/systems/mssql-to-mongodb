import {
    ServiceTicketCharge,
    TenantCollection,
    ServiceTicketChargeCollection,
    ServiceTicketCollection,
    ChargeCodeCollection,
    VendorCollection,
    VendorBillCollection
} from "../models";

const associateRelations = async (result: ServiceTicketCharge) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const vendor = await VendorCollection
        .findOne({ OldId: result.VendorId });
    const vendorBill = await VendorBillCollection
        .findOne({ OldId: result.VendorBillId });
    const chargeCode = await ChargeCodeCollection
        .findOne({ OldId: result.ChargeCodeId });
    const serviceTicket = await ServiceTicketCollection
        .findOne({ OldId: result.ServiceTicketId });

    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.VendorBillId = vendorBill && vendorBill._id || null;
    result.ChargeCodeId = chargeCode && chargeCode._id || null;
    result.VendorId = vendor && vendor._id || null;
    result.ServiceTicketId = serviceTicket && serviceTicket._id || null;
};


export const up = async () => {
    await ServiceTicketCharge.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await ServiceTicketChargeCollection.insertMany(results);
    });
};

export const down = async () => {
    await ServiceTicketChargeCollection.deleteMany({});
};