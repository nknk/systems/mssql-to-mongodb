import {
    JobDocument,
    TenantCollection,
    JobDocumentCollection,
    JobCollection,
    DocumentTagCollection
} from "../models";

const associateRelations = async (result: JobDocument) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const job = await JobCollection
        .findOne({ OldId: result.JobId });
    const documentTag = await DocumentTagCollection
        .findOne({ OldId: result.DocumentTagId });
    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.JobId = job && job._id || null;
    result.DocumentTagId = documentTag && documentTag._id || null;
};

export const up = async () => {
    await JobDocument.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await JobDocumentCollection.insertMany(results);
    });
};

export const down = async () => {
    await JobDocumentCollection.deleteMany({});
};