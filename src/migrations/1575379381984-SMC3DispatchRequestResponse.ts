import {
    SMC3DispatchRequestResponse,
    SMC3DispatchRequestResponseCollection,
    ShipmentCollection
} from "../models";

const associateRelations = async (result: SMC3DispatchRequestResponse) => {
    const shipment = await ShipmentCollection
        .findOne({ OldId: result.ShipmentId });
    result.OldId = result.Id;
    result.ShipmentId = shipment && shipment._id || null;
};

export const up = async () => {
    await SMC3DispatchRequestResponse
        .rollPagination(async (results) => {
            for (const result of results) {
                await associateRelations(result);
            };
            await SMC3DispatchRequestResponseCollection.insertMany(results);
        });
};

export const down = async () => {
    await SMC3DispatchRequestResponseCollection.deleteMany({});
};