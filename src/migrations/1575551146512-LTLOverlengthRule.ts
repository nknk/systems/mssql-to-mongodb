import {
    TenantCollection,
    VendorRatingCollection,
    ChargeCodeCollection,
    LTLOverlengthRule,
    LTLOverlengthRuleCollection
} from "../models";

export const up = async () => {
    await LTLOverlengthRule
        .rollPagination(async (results) => {
            for (const result of results) {
                const tenant = await TenantCollection
                    .findOne({ OldId: result.TenantId });
                const vendorRating = await VendorRatingCollection
                    .findOne({ OldId: result.VendorRatingId });
                const chargeCode = await ChargeCodeCollection
                    .findOne({ OldId: result.ChargeCodeId });
                result.TenantId = tenant._id;
                result.OldId = result.Id;
                result.VendorRatingId = vendorRating ? vendorRating._id : null;
                result.ChargeCodeId = chargeCode ? chargeCode._id : null;
            };

            await LTLOverlengthRuleCollection
                .insertMany(results);
        });


};

export const down = async () => {
    await LTLOverlengthRuleCollection.deleteMany({});
};

