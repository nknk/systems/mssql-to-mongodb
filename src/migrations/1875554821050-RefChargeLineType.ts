import { RefChargeLineType, RefChargeLineTypeCollection } from "../models";

export const up = async () => {
    await RefChargeLineType
        .rollPagination(async (results) => {
            await RefChargeLineTypeCollection
                .insertMany(results);
        });
};

export const down = async () => {
    await RefChargeLineTypeCollection.deleteMany({});
};