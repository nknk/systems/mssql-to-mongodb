import {
    TenantCollection,
    UserCollection,
    MacroPointOrder, 
    MacroPointOrderCollection,
} from "../models";
export const up = async () => {
    await MacroPointOrder
        .rollPagination(async (results) => {
            for (const result of results) {
                const tenant = await TenantCollection
                    .findOne({ OldId: result.TenantId });
                const user = await UserCollection
                    .findOne({ OldId: result.UserId });

                result.TenantId = tenant ? tenant._id : null;
                result.UserId = user ? user._id : null;
            }
            await MacroPointOrderCollection
                .insertMany(results);
        }, {
            attributes: ["*", ["Id", "OldId"]]
        });
};

export const down = async () => {
    await MacroPointOrderCollection
        .deleteMany({});
};

