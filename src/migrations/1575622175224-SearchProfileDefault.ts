import {
    UserCollection,
    SearchProfileDefault,
    SearchProfileDefaultCollection,
    TenantCollection
} from "../models";

const associateRelations = async (result: SearchProfileDefault) => {
    const tenant = await TenantCollection
    .findOne({ OldId: result.TenantId });
    const user = await UserCollection
    .findOne({ OldId: result.UserId });
    result.OldId = result.Id;
    result.TenantId = tenant? tenant._id: null;
    result.UserId = user? user._id: null;

};

export const up = async () => {
    await SearchProfileDefault.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await SearchProfileDefaultCollection.insertMany(results);
    });
};

export const down = async () => {
    await SearchProfileDefaultCollection.deleteMany({});
};