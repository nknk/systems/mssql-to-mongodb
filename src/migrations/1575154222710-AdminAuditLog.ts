import {
    AdminAuditLog,
    AdminAuditLogCollection,
    AdminUserCollection
} from "../models";

export const up = async () => {
    await AdminAuditLog.rollPagination(async (logs) => {
        for (const log of logs) {
            const adminUser = await AdminUserCollection
                .findOne({ OldId: log.AdminUserId });
            const adminId = adminUser && adminUser._id || null;
            log.AdminUserId = adminId;
            log.EntityId = adminId;
        }
        await AdminAuditLogCollection.insertMany(logs);
    }, {
        attributes: ["*", ["Id", "OldId"]],
    });

};

export const down = async () => {
    await AdminAuditLogCollection.deleteMany({});
};

