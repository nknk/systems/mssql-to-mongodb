import {
    ChangeLog,
    TenantCollection,
    ChangeLogCollection,
    UserCollection
} from "../models";

const associateRelations = async (log: ChangeLog) => {
    const tenant = await TenantCollection
        .findOne({ OldId: log.TenantId });
    const user = await UserCollection
        .findOne({ OldId: log.UserId });
    log.TenantId = tenant._id;
    log.OldId = log.Id;
    log.UserId = user ? user._id : null;
};

export const up = async () => {
    await ChangeLog.rollPagination(async (logs) => {
        for (const log of logs) {
            await associateRelations(log);
        };
        await ChangeLogCollection.insertMany(logs);
    });
};

export const down = async () => {
    await ChangeLogCollection.deleteMany({});
};