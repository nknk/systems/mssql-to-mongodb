import {
    VendorGroupMap,
    TenantCollection,
    VendorGroupMapCollection,
    VendorGroupCollection,
    VendorCollection
} from "../models";

const associateRelations = async (result: VendorGroupMap) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const vendorGroup = await VendorGroupCollection
        .findOne({ OldId: result.VendorGroupId });
    const vendor = await VendorCollection
        .findOne({ OldId: result.VendorId });
    result.TenantId = tenant._id;
    result.VendorGroupId = vendorGroup && vendorGroup._id || null;
    result.VendorId = vendor && vendor._id || null;
};

export const up = async () => {
    const results = await VendorGroupMap
        .findAll({
            raw: true,
            attributes: ["TenantId", "VendorGroupId", "VendorId"]
        });
    for (const result of results) {
        await associateRelations(result);
    };
    await VendorGroupMapCollection.insertMany(results);
};

export const down = async () => {
    await VendorGroupMapCollection.deleteMany({});
};