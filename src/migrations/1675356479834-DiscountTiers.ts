import {
    TenantCollection,
    ChargeCodeCollection,
    DiscountTier,
    RegionCollection,
    DiscountTierCollection,
} from "../models";
import { VendorRatingCollection } from "../models/vendor_rating";

export const up = async () => {
    await DiscountTier
        .rollPagination(async (tiers) => {
            for (const tier of tiers) {
                const tenant = await TenantCollection
                    .findOne({ OldId: tier.TenantId });
                const vendorRating = await VendorRatingCollection
                    .findOne({ OldId: tier.VendorRatingId });
                const freightChargeCode = await ChargeCodeCollection
                    .findOne({ OldId: tier.FreightChargeCodeId });
                const originRegion = await RegionCollection
                    .findOne({ OldId: tier.OriginRegionId });
                const destinationRegion = await RegionCollection
                    .findOne({ OldId: tier.DestinationRegionId });

                tier.TenantId = tenant._id;
                tier.FreightChargeCodeId = freightChargeCode && freightChargeCode._id || null;
                tier.OriginRegionId = originRegion && originRegion._id || null;
                tier.DestinationRegionId = destinationRegion && destinationRegion._id || null;
                tier.VendorRatingId = vendorRating && vendorRating._id || null;
                tier.OldId = tier.Id;
            }
            await DiscountTierCollection
                .insertMany(tiers);
        });  

};

export const down = async () => {
    await DiscountTierCollection.deleteMany({});
};

