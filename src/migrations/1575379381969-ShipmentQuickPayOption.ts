import {
    ShipmentQuickPayOption,
    TenantCollection,
    ShipmentQuickPayOptionCollection,
    ShipmentCollection
} from "../models";

const associateRelations = async (result: ShipmentQuickPayOption) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const shipment = await ShipmentCollection
        .findOne({ OldId: result.ShipmentId });

    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.ShipmentId = shipment && shipment._id || null;
};

export const up = async () => {
    await ShipmentQuickPayOption.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await ShipmentQuickPayOptionCollection.insertMany(results);
    });
};

export const down = async () => {
    await ShipmentQuickPayOptionCollection.deleteMany({});
};