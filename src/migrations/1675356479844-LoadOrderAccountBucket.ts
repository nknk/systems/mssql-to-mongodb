import {
    TenantCollection,
    LoadOrderCollection,
    LoadOrderAccountBucket,
    AccountBucketCollection,
    LoadOrderAccountBucketCollection,
} from "../models";

export const up = async () => {
    await LoadOrderAccountBucket
        .rollPagination(async (buckets) => {
            for (const bucket of buckets) {
                const tenant = await TenantCollection
                    .findOne({ OldId: bucket.TenantId });
                const loadOrder = await LoadOrderCollection
                    .findOne({ OldId: bucket.LoadOrderId });
                const accountBucket = await AccountBucketCollection
                    .findOne({ OldId: bucket.AccountBucketId });

                bucket.LoadOrderId = loadOrder && loadOrder._id || null;
                bucket.AccountBucketId = accountBucket && accountBucket._id || null;
                bucket.TenantId = tenant._id;
                bucket.OldId = bucket.Id;
            };
            await LoadOrderAccountBucketCollection
            .insertMany(buckets);
        });
};

export const down = async () => {
    await LoadOrderAccountBucketCollection.deleteMany({});
};

