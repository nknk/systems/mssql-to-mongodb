import {
    VendorBillDetail,
    TenantCollection,
    VendorBillDetailCollection,
    ChargeCodeCollection,
    VendorBillCollection,
    AccountBucketCollection
} from "../models";

const associateRelations = async (result: VendorBillDetail) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const vendorBill = await VendorBillCollection
        .findOne({ OldId: result.VendorBillId });
    const chargeCode = await ChargeCodeCollection
        .findOne({ OldId: result.ChargeCodeId });
    const accountBucket = await AccountBucketCollection
        .findOne({ OldId: result.AccountBucketId });

    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.VendorBillId = vendorBill && vendorBill._id || null;
    result.ChargeCodeId = chargeCode && chargeCode._id || null;
    result.AccountBucketId = accountBucket && accountBucket._id || null;
};


export const up = async () => {
    await VendorBillDetail.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await VendorBillDetailCollection.insertMany(results);
    });
};

export const down = async () => {
    await VendorBillDetailCollection.deleteMany({});
};