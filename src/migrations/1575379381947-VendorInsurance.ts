import {
    VendorInsurance,
    TenantCollection,
    VendorInsuranceCollection,
    InsuranceTypeCollection,
    VendorCollection
} from "../models";

const associateRelations = async (result: VendorInsurance) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const vendor = await VendorCollection
        .findOne({ OldId: result.VendorId });
    const insuranceType = await InsuranceTypeCollection
        .findOne({ OldId: result.InsuranceTypeId });    

    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.VendorId = vendor && vendor._id || null;
    result.InsuranceTypeId = insuranceType && insuranceType._id || null;
};

export const up = async () => {
    await VendorInsurance.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await VendorInsuranceCollection.insertMany(results);
    });
};

export const down = async () => {
    await VendorInsuranceCollection.deleteMany({});
};