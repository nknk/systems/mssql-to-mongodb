import {
    AccountBucketUnit,
    AccountBucketUnitCollection,
    ObjectAssociationCollection,
} from "../models";
import sequelize from "../config/sequelize";
import { QueryTypes } from "sequelize";


export const up = async () => {

    const associations: any[] = [];

    await AccountBucketUnit
        .rollPagination(async (units) => {
            for (const unit of units) {
                const unitDoc = await AccountBucketUnitCollection
                    .findOne({ OldId: unit.Id });

                if (!unitDoc) {
                    continue;
                }

                const result: any = await sequelize
                    .query(
                        `SELECT dbo.AccountBucketUnitIdUsageCount(:Id) 
                        AS UsageCount;`,
                        {
                            replacements: { Id: unit.Id },
                            type: QueryTypes.SELECT,
                            plain: true
                        });

                const Occurrences = parseInt(result.UsageCount);

                if (Occurrences > 0) {
                    associations.push({
                        EntityId: unitDoc._id,
                        EntityName: "LogisticsPlus.Eship.RestClient.Models.AccountBucketUnit",
                        Occurrences,
                        Status: 0,
                    });
                }
            }

        });


    if (associations.length) {
        await ObjectAssociationCollection
            .insertMany(associations);
    }
};

export const down = async () => {
    ObjectAssociationCollection.deleteMany({
        EntityName: "LogisticsPlus.Eship.RestClient.Models.AccountBucketUnit"
    });
};

