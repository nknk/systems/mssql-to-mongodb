import { RefEquipmentGroup, RefEquipmentGroupCollection } from "../models";

export const up = async () => {
    await RefEquipmentGroup
        .rollPagination(async (results) => {
            await RefEquipmentGroupCollection
                .insertMany(results);
        });

};

export const down = async () => {
    await RefEquipmentGroupCollection.deleteMany({});
};