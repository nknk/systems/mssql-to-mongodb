import {
    ReportCustomizationGroup,
    ReportCustomizationGroupCollection,
    TenantCollection,
    GroupCollection,
    ReportConfigurationCollection
} from "../models";

export const up = async () => {

    await ReportCustomizationGroup
        .rollByRow("ReportConfigurationId", async (results) => {
            for (const result of results) {
                const tenant = await TenantCollection.findOne({ OldId: result.TenantId });
                const group = await GroupCollection.findOne({ OldId: result.GroupId });
                const reportConfiguration = await ReportConfigurationCollection
                    .findOne({ OldId: result.ReportConfigurationId });
                result.TenantId = tenant ? tenant._id : null;
                result.GroupId = group ? group._id : null;
                result.ReportConfigurationId = reportConfiguration ? reportConfiguration._id : null;
            }
            await ReportCustomizationGroupCollection.insertMany(results);
        });
};

export const down = async () => {
    await ReportCustomizationGroupCollection.deleteMany({});
};

