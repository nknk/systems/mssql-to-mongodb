import {
    TenantCollection,
    BatchRateData,
    ChargeCodeCollection,
    UserCollection,
    PackageTypeCollection,
    BatchRateDataCollection,
} from "../models";
import { VendorRatingCollection } from "../models/vendor_rating";

export const up = async () => {
    await BatchRateData
        .rollPagination(async (data) => {
            for (const datum of data) {
                const tenant = await TenantCollection
                    .findOne({ OldId: datum.TenantId });
                const chargeCode = await ChargeCodeCollection
                    .findOne({ OldId: datum.ChargeCodeId });
                const user = await UserCollection
                    .findOne({ OldId: datum.SubmittedByUserId });
                const packageType = await PackageTypeCollection
                    .findOne({ OldId: datum.PackageTypeId });
                const vendorRating = await VendorRatingCollection
                    .findOne({ OldId: datum.VendorRatingId });

                datum.TenantId = tenant._id;
                datum.ChargeCodeId = chargeCode && chargeCode._id || null;
                datum.PackageTypeId = packageType && packageType._id || null;
                datum.SubmittedByUserId = user && user._id || null;
                datum.VendorRatingId = vendorRating && vendorRating._id || null;
                datum.OldId = datum.Id;
            }
            await BatchRateDataCollection
                .insertMany(data);
        });  

};

export const down = async () => {
    await BatchRateDataCollection.deleteMany({});
};

