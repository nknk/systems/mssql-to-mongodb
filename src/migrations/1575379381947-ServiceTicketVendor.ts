import {
    ServiceTicketVendor,
    TenantCollection,
    ServiceTicketVendorCollection,
    ServiceTicketCollection,
    VendorCollection
} from "../models";

const associateRelations = async (result: ServiceTicketVendor) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const serviceTicket = await ServiceTicketCollection
        .findOne({ OldId: result.ServiceTicketId });
    const vendor = await VendorCollection
        .findOne({ OldId: result.VendorId });

    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.ServiceTicketId = serviceTicket && serviceTicket._id || null;
    result.VendorId = vendor && vendor._id || null;
};


export const up = async () => {
    await ServiceTicketVendor.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await ServiceTicketVendorCollection.insertMany(results);
    });
};

export const down = async () => {
    await ServiceTicketVendorCollection.deleteMany({});
};