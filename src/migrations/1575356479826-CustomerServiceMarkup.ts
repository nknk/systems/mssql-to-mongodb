import {
    TenantCollection,
    ServiceCollection,
    CustomerServiceMarkup,
    CustomerRatingCollection,
    CustomerServiceMarkupCollection,
} from "../models";

const associateRelations = async (markup: CustomerServiceMarkup) => {
    const tenant = await TenantCollection
        .findOne({ OldId: markup.TenantId });
    markup.TenantId = tenant._id;

    const customerRating = await CustomerRatingCollection
        .findOne({ OldId: markup.CustomerRatingId });
    markup.CustomerRatingId = customerRating && customerRating._id || null;

    const service = await ServiceCollection
        .findOne({ OldId: markup.ServiceId });
    markup.ServiceId = service && service._id || null;
};

export const up = async () => {
    await CustomerServiceMarkup
        .rollPagination(async (markups) => {
            for (const markup of markups) {
                await associateRelations(markup);
            };
            await CustomerServiceMarkupCollection
                .insertMany(markups);
        });
};

export const down = async () => {
    await CustomerServiceMarkupCollection.deleteMany({});
};

