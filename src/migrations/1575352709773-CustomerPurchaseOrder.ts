import {
    TenantCollection,
    CountryCollection,
    CustomerCollection,
    CustomerPurchaseOrder,
    CustomerPurchaseOrderCollection,
} from "../models";

export const up = async () => {
    await CustomerPurchaseOrder.rollPagination(async (orders) => {
        for (const order of orders) {
            const tenant = await TenantCollection
                .findOne({ OldId: order.TenantId });
            const country = await CountryCollection
                .findOne({ OldId: order.CountryId });
            const customer = await CustomerCollection
                .findOne({ OldId: order.CustomerId });
            order.TenantId = tenant._id;
            order.OldId = order.Id;
            order.CountryId = country && country._id || null;
            order.CustomerId = customer && customer._id || null;
        };
        await CustomerPurchaseOrderCollection.insertMany(orders);
    });
};

export const down = async () => {
    await CustomerPurchaseOrderCollection.deleteMany({});
};

