import {
    ReportTemplateCollection,
    ReportTemplate
} from "../models";

const associateRelations = async (result: ReportTemplate) => {
    result.OldId = result.Id;
};

export const up = async () => {
    await ReportTemplate.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await ReportTemplateCollection.insertMany(results);
    });
};

export const down = async () => {
    await ReportTemplateCollection.deleteMany({});
};