import {
    TenantCollection,
    VendorCollection,
    CustomerTLTenderingProfileVendor,
    CustomerTLTenderingProfileVendorCollection,
    CustomerTLTenderingProfileCollection,
} from "../models";

export const up = async () => {
    await CustomerTLTenderingProfileVendor
        .rollPagination(async (vendors) => {
            for (const vendor of vendors) {
                const tenant = await TenantCollection
                    .findOne({ OldId: vendor.TenantId });

                const tlTenderingProfileId = await CustomerTLTenderingProfileCollection
                    .findOne({ OldId: vendor.TLTenderingProfileId });

                const vendorId = await VendorCollection
                    .findOne({ OldId: vendor.VendorId });

                vendor.TLTenderingProfileId = tlTenderingProfileId && tlTenderingProfileId._id || null;
                vendor.VendorId = vendorId && vendorId._id || null;
                vendor.TenantId = tenant._id;
                vendor.OldId = vendor.Id;
            }

            await CustomerTLTenderingProfileVendorCollection
                .insertMany(vendors);
        });
};

export const down = async () => {
    await CustomerTLTenderingProfileVendorCollection.deleteMany({});
};

