import {
    ClaimNote,
    TenantCollection,
    ClaimNoteCollection,
    UserCollection,
    ClaimCollection
} from "../models";

const associateRelations = async (result: ClaimNote) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const user = await UserCollection
        .findOne({ OldId: result.UserId });
    const claim = await ClaimCollection
        .findOne({ OldId: result.ClaimId });
    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.UserId = user && user._id || null;
    result.ClaimId = claim && claim._id || null;
};

export const up = async () => {
    await ClaimNote.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await ClaimNoteCollection.insertMany(results);
    });
};

export const down = async () => {
    await ClaimNoteCollection.deleteMany({});
};