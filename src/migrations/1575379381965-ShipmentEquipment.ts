import {
    ShipmentEquipment,
    TenantCollection,
    ShipmentEquipmentCollection,
    ShipmentCollection,
    EquipmentTypeCollection
} from "../models";

const associateRelations = async (result: ShipmentEquipment) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const shipment = await ShipmentCollection
        .findOne({ OldId: result.ShipmentId });
    const equipmentType = await EquipmentTypeCollection
        .findOne({ OldId: result.EquipmentTypeId });
    result.TenantId = tenant._id;
    result.ShipmentId = shipment && shipment._id || null;
    result.EquipmentTypeId = equipmentType && equipmentType._id || null;
};

export const up = async () => {
    await ShipmentEquipment
        .rollByRow("EquipmentTypeId", async (results) => {
            for (const result of results) {
                await associateRelations(result);
            };
            await ShipmentEquipmentCollection.insertMany(results);
        });
};

export const down = async () => {
    await ShipmentEquipmentCollection.deleteMany({});
};