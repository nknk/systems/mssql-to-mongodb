import {
    TenantCollection,
    CustomerControlAccount,
    CustomerControlAccountCollection,
    CountryCollection,
    CustomerCollection,
} from "../models";

export const up = async () => {
    const results = await CustomerControlAccount.findAll({ raw: true });

    for (const result of results) {
        const tenant = await TenantCollection.findOne({ OldId: result.TenantId });
        const country = await CountryCollection.findOne({ OldId: result.CountryId });
        const customer = await CustomerCollection.findOne({ OldId: result.CustomerId });
        result.TenantId = tenant._id;
        result.OldId = result.Id;
        result.CountryId = country? country._id: null;
        result.CustomerId = customer? customer._id: null;
    };

    await CustomerControlAccountCollection.insertMany(results);
};

export const down = async () => {
    await CustomerControlAccountCollection.deleteMany({});
};

