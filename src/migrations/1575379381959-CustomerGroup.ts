import {
    TenantCollection,
    CustomerGroup,
    CustomerGroupCollection,
} from "../models";

export const up = async () => {
    await CustomerGroup
        .rollPagination(async (groups) => {
            for (const group of groups) {
                const tenant = await TenantCollection
                    .findOne({ OldId: group.TenantId });
                group.TenantId = tenant._id;
                group.OldId = group.Id;
            };
            await CustomerGroupCollection
                .insertMany(groups);
        });
};

export const down = async () => {
    await CustomerGroupCollection.deleteMany({});
};

