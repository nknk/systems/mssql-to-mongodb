import { RefInvoiceDetailReferenceType, RefInvoiceDetailReferenceTypeCollection } from "../models";

export const up = async () => {
    await RefInvoiceDetailReferenceType
        .rollPagination(async (results) => {
            await RefInvoiceDetailReferenceTypeCollection
                .insertMany(results);
        });
};

export const down = async () => {
    await RefInvoiceDetailReferenceTypeCollection
    .deleteMany({});
};