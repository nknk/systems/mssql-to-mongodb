import { TenantCollection } from "../models";
import { FailureCodeCollection, FailureCode } from "../models";

export const up = async () => {
    const results = await FailureCode.findAll({ raw: true });

    for (const result of results) {
        const tenant = await TenantCollection.findOne({ OldId: result.TenantId });
        result.TenantId = tenant._id;
        result.OldId = result.Id;
    };

    await FailureCodeCollection.insertMany(results);
};

export const down = async () => {
    await FailureCodeCollection.deleteMany({});
};

