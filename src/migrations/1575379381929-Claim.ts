import {
    Claim,
    TenantCollection,
    ClaimCollection,
    UserCollection,
    CustomerCollection
} from "../models";

const associateRelations = async (result: Claim) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const user = await UserCollection
        .findOne({ OldId: result.UserId });
    const customer = await CustomerCollection
        .findOne({ OldId: result.CustomerId });
    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.UserId = user && user._id || null;
    result.CustomerId = customer && customer._id || null;
};

export const up = async () => {
    await Claim.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await ClaimCollection.insertMany(results);
    });
};

export const down = async () => {
    await ClaimCollection.deleteMany({});
};