import { RefMileageEngine, RefMileageEngineCollection } from "../models";

export const up = async () => {
    await RefMileageEngine
        .rollPagination(async (results) => {
            await RefMileageEngineCollection
                .insertMany(results);
        });

};

export const down = async () => {
    await RefMileageEngineCollection.deleteMany({});
};