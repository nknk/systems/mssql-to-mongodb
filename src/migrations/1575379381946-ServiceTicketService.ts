import {
    ServiceTicketService,
    TenantCollection,
    ServiceTicketServiceCollection,
    ServiceTicketCollection,
    ServiceCollection
} from "../models";

const associateRelations = async (result: ServiceTicketService) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const serviceTicket = await ServiceTicketCollection
        .findOne({ OldId: result.ServiceTicketId });
    const service = await ServiceCollection
        .findOne({ OldId: result.ServiceId });
    result.TenantId = tenant._id;
    result.ServiceTicketId = serviceTicket && serviceTicket._id || null;
    result.ServiceId = service && service._id || null;
};

export const up = async () => {
    const results = await ServiceTicketService.findAll({
        raw: true,
        attributes: ["*"]
    });
    for (const result of results) {
        await associateRelations(result);
    };
    await ServiceTicketServiceCollection.insertMany(results);
};

export const down = async () => {
    await ServiceTicketServiceCollection.deleteMany({});
};