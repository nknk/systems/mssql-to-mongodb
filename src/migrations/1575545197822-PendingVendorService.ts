import {
    TenantCollection,
    PendingVendorCollection,
    PendingVendorService,
    PendingVendorServiceCollection,
    ServiceCollection
} from "../models";

export const up = async () => {
    const results = await PendingVendorService
        .findAll({ raw: true, attributes: ["TenantId", "ServiceId", "PendingVendorId"] });

    for (const result of results) {
        const tenant = await TenantCollection
            .findOne({ OldId: result.TenantId });
        const pendingVendor = await PendingVendorCollection
            .findOne({ OldId: result.PendingVendorId });
        const service = await ServiceCollection
            .findOne({ OldId: result.ServiceId });
        result.TenantId = tenant._id;
        result.ServiceId = service? service._id: null;
        result.PendingVendorId = pendingVendor? pendingVendor._id: null;
    };

    await PendingVendorServiceCollection.insertMany(results);
};

export const down = async () => {
    await PendingVendorServiceCollection.deleteMany({});
};

