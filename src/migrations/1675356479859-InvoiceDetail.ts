import {
    TenantCollection,
    InvoiceDetail,
    AccountBucketCollection,
    ChargeCodeCollection,
    InvoiceCollection,
    InvoiceDetailCollection,
} from "../models";

export const up = async () => {
    await InvoiceDetail
        .rollPagination(async (details) => {
            for (const detail of details) {
                const tenant = await TenantCollection
                    .findOne({ OldId: detail.TenantId });
                const accountBucket = await AccountBucketCollection
                    .findOne({ OldId: detail.AccountBucketId });
                const chargeCode = await ChargeCodeCollection
                    .findOne({ OldId: detail.ChargeCodeId });
                const invoice = await InvoiceCollection
                    .findOne({ OldId: detail.InvoiceId });


                detail.TenantId = tenant._id;
                detail.AccountBucketId = accountBucket ? accountBucket._id : null;
                detail.ChargeCodeId = chargeCode ? chargeCode._id : null;
                detail.InvoiceId = invoice ? invoice._id : null;
                detail.OldId = detail.Id;
            };
            await InvoiceDetailCollection
                .insertMany(details);
        });
};

export const down = async () => {
    await InvoiceDetailCollection.deleteMany({});
};

