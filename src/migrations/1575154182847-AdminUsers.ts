import { AdminUser, AdminUserPermission, AdminUserCollection } from "../models";

export const up = async () => {
    const results = await AdminUser
        .findAll(
            {
                include: [AdminUserPermission],
            }
        );
    const jsonResults: Array<any> = [];
    results.map(result => {
        const json: Record<any, any> = result.toJSON();
        json.OldId = json.Id;
        jsonResults.push(json);
    });
    await AdminUserCollection.insertMany(jsonResults);
};

export const down = async () => {
    await AdminUserCollection.deleteMany({});
};
