import { RefEdiDocumentType, RefEdiDocumentTypeCollection } from "../models";

export const up = async () => {
    await RefEdiDocumentType
        .rollPagination(async (results) => {
            await RefEdiDocumentTypeCollection.insertMany(results);
        });
};

export const down = async () => {
    await RefEdiDocumentTypeCollection.deleteMany({});
};