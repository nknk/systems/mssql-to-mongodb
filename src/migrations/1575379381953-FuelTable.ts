import {
    FuelTable,
    TenantCollection,
    FuelTableCollection
} from "../models";

const associateRelations = async (result: FuelTable) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId }); 
    result.TenantId = tenant._id;
    result.OldId = result.Id;
};

export const up = async () => {
    await FuelTable.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await FuelTableCollection.insertMany(results);
    });
};

export const down = async () => {
    await FuelTableCollection.deleteMany({});
};