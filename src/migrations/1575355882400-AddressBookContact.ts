import {
    TenantCollection,
    AddressBookCollection,
    AddressBookContact,
    AddressBookContactCollection,
    ContactTypeCollection,
} from "../models";

export const up = async () => {
    await AddressBookContact.rollPagination(async (contacts) => {
        for (const contact of contacts) {
            const tenant = await TenantCollection
                .findOne({ OldId: contact.TenantId });
            const contactType = await ContactTypeCollection
                .findOne({ OldId: contact.ContactTypeId });
            const addressBook = await AddressBookCollection
                .findOne({ OldId: contact.AddressBookId });
            contact.TenantId = tenant._id;
            contact.ContactTypeId = contactType && contactType._id || null;
            contact.AddressBookId = addressBook && addressBook._id || null;
        }
        await AddressBookContactCollection.insertMany(contacts);
    },  { attributes: ["*", ["Id", "OldId"]] });

};

export const down = async () => {
    await AddressBookContactCollection.deleteMany({});
};

