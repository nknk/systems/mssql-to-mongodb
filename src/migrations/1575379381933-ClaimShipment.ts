import {
    ClaimShipment,
    TenantCollection,
    ClaimShipmentCollection,
    ShipmentCollection,
    ClaimCollection
} from "../models";

const associateRelations = async (result: ClaimShipment) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const shipment = await ShipmentCollection
        .findOne({ OldId: result.ShipmentId });
    const claim = await ClaimCollection
        .findOne({ OldId: result.ClaimId });
    result.TenantId = tenant._id;
    result.ShipmentId = shipment && shipment._id || null;
    result.ClaimId = claim && claim._id || null;
};

export const up = async () => {
    const results = await ClaimShipment
        .findAll({
            raw: true,
            attributes: ["TenantId", "ClaimId", "ShipmentId"]
        });
    for (const result of results) {
        await associateRelations(result);
    };
    await ClaimShipmentCollection.insertMany(results);
};

export const down = async () => {
    await ClaimShipmentCollection.deleteMany({});
};