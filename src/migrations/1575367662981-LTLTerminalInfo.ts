import {
    TenantCollection,
    LTLTerminalInfo,
    LTLTerminalInfoCollection,
    CountryCollection,
} from "../models";

export const up = async () => {
    await LTLTerminalInfo
        .rollPagination(async (results) => {
            for (const result of results) {
                const tenant = await TenantCollection
                    .findOne({ OldId: result.TenantId });
                const country = await CountryCollection
                    .findOne({ OldId: result.CountryId });
                result.TenantId = tenant._id;
                result.ShipmentId = null;
                result.CountryId = country ? country._id : null;
                result.OldId = result.Id;
            };
            await LTLTerminalInfoCollection.insertMany(results);
        });


};

export const down = async () => {
    await LTLTerminalInfoCollection.deleteMany({});
};

