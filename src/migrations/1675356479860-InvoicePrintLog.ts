import {
    TenantCollection,
    InvoicePrintLog,
    InvoiceCollection,
    InvoicePrintLogCollection,
} from "../models";

export const up = async () => {
    await InvoicePrintLog
        .rollPagination(async (printDetails) => {
            for (const printDetail of printDetails) {
                const tenant = await TenantCollection
                    .findOne({ OldId: printDetail.TenantId });
                const invoice = await InvoiceCollection
                    .findOne({ OldId: printDetail.InvoiceId });

                printDetail.TenantId = tenant._id;
                printDetail.InvoiceId = invoice? invoice._id: null;
                printDetail.OldId = printDetail.Id;
            };
            await InvoicePrintLogCollection
                .insertMany(printDetails);
        });
};

export const down = async () => {
    await InvoicePrintLogCollection.deleteMany({});
};

