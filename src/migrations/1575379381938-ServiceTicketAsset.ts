import {
    ServiceTicketAsset,
    TenantCollection,
    ServiceTicketAssetCollection,
    ServiceTicketCollection,
    AssetCollection
} from "../models";

const associateRelations = async (result: ServiceTicketAsset) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const serviceTicket = await ServiceTicketCollection
        .findOne({ OldId: result.ServiceTicketId });
    const driverAsset = await AssetCollection
        .findOne({ OldId: result.DriverAssetId });
    const trailerAsset = await AssetCollection
        .findOne({ OldId: result.TrailerAssetId });
    const tractorAsset = await AssetCollection
        .findOne({ OldId: result.TractorAssetId });
    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.ServiceTicketId = serviceTicket && serviceTicket._id || null;
    result.DriverAssetId = driverAsset && driverAsset._id || null;
    result.TrailerAssetId = trailerAsset && trailerAsset._id || null;
    result.TractorAssetId = tractorAsset && tractorAsset._id || null;
};

export const up = async () => {
    await ServiceTicketAsset.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await ServiceTicketAssetCollection.insertMany(results);
    });
};

export const down = async () => {
    await ServiceTicketAssetCollection.deleteMany({});
};