import {
    VendorInsuranceLiabilityTier,
    TenantCollection,
    VendorInsuranceLiabilityTierCollection,
    VendorRatingCollection
} from "../models";

const associateRelations = async (result: VendorInsuranceLiabilityTier) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const VendorRating = await VendorRatingCollection
        .findOne({ OldId: result.VendorRatingId });

    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.VendorRatingId = VendorRating && VendorRating._id || null;
};

export const up = async () => {
    await VendorInsuranceLiabilityTier.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await VendorInsuranceLiabilityTierCollection.insertMany(results);
    });
};

export const down = async () => {
    await VendorInsuranceLiabilityTierCollection.deleteMany({});
};