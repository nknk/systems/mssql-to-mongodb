import { TenantCollection, ShipmentPriority, ShipmentPriorityCollection } from "../models";

export const up = async () => {
    await ShipmentPriority.
        rollPagination(async (results) => {
            for (const result of results) {
                const tenant = await TenantCollection
                    .findOne({ OldId: result.TenantId });
                result.TenantId = tenant._id;
                result.OldId = result.Id;
            };
            await ShipmentPriorityCollection.insertMany(results);
        });


};

export const down = async () => {
    await ShipmentPriorityCollection.deleteMany({});
};

