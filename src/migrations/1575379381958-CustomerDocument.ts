import {
    TenantCollection,
    CustomerCollection,
    CustomerDocument,
    CustomerDocumentCollection,
    DocumentTagCollection,
} from "../models";

export const up = async () => {
    await CustomerDocument
        .rollPagination(async (documents) => {
            for (const document of documents) {
                const tenant = await TenantCollection
                    .findOne({ OldId: document.TenantId });

                const customer = await CustomerCollection
                    .findOne({ OldId: document.CustomerId });

                const docTag = await DocumentTagCollection
                    .findOne({ OldId: document.DocumentTagId });

                document.TenantId = tenant._id;
                document.CustomerId = customer && customer._id || null;
                document.OldId = document.Id;
                document.DocumentTagId = docTag ? docTag._id : null;
            };
            await CustomerDocumentCollection.insertMany(documents);
        });
};

export const down = async () => {
    await CustomerDocumentCollection.deleteMany({});
};

