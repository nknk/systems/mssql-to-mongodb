import {
    TenantCollection,
    LoadOrderCollection,
    LoadOrderDocument,
    DocumentTagCollection,
    LoadOrderDocumentCollection,
} from "../models";

export const up = async () => {
    await LoadOrderDocument
        .rollPagination(async (documents) => {
            for (const document of documents) {
                const tenant = await TenantCollection
                    .findOne({ OldId: document.TenantId });
                const loadOrder = await LoadOrderCollection
                    .findOne({ OldId: document.LoadOrderId });
                const documentTag = await DocumentTagCollection
                    .findOne({ OldId: document.DocumentTagId });

                
                document.LoadOrderId = loadOrder && loadOrder._id || null;
                document.DocumentTagId = documentTag && documentTag._id || null;
                document.TenantId = tenant._id;
                document.OldId = document.Id;
            };
            await LoadOrderDocumentCollection
            .insertMany(documents);
        });
};

export const down = async () => {
    await LoadOrderDocumentCollection.deleteMany({});
};

