import {
    VendorDocument,
    TenantCollection,
    VendorDocumentCollection,
    DocumentTagCollection,
    VendorCollection
} from "../models";

const associateRelations = async (result: VendorDocument) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const vendor = await VendorCollection
        .findOne({ OldId: result.VendorId });
    const documentTag = await DocumentTagCollection
        .findOne({ OldId: result.DocumentTagId });    

    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.VendorId = vendor && vendor._id || null;
    result.DocumentTagId = documentTag && documentTag._id || null;
};

export const up = async () => {
    await VendorDocument.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await VendorDocumentCollection.insertMany(results);
    });
};

export const down = async () => {
    await VendorDocumentCollection.deleteMany({});
};