import {
    SmallPackRate,
    TenantCollection,
    SmallPackRateCollection,
    VendorCollection,
    CustomerRatingCollection,
    ChargeCodeCollection
} from "../models";

const associateRelations = async (result: SmallPackRate) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const vendor = await VendorCollection
        .findOne({ OldId: result.VendorId });
    const customerRating = await CustomerRatingCollection
        .findOne({ OldId: result.CustomerRatingId });
    const chargeCode = await ChargeCodeCollection
        .findOne({ OldId: result.ChargeCodeId });

    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.VendorId = vendor && vendor._id || null;
    result.CustomerRatingId = customerRating && customerRating._id || null;
    result.ChargeCodeId = chargeCode && chargeCode._id || null;
};

export const up = async () => {
    await SmallPackRate.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await SmallPackRateCollection.insertMany(results);
    });
};

export const down = async () => {
    await SmallPackRateCollection.deleteMany({});
};