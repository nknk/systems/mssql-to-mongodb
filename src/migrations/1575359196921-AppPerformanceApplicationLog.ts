import {
    AppPerformanceApplicationLog,
    AppPerformanceApplicationLogCollection,
} from "../models";

export const up = async () => {
    await AppPerformanceApplicationLog
        .rollPagination(async (logs) => {
            await AppPerformanceApplicationLogCollection
                .insertMany(logs);
        }, {
            attributes: ["*", ["AppLogId", "OldId"]]
        });
};

export const down = async () => {
    await AppPerformanceApplicationLogCollection
        .deleteMany({});
};

