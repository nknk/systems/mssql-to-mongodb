import {
    TenantCollection,
    LTLAccessorial,
    LTLAccessorialCollection,
    ServiceCollection,
    VendorRatingCollection
} from "../models";

export const up = async () => {
    await LTLAccessorial.
        rollPagination(async (results) => {
            for (const result of results) {
                const tenant = await TenantCollection
                    .findOne({ OldId: result.TenantId });
                const service = await ServiceCollection
                    .findOne({ OldId: result.ServiceId });
                const vendorRating = await VendorRatingCollection
                    .findOne({ OldId: result.VendorRatingId });
                result.TenantId = tenant._id;
                result.OldId = result.Id;
                result.ServiceId = service ? service._id : null;
                result.VendorRatingId = vendorRating ? vendorRating._id : null;
            };
            await LTLAccessorialCollection.insertMany(results);
        });
};

export const down = async () => {
    await LTLAccessorialCollection.deleteMany({});
};

