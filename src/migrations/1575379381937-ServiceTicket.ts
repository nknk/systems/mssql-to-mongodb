import {
    ServiceTicket,
    TenantCollection,
    ServiceTicketCollection,
    JobCollection,
    PrefixCollection,
    AccountBucketCollection,
    UserCollection,
    CustomerCollection,
    SalesRepresentativeCollection,
    ResellerAdditionCollection,
    AccountBucketUnitCollection,
    CustomerLocationCollection
} from "../models";

const associateRelations = async (result: ServiceTicket) => {

    const {
        PrefixId,
        AccountBucketId,
        UserId,
        CustomerId,
        SalesRepresentativeId,
        ResellerAdditionId,
        AccountBucketUnitId,
        OverrideCustomerLocationId,
        JobId,
    } = result;

    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });

    const prefix = await PrefixCollection
        .findOne({ OldId: PrefixId });

    const bucket = await AccountBucketCollection
        .findOne({ OldId: AccountBucketId });

    const user = await UserCollection
        .findOne({ OldId: UserId });

    const customer = await CustomerCollection
        .findOne({ OldId: CustomerId });

    const saleRep = await SalesRepresentativeCollection
        .findOne({ OldId: SalesRepresentativeId });

    const resellerAddition = await ResellerAdditionCollection
        .findOne({ OldId: ResellerAdditionId });

    const bucketUnit = await AccountBucketUnitCollection
        .findOne({ OldId: AccountBucketUnitId });

    const customerLocation = await CustomerLocationCollection
        .findOne({ OldId: OverrideCustomerLocationId });

    const job = await JobCollection
        .findOne({ OldId: JobId });

    result.OldId = result.Id;
    result.TenantId = tenant._id;
    result.JobId = job && job._id || null;
    result.PrefixId = prefix ? prefix._id : null;
    result.AccountBucketId = bucket ? bucket._id : null;
    result.UserId = user ? user._id : null;
    result.CustomerId = customer ? customer._id : null;
    result.SalesRepresentativeId = saleRep ? saleRep._id : null;
    result.ResellerAdditionId = resellerAddition ? resellerAddition._id : null;
    result.AccountBucketUnitId = bucketUnit ? bucketUnit._id : null;
    result.OverrideCustomerLocationId = customerLocation ? customerLocation._id : null;
};

export const up = async () => {
    await ServiceTicket.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await ServiceTicketCollection.insertMany(results);
    });
};

export const down = async () => {
    await ServiceTicketCollection.deleteMany({});
};