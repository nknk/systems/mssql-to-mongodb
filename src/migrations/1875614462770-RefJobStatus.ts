import { RefJobStatus, RefJobStatusCollection } from "../models";

export const up = async () => {
    await RefJobStatus
        .rollPagination(async (results) => {
            await RefJobStatusCollection.insertMany(results);
        });

};

export const down = async () => {
    await RefJobStatusCollection.deleteMany({});
};