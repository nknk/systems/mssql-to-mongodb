import {
    TenantCollection,
    LoadOrderContact,
    ContactTypeCollection,
    LoadOrderContactCollection,
    LoadOrderLocationCollection,
} from "../models";

export const up = async () => {
    await LoadOrderContact
        .rollPagination(async (contacts) => {
            for (const contact of contacts) {
                const tenant = await TenantCollection
                    .findOne({ OldId: contact.TenantId });
                const loadOrderLocation = await LoadOrderLocationCollection
                    .findOne({ OldId: contact.LoadOrderLocationId });
                const contactType = await ContactTypeCollection
                    .findOne({ OldId: contact.ContactTypeId });
                
                contact.LoadOrderLocationId = loadOrderLocation && loadOrderLocation._id || null;
                contact.ContactTypeId = contactType && contactType._id || null;
                contact.TenantId = tenant._id;
                contact.OldId = contact.Id;
            };
            await LoadOrderContactCollection
            .insertMany(contacts);
        });
};

export const down = async () => {
    await LoadOrderContactCollection.deleteMany({});
};

