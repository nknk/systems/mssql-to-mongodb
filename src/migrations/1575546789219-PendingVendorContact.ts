import {
    TenantCollection,
    PendingVendorCollection,
    PendingVendorContact,
    ContactTypeCollection,
    PendingVendorContactCollection
} from "../models";

export const up = async () => {
    const results = await PendingVendorContact.findAll({ raw: true });

    for (const result of results) {
        const tenant = await TenantCollection
            .findOne({ OldId: result.TenantId });
        const pendingVendorLocation = await PendingVendorCollection
            .findOne({ OldId: result.PendingVendorLocationId });
        const contactType = await ContactTypeCollection
            .findOne({ OldId: result.ContactTypeId });
        result.TenantId = tenant._id;
        result.OldId = result.Id;
        result.PendingVendorLocationId = pendingVendorLocation ?
            pendingVendorLocation._id : null;
        result.ContactTypeId = contactType ? contactType._id : null;
    };

    await PendingVendorContactCollection.insertMany(results);
};

export const down = async () => {
    await PendingVendorContactCollection.deleteMany({});
};

