import {
    ShipmentHistoricalData,
    ShipmentHistoricalDataCollection,
    ShipmentCollection
} from "../models";

const associateRelations = async (result: ShipmentHistoricalData) => {
    const shipment = await ShipmentCollection
        .findOne({ OldId: result.ShipmentId });
    result.OldId = result.Id;
    result.ShipmentId = shipment && shipment._id || null;
};

export const up = async () => {
    await ShipmentHistoricalData.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await ShipmentHistoricalDataCollection.insertMany(results);
    });
};

export const down = async () => {
    await ShipmentHistoricalDataCollection.deleteMany({});
};