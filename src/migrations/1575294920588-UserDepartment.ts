import { TenantCollection, UserDepartment, UserDepartmentCollection } from "../models";

export const up = async () => {
    const results = await UserDepartment.findAll({ raw: true });

    for (const result of results) {
        const tenant = await TenantCollection
            .findOne({ OldId: result.TenantId });
        result.TenantId = tenant._id;
        result.OldId = result.Id;
    };

    await UserDepartmentCollection.insertMany(results);
};

export const down = async () => {
    await UserDepartmentCollection.deleteMany({});
};

