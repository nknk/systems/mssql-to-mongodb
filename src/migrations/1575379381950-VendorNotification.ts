import {
    VendorNotification,
    TenantCollection,
    VendorNotificationCollection,
    VendorCommunicationCollection
} from "../models";

const associateRelations = async (result: VendorNotification) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const vendorCommunication = await VendorCommunicationCollection
        .findOne({ OldId: result.VendorCommunicationId });

    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.VendorCommunicationId = vendorCommunication && vendorCommunication._id || null;
};

export const up = async () => {
    await VendorNotification.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await VendorNotificationCollection.insertMany(results);
    });
};

export const down = async () => {
    await VendorNotificationCollection.deleteMany({});
};