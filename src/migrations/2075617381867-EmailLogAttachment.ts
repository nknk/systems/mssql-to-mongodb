import {
    TenantCollection,
    EmailLogCollection,
    EmailLogAttachment,
    EmailLogAttachmentCollection
} from "../models";


export const up = async () => {
    await EmailLogAttachment
        .rollPagination(async (results) => {

            for (const result of results) {

                const tenant = await TenantCollection
                    .findOne({ OldId: result.TenantId });

                result.TenantId = tenant ? tenant._id : null;

                if (result.EmailLogId.toString() === "0") {
                    result.EmailLogId = null;
                } else {
                    const emailLog = await EmailLogCollection
                        .findOne({ OldId: result.EmailLogId });
                    result.EmailLogId = emailLog ? emailLog._id : null;
                }

            }

            await EmailLogAttachmentCollection
                .insertMany(results);
            
        }, {
            attributes: ["*", ["Id", "OldId"]]
        });
};

export const down = async () => {
    await EmailLogAttachmentCollection
        .deleteMany({});
};

