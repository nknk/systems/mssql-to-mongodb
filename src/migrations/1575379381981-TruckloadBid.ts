import {
    TruckloadBid,
    TenantCollection,
    TruckloadBidCollection,
    LoadOrderCollection,
    CustomerTLTenderingProfileCollection,
    CustomerTLTenderingProfileLaneCollection,
    VendorCollection,
    UserCollection
} from "../models";

const associateRelations = async (result: TruckloadBid) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const loadOrder = await LoadOrderCollection
        .findOne({ OldId: result.LoadOrderId });
    const customerTLTenderingProfile = await CustomerTLTenderingProfileCollection
        .findOne({ OldId: result.TLTenderingProfileId });
    const customerTLTenderingProfileLane = await CustomerTLTenderingProfileLaneCollection
        .findOne({ OldId: result.TLLaneId });
    const firstPreferredVendor = await VendorCollection
        .findOne({ OldId: result.FirstPreferredVendorId });
    const secondPreferredVendor = await VendorCollection
        .findOne({ OldId: result.SecondPreferredVendorId });
    const thirdPreferredVendor = await VendorCollection
        .findOne({ OldId: result.ThirdPreferredVendorId });
    const user = await UserCollection
        .findOne({ OldId: result.UserId });

    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.LoadOrderId = loadOrder && loadOrder._id || null;
    result.TLTenderingProfileId = customerTLTenderingProfile && customerTLTenderingProfile._id || null;
    result.TLLaneId = customerTLTenderingProfileLane && customerTLTenderingProfileLane._id || null;
    result.FirstPreferredVendorId = firstPreferredVendor && firstPreferredVendor._id || null;
    result.SecondPreferredVendorId = secondPreferredVendor && secondPreferredVendor._id || null;
    result.ThirdPreferredVendorId = thirdPreferredVendor && thirdPreferredVendor._id || null;
    result.UserId = user && user._id || null;
};

export const up = async () => {
    await TruckloadBid.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await TruckloadBidCollection.insertMany(results);
    });
};

export const down = async () => {
    await TruckloadBidCollection.deleteMany({});
};