import {
    QlikUserCollection,
    QlikUser
} from "../models";

export const up = async () => {
    await QlikUser
        .rollByRow("userid", async (results) => {
            await QlikUserCollection
                .insertMany(results);
        });
};

export const down = async () => {
    await QlikUserCollection
        .deleteMany({});
};
