import {
    TenantCollection,
    VendorRatingCollection,
    LTLAdditionalCharge,
    LTLAdditionalChargeCollection,
    ChargeCodeCollection
} from "../models";

export const up = async () => {
    await LTLAdditionalCharge
        .rollPagination(async (results) => {
            for (const result of results) {
                const tenant = await TenantCollection
                    .findOne({ OldId: result.TenantId });
                const chargeCode = await ChargeCodeCollection
                    .findOne({ OldId: result.ChargeCodeId });
                const vendorRating = await VendorRatingCollection
                    .findOne({ OldId: result.VendorRatingId });
                result.TenantId = tenant._id;
                result.OldId = result.Id;
                result.ChargeCodeId = chargeCode ? chargeCode._id : null;
                result.VendorRatingId = vendorRating ? vendorRating._id : null;
            };

            await LTLAdditionalChargeCollection.insertMany(results);
        });
};

export const down = async () => {
    await LTLAdditionalChargeCollection.deleteMany({});
};

