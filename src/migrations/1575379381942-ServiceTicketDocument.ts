import {
    ServiceTicketDocument,
    TenantCollection,
    ServiceTicketDocumentCollection,
    ServiceTicketCollection,
    DocumentTagCollection,
    VendorBillCollection
} from "../models";

const associateRelations = async (result: ServiceTicketDocument) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const vendorBill = await VendorBillCollection
        .findOne({ OldId: result.VendorBillId });
    const documentTag = await DocumentTagCollection
        .findOne({ OldId: result.DocumentTagId });
    const serviceTicket = await ServiceTicketCollection
        .findOne({ OldId: result.ServiceTicketId });

    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.VendorBillId = vendorBill && vendorBill._id || null;
    result.DocumentTagId = documentTag && documentTag._id || null;
    result.ServiceTicketId = serviceTicket && serviceTicket._id || null;
};


export const up = async () => {
    await ServiceTicketDocument.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await ServiceTicketDocumentCollection.insertMany(results);
    });
};

export const down = async () => {
    await ServiceTicketDocumentCollection.deleteMany({});
};