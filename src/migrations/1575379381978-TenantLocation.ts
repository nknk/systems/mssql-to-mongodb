import {
    TenantLocation,
    TenantCollection,
    TenantLocationCollection,
    CountryCollection
} from "../models";

const associateRelations = async (result: TenantLocation) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const country = await CountryCollection
        .findOne({ OldId: result.CountryId });

    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.CountryId = country && country._id || null;
};

export const up = async () => {
    await TenantLocation.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await TenantLocationCollection.insertMany(results);
    });

    const tenants = await TenantCollection.find({});
    for (const tenant of tenants) {
        const mailingLocation = await TenantLocationCollection
            .findOne({ OldId: tenant.MailingLocationId });

        const billingLocation = await TenantLocationCollection
            .findOne({ OldId: tenant.BillingLocationId });

        tenant.MailingLocationId = mailingLocation ?
            mailingLocation._id : null;
        tenant.BillingLocationId = billingLocation ?
            billingLocation._id : null;

        await tenant.save();
    }

};

export const down = async () => {
    await TenantLocationCollection.deleteMany({});
};