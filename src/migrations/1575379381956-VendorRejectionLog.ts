import {
    VendorRejectionLog,
    TenantCollection,
    VendorRejectionLogCollection,
    VendorCollection,
    UserCollection,
    CustomerCollection
} from "../models";

const associateRelations = async (result: VendorRejectionLog) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const vendor = await VendorCollection
        .findOne({ OldId: result.VendorId });
    const user = await UserCollection
        .findOne({ OldId: result.UserId });
    const customer = await CustomerCollection
        .findOne({ OldId: result.CustomerId });

    result.TenantId = tenant._id;
    result.OldId = result.Id;
    result.VendorId = vendor && vendor._id || null;
    result.UserId = user && user._id || null;
    result.CustomerId = customer && customer._id || null;
};

export const up = async () => {
    await VendorRejectionLog.rollPagination(async (results) => {
        for (const result of results) {
            await associateRelations(result);
        };
        await VendorRejectionLogCollection.insertMany(results);
    });
};

export const down = async () => {
    await VendorRejectionLogCollection.deleteMany({});
};