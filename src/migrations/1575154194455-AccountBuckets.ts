import { AccountBucket, AccountBucketCollection, TenantCollection } from "../models";

export const up = async () => {
    const results = await AccountBucket
        .findAll({ raw: true });
    for (const result of results) {
        const tenant = await TenantCollection
            .findOne({ OldId: result.TenantId });
        result.TenantId = tenant._id;
        result.OldId = result.Id;
    };
    await AccountBucketCollection.insertMany(results);
};

export const down = async () => {
    await AccountBucketCollection.deleteMany({});
};

