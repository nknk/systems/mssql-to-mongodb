import {
    CheckCall,
    UserCollection,
    ShipmentCollection,
    CheckCallCollection,
    TenantCollection
} from "../models";

export const up = async () => {
    await CheckCall
        .rollPagination(async (results) => {
            for (const result of results) {
                const tenant = await TenantCollection
                    .findOne({ OldId: result.TenantId });
                const user = await UserCollection
                    .findOne({ OldId: result.UserId });
                const shipment = await ShipmentCollection
                    .findOne({ OldId: result.ShipmentId });
                result.TenantId = tenant ? tenant._id : null;
                result.UserId = user ? user._id : null;
                result.ShipmentId = shipment ? shipment._id : null;
                result.OldId = result.Id;
            }
            await CheckCallCollection.insertMany(results);
        });
};

export const down = async () => {
    await CheckCallCollection.deleteMany({});
};

