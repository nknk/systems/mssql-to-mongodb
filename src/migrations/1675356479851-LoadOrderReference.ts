import {
    TenantCollection,
    LoadOrderCollection,
    LoadOrderReference,
    LoadOrderReferenceCollection,
} from "../models";

export const up = async () => {
    await LoadOrderReference
        .rollPagination(async (references) => {
            for (const reference of references) {
                const tenant = await TenantCollection
                    .findOne({ OldId: reference.TenantId });
                const loadOrder = await LoadOrderCollection
                    .findOne({ OldId: reference.LoadOrderId });

                reference.LoadOrderId = loadOrder && loadOrder._id || null;
                reference.TenantId = tenant._id;
                reference.OldId = reference.Id;
            };
            await LoadOrderReferenceCollection
            .insertMany(references);
        });
};

export const down = async () => {
    await LoadOrderReferenceCollection.deleteMany({});
};

