import {
    ShoppedRateService,
    TenantCollection,
    ShoppedRateServiceCollection,
    ShoppedRateCollection,
    ServiceCollection
} from "../models";

const associateRelations = async (result: ShoppedRateService) => {
    const tenant = await TenantCollection
        .findOne({ OldId: result.TenantId });
    const shoppedRate = await ShoppedRateCollection
        .findOne({ OldId: result.ShoppedRateId });
    const service = await ServiceCollection
        .findOne({ OldId: result.ServiceId });

    result.TenantId = tenant._id;
    result.ShoppedRateId = shoppedRate && shoppedRate._id || null;
    result.ServiceId = service && service._id || null;
};

export const up = async () => {
    await ShoppedRateService
        .rollByRow("ServiceId", async (results) => {
            for (const result of results) {
                await associateRelations(result);
            };
            await ShoppedRateServiceCollection.insertMany(results);

        });
};

export const down = async () => {
    await ShoppedRateServiceCollection.deleteMany({});
};