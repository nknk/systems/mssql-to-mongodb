import {
    TenantCollection,
    VendorRatingCollection,
    LTLPackageSpecificRate,
    LTLPackageSpecificRateCollection,
    RegionCollection
} from "../models";

export const up = async () => {
    await LTLPackageSpecificRate
        .rollPagination(async (results) => {
            for (const result of results) {
                const tenant = await TenantCollection
                    .findOne({ OldId: result.TenantId });
                const vendorRating = await VendorRatingCollection
                    .findOne({ OldId: result.VendorRatingId });
                const originRegion = await RegionCollection
                    .findOne({ OldId: result.OriginRegionId });
                const destinationRegion = await RegionCollection
                    .findOne({ OldId: result.DestinationRegionId });
                result.TenantId = tenant._id;
                result.OldId = result.Id;
                result.VendorRatingId = vendorRating ?
                    vendorRating._id : null;
                result.OriginRegionId = originRegion ?
                    originRegion._id : null;
                result.DestinationRegionId = destinationRegion ?
                    destinationRegion._id : null;
            };

            await LTLPackageSpecificRateCollection
                .insertMany(results);
        });


};

export const down = async () => {
    await LTLPackageSpecificRateCollection.deleteMany({});
};

