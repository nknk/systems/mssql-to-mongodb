import {
    TenantCollection,
    CustomerContact,
    CustomerContactCollection,
    ContactTypeCollection,
    CustomerLocationCollection,
} from "../models";


const updateRelations = async (location: CustomerContact) => {
    const tenant = await TenantCollection
        .findOne({ OldId: location.TenantId });
    const contactType = await ContactTypeCollection
        .findOne({ OldId: location.ContactTypeId });
    const customerLocation = await CustomerLocationCollection
        .findOne({ OldId: location.CustomerLocationId });
    location.TenantId = tenant && tenant._id || null;
    location.CustomerLocationId = customerLocation && customerLocation._id || null;
    location.ContactTypeId = contactType && contactType._id || null;
};


export const up = async () => {
    await CustomerContact.rollPagination(async (locations) => {
        for (const location of locations) {
            await updateRelations(location);
        };
        await CustomerContactCollection
            .insertMany(locations);
    }, {
        attributes: ["*", ["Id", "OldId"]]
    });
};

export const down = async () => {
    await CustomerContactCollection
        .deleteMany({});
};