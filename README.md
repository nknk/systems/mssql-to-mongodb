Algorithm for the migration

1 - Get the tables which don't have foreign keys
2 - pick one table from them
3 - write a sequelize model against it
4 - Run `npm run generate TableName` and go to `src/migrations`
5 - Complete `up` and `down` methods in migration file
5 - create a sequelize connection 
6 - read data from the sequelize model
7 - implement paging using reading of data
8 - Execute migrations in order they were generated
9 - write a mongoose model for the writing of the data
10 - write to mongo db


Done above?
2 - selected the Tenant Table
4 - made a successful connection to sequelize
5 - read data from tenant


how to write admin permissions?

1 - create AdminUser model in sequelize
2 - create AdminUserPermission model in sequelize
3 - create foreign key AdminUserPermission -> AdminUser
4 - query AdminUser joining AdminUserPermission
5 - insert into mongoose db as sub doc


how to write FK?

1 - create PK table
2 - create FK table
3 - first insert FK table into mongoose
4 - then insert PK table into mongoose
5 - while inserting PK, first read FK_column from FK table in mongoose
6 - insert record in PK collection with FK from step 5
