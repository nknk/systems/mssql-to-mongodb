import dotenv from "dotenv";
dotenv.config();
import { setup } from "applicationinsights";
if (process.env.APP_INSIGHTS_KEY) {
    setup(process.env.APP_INSIGHTS_KEY).start();
}
import { AzureFunction, Context, HttpRequest } from "@azure/functions";
import { Migration } from "../src/migration";

async function runMigrations() {
    console.time("runMigrations");
    const migration = new Migration();
    await migration.down();
    await migration.up();
    console.timeEnd("runMigrations");
}

const httpTrigger: AzureFunction = async function (context: Context, req: HttpRequest): Promise<void> {
    const name = (req.query.name || (req.body && req.body.name));

    if (name) {

        runMigrations().then(() => {
            context.res = {
                // status: 200, /* Defaults to 200 */
                body: "Hello " + (req.query.name || req.body.name)
            };
        });

    }
    else {
        context.res = {
            status: 400,
            body: "Please pass a name on the query string or in the request body"
        };
    }
};

export default httpTrigger;
