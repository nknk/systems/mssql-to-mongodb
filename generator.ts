const [, , ...args] = process.argv;
let filename = args[0];
if (!filename) {
    throw new Error("Please provide filename");
}
const date = new Date();
filename = `${date.getTime()}-${filename}.ts`;
import { join } from "path";
import { writeFileSync, existsSync, mkdirSync } from "fs";

const template = `export const up = async () => {

};

export const down = async () => {

};

`;

const dir = join(__dirname,"src", "migrations");
if (!existsSync(dir)) {
    mkdirSync(dir);
}

writeFileSync(join(dir, filename), template);